package indo.com.mylibrary.xmpp

import android.app.IntentService
import android.content.Context
import android.content.Intent
import indo.com.ridehub_lyft_uber.Activities.AdsPage
import indo.com.ridehub_lyft_uber.Activities.FareBreakUp
import indo.com.ridehub_lyft_uber.Activities.PushNotificationAlert
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import org.jivesoftware.smack.packet.Message
import org.json.JSONObject
import java.net.URLDecoder


/**
 * Created by user88 on 11/4/2015.
 */
class ChatHandler(private val context: Context, private val service: IntentService) {

    fun onHandleChatMessage(message: Message) {
        try {
            val data = URLDecoder.decode(message.body, "UTF-8")
            val messageObject = JSONObject(data)

            if (messageObject.length() > 0) {
                println("--------------xmpp service data----------------------$data")
                val action = messageObject.get(Iconstant.Push_Action) as String
                if (action.equals(Iconstant.PushNotification_AcceptRide_Key, ignoreCase = true)) {
                    sendBroadCastToRideConfirm(messageObject)
                } else if (action.equals(Iconstant.PushNotification_CabArrived_Key, ignoreCase = true)) {
                    showCabArrivedAlert(messageObject)
                } else if (action.equals(Iconstant.PushNotification_RideCancelled_Key, ignoreCase = true)) {
                    rideCancelledAlert(messageObject)
                } else if (action.equals(Iconstant.PushNotification_RideCompleted_Key, ignoreCase = true)) {
                    rideCompletedAlert(messageObject)
                } else if (action.equals(Iconstant.PushNotification_RequestPayment_Key, ignoreCase = true)) {
                    requestPayment(messageObject)
                } else if (action.equals(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key, ignoreCase = true)) {
                    rideCompletedAlert(messageObject)
                    //makePaymentStripAni(messageObject);
                } else if (action.equals(Iconstant.PushNotification_PaymentPaid_Key, ignoreCase = true)) {
                    paymentPaid(messageObject)
                } else if (action.equals(Iconstant.pushNotificationBeginTrip, ignoreCase = true)) {
                    beginTripMessage(messageObject)
                } else if (action.equals(Iconstant.pushNotificationDriverLoc, ignoreCase = true)) {
                    updateDriverLocation_TrackRide(messageObject)
                } else if (action.equals(Iconstant.pushNotification_Ads, ignoreCase = true)) {
                    display_Ads(messageObject)
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Throws(Exception::class)
    private fun sendBroadCastToRideConfirm(messageObject: JSONObject) {
        val local = Intent()
        local.action = "com.app.pushnotification.RideAccept"
        local.putExtra("driverID", messageObject.getString(Iconstant.DriverID))
        local.putExtra("driverName", messageObject.getString(Iconstant.DriverName))
        local.putExtra("driverEmail", messageObject.getString(Iconstant.DriverEmail))
        local.putExtra("driverImage", messageObject.getString(Iconstant.DriverImage))
        local.putExtra("driverRating", messageObject.getString(Iconstant.DriverRating))
        local.putExtra("driverLat", messageObject.getString(Iconstant.DriverLat))
        local.putExtra("driverLong", messageObject.getString(Iconstant.DriverLong))
        local.putExtra("driverTime", messageObject.getString(Iconstant.DriverTime))
        local.putExtra("rideID", messageObject.getString(Iconstant.RideID))
        local.putExtra("driverMobile", messageObject.getString(Iconstant.DriverMobile))
        local.putExtra("driverCar_no", messageObject.getString(Iconstant.DriverCar_No))
        local.putExtra("driverCar_model", messageObject.getString(Iconstant.DriverCar_Model))
        local.putExtra("source_Location", messageObject.getString(Iconstant.SourceLocation))
        local.putExtra("userLatitude", messageObject.getString(Iconstant.UserLat))
        local.putExtra("userLongitude", messageObject.getString(Iconstant.UserLong))
        local.putExtra("cab_type", messageObject.getString(Iconstant.CabType))
        local.putExtra("destination_Location", messageObject.getString(Iconstant.DestinationLocation))
        local.putExtra("message", messageObject.getString(Iconstant.Push_Message))
        local.putExtra("Action", messageObject.getString(Iconstant.Push_Action))
        context.sendBroadcast(local)
    }

    @Throws(Exception::class)
    private fun showCabArrivedAlert(messageObject: JSONObject) {
        val broadcastIntent = Intent()
        broadcastIntent.action = "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver"
        broadcastIntent.putExtra("driverLat", messageObject.getString("key3"))
        broadcastIntent.putExtra("driverLong", messageObject.getString("key4"))
        context.sendBroadcast(broadcastIntent)
    }

    @Throws(Exception::class)
    private fun rideCancelledAlert(messageObject: JSONObject) {
        refreshMethod()

        val i1 = Intent(context, PushNotificationAlert::class.java)
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Cancelled))
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Cancelled))
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Cancelled))
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)
    }


    @Throws(Exception::class)
    private fun rideCompletedAlert(messageObject: JSONObject) {
        refreshMethod()

        val i1 = Intent(context, PushNotificationAlert::class.java)
        i1.putExtra("message", context.resources.getString(R.string.pushnotification_alert_label_ride_completed))
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Completed))
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)

    }

    @Throws(Exception::class)
    private fun requestPayment(messageObject: JSONObject) {
        refreshMethod()

        val i1 = Intent(context, FareBreakUp::class.java)
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Request_Payment))
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Request_Payment))
        i1.putExtra("CurrencyCode", messageObject.getString(Iconstant.CurrencyCode_Request_Payment))
        i1.putExtra("TotalAmount", messageObject.getString(Iconstant.TotalAmount_Request_Payment))
        i1.putExtra("TravelDistance", messageObject.getString(Iconstant.TravelDistance_Request_Payment))
        i1.putExtra("Duration", messageObject.getString(Iconstant.Duration_Request_Payment))
        i1.putExtra("WaitingTime", messageObject.getString(Iconstant.WaitingTime_Request_Payment))
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Request_Payment))
        i1.putExtra("UserID", messageObject.getString(Iconstant.UserID_Request_Payment))
        i1.putExtra("DriverName", messageObject.getString(Iconstant.DriverName_Request_Payment))
        i1.putExtra("DriverImage", messageObject.getString(Iconstant.DriverImage_Request_Payment))
        i1.putExtra("DriverRating", messageObject.getString(Iconstant.DriverRating_Request_Payment))
        i1.putExtra("DriverLatitude", messageObject.getString(Iconstant.Driver_Latitude_Request_Payment))
        i1.putExtra("DriverLongitude", messageObject.getString(Iconstant.Driver_Longitude_Request_Payment))
        i1.putExtra("UserName", messageObject.getString(Iconstant.UserName_Request_Payment))
        i1.putExtra("UserLatitude", messageObject.getString(Iconstant.User_Latitude_Request_Payment))
        i1.putExtra("UserLongitude", messageObject.getString(Iconstant.User_Longitude_Request_Payment))
        i1.putExtra("SubTotal", messageObject.getString(Iconstant.subTotal_Request_Payment))
        i1.putExtra("ServiceTax", messageObject.getString(Iconstant.serviceTax_Request_Payment))
        i1.putExtra("TotalPayment", messageObject.getString(Iconstant.Total_Request_Payment))
        i1.putExtra("CouponDiscount", messageObject.getString(Iconstant.coupon_Request_Payment))
        i1.putExtra("ServiceTax_Percent", messageObject.getString(Iconstant.serviceTax_Percentage))
        i1.putExtra("ReferPoints_Total", messageObject.getString(Iconstant.refer_Points))
        i1.putExtra("number_of_points", messageObject.getString(Iconstant.number_of_Points))
        i1.putExtra("amount_per_point", messageObject.getString(Iconstant.amount_per_Point))

        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)
    }

    @Throws(Exception::class)
    private fun paymentPaid(messageObject: JSONObject) {
        refreshMethod()

        val finish_fareBreakUp = Intent()
        finish_fareBreakUp.action = "com.pushnotification.finish.FareBreakUpPaymentList"
        context.sendBroadcast(finish_fareBreakUp)

        val finish_MyRidePaymentList = Intent()
        finish_fareBreakUp.action = "com.pushnotification.finish.MyRidePaymentList"
        context.sendBroadcast(finish_MyRidePaymentList)

        val i1 = Intent(context, PushNotificationAlert::class.java)
        i1.putExtra("message", messageObject.getString(Iconstant.Push_Message_Payment_paid))
        i1.putExtra("Action", messageObject.getString(Iconstant.Push_Action_Payment_paid))
        i1.putExtra("RideID", messageObject.getString(Iconstant.RideID_Payment_paid))
        i1.putExtra("UserID", messageObject.getString(Iconstant.UserID_Payment_paid))
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)
    }


    @Throws(Exception::class)
    private fun updateDriverLocation_TrackRide(messageObject: JSONObject) {

        println("--------chat handler driver update----------------")

        val local = Intent()
        local.action = "com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver"

        /* Log.e("CHATHANDLER", messageObject.getString(Iconstant.latitude)+"----"+messageObject.getString(Iconstant.longitude)+"----"+messageObject.getString(Iconstant.ride_id));
        Log.e("CHATHANDLER_KEYS", messageObject.getString("key2")+"-----"+messageObject.getString("key3")+"----"+messageObject.getString("key4"));*/

        if (messageObject.getString(Iconstant.background).equals("no", ignoreCase = true)) {

            local.putExtra("isContinousRide", messageObject.getString(Iconstant.latitude))
            local.putExtra("latitude", messageObject.getString(Iconstant.latitude))
            local.putExtra("longitude", messageObject.getString(Iconstant.longitude))
            local.putExtra("ride_id", messageObject.getString(Iconstant.ride_id))
            context.sendBroadcast(local)

        } else {
            local.putExtra("isContinousRide", messageObject.getString("key2"))
            local.putExtra("latitude", messageObject.getString("key2"))
            local.putExtra("longitude", messageObject.getString("key3"))
            local.putExtra("ride_id", messageObject.getString("key4"))
            context.sendBroadcast(local)
        }


    }

    @Throws(Exception::class)
    private fun beginTripMessage(messageObject: JSONObject) {
        val broadcastIntent = Intent()
        broadcastIntent.action = "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip"
        broadcastIntent.putExtra("drop_lat", messageObject.getString("key3"))
        broadcastIntent.putExtra("drop_lng", messageObject.getString("key4"))
        broadcastIntent.putExtra("pickUp_lat", messageObject.getString("key5"))
        broadcastIntent.putExtra("pickUp_lng", messageObject.getString("key6"))
        context.sendBroadcast(broadcastIntent)
    }

    @Throws(Exception::class)
    private fun makePaymentStripAni(messageObject: JSONObject) {
        refreshMethod()

        val broadcastIntent = Intent()
        broadcastIntent.action = "com.package.ACTION_CLASS_TrackYourRide_REFRESH_MakePayment"
        context.sendBroadcast(broadcastIntent)

        val i1 = Intent(context, FareBreakUp::class.java)
        i1.putExtra("RideID", messageObject.getString(Iconstant.Make_Payment))
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)
    }


    @Throws(Exception::class)
    private fun display_Ads(messageObject: JSONObject) {
        val i1 = Intent(context, AdsPage::class.java)
        i1.putExtra("AdsTitle", messageObject.getString(Iconstant.Ads_title))
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Ads_Message))
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image))
        }
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)
    }


    private fun refreshMethod() {
        val finish_fareBreakUp = Intent()
        finish_fareBreakUp.action = "com.pushnotification.finish.FareBreakUp"
        context.sendBroadcast(finish_fareBreakUp)

        val finish_timerPage = Intent()
        finish_timerPage.action = "com.pushnotification.finish.TimerPage"
        context.sendBroadcast(finish_timerPage)

        val finish_pushAlert = Intent()
        finish_pushAlert.action = "com.pushnotification.finish.PushNotificationAlert"
        context.sendBroadcast(finish_pushAlert)

        val finish_MyRideDetails = Intent()
        finish_MyRideDetails.action = "com.pushnotification.finish.MyRideDetails"
        context.sendBroadcast(finish_MyRideDetails)

        val local = Intent()
        local.action = "com.pushnotification.finish.trackyourRide"
        context.sendBroadcast(local)

        val broadcastIntent = Intent()
        broadcastIntent.action = "com.pushnotification.updateBottom_view"
        context.sendBroadcast(broadcastIntent)

    }

    private fun trackRideRefreshMethod() {
        val finish_fareBreakUp = Intent()
        finish_fareBreakUp.action = "com.pushnotification.finish.FareBreakUp"
        context.sendBroadcast(finish_fareBreakUp)

        val finish_timerPage = Intent()
        finish_timerPage.action = "com.pushnotification.finish.TimerPage"
        context.sendBroadcast(finish_timerPage)

        val finish_pushAlert = Intent()
        finish_pushAlert.action = "com.pushnotification.finish.PushNotificationAlert"
        context.sendBroadcast(finish_pushAlert)

        val finish_MyRideDetails = Intent()
        finish_MyRideDetails.action = "com.pushnotification.finish.MyRideDetails"
        context.sendBroadcast(finish_MyRideDetails)

        val broadcastIntent = Intent()
        broadcastIntent.action = "com.pushnotification.updateBottom_view"
        context.sendBroadcast(broadcastIntent)

    }
}
