
package indo.com.mylibrary.xmpp

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.Messenger
import android.util.Log
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.jivesoftware.smack.*
import org.jivesoftware.smack.chat.Chat
import org.jivesoftware.smack.chat.ChatManager
import org.jivesoftware.smack.chat.ChatManagerListener
import org.jivesoftware.smack.chat.ChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import java.io.IOException
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

class ChatService : IntentService("ChatService"), ChatManagerListener, ChatMessageListener {

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            handleActionFoo()
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionFoo() {
        val configBuilder = XMPPTCPConnectionConfiguration.builder()
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
        var context: SSLContext? = null
        try {
            context = SSLContext.getInstance("TLS")
            context!!.init(null, arrayOf<X509TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }
            }), SecureRandom())
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
        }

        configBuilder.setHost(Iconstant.XMPP_HOST_URL)//http://192.168.1.116/67.219.149.186
        configBuilder.setServiceName(Iconstant.XMPP_SERVICE_NAME)
        //sec_key
        connection = XMPPTCPConnection(configBuilder.build())
        connection!!.addConnectionListener(object : ConnectionListener {
            override fun connected(connection: XMPPConnection) {
                isConnected = true
            }

            override fun authenticated(connection: XMPPConnection, resumed: Boolean) {}

            override fun connectionClosed() {
                isConnected = false
            }

            override fun connectionClosedOnError(e: Exception) {
                isConnected = false
            }

            override fun reconnectionSuccessful() {
                isConnected = true
            }

            override fun reconnectingIn(seconds: Int) {}

            override fun reconnectionFailed(e: Exception) {
                isConnected = false
            }
        })

        connection!!.packetReplyTimeout = 30000
        try {
            connection!!.connect()
        } catch (e: SmackException) {
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XMPPException) {
            e.printStackTrace()
        }

        try {
            var userName = ""
            var password = ""
            if (session != null && session!!.getXmppKey() != null) {
                userName = session!!.getXmppKey()!![SessionManager.KEY_XMPP_USERID]!!
                password = session!!.getXmppKey()!![SessionManager.KEY_XMPP_SEC_KEY]!!
                Log.e("XMPPPP", "$userName==$password")
            }
            if (userName.length > 0 && password.length > 0) {
                Log.e("test login", "$userName==$password")

                /* XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
                config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                config.setUsernameAndPassword(userName, password);
                config.setServiceName("52.74.12.27");
                config.setHost("52.74.12.27");
                config.setPort(5222);
                config.setDebuggerEnabled(true);
                Log.e("test login111",userName+"=="+password);

                connection = new XMPPTCPConnection(config.build());
                Log.e("test login222",userName+"=="+password);
               // try {
                    //Conectamos al servidor
                    Log.e("test login333",userName+"=="+password);
                    connection.connect().login();
                    System.out.println("Esta conectat? "+ connection.isConnected());
                *//*} catch (SmackException | IOException | XMPPException e) {
                    e.printStackTrace();
                }*//*
                Log.e("test login44444",userName+"=="+password);*/
                connection!!.login(userName, password)

                chatManager = ChatManager.getInstanceFor(connection)
                chatManager!!.addChatListener(this)
            }
        } catch (e: XMPPException) {
            e.printStackTrace()
        } catch (e: SmackException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }


    override fun processMessage(chat: Chat, message: Message) {
        if (chatHandler == null) {
            chatHandler = ChatHandler(applicationContext, this)
        }
        chatHandler!!.onHandleChatMessage(message)
    }

    override fun chatCreated(chat: Chat, createdLocally: Boolean) {
        chat.addMessageListener(this)
    }

    companion object {
        private val ACTION_FOO = "com.casperon.smackclient.action.FOO"
        private var session: SessionManager? = null
        private var connection: AbstractXMPPConnection? = null
        private var isConnected: Boolean = false
        //Declaration for Chat
        private var chat: Chat? = null
        private var chatManager: ChatManager? = null
        private var isChatEnabled: Boolean = false
        private var chatMessenger: Messenger? = null
        private var chatHandler: ChatHandler? = null

        /**
         */
        fun startUserAction(context: Context) {
            val intent = Intent(context, ChatService::class.java)
            intent.action = ACTION_FOO
            session = SessionManager(context)
            context.startService(intent)
        }

       /* fun setChatMessenger(messenger: Messenger) {
            chatMessenger = messenger
        }
*/
        /*
    Need add chat validation object
    */
        fun createChat(chatID: String?): Chat? {
            if (chatID != null && chatManager != null) {
                chat = chatManager!!.createChat(chatID)
            }
            return chat
        }

        fun enableChat() {
            isChatEnabled = true
        }

        fun disableChat() {
            isChatEnabled = false
        }
    }


    /* public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        System.out.println("---------------service destroied--------------------");
    }*/
}
