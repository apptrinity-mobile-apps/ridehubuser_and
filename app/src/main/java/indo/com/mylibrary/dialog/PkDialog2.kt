package indo.com.mylibrary.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.ridehub_lyft_uber.R

class PkDialog2(private val mContext: Context) {
    private val Bt_action: Button
    private val Bt_dismiss: Button
    private val alert_title: TextView
    private val alert_message: TextView
    //private val Rl_button: RelativeLayout
    private val dialog: Dialog
    private val view: View
    private var isPositiveAvailable = false
    private var isNegativeAvailable = false


    init {

        //--------Adjusting Dialog width-----
        val metrics = mContext.resources.displayMetrics
        val screenWidth = (metrics.widthPixels * 0.85).toInt()//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.custom_dialog_library_payment_succ, null)
        dialog = Dialog(mContext)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(view)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT)

        alert_title = view.findViewById(R.id.custom_dialog_library_title_textview) as TextView
        alert_message = view.findViewById(R.id.custom_dialog_library_message_textview) as TextView
        Bt_action = view.findViewById(R.id.custom_dialog_library_ok_button) as Button
        Bt_dismiss = view.findViewById(R.id.custom_dialog_library_cancel_button) as Button
        //Rl_button = view.findViewById(R.id.custom_dialog_library_button_layout) as RelativeLayout
    }


    fun show() {

        /*Enable or Disable positive Button*/
        if (isPositiveAvailable) {
            Bt_action.visibility = View.VISIBLE
        } else {
            Bt_action.visibility = View.GONE
        }

        /*Enable or Disable Negative Button*/
        if (isNegativeAvailable) {
            Bt_dismiss.visibility = View.VISIBLE
        } else {
            Bt_dismiss.visibility = View.GONE
        }

        /*Changing color for Button Layout*/
        if (isPositiveAvailable && isNegativeAvailable) {
          //  Rl_button.setBackgroundColor(mContext.resources.getColor(R.color.white))
        } else {
           // Rl_button.setBackgroundColor(mContext.resources.getColor(R.color.app_color_transperent))
        }

        dialog.show()
    }


    fun dismiss() {
        dialog.dismiss()
    }


    fun setDialogTitle(title: String) {
        alert_title.text = title
    }


    fun setDialogMessage(message: String) {
        alert_message.text = message
    }


    fun setCancelOnTouchOutside(value: Boolean) {
        dialog.setCanceledOnTouchOutside(value)
    }


    /*Action Button for Dialog*/
    fun setPositiveButton(text: String, listener: View.OnClickListener) {

        isPositiveAvailable = true
        Bt_action.typeface = Typeface.createFromAsset(mContext.assets, "fonts/Raleway-Medium.ttf")
        Bt_action.text = text
        Bt_action.setOnClickListener(listener)
    }

    fun setNegativeButton(text: String, listener: View.OnClickListener) {
        isNegativeAvailable = true
        Bt_dismiss.typeface = Typeface.createFromAsset(mContext.assets, "fonts/Raleway-Medium.ttf")
        Bt_dismiss.text = text
        Bt_dismiss.setOnClickListener(listener)
    }

}