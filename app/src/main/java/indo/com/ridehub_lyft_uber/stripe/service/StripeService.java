
package indo.com.ridehub_lyft_uber.stripe.service;


import indo.com.ridehub_lyft_uber.stripe.AllCards;
import indo.com.ridehub_lyft_uber.stripe.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * A Retrofit service used to communicate with a server.
 */
public interface StripeService {



    @GET("/user/striperide/cards")
    Call<AllCards> cards();

    @FormUrlEncoded
    @POST("/user/striperide")
    Call<User> striperide(@Field("amount") String amount, @Field("stripeToken") String token);


}
