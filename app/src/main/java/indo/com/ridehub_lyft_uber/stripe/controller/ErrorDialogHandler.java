package indo.com.ridehub_lyft_uber.stripe.controller;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import indo.com.ridehub_lyft_uber.R;
import indo.com.ridehub_lyft_uber.stripe.dialog.ErrorDialogFragment;


/**
 * A convenience class to handle displaying error dialogs.
 */
public class ErrorDialogHandler {

    FragmentManager mFragmentManager;

    public ErrorDialogHandler(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    public void showError(String errorMessage) {
        DialogFragment fragment = ErrorDialogFragment.newInstance(
                R.string.validationErrors, errorMessage);
        fragment.show(mFragmentManager, "error");
    }
}
