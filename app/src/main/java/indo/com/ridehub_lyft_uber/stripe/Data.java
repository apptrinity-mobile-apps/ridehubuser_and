package indo.com.ridehub_lyft_uber.stripe;

public class Data {
    private String statement_descriptor;

    private String status;

    private String object;

    private String flow;

    private String type;

    private String customer;

    private String currency;

    private String id;

    private String amount;

    private String client_secret;

    private String created;

    private Owner owner;

    private String usage;

    private Card card;

    private String livemode;

    private String[] metadata;

    public String getStatement_descriptor ()
    {
        return statement_descriptor;
    }

    public void setStatement_descriptor (String statement_descriptor)
    {
        this.statement_descriptor = statement_descriptor;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getObject ()
    {
        return object;
    }

    public void setObject (String object)
    {
        this.object = object;
    }

    public String getFlow ()
    {
        return flow;
    }

    public void setFlow (String flow)
    {
        this.flow = flow;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getCustomer ()
    {
        return customer;
    }

    public void setCustomer (String customer)
    {
        this.customer = customer;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getClient_secret ()
    {
        return client_secret;
    }

    public void setClient_secret (String client_secret)
    {
        this.client_secret = client_secret;
    }

    public String getCreated ()
    {
        return created;
    }

    public void setCreated (String created)
    {
        this.created = created;
    }

    public Owner getOwner ()
    {
        return owner;
    }

    public void setOwner (Owner owner)
    {
        this.owner = owner;
    }

    public String getUsage ()
    {
        return usage;
    }

    public void setUsage (String usage)
    {
        this.usage = usage;
    }

    public Card getCard ()
    {
        return card;
    }

    public void setCard (Card card)
    {
        this.card = card;
    }

    public String getLivemode ()
    {
        return livemode;
    }

    public void setLivemode (String livemode)
    {
        this.livemode = livemode;
    }

    public String[] getMetadata ()
    {
        return metadata;
    }

    public void setMetadata (String[] metadata)
    {
        this.metadata = metadata;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statement_descriptor = "+statement_descriptor+", status = "+status+", object = "+object+", flow = "+flow+", type = "+type+", customer = "+customer+", currency = "+currency+", id = "+id+", amount = "+amount+", client_secret = "+client_secret+", created = "+created+", owner = "+owner+", usage = "+usage+", card = "+card+", livemode = "+livemode+", metadata = "+metadata+"]";
    }
}
