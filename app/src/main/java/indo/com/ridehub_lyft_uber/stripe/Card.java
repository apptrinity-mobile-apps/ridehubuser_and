package indo.com.ridehub_lyft_uber.stripe;

public class Card {
    private String address_zip_check;

    private String exp_year;

    private String exp_month;

    private String funding;

    private String cvc_check;

    private String three_d_secure;

    private String tokenization_method;

    private String country;

    private String fingerprint;

    private String name;

    private String brand;

    private String address_line1_check;

    private String dynamic_last4;

    private String last4;

    public String getAddress_zip_check ()
    {
        return address_zip_check;
    }

    public void setAddress_zip_check (String address_zip_check)
    {
        this.address_zip_check = address_zip_check;
    }

    public String getExp_year ()
    {
        return exp_year;
    }

    public void setExp_year (String exp_year)
    {
        this.exp_year = exp_year;
    }

    public String getExp_month ()
    {
        return exp_month;
    }

    public void setExp_month (String exp_month)
    {
        this.exp_month = exp_month;
    }

    public String getFunding ()
    {
        return funding;
    }

    public void setFunding (String funding)
    {
        this.funding = funding;
    }

    public String getCvc_check ()
    {
        return cvc_check;
    }

    public void setCvc_check (String cvc_check)
    {
        this.cvc_check = cvc_check;
    }

    public String getThree_d_secure ()
    {
        return three_d_secure;
    }

    public void setThree_d_secure (String three_d_secure)
    {
        this.three_d_secure = three_d_secure;
    }

    public String getTokenization_method ()
    {
        return tokenization_method;
    }

    public void setTokenization_method (String tokenization_method)
    {
        this.tokenization_method = tokenization_method;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getFingerprint ()
    {
        return fingerprint;
    }

    public void setFingerprint (String fingerprint)
    {
        this.fingerprint = fingerprint;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getBrand ()
    {
        return brand;
    }

    public void setBrand (String brand)
    {
        this.brand = brand;
    }

    public String getAddress_line1_check ()
    {
        return address_line1_check;
    }

    public void setAddress_line1_check (String address_line1_check)
    {
        this.address_line1_check = address_line1_check;
    }

    public String getDynamic_last4 ()
    {
        return dynamic_last4;
    }

    public void setDynamic_last4 (String dynamic_last4)
    {
        this.dynamic_last4 = dynamic_last4;
    }

    public String getLast4 ()
    {
        return last4;
    }

    public void setLast4 (String last4)
    {
        this.last4 = last4;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [address_zip_check = "+address_zip_check+", exp_year = "+exp_year+", exp_month = "+exp_month+", funding = "+funding+", cvc_check = "+cvc_check+", three_d_secure = "+three_d_secure+", tokenization_method = "+tokenization_method+", country = "+country+", fingerprint = "+fingerprint+", name = "+name+", brand = "+brand+", address_line1_check = "+address_line1_check+", dynamic_last4 = "+dynamic_last4+", last4 = "+last4+"]";
    }
}
