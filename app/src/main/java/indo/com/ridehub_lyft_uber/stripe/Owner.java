package indo.com.ridehub_lyft_uber.stripe;

public class Owner {
    private String verified_name;

    private String phone;

    private String email;

    private String address;

    private String verified_address;

    private String name;

    private String verified_email;

    private String verified_phone;

    public String getVerified_name ()
    {
        return verified_name;
    }

    public void setVerified_name (String verified_name)
    {
        this.verified_name = verified_name;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getVerified_address ()
    {
        return verified_address;
    }

    public void setVerified_address (String verified_address)
    {
        this.verified_address = verified_address;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getVerified_email ()
    {
        return verified_email;
    }

    public void setVerified_email (String verified_email)
    {
        this.verified_email = verified_email;
    }

    public String getVerified_phone ()
    {
        return verified_phone;
    }

    public void setVerified_phone (String verified_phone)
    {
        this.verified_phone = verified_phone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [verified_name = "+verified_name+", phone = "+phone+", email = "+email+", address = "+address+", verified_address = "+verified_address+", name = "+name+", verified_email = "+verified_email+", verified_phone = "+verified_phone+"]";
    }
}
