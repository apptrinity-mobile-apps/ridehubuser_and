package indo.com.ridehub_lyft_uber.utils

import android.content.Context
import android.content.SharedPreferences
import java.util.*

class AppInfoSessionManager(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0

    //-----------Get AppInfo-------
    val appInfo: HashMap<String, String>
        get() {
            val timeZone = HashMap<String, String>()
            timeZone[KEY_CONTACT_EMAIL] = pref.getString(KEY_CONTACT_EMAIL, "")
            timeZone[KEY_CUSTOMER_NUMBER] = pref.getString(KEY_CUSTOMER_NUMBER, "")
            timeZone[KEY_SITE_URL] = pref.getString(KEY_SITE_URL, "")
            timeZone[KEY_HOST_URL] = pref.getString(KEY_HOST_URL, "")
            timeZone[KEY_HOST_NAME] = pref.getString(KEY_HOST_NAME, "")
            timeZone[KEY_FACEBOOK_ID] = pref.getString(KEY_FACEBOOK_ID, "")
            timeZone[KEY_GOOGLE_PLUS_ID] = pref.getString(KEY_GOOGLE_PLUS_ID, "")
            timeZone[KEY_CATEGORY_IMAGE] = pref.getString(KEY_CATEGORY_IMAGE, "")
            timeZone[KEY_ON_GOING_RIDE] = pref.getString(KEY_ON_GOING_RIDE, "")
            timeZone[KEY_ON_GOING_RIDE_ID] = pref.getString(KEY_ON_GOING_RIDE_ID, "")
            timeZone[KEY_PENDING_RIDE_ID] = pref.getString(KEY_PENDING_RIDE_ID, "")
            timeZone[KEY_RATING_STATUS] = pref.getString(KEY_RATING_STATUS, "")
            return timeZone
        }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    //---------set AppInfo--------
    fun setAppInfo(contactEmail: String, customerNumber: String, siteUrl: String, hostUrl: String, hostName: String, facebookId: String, googlePlusId: String, sCategoryImage: String, sOngoingRide: String, sOngoingRideId: String, sPendingRideId: String, sRatingStatus: String) {
        editor.putString(KEY_CONTACT_EMAIL, contactEmail)
        editor.putString(KEY_CUSTOMER_NUMBER, customerNumber)
        editor.putString(KEY_SITE_URL, siteUrl)
        editor.putString(KEY_HOST_URL, hostUrl)
        editor.putString(KEY_HOST_NAME, hostName)
        editor.putString(KEY_FACEBOOK_ID, facebookId)
        editor.putString(KEY_GOOGLE_PLUS_ID, googlePlusId)

        editor.putString(KEY_CATEGORY_IMAGE, sCategoryImage)
        editor.putString(KEY_ON_GOING_RIDE, sOngoingRide)
        editor.putString(KEY_ON_GOING_RIDE_ID, sOngoingRideId)
        editor.putString(KEY_PENDING_RIDE_ID, sPendingRideId)
        editor.putString(KEY_RATING_STATUS, sRatingStatus)

        editor.commit()
    }

    companion object {
        // Shared_pref file name
        private val PREF_NAME = "AppInfo_PremKumar"

        val KEY_CONTACT_EMAIL = "contactEmail"
        val KEY_CUSTOMER_NUMBER = "customerNumber"
        val KEY_SITE_URL = "siteUrl"
        val KEY_HOST_URL = "hostUrl"
        val KEY_HOST_NAME = "hostName"
        val KEY_FACEBOOK_ID = "facebookId"
        val KEY_GOOGLE_PLUS_ID = "googlePlusId"

        val KEY_CATEGORY_IMAGE = "sCategoryImage"
        val KEY_ON_GOING_RIDE = "sOngoingRide"
        val KEY_ON_GOING_RIDE_ID = "sOngoingRideId"
        val KEY_PENDING_RIDE_ID = "sPendingRideId"
        val KEY_RATING_STATUS = "sRatingStatus"
    }
}