package indo.com.ridehub_lyft_uber.utils;

public interface ProgressListener {
    public void onProgressChanged(boolean state);
}
