package indo.com.ridehub_lyft_uber.utils

interface SmsListener {

    fun messageReceived(sender: String, messageText: String)

}