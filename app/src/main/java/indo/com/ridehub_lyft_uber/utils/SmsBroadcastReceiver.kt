package indo.com.ridehub_lyft_uber.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log

class SmsBroadcastReceiver : BroadcastReceiver() {

    private val TAG = "SmsBroadcastReceiver"
    private var mSmsListener: SmsListener? = null

    override fun onReceive(context: Context, intent: Intent) {
        Log.e(TAG, "onReceive called()")
        val data = intent.extras

        val pdus = data!!.get("pdus") as Array<*>

        for (i in pdus.indices) {
            val smsMessage = SmsMessage.createFromPdu(pdus[i] as ByteArray)

            val sender = smsMessage.displayOriginatingAddress

            val messageBody = smsMessage.messageBody
            mSmsListener!!.messageReceived(sender, messageBody)

        }

    }

    internal fun bindListener(listener: SmsListener) {
        mSmsListener = listener
    }

}