package indo.com.ridehub_lyft_uber.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import indo.com.mylibrary.xmpp.ChatService

class NetworkChangeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (isOnline(context)) {
            ChatService.startUserAction(context)
        }
    }

    fun isOnline(context: Context): Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        //should check null because in air plan mode it will be null
        return netInfo != null && netInfo.isConnected

    }

}