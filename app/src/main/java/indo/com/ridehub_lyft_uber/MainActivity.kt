package indo.com.ridehub_lyft_uber

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.utils.SessionManager

class MainActivity : AppCompatActivity() {

    private var session: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        session = SessionManager(this)
        session!!.setXmppKey("5ac1fa19ff2e4ea56268abbc", "c4fc7de2fa2ed65022d73c75bd585d39")
        ChatService.startUserAction(this@MainActivity)

    }
}
