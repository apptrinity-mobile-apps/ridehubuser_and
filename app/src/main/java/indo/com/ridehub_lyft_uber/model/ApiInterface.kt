package com.indobytes.biil.model

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by indo67 on 4/13/2018.
 */
public interface ApiInterface {

/*
    @FormUrlEncoded
    @POST("/api/login")
    abstract fun login(@Field("username") username: String,
                       @Field("password") password: String,
                       @Field("mobile_token") mobile_token: String,
                       @Field("device_type") device_type: String
    ): Call<UserLoginResponse>*/







    companion object Factory {
        val BASE_URL = "https://ridehub.co/v4/"
        // const val IMAGE_BASE_URL = "https://www.4print.com/uploads/products/"
        //  val ENCRIPTION_KEY ="7d94c049db8081c50400d84cb96ed302"
        //  const val IMAGE_BASE_ORDER_URL="https://www.4print.com/uploads/orders/"

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}