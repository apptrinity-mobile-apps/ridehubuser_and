package indo.com.ridehub_lyft_uber.Fragments

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.CountDownTimer
import android.os.StrictMode
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.lyft.networking.ApiConfig
import com.lyft.networking.LyftApiFactory
import com.lyft.networking.apis.LyftPublicApi
import com.uber.sdk.android.core.UberSdk
import com.uber.sdk.rides.client.ServerTokenSession
import com.uber.sdk.rides.client.SessionConfiguration
import com.uber.sdk.rides.client.UberRidesApi
import com.uber.sdk.rides.client.services.RidesService
import com.wang.avi.AVLoadingIndicatorView
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import indo.com.mylibrary.gps.GPSTracker
import indo.com.mylibrary.materialprogresswheel.ProgressWheel
import indo.com.mylibrary.volley.AppController
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.volley.VolleyErrorResponse
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.Activities.*
import indo.com.ridehub_lyft_uber.Adapters.BookMyRide_Adapter
import indo.com.ridehub_lyft_uber.HockeyApp.FragmentHockeyApp
import indo.com.ridehub_lyft_uber.PojoResponse.HomePojo
import indo.com.ridehub_lyft_uber.PojoResponse.Lyft_Nearby_Drivers_Pojo
import indo.com.ridehub_lyft_uber.PojoResponse.Lyft_Ride_Estimate_Pojo
import indo.com.ridehub_lyft_uber.PojoResponse.Lyft_Ride_Types_Pojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.ImageLoader
import indo.com.ridehub_lyft_uber.utils.MapAnimator
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.min


class Fragment_HomePage : FragmentHockeyApp(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnected(p0: Bundle?) {

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        //enableGpsService();
        mMap = googleMap


        if (gps.canGetLocation() && gps.isgpsenabled()) {
            val Dlatitude = gps.getLatitude()
            val Dlongitude = gps.getLongitude()

            Log.e("GPSSSS", gps.getLatitude().toString() + "--" + gps.getLongitude())
            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude

            Recent_lat = Dlatitude
            Recent_long = Dlongitude

            val currLatLng = LatLng(gps.getLatitude(), gps.getLongitude())
            cd = ConnectionDetector(activity!!)
            isInternetPresent = cd!!.isConnectingToInternet


            Log.e("CURRENTLOCATIONLATLNG", currLatLng.toString())

            if (isInternetPresent) {
                // lyft authorization
                Log.e("CURRENTLOCATIONLATLNG_INSIDE", currLatLng.toString())
////Uncomment this  while working for LYFT/////
                //postRequest_auth_lyft(currLatLng)
            }



            if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return
            }
            mMap!!.getUiSettings().setAllGesturesEnabled(true)
            mMap!!.getUiSettings().isMapToolbarEnabled = false
            mMap!!.setIndoorEnabled(true)
            mMap!!.setBuildingsEnabled(true)
            mMap!!.getUiSettings().isZoomControlsEnabled = true
            mMap!!.setIndoorEnabled(true)
            mMap!!.setMyLocationEnabled(true)
            mMap!!.getUiSettings().isMyLocationButtonEnabled = false
            mMap!!.setBuildingsEnabled(true)
            mMap!!.getUiSettings().isZoomControlsEnabled = false
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(LatLng(Recent_lat, Recent_long)))
            mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.Builder()
                    .target(mMap!!.getCameraPosition().target)
                    .zoom(15f)
                    .bearing(30f)
                    .tilt(45f)
                    .build()))
            // Move the camera to last position with a zoom level
            val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(15f).build()
            googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        } else {
            enableGpsService()
        }

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
            //mMap.setMyLocationEnabled(true);


            val styleManager = MapStyleManager.attachToMap(activity!!, mMap!!)
            styleManager.addStyle(0F, R.raw.map_silver_json)
            styleManager.addStyle(10F, R.raw.map_silver_json)
            styleManager.addStyle(12F, R.raw.map_silver_json)

        }

        address = getCompleteAddressString(MyCurrent_lat, MyCurrent_long)
        source_address!!.text = address
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }

    private var rootview: View? = null
    private val REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001
    private var mMap: GoogleMap? = null
    private var isInternetPresent: Boolean = false
    private var cd: ConnectionDetector? = null
    internal lateinit var gps: GPSTracker
    private var session: SessionManager? = null

    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var Recent_lat = 0.0
    private var Recent_long = 0.0
    internal lateinit var searchValue: String
    private var drawer_layout: RelativeLayout? = null

    //-----Declaration For Enabling Gps-------
    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var mGoogleApiClient: GoogleApiClient
    internal lateinit var result: PendingResult<LocationSettingsResult>
    internal val REQUEST_LOCATION = 299


    private var currentLocation_image: ImageView? = null
    private var mRequest: ServiceRequest? = null
    internal var Sselected_latitude = ""
    internal var Sselected_longitude = ""
    private var UserID = ""

    internal var driver_list = ArrayList<HomePojo>()
    internal var category_list = ArrayList<HomePojo>()


    private var ratecard_status = false
    private var driver_status = false
    private var category_status = false
    private var main_response_status = false
    private var ride_adapter: BookMyRide_Adapter? = null
    private var lv_rydetypes: ListView? = null
    private var rl_continue_booking: ScrollView? = null
    private var iv_close_continue_booking: ImageView? = null
    private var tv_continue_to_uber: CustomTextView? = null
    private var rl_continue_uberapp: RelativeLayout? = null
    private var btn_continue_booking: Button? = null
    private var book_cardview_source_address_layout: CardView? = null
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var book_my_ride_source_address_layout: RelativeLayout? = null
    private val timer_request_code = 100
    private val placeSearch_request_code = 200
    private var search_status = 0
    private var SdestinationLatitude = ""
    private var SsourceLatitude = ""
    private var SsourceLongitude = ""
    private var SdestinationLongitude = ""
    private var SdestinationLocation = ""
    private var setpickuploc: String? = null
    private var Sselected_DestinationLocation: String? = null
    private var Selected_SourceLocation: String? = null
    private var SselectedLatitude = ""
    private var SselectedLongitude = ""
    private var DselectedLatitude = ""
    private var DselectedLongitude = ""
    private var locatiosearch = "no"
    private var markerOptions: MarkerOptions? = null
    internal lateinit var address: String
    private var destination_address: TextView? = null
    private var destination_address_rl: RelativeLayout? = null
    private var tv_selected_cab_estimate_time: CustomTextView? = null
    private var tv_nearbycabs: CustomTextView? = null
    private var tv_selected_cab_type: ImageView? = null
    private var tv_selected_cab_type_desti: CustomTextView? = null
    private var tv_selected_cab__fare: CustomTextView? = null
    // private var book_cardview_confirmride_layout: CardView? = null
    private var source_address: TextView? = null

    private var progressWheel: ProgressWheel? = null
    internal lateinit var dialog: Dialog


    private var CategoryID = ""
    private var SubCategoryID = ""
    private var list_item_selected = false
    private var CarAvailable = ""
    private var ScarType = ""
    private var selectedType = ""

    internal lateinit var service: RidesService
    lateinit var lyftPublicApi: LyftPublicApi

    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null

    internal var ratecard_list = ArrayList<HomePojo>()

    private var adapter: BookMyRide_Adapter? = null


    private var response_time = ""
    private var riderId = ""

    private var imageLoader: ImageLoader? = null
    internal lateinit var logoutReciver: BroadcastReceiver

    //UBER

    lateinit var uber_Req_current: StringRequest
    lateinit var uber_Request: StringRequest
    var fare_id_stg: String = ""
    var trip_dist_unit_stg: String = ""
    var trip_dur_unit_stg: String = ""
    var trip_dist_est_stg: String = ""
    var product_id_stg: String = ""

    var value_stg: String = ""
    var main_type_stg = ""
    var request_id_stg = ""

    var uber_start_lat = ""
    var uber_start_lng = ""
    var uber_end_lat = ""
    var uber_end_lng = ""
    lateinit var uber_dialog: Dialog
    lateinit var uber_receipt_dialog: Dialog
    var uber_timer: Timer? = null
    var timerTask_uber: TimerTask? = null
    var timer_stop = "0"

    internal var cat_images = ArrayList<HomePojo>()
    internal var cat_time = ArrayList<HomePojo>()
    internal var cat_time_sorted = ArrayList<Double>()

    lateinit var mDialogg: PkDialog


    ////lyft API Start////
    private var lyft_ClientRequest: StringRequest? = null

    private var public_access_token: String? = null
    private var token_type: String? = null
    private var user_key_expires_in: String? = null
    private var user_refresh_token: String? = null
    private var user_access_token: String? = null
    private var user_token_type: String? = null

    internal var estimate_list_lyft = ArrayList<Lyft_Ride_Estimate_Pojo>()
    internal var ride_type_list_lyft = ArrayList<Lyft_Ride_Types_Pojo>()
    internal var nearby_drivers_list_lyft = ArrayList<Lyft_Nearby_Drivers_Pojo>()

    // ride estimate
    private var lyft_RideEstimateRequest: StringRequest? = null
    // ride types
    private var lyft_RideTypesRequest: StringRequest? = null
    // nearby drivers
    private var lyft_NearbyDriversRequest: StringRequest? = null
    // ride request
    private var lyft_RideRequest: StringRequest? = null
    // user auth request
    private var lyft_UserAuthRequest: StringRequest? = null
    // user auth  refresh token request
    private var lyft_RefreshTokenRequest: StringRequest? = null
    // trip details
    private var lyft_TripDetailsRequest: StringRequest? = null
    // trip receipt
    private var lyft_TripReceiptRequest: StringRequest? = null
    // cancel trip
    private var lyft_TripCancelRequest: StringRequest? = null


    // user generated access key
    private var authorization_code: String = ""

    private var lyft_dialog: Dialog? = null
    private var endLoc: LatLng? = null
    private var curLoc: LatLng? = null


    private var lyft_ride_id: String = ""

    lateinit var tv_ride_status: TextView
    lateinit var tv_cancel_status: TextView

    lateinit var ll_fare_breakup_total_amount: LinearLayout
    lateinit var ll_farebreakup_discount_uber: LinearLayout
    lateinit var ll_fare_breakup_duration_uber: LinearLayout
    lateinit var ll_fare_breakup_uberdistance: LinearLayout
    lateinit var ll_subtotal_uber: LinearLayout
    lateinit var fare_breakup_total_amount_textview: CustomTextView
    lateinit var fare_breakup_discount_amount_uber: CustomTextView
    lateinit var fare_breakup_duration_uber: CustomTextView
    lateinit var fare_breakup_uber_distance: CustomTextView
    lateinit var fare_breakup_subtotal_uber: CustomTextView
    lateinit var btn_uberreceipt_ok_dialog: Button

    var old_lat_long = "old"
    var first_time_long = "old"

    var second_lat = ""
    var second_long = ""


    lateinit var uber_login_dialog: Dialog
    lateinit var web_uber_login_id: WebView
    var authorization_code_uber = ""


    private var ridehubservice: Boolean = false
    private var uberservice: Boolean = false
    private var lyftservice: Boolean = false
    private var publictransitservice: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootview != null) {
            val parent = rootview!!.getParent() as ViewGroup
            parent?.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.homepage, container, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        initialize(rootview)
        initializeMap()

//Start XMPP Chat Service
        ChatService.startUserAction(activity!!)
        // Finishing the activity using broadcast
        val filter = IntentFilter()
        filter.addAction("com.app.logout")
        filter.addAction("com.pushnotification.updateBottom_view")
        logoutReciver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.app.logout") {
                    activity!!.finish()
                } else if (intent.action == "com.pushnotification.updateBottom_view") {
                    // mMap.getUiSettings().setAllGesturesEnabled(true);
                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet
                    // startTimer();

                    book_cardview_source_address_layout!!.visibility = View.VISIBLE
                    mMap!!.clear()

                    drawer_layout!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setClickable(true)
                    currentLocation_image!!.setVisibility(View.VISIBLE)

                    currentLocation_image!!.setClickable(true)
                    currentLocation_image!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setEnabled(true)
                    NavigationDrawer.enableSwipeDrawer()
                    val Dlatitude = gps.getLatitude()
                    val Dlongitude = gps.getLongitude()
                    // Move the camera to last position with a zoom level
                    val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f).build()
                    mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    list_item_selected = false
                    // PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                    destination_address!!.text = ""
                }

            }
        }
        activity!!.registerReceiver(logoutReciver, filter)

        //***********************Uber Initialization************************
        val config = SessionConfiguration.Builder()
                // mandatory
                .setClientId("-US6suVLBhxp6tUYDiNnPHbgJuUpJHpF")
                // required for enhanced button features
                .setServerToken("vGcu0unmRQlwL3RJsQMxNXjrWKYhrqh2PmgH9o53")
                // required for implicit grant authentication
                .setRedirectUri("01")
                // optional: set sandbox as operating environment
                .setEnvironment(SessionConfiguration.Environment.SANDBOX)
                .build()
        UberSdk.initialize(config);
        val session = ServerTokenSession(config)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        service = UberRidesApi.with(session).build().createService()

//*******************Lyft Initialization********************************

        val apiConfig = ApiConfig.Builder()
                .setClientId("TVjiTyh0RVRR")
                .setClientToken("v/0tmOY4wr8UDxAJIcTk/NiHOyXnJorI3lXEVTP6a0oCTXgRNZR32ZuMVDgxYiDpt4RTIUtXNjtvb31Z7KeTz1BaF2YXWemKobR/s6pt6Y3PNoeb+CWY1gI=")
                .build()
        lyftPublicApi = LyftApiFactory(apiConfig).lyftPublicApi

///Uncomment this while working for uber///
        //uber_login_WebView()
        //UberCurrentReq()


        return rootview
    }

    private fun initialize(rootview: View?) {

        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        session = SessionManager(activity!!)
        gps = GPSTracker(activity!!)

        currentLocation_image = rootview!!.findViewById(R.id.book_current_location_imageview) as ImageView
        progressWheel = rootview!!.findViewById(R.id.book_my_ride_progress_wheel) as ProgressWheel

        currentLocation_image!!.setOnClickListener {

            gps = GPSTracker(activity!!)
            if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return@setOnClickListener
            }
            mMap!!.setMyLocationEnabled(true)
            if (gps.canGetLocation() && gps.isgpsenabled()) {

                MyCurrent_lat = gps.getLatitude()
                MyCurrent_long = gps.getLongitude()

                /*   if (mRequest != null) {
                       mRequest.cancelRequest()
                   }*/
                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                currentLocation_image!!.setVisibility(View.VISIBLE)

            } else {
                enableGpsService()
                //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
            }

        }

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]!!

        drawer_layout = rootview.findViewById(R.id.book_navigation_layout) as RelativeLayout

        drawer_layout!!.setOnClickListener(View.OnClickListener { NavigationDrawer.openDrawer() })
        lv_rydetypes = rootview.findViewById(R.id.lv_rydetypes) as ListView
        rl_continue_booking = rootview.findViewById(R.id.rl_continue_booking) as ScrollView
        iv_close_continue_booking = rootview.findViewById(R.id.iv_close_continue_booking) as ImageView
        tv_continue_to_uber = rootview.findViewById(R.id.tv_continue_to_uber) as CustomTextView
        rl_continue_uberapp = rootview.findViewById(R.id.rl_continue_uberapp) as RelativeLayout
        btn_continue_booking = rootview.findViewById(R.id.btn_continue_booking) as Button
        book_cardview_source_address_layout = rootview.findViewById(R.id.book_cardview_source_address_layout) as CardView


        ridehubservice = session!!.getRidehubService()
        uberservice = session!!.getUberService()
        lyftservice = session!!.getLyftService()
        publictransitservice = session!!.getPublicTransitService()
        Log.e("ALLSERVICES", "" + ridehubservice + "------" + uberservice + "-----" + lyftservice + "-----" + publictransitservice)


        lv_rydetypes!!.setOnItemClickListener { parent, view, position, id ->
            list_item_selected = true
            try {
                Log.e("CABTYPEPOSITION", position.toString() + "--------" + category_list[position].getCat_name())
                rl_continue_booking!!.visibility = View.GONE
                book_cardview_source_address_layout!!.visibility = View.GONE

                mMap!!.getUiSettings().setAllGesturesEnabled(true)
                mMap!!.getUiSettings().isScrollGesturesEnabled()

                selectedType = "0"
                CategoryID = category_list[position].getCat_id().toString()
                CarAvailable = category_list[position].getCat_time().toString()
                ScarType = category_list[position].getCat_name().toString()


                Log.e("CABITEM_CLICK", "" + CategoryID + "----" + CarAvailable + "-----" + ScarType)
                main_type_stg = category_list.get(position).getMain_type()!!
                if (main_type_stg.equals("Ridehub")) {

                    Log.e("INSIDE", ridehubservice.toString())
                    if (ridehubservice == true) {
                        Log.e("INSIDE_TRUE", ridehubservice.toString())
                        tv_selected_cab_type!!.setImageDrawable(resources.getDrawable(R.drawable.cab_type_ridehub))
                        tv_selected_cab_type!!.setBackgroundDrawable(activity!!.resources.getDrawable(R.drawable.circle_white))
                        rl_continue_uberapp!!.visibility = View.GONE
                        btn_continue_booking!!.visibility = View.VISIBLE
                        //Toast.makeText(activity, position.toString() + "--------" + category_list[position].getCat_name(), Toast.LENGTH_LONG).show()
                        PostRequest(Iconstant.BookMyRide_url, java.lang.Double.parseDouble(SselectedLatitude), java.lang.Double.parseDouble(SselectedLongitude), java.lang.Double.parseDouble(DselectedLatitude), java.lang.Double.parseDouble(DselectedLongitude))
                    } else if (ridehubservice == false) {
                        Log.e("INSIDE_FALSE", ridehubservice.toString())
                        AlertService(main_type_stg)
                    }

                } else if (category_list.get(position).getMain_type().equals("Uber")) {

                    if (uberservice == true) {
                        tv_selected_cab_type!!.setImageDrawable(resources.getDrawable(R.mipmap.cab_type_uber))

                        if (session!!.getUberData().equals("")) {
                            web_uber_login_id.loadUrl("https://login.uber.com/oauth/v2/authorize?client_id=a7juqQXbfgGt7e40Wepypa8jH9tXwLsF&response_type=code&scope=profile")
                            uber_login_dialog.show()
                        } else {
                            val user_UBER = session!!.getUberData()
                            val uber_code = user_UBER!!

                            if (uber_code.equals("")) {
                                web_uber_login_id.loadUrl("https://login.uber.com/oauth/v2/authorize?client_id=a7juqQXbfgGt7e40Wepypa8jH9tXwLsF&response_type=code&scope=profile")
                                uber_login_dialog.show()
                            } else {
                                Toast.makeText(activity, "Uber Login Success", Toast.LENGTH_SHORT).show()
                                //web_uber_login_id.loadUrl("https://login.uber.com/oauth/v2/authorize?client_id=a7juqQXbfgGt7e40Wepypa8jH9tXwLsF&response_type=code&scope=profile")
                                uber_login_dialog.dismiss()
                                // uber_login_token(uber_code)

                                UberEstimate(SselectedLatitude, SselectedLongitude, DselectedLatitude, DselectedLongitude, category_list.get(position).getCat_id().toString())

                            }
                        }
                    } else if (uberservice == false) {
                        AlertService(main_type_stg)
                    }

                } else if (category_list.get(position).getMain_type().equals("Lyft")) {

                    /*************** user login LYFT***************/

                    if (session!!.getUserAccessToken().equals("")) {
                        custom_dialog_webView() // user login
                    } else {
                        if (!session!!.getRefreshToken().equals("")) {
                            val r_token = session!!.getRefreshToken()
                            postRequest_refresh_access_token_lyft(r_token) // refresh access_token every time when activity is created
                        }
                    }

                    rl_continue_uberapp!!.visibility = View.VISIBLE

                    btn_continue_booking!!.visibility = View.VISIBLE
                    tv_continue_to_uber!!.visibility = View.GONE
                    tv_continue_to_uber!!.setText("Lyft")
                    tv_selected_cab_type!!.setImageDrawable(resources.getDrawable(R.drawable.lyft_icon_latest))
                    //tv_selected_cab_type!!.setBackgroundDrawable(activity!!.resources.getDrawable(R.drawable.circle_white))
                    // tv_selected_cab_type!!.setText("Lyft")
                    ride_estimate_lyft_single(SselectedLatitude, SselectedLongitude, DselectedLatitude, DselectedLongitude, category_list.get(position).getCat_id().toString())

                }

            } catch (e: java.lang.Exception) {
                Log.e("RIDETYPE_EXCEPTION", e.toString())
            }

        }


        iv_close_continue_booking!!.setOnClickListener {
            rl_continue_booking!!.visibility = View.GONE

        }

        book_my_ride_source_address_layout = rootview.findViewById(R.id.book_my_ride_source_address_layout) as RelativeLayout
        source_address = rootview.findViewById(R.id.book_navigation_source_address_address_textView) as TextView

        destination_address = rootview.findViewById(R.id.book_navigation_destination_address_search_address) as TextView

        destination_address_rl = rootview.findViewById(R.id.book_navigation_destination_address_layout) as RelativeLayout
        tv_selected_cab_estimate_time = rootview.findViewById(R.id.tv_selected_cab_estimate_time) as CustomTextView
        tv_nearbycabs = rootview.findViewById(R.id.tv_nearbycabs) as CustomTextView
        tv_selected_cab_type = rootview.findViewById(R.id.tv_selected_cab_type) as ImageView
        tv_selected_cab__fare = rootview.findViewById(R.id.tv_selected_cab__fare) as CustomTextView
        tv_selected_cab_type_desti = rootview.findViewById(R.id.tv_selected_cab_type_desti) as CustomTextView
        // book_cardview_confirmride_layout = rootview.findViewById(R.id.book_cardview_confirmride_layout) as CardView

        avLoadingIndicatorView = rootview.findViewById(R.id.splash_avLoadingIndicatorView) as AVLoadingIndicatorView

        destination_address_rl!!.setOnClickListener(View.OnClickListener {

            val intent = Intent(activity, LocationSearch::class.java)
            intent.putExtra("source_address", address)
            intent.putExtra("Slatitude", MyCurrent_lat.toString())
            intent.putExtra("Slongitude", MyCurrent_long.toString())
            intent.putExtra("Fav_selected_Location", "")
            intent.putExtra("setpickuploc", "setpickuploc")
            startActivityForResult(intent, placeSearch_request_code)
            activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            stoptimertask()

        })

        btn_continue_booking!!.setOnClickListener {
            rl_continue_booking!!.visibility = View.GONE
            //  book_cardview_confirmride_layout!!.visibility = View.GONE
            lv_rydetypes!!.visibility = View.GONE
            if (main_type_stg.equals("Uber")) {
                UberRideReq()
            } else if (main_type_stg.equals("Lyft")) {

                postRequest_ride_req_lyft(SselectedLatitude, SselectedLongitude, DselectedLatitude, DselectedLongitude, fare_id_stg)
            } else {
                ConfirmRideRequest(Iconstant.confirm_ride_url, "", "", "", selectedType, CategoryID, source_address!!.text.toString(), Recent_lat.toString(), Recent_long.toString(), "", Sselected_DestinationLocation.toString(), DselectedLatitude, DselectedLongitude)
            }

        }


        tv_continue_to_uber!!.setOnClickListener {


            if (main_type_stg.equals("Uber")) {

                var intent = context!!.getPackageManager().getLaunchIntentForPackage("com.ubercab");
                if (intent == null) {
                    // Bring user to the market or let them choose an app?
                    intent = Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + "com.ubercab"));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (main_type_stg.equals("Lyft")) {

                var intent = context!!.getPackageManager().getLaunchIntentForPackage("me.lyft.android");
                if (intent == null) {
                    // Bring user to the market or let them choose an app?
                    intent = Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + "me.lyft.android"));
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }


        }

        imageLoader = ImageLoader(activity!!)


        uber_dialog = Dialog(activity)
        uber_dialog.window
        uber_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        uber_dialog.setContentView(R.layout.uber_ride_dialog)
        //uber_dialog.setCanceledOnTouchOutside(false)
        // uber_dialog.show()
        tv_ride_status = uber_dialog.findViewById<TextView>(R.id.tv_ride_status)
        tv_cancel_status = uber_dialog.findViewById<TextView>(R.id.tv_cancel_status)

        tv_cancel_status!!.setOnClickListener {

            postRequest_cancel_lyft(session!!.getlyft_ride_id(), "")
        }


        //Uber Receipt Dialog

        uber_receipt_dialog = Dialog(activity)
        uber_receipt_dialog.window
        uber_receipt_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        uber_receipt_dialog.setContentView(R.layout.uber_reciept_dialog)
        uber_dialog.setCanceledOnTouchOutside(false)
        // uber_dialog.show()

        ll_fare_breakup_total_amount = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_total_amount)
        ll_farebreakup_discount_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_farebreakup_discount_uber)
        ll_fare_breakup_duration_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_duration_uber)
        ll_fare_breakup_uberdistance = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_uberdistance)
        ll_subtotal_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_subtotal_uber)
        fare_breakup_total_amount_textview = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_total_amount_textview)
        fare_breakup_discount_amount_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_discount_amount_uber)
        fare_breakup_duration_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_duration_uber)
        fare_breakup_uber_distance = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_uber_distance)
        fare_breakup_subtotal_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_subtotal_uber)
        btn_uberreceipt_ok_dialog = uber_receipt_dialog.findViewById<Button>(R.id.btn_uberreceipt_ok_dialog)
        btn_uberreceipt_ok_dialog.setOnClickListener {
            uber_receipt_dialog.dismiss()

        }


    }


    private fun AlertService(title: String) {
        val builder = AlertDialog.Builder(activity)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        val alert_title = dialogView.findViewById(R.id.custom_dialog_library_title_textview) as TextView
        val alert_message = dialogView.findViewById(R.id.custom_dialog_library_message_textview) as TextView
        val Bt_action = dialogView.findViewById(R.id.custom_dialog_library_ok_button) as Button
        Bt_action.text = "Ok"
        val Bt_dismiss = dialogView.findViewById(R.id.custom_dialog_library_cancel_button) as Button
        Bt_dismiss.visibility = View.GONE
        builder.setView(dialogView)
        alert_title.text = title
        alert_message.text = "Enable " + title + " Service in Settings, Go to Settings?"
        builder.setCancelable(false)
        Bt_action.setOnClickListener {
            imageLoader!!.clearCache()
            val intent = Intent(activity, AccountSettings::class.java)
            startActivity(intent)
            activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            dialog.dismiss()

        }
        dialog = builder.create()
        dialog.show()
    }


    //UBER WEBVIEW FOR LOGIN
    fun uber_login_WebView() {

        Log.e("WEBVIEWUBER", "UBERWEBVIEW")

        uber_login_dialog = Dialog(activity)
        uber_login_dialog.window
        uber_login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        uber_login_dialog.setContentView(R.layout.uber_login_layout)
        uber_login_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        web_uber_login_id = uber_login_dialog.findViewById<WebView>(R.id.web_uber_login_id)
        val img_close_id = uber_login_dialog.findViewById<ImageView>(R.id.img_close_id)
        img_close_id.setOnClickListener {
            uber_login_dialog.dismiss()
            if (authorization_code_uber.equals("")) {
                Toast.makeText(activity, "Login Failed", Toast.LENGTH_SHORT).show()
            }
        }
        web_uber_login_id.settings.javaScriptEnabled = true


        web_uber_login_id.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {

                // progressBar.visibility = View.GONE

                val redirect_url = view.url
                Log.d("redirect url", redirect_url)

                if (redirect_url.contains("https://ridehub.co/")) {
                    //dialog!!.dismiss()
                    authorization_code_uber = ""
                    authorization_code_uber = inBetween(redirect_url, "?code=", "#_")

                    if (!authorization_code_uber.equals("")) {
                        //  uber_login_token(authorization_code_uber)
                        Log.e("authorization", authorization_code_uber)
                        session!!.uber_code(authorization_code_uber)
                        uber_login_token(authorization_code_uber)
                    } else {
                        Toast.makeText(activity, "error!", Toast.LENGTH_SHORT).show()
                    }
                } else if (redirect_url.contains("https://ridehub.co/?error=")) {
                    // dialog!!.dismiss()
                    Toast.makeText(activity, "Permission not granted!", Toast.LENGTH_SHORT).show()
                }


            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                //  progressBar.visibility = View.VISIBLE

                val redirect_url = view!!.url!!

                if (redirect_url.contains("https://ridehub.co/?code=")) {
                    //dialog!!.dismiss()
                    authorization_code_uber = ""
                    authorization_code_uber = inBetween(redirect_url, "?code=", "#_")

                    if (!authorization_code_uber.equals("")) {
                        // uber_login_token(authorization_code_uber)
                        Log.e("authorization", authorization_code_uber)
                        session!!.uber_code(authorization_code_uber)
                        uber_login_token(authorization_code_uber)
                    } else {
                        Toast.makeText(activity, "error!", Toast.LENGTH_SHORT).show()
                    }
                } else if (redirect_url.contains("https://ridehub.co/?error=")) {
                    // dialog!!.dismiss()
                    Toast.makeText(activity, "Permission not granted!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        web_uber_login_id.settings.javaScriptCanOpenWindowsAutomatically = true


    }

    //LYFT WEBVIEW FOR LOGIN

    fun custom_dialog_webView() {
        val client_id = Iconstant.Lyft_client_id    //client id
        val scope = "public%20profile%20rides.read%20rides.request%20offline"   //required permissions for getting details of user
        val code = "code"
        val state_string = R.string.app_name
        val _url = Iconstant.LyftBaseUrl + "oauth/authorize?client_id=" + client_id + "&scope=" + scope + "&state=" + state_string + "&response_type=" + code
        //  "https://api.lyft.com/oauth/authorize?client_id=9RIzrmiCR5BM&scope=public%20profile%20rides.read%20rides.request%20offline&state=%3Cstate_string%3E&response_type=code"

        lyft_dialog = Dialog(activity)
        lyft_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        lyft_dialog!!.setContentView(R.layout.custom_dialog_webview)
        lyft_dialog!!.setCanceledOnTouchOutside(false)
        lyft_dialog!!.show()
        lyft_dialog!!.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        val progressBar = lyft_dialog!!.findViewById<View>(R.id.web_progressBar) as ProgressBar
        val my_web_view = lyft_dialog!!.findViewById<View>(R.id.webview) as WebView
        progressBar.isIndeterminate = true

        my_web_view.settings.javaScriptEnabled = true
        my_web_view.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {

                progressBar.visibility = View.GONE

                val redirect_url = view.url
                Log.d("redirect url", redirect_url)
                if (redirect_url.equals("https://www.lyft.com/oauth/authorize_app")) {
                    Log.d("===ridehub===onPageFinished", "reload")

                    my_web_view.loadUrl(_url)

                } else if (redirect_url.contains("https://ridehub.co/?code=")) {
                    lyft_dialog!!.dismiss()
                    authorization_code = inBetween(redirect_url, "?code=", "&state=")

                    if (!authorization_code.equals("")) {
                        postRequest_retrieve_access_lyft(authorization_code)
                    } else {
                        Toast.makeText(activity, "error!", Toast.LENGTH_SHORT).show()
                    }
                } else if (redirect_url.contains("https://ridehub.co/?error=")) {
                    lyft_dialog!!.dismiss()
                    Toast.makeText(activity, "Permission not granted!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar.visibility = View.VISIBLE
                my_web_view.visibility = View.VISIBLE
                var redirect_url = ""
                redirect_url = view!!.url!!

                if (redirect_url.contains("https://ridehub.co/?code=")) {
                    lyft_dialog!!.dismiss()
                    authorization_code = inBetween(redirect_url, "?code=", "&state=")

                    if (!authorization_code.equals("")) {
                        postRequest_retrieve_access_lyft(authorization_code)
                    } else {
                        Toast.makeText(activity, "error!", Toast.LENGTH_SHORT).show()
                    }
                } else if (redirect_url.contains("https://ridehub.co/?error=")) {
                    lyft_dialog!!.dismiss()
                    Toast.makeText(activity, "Permission not granted!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        my_web_view.settings.javaScriptCanOpenWindowsAutomatically = true

        my_web_view.loadUrl(_url)

    }


    fun inBetween(value: String, a: String, b: String): String {
        val posA: Int = value.indexOf(a)
        if (posA == -1) {
            return ""
        }
        val posB: Int = value.lastIndexOf(b)
        if (posB == -1) {
            return ""
        }
        val adjustedPosA: Int = posA + a.length
        if (adjustedPosA >= posB) {
            return ""
        }
        return value.substring(adjustedPosA, posB);
    }


    fun uber_login_token(code: String) {

        Log.e("uber_login_token", "---" + "uber_login_token")
        val uber_Request = object : StringRequest(Request.Method.POST, Iconstant.API_LOGIN_TOKEN_stg,
                object : com.android.volley.Response.Listener<String> {
                    @SuppressLint("MissingPermission")
                    override fun onResponse(response: String) {

                        Log.e("uber_login_token", "---" + response)

                        try {
                            val access_token_obj = JSONObject(response)
                            val access_token_stg = access_token_obj.getString("access_token")
                            val token_type_stg = access_token_obj.getString("token_type")
                            val expires_in_stg = access_token_obj.getString("expires_in")
                            val refresh_token_stg = access_token_obj.getString("refresh_token")
                            val scope_stg = access_token_obj.getString("scope")
                            session!!.uber_tokens(access_token_stg, refresh_token_stg, expires_in_stg)


                        } catch (e: Exception) {

                        }


                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_error", "---" + error)
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val jsonParams = HashMap<String, String>()

                jsonParams.put("client_secret", "zVBPyiLEDzwn-RHEZlfnsY2F3nQAK7sdrTG0T1cw")
                jsonParams.put("client_id", "a7juqQXbfgGt7e40Wepypa8jH9tXwLsF")
                jsonParams.put("grant_type", "authorization_code")
                jsonParams.put("redirect_uri", "https://ridehub.co/")
                jsonParams.put("scope", "profile")
                jsonParams.put("code", code)
                return jsonParams
            }

        }
        uber_Request.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Request.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Request)


    }


    /*  fun uber_products_ridehub_API() {

          val uber_Request = object : StringRequest(Request.Method.POST, "https://ridehub.co/v4/android/user/getProducts",
                  object : com.android.volley.Response.Listener<String> {
                      @SuppressLint("MissingPermission")
                      override fun onResponse(response: String) {

                          Log.e("uber_products_ridehub", "---" + response)

                          try {
                              val status_obj=JSONObject(response)
                              val data_array=status_obj.getJSONArray("data")
                              for (i in 0 until data_array.length()) {
                                  val array_stg = data_array.getJSONObject(i)
                                  val product_id_array = array_stg.getString("product_id")
                                  val display_name_stg=array_stg.getString("display_name")
                                  val type_stg=array_stg.getString("type")
                                  val status_stg=array_stg.getString("status")

                                  val pojo = HomePojo()
                                  pojo.setCat_time("20")

                                  pojo.setCat_name(display_name_stg)
                                  pojo.setCat_id(product_id_array)
                                  pojo.setMain_type("Uber")
                                  category_list.add(pojo)

                                  //  }


                              }
                              main_response_status = true
                              ride_adapter = BookMyRide_Adapter(activity!!, category_list)
                              lv_rydetypes!!.adapter = ride_adapter
                              ride_adapter!!.notifyDataSetChanged()

                          } catch (e: Exception) {

                          }

                      }
                  }, object : com.android.volley.Response.ErrorListener {

              override fun onErrorResponse(error: VolleyError) {
                  VolleyErrorResponse.volleyError(activity, error)
                  Log.e("estimation_error", "---" + error)
              }
          }) {

              @Throws(AuthFailureError::class)
              override fun getParams(): Map<String, String> {
                  val jsonParams = HashMap<String, String>()

                  jsonParams.put("token", Iconstant.API_UBER_AUTH_KEY_stg)
                  jsonParams.put("lat", SselectedLatitude)
                  jsonParams.put("lang",  SselectedLongitude)
                  jsonParams.put("type", "uber")

                  return jsonParams
              }

          }
          uber_Request.retryPolicy = DefaultRetryPolicy(30000,
                  DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                  DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
          uber_Request.setShouldCache(false)
          AppController.getInstance().addToRequestQueue(uber_Request)


      }*/


    private fun initializeMap() {

        if (mMap == null) {
            //mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.book_my_ride_mapview));
            val mapFragment = childFragmentManager.findFragmentById(R.id.book_my_ride_mapview) as SupportMapFragment
            mapFragment!!.getMapAsync(this)
            if (mMap == null) {
                // Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }

        if (CheckPlayService()) {

            if (gps.canGetLocation() && gps.isgpsenabled()) {
                val Dlatitude = gps.getLatitude()
                val Dlongitude = gps.getLongitude()
                Log.e("Dlatitude", Dlatitude.toString() + "---" + Dlongitude)

                MyCurrent_lat = Dlatitude
                MyCurrent_long = Dlongitude

                Recent_lat = Dlatitude
                Recent_long = Dlongitude

                val search = session!!.locationSearchValue
                searchValue = search.get(SessionManager.Companion.KEY_LOCATION_SEARCH_VALUE)!!
                Log.e("searchValue", searchValue)

                if (searchValue.equals("no", ignoreCase = true)) {
                    // PostRequest(Iconstant.BookMyRide_url, Dlatitude, Dlongitude);
                }
                // Move the camera to last position with a zoom level
            } else {
                enableGpsService()
            }
        } else {

            val mDialog = PkDialog(activity!!)
            mDialog.setDialogTitle(activity!!.resources.getString(R.string.alert_label_title))
            mDialog.setDialogMessage(activity!!.resources.getString(R.string.action_unable_to_create_map))
            mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                mDialog.dismiss()
                activity!!.finish()
            })
            mDialog.show()
        }

    }

    //-----------Check Google Play Service--------
    private fun CheckPlayService(): Boolean {
        val connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity!!)
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
            return false
        }
        return true
    }

    internal fun showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode: Int) {
        activity!!.runOnUiThread {
            val dialog = GooglePlayServicesUtil.getErrorDialog(
                    connectionStatusCode, activity, REQUEST_CODE_RECOVER_PLAY_SERVICES)
            if (dialog == null) {
                Toast.makeText(activity, "incompatible version of Google Play Services", Toast.LENGTH_LONG).show()
            }
        }
    }

    //Enabling Gps Service
    private fun enableGpsService() {

        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        mGoogleApiClient.connect()

        mLocationRequest = LocationRequest.create()
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mLocationRequest.setInterval((30 * 1000).toLong())
        mLocationRequest.setFastestInterval((5 * 1000).toLong())

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback(ResultCallback<LocationSettingsResult> { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        })
    }

    //Get AVAILABLE DRIVERS ON MAP//START SERVICE
    private fun PostRequestDrivers(Url: String, latitude: Double, longitude: Double) {

        println("--------------AvailableDriver url-------------------$Url")

        Sselected_latitude = latitude.toString()
        Sselected_longitude = longitude.toString()

        println("--------------AvailableDriver UserID-------------------$UserID")
        println("--------------AvailableDriver latitude-------------------$latitude")
        println("--------------AvailableDriver longitude-------------------$longitude")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["lat"] = latitude.toString()
        jsonParams["lon"] = longitude.toString()

        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            @SuppressLint("LongLogTag")
            override fun onCompleteListener(response: String) {
                println("--------------Get Drivers Response-------------------$response")
                var fail_response = ""

                lv_rydetypes!!.visibility = View.VISIBLE

                if (list_item_selected == true) {
                    book_cardview_source_address_layout!!.visibility = View.GONE
                } else {
                    book_cardview_source_address_layout!!.visibility = View.VISIBLE
                }

                avLoadingIndicatorView!!.visibility = View.GONE
                //mMap!!.getUiSettings().setAllGesturesEnabled(false)

                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {
                        if (`object`.getString("status").equals("1", ignoreCase = true)) {

                            val jobject = `object`.getJSONObject("response")
                            Log.e("AVAIALABLEDRIVER_RESPONSE", response)
                            //rideLater_layout.setVisibility(View.VISIBLE)
                            //book_navigation_search_view1.setVisibility(View.VISIBLE)
                            //rideLater_layout.setClickable(true)
                            if (jobject.length() > 0) {
                                for (i in 0 until jobject.length()) {

                                    val check_driver_object = jobject.get("drivers")
                                    if (check_driver_object is JSONArray) {

                                        val driver_array = jobject.getJSONArray("drivers")
                                        if (driver_array.length() > 0) {
                                            driver_list.clear()

                                            for (j in 0 until driver_array.length()) {
                                                val driver_object = driver_array.getJSONObject(j)

                                                val pojo = HomePojo()
                                                pojo.setDriver_lat(driver_object.getString("lat"))
                                                pojo.setDriver_long(driver_object.getString("lon"))

                                                driver_list.add(pojo)
                                            }
                                            driver_status = true
                                        } else {
                                            driver_list.clear()
                                            driver_status = false
                                        }
                                    } else {
                                        driver_status = false
                                    }

                                    val check_category_object = jobject.get("category")
                                    if (check_category_object is JSONArray) {
                                        val cat_array = jobject.getJSONArray("category")
                                        if (cat_array.length() > 0) {
                                            category_list.clear()
                                            for (k in 0 until cat_array.length()) {
                                                val cat_object = cat_array.getJSONObject(k)
                                                val pojo = HomePojo()
                                                pojo.setCat_name(cat_object.getString("name"))
                                                //pojo.setCat_time(cat_object.getString("eta"));
                                                pojo.setCat_id(cat_object.getString("id"))
                                                pojo.setCat_time(cat_object.getString("eta"))

                                                pojo.setCat_image(cat_object.getString("icon_normal"))
                                                pojo.setMin_amount(cat_object.getString("eta"))
                                                pojo.setMain_type("Ridehub")
                                                category_list.add(pojo)


                                            }

                                            category_status = true
                                            Log.e("CategorySTATUSIF", "" + category_status)

                                        } else {
                                            Log.e("CategorySTATUSELSE", "" + category_status)
                                            category_list.clear()
                                            category_status = false
                                        }
                                    } else {
                                        category_status = false
                                    }
                                }
                            }
                            main_response_status = true
                            ride_adapter = BookMyRide_Adapter(activity!!, category_list)
                            lv_rydetypes!!.setAdapter(ride_adapter)
                            ride_adapter!!.notifyDataSetChanged()


                        } else {
                            fail_response = `object`.getString("response")
                            main_response_status = false
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (main_response_status) {
                    Log.e("DRIVERSTAT", "$driver_status----------$main_response_status---------$category_status")
                    if (driver_status) {
                        for (i in driver_list.indices) {
                            val Dlatitude = java.lang.Double.parseDouble(driver_list.get(i).getDriver_lat())
                            val Dlongitude = java.lang.Double.parseDouble(driver_list.get(i).getDriver_long())
                            // create marker double Dlatitude = gps.getLatitude();
                            val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
                            val height = 80
                            val width = 45
                            val bitmapdraw = resources.getDrawable(R.mipmap.car_icon) as BitmapDrawable
                            val b = bitmapdraw.bitmap
                            val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                            marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                           // marker.rotation(100f)
                            // adding marker
                            mMap!!.addMarker(marker)

                        }
                    } else {

                    }
                } else {

                }


            }

            override fun onErrorListener() {
            }
        })


        //***************UBER RIDE ESTIMATE REQUEST*************


    }

    fun startTimer() {
        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent) {
            timer = Timer()
            //initialize the TimerTask's job
            initializeTimerTask()
            //schedule the timer, to wake up every 1 second
            timer!!.schedule(timerTask, 3000, 10000)
        }//
    }


    fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                try {
                    Log.e("TIMER", "MANIBABU")
                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet
                    if (isInternetPresent) {
                        PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                    }
                } catch (e: Exception) {
                    Log.e("TIMER_EXCEPTION", e.toString())
                }

            }
        }
    }

    fun stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        println("--------------onActivityResult requestCode----------------$requestCode")
        // code to get country name
        if (requestCode == timer_request_code && resultCode == RESULT_OK && data != null) {
            val ride_accepted = data.getStringExtra("Accepted_or_Not")
            val retry_count = data.getStringExtra("Retry_Count")


            if (retry_count.equals("1", ignoreCase = true) && ride_accepted.equals("not", ignoreCase = true)) run {


                val mDialog = PkDialog(activity!!)
                mDialog.setDialogTitle(resources.getString(R.string.timer_label_alert_sorry))
                mDialog.setDialogMessage(resources.getString(R.string.timer_label_alert_content))
                mDialog.setPositiveButton(resources.getString(R.string.timer_label_alert_retry), View.OnClickListener {
                    mDialog.dismiss()

                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet

                    if (isInternetPresent) {
                       // val code = session.getCouponCode()
                      //  val coupon = code.get(SessionManager.KEY_COUPON_CODE)

                        Log.e("DISTANCE_CALCU_ONACTIVITY", DselectedLatitude + "" + DselectedLongitude)
                        ConfirmRideRequest(Iconstant.confirm_ride_url, "", "", "", selectedType, CategoryID, source_address!!.text.toString(), Recent_lat.toString(), Recent_long.toString(), "", Sselected_DestinationLocation.toString(), DselectedLatitude, DselectedLongitude)
                    } else {
                        Alert(activity!!.getResources().getString(R.string.alert_label_title), activity!!.getResources().getString(R.string.alert_nointernet))
                    }
                })
                mDialog.setNegativeButton(resources.getString(R.string.timer_label_alert_cancel), View.OnClickListener {
                    mDialog.dismiss()

                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet

                    if (isInternetPresent) {
                        DeleteRideRequest(Iconstant.delete_ride_url)
                    } else {
                        Alert(activity!!.getResources().getString(R.string.alert_label_title), activity!!.getResources().getString(R.string.alert_nointernet))
                    }
                })
                mDialog.show()

            } else if (retry_count.equals("2", ignoreCase = true) && ride_accepted.equals("not", ignoreCase = true)) run { DeleteRideRequest(Iconstant.delete_ride_url) } else


            if ((retry_count.equals("1", ignoreCase = true) || retry_count.equals("2", ignoreCase = true)) && ride_accepted.equals("Cancelled", ignoreCase = true)) {
                riderId = ""

                //---------Hiding the bottom layout after cancel request--------
                //  mMap.getUiSettings().setAllGesturesEnabled(true);
                mMap!!.clear()
                onMapReady(mMap)
                drawer_layout!!.setVisibility(View.VISIBLE)
                drawer_layout!!.setClickable(true)
                book_cardview_source_address_layout!!.setVisibility(View.VISIBLE)
                destination_address!!.setText("")
                currentLocation_image!!.setClickable(true)
                currentLocation_image!!.setVisibility(View.VISIBLE)
                drawer_layout!!.setEnabled(true)
                NavigationDrawer.enableSwipeDrawer()

            }


        } else if (requestCode == placeSearch_request_code && resultCode == RESULT_OK && data != null) {

            if (search_status == 0) {

                setpickuploc = data.getStringExtra("setpickuploc")
                Log.e("setpickuploc", setpickuploc)

                if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                    lv_rydetypes!!.setVisibility(View.GONE)
                }
                SselectedLatitude = data.getStringExtra("Selected_Latitude")
                SselectedLongitude = data.getStringExtra("Selected_Longitude")
                DselectedLatitude = data.getStringExtra("Selected_DLatitude")
                DselectedLongitude = data.getStringExtra("Selected_DLongitude")
                locatiosearch = data.getStringExtra("searchlocation")

                Log.e("OnACTIVTY_IF", "SOURCE-->" + SselectedLatitude + "" + SselectedLongitude + "DESTINATION--->" + DselectedLatitude + "-------" + DselectedLongitude + "---------" + locatiosearch)

                try {
                    val lat_decimal = java.lang.Double.parseDouble(SselectedLatitude)
                    val lng_decimal = java.lang.Double.parseDouble(SselectedLongitude)
                    val pick_lat_decimal = java.lang.Double.parseDouble(DselectedLatitude)
                    val pick_lng_decimal = java.lang.Double.parseDouble(DselectedLongitude)
                    stoptimertask()


                    updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal, "")


                    println("inside updategoogle1--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }


                Sselected_DestinationLocation = data.getStringExtra("Selected_Location")

                destination_address!!.setText(Sselected_DestinationLocation)
                tv_selected_cab_type_desti!!.setText(Sselected_DestinationLocation)

                Selected_SourceLocation = data.getStringExtra("Selected_SourceLocation")
                Log.e("SELECTED_SOURCE", Selected_SourceLocation)
                source_address!!.setText(Selected_SourceLocation)

//uncomment this while working for uber
                //uber_rides_ridehub()
                if (!SselectedLatitude.equals("", ignoreCase = true) && SselectedLatitude.length > 0 && !SselectedLongitude.equals("", ignoreCase = true) && SselectedLongitude.length > 0) {
                    // Move the camera to last position with a zoom level
                    val cameraPosition = CameraPosition.Builder().target(LatLng(java.lang.Double.parseDouble(SselectedLatitude), java.lang.Double.parseDouble(SselectedLongitude))).zoom(17f).build()
                    mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                }


            } else {
                SdestinationLatitude = data.getStringExtra("Selected_Latitude")
                SdestinationLongitude = data.getStringExtra("Selected_Longitude")
                SsourceLatitude = data.getStringExtra("Selected_DLatitude")
                SsourceLongitude = data.getStringExtra("Selected_DLongitude")

                SdestinationLocation = data.getStringExtra("Selected_Location")


                Log.e("OnACTIVTY_ELSE", SsourceLatitude + "-------" + SsourceLongitude + "----" + SdestinationLatitude + "-----" + SdestinationLongitude + "------------" + SdestinationLocation)

                println("-----------SdestinationLatitude----------------$SdestinationLatitude")
                println("-----------SdestinationLongitude----------------$SdestinationLongitude")
                println("-----------SsourceLatitude----------------$SsourceLatitude")
                println("-----------SsourceLongitude----------------$SsourceLongitude")
                println("-----------SdestinationLocation----------------$SdestinationLocation")


            }
        } else if (requestCode == REQUEST_LOCATION) {
            println("----------inside request location------------------")

            when (resultCode) {
                RESULT_OK -> {
                    Toast.makeText(activity, "Location enabled!", Toast.LENGTH_LONG).show()
                }
                Activity.RESULT_CANCELED -> {
                    enableGpsService()
                }
                else -> {
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun updateGoogleMapTrackRide(lat_decimal: Double, lng_decimal: Double, pick_lat_decimal: Double, pick_lng_decimal: Double, status: String) {
        if (mMap != null) {
            mMap!!.clear()
        }


        val dropLatLng = LatLng(lat_decimal, lng_decimal)
        val pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)
        /*if(currentLocation  != null){
            pickUpLatLng = new  LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
        }else{
            pickUpLatLng = new  LatLng(MyCurrent_lat,MyCurrent_long);
        }*/

        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng, pickUpLatLng, status)
        draw_route_asyncTask.execute()
    }

    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var currentLocation: LatLng? = null
        private var endLocation: LatLng? = null
        internal var status = ""

        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng, status: String) {
            this.currentLocation = currentLocation
            this.endLocation = endLocation
            this.status = status

        }

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(currentLocation, endLocation, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"

            if (isInternetPresent as Boolean) {
                if (list_item_selected.equals(false)) {
                    PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                }
            }

            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()

                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)



                    val rectLine = PolylineOptions().width(10f).color(
                            resources.getColor(R.color.blue_text_bg)).geodesic(true)
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }

                    mMap!!.setMyLocationEnabled(false)
                    mMap!!.addMarker(MarkerOptions()
                            .position(currentLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))

                    mMap!!.addMarker(MarkerOptions()
                            .position(endLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))

                    val builder = LatLngBounds.Builder()
                    /*builder.include(currentLocation!!)
                    builder.include(endLocation!!)*/

                    for (i in directionPoint.indices) {
                        builder.include(directionPoint.get(i))
                    }
                    val bounds = builder.build()



                    val routePadding = 120
                    mMap!!.animateCamera(
                            CameraUpdateFactory.newLatLngBounds(bounds, routePadding),
                            600,
                            null
                    );


                   // mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                   // mMap!!.animateCamera(CameraUpdateFactory.zoomTo(mMap!!.getCameraPosition().zoom  +0.5f))
                    //mMap!!.getMinZoomLevel()
                   // mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(currentLocation!!.latitude, currentLocation!!.longitude), 15f))

                    MapAnimator.getInstance().setPrimaryLineColor(resources.getColor(R.color.blue_text_bg))
                    MapAnimator.getInstance().setSecondaryLineColor(resources.getColor(R.color.cab_selected))
                    MapAnimator.getInstance().animateRoute(mMap, directionPoint)


                    ///************Lyft**************///

                    curLoc = currentLocation
                    endLoc = endLocation
//Uncomment this while working with Lyft///
                    //postRequest_ride_type_lyft(curLoc!!)
                    //postRequest_nearby_drivers_eta_lyft(curLoc!!) // nearby drivers service
                    //postRequest_ride_estimate_lyft(curLoc!!, endLoc!!)    // ride estimates service


                    // Adding route on the map
                    // mMap!!.addPolyline(rectLine)
                    /* markerOptions!!.position(currentLocation!!)
                     markerOptions!!.position(endLocation!!)*/
                    markerOptions!!.draggable(true)

                    //googleMap.addMarker(markerOptions);


                    println("inside---------marker--------------")

                    //Show path in


                    if (!status.equals("nocars", ignoreCase = true)) {

                        ///PostRequest(Iconstant.BookMyRide_url, java.lang.Double.parseDouble(SselectedLatitude), java.lang.Double.parseDouble(SselectedLongitude), java.lang.Double.parseDouble(DselectedLatitude), java.lang.Double.parseDouble(DselectedLongitude))
                    }

                    if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                        lv_rydetypes!!.setVisibility(View.VISIBLE)
                    } else {
                        //listview!!.setVisibility(View.VISIBLE)
                        currentLocation_image!!.setVisibility(View.GONE)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }





    //-------------Method to get Complete Address------------
    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {

        var strAdd = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                } else {
                    try {
                        strReturnedAddress.append(returnedAddress.getAddressLine(0))
                    } catch (ignored: Exception) {
                    }

                }
                /* for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }*/
                strAdd = strReturnedAddress.toString()
            } else {
                Log.e("Current loction address", "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Current loction address", "Canont get Address!")
        }

        return strAdd
    }


    private fun ConfirmRideRequest(Url: String, code: String, pickUpDate: String, pickup_time: String, type: String, category: String, pickup_location: String, pickup_lat: String, pickup_lon: String, try_value: String, destination_location: String, destination_lat: String, destination_lon: String) {

        dialog = Dialog(activity)
        dialog.getWindow()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val loading = dialog.findViewById(R.id.custom_loading_textview) as TextView
        loading.text = resources.getString(R.string.action_pleasewait)

        Log.e("DISTANCE_CALCU_CONFIRMRIDE", destination_lat + "" + destination_lon)
        println("--------------Confirm Ride url-------------------$Url")


        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["code"] = code
        jsonParams["pickup_date"] = pickUpDate
        jsonParams["pickup_time"] = pickup_time
        jsonParams["type"] = type
        jsonParams["category"] = category
        jsonParams["subcategory"] = SubCategoryID
        jsonParams["pickup"] = pickup_location
        jsonParams["pickup_lat"] = pickup_lat
        jsonParams["pickup_lon"] = pickup_lon
        jsonParams["ride_id"] = riderId
        jsonParams["goods_type"] = ""
        jsonParams["goods_type_quanity"] = ""
        jsonParams["drop_loc"] = destination_location
        jsonParams["drop_lat"] = destination_lat
        jsonParams["drop_lon"] = destination_lon

        println("---------------destination_location----------$destination_location")
        println("---------------destination_lat----------$destination_lat")
        println("---------------destination_lon----------$destination_lon")
        //}


        println("---------------user_id----------$UserID")
        println("---------------code----------$code")
        println("---------------pickpudate----------$pickUpDate")
        println("---------------pickup_time----------$pickup_time")
        println("---------------type----------$type")
        println("---------------category----------$category")
        println("---------------pickup----------$pickup_location")
        println("---------------pickup_lat----------$pickup_lat")
        println("---------------pickup_lon----------$pickup_lon")
        println("---------------try----------$try_value")
        println("---------------riderId----------$riderId")


        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("--------------Confirm Ride reponse-------------------$response")
                Log.e("response_test", response)
                var selected_type = ""
                var Sacceptance = ""
                var Str_driver_id = ""
                var Str_driver_name = ""
                var Str_driver_email = ""
                var Str_driver_image = ""
                var Str_driver_review = ""
                var Str_driver_lat = ""
                var Str_driver_lon = ""
                var Str_min_pickup_duration = ""
                var Str_ride_id = ""
                var Str_phone_number = ""
                var Str_vehicle_number = ""
                var Str_vehicle_model = ""
                try {
                    val `object` = JSONObject(response)

                    Log.e("response_test_trycahhe", response)
                    if (`object`.length() > 0) {
                        val status = `object`.getString("status")
                        if (status.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            Log.e("hemanth_test", response_object.toString())
                            selected_type = response_object.getString("type")
                            Sacceptance = `object`.getString("acceptance")
                            Log.e("Sacceptance", Sacceptance)
                            if (Sacceptance.equals("No", ignoreCase = true)) {
                                response_time = response_object.getString("response_time")
                            }
                            riderId = response_object.getString("ride_id")

                            if (Sacceptance.equals("Yes", ignoreCase = true)) {
                                val driverObject = response_object.getJSONObject("driver_profile")

                                Str_driver_id = driverObject.getString("driver_id")
                                Str_driver_name = driverObject.getString("driver_name")
                                Str_driver_email = driverObject.getString("driver_email")
                                Str_driver_image = driverObject.getString("driver_image")
                                Str_driver_review = driverObject.getString("driver_review")
                                Str_driver_lat = driverObject.getString("driver_lat")
                                Str_driver_lon = driverObject.getString("driver_lon")
                                Str_min_pickup_duration = driverObject.getString("min_pickup_duration")

                                Log.e("ride_id_test", riderId)
                                Str_ride_id = driverObject.getString("ride_id")
                                Str_phone_number = driverObject.getString("phone_number")
                                Str_vehicle_number = driverObject.getString("vehicle_number")
                                Str_vehicle_model = driverObject.getString("vehicle_model")
                            }
                            Log.e("selected_type", selected_type)

                            if (selected_type.equals("1", ignoreCase = true)) {

                                val mDialog = PkDialog(activity!!)
                                mDialog.setDialogTitle(activity!!.getResources().getString(R.string.action_success))
                                mDialog.setDialogMessage(activity!!.getResources().getString(R.string.ridenow_label_confirm_success))
                                mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                                    mDialog.dismiss()
                                    destination_address!!.setText("")
                                    startTimer()
                                    mMap!!.clear()
                                    cd = ConnectionDetector(activity!!)
                                    isInternetPresent = cd!!.isConnectingToInternet
                                    /* if (isInternetPresent) {
                                         postRequest_recentRides(home_recent_reides_url)
                                     }*/
                                    /* book_cardview_address_layout2.setVisibility(GONE)
                                     book_cardview_address_layout.setVisibility(View.VISIBLE)
                                     book_cardview_address_layout_recentRides.setVisibility(View.VISIBLE)
                                     ll_book_cardview_address_layout34.setVisibility(View.VISIBLE)
                                     book_cardview_address_layout3.setVisibility(View.VISIBLE)
                                     book_cardview_address_layout4.setVisibility(View.VISIBLE)
                                     book_cardview_address_layout.setVisibility(View.VISIBLE)
                                     bottom_layout.setVisibility(View.GONE)
                                     Tv_marker_time.setVisibility(View.GONE)
                                     Tv_marker_min.setVisibility(View.GONE)
                                     center_marker.setVisibility(View.GONE)
                                     back_menu_layout.setVisibility(View.GONE)
                                     drawer_layout!!.setVisibility(View.VISIBLE)
                                     drawer_layout!!.setClickable(true)
                                     mBottomSheetBehavior2.setHideable(true)
                                     mBottomSheetBehavior2.setState(BottomSheetBehavior.STATE_COLLAPSED)*/


                                    //---------Hiding the bottom layout after success request--------
                                    // mMap.getUiSettings().setAllGesturesEnabled(true);

                                    val animFadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out)
                                    /*  ridenow_option_layout.startAnimation(animFadeOut)
                                      ridenow_option_layout.setVisibility(View.GONE)
                                      center_marker.setEnabled(true)

                                      listview.setVisibility(View.GONE)
                                      rideNow_textview.setText(resources.getString(R.string.home_label_ride_now))
                                      rl_cancel_layout.setVisibility(View.VISIBLE)
                                      rideNow_layout.setBackground(resources.getDrawable(R.drawable.leftcurve_triplater_ridenow))

                                      bottom_layout.setBackgroundColor(Color.parseColor("#00000000"))*/
                                    currentLocation_image!!.setClickable(true)
                                    currentLocation_image!!.setVisibility(View.GONE)
                                    // pickTime_layout.setEnabled(true)
                                    drawer_layout!!.setEnabled(true)
                                    //address_layout.setEnabled(true)
                                    NavigationDrawer.enableSwipeDrawer()
                                })
                                mDialog.show()

                            } else if (selected_type.equals("0", ignoreCase = true)) {
                                Log.e("selected_type 1", selected_type)
                                if (Sacceptance.equals("Yes", ignoreCase = true)) {
                                    Log.e("selected_type_2", selected_type + "-----" + source_address!!.text.toString() + "-----" + Sselected_DestinationLocation)
                                    //Move to ride Detail page
                                    val i = Intent(activity, TrackYourRide::class.java)
                                    i.putExtra("driverID", Str_driver_id)
                                    // i.putExtra("cab_type_image", "Ridehub")
                                    i.putExtra("driverName", Str_driver_name)
                                    i.putExtra("driverImage", Str_driver_image)
                                    i.putExtra("driverRating", Str_driver_review)
                                    i.putExtra("driverLat", Str_driver_lat)
                                    i.putExtra("driverLong", Str_driver_lon)
                                    i.putExtra("driverTime", Str_min_pickup_duration)
                                    i.putExtra("rideID", Str_ride_id)
                                    i.putExtra("driverMobile", Str_phone_number)
                                    i.putExtra("driverCar_no", Str_vehicle_number)
                                    i.putExtra("driverCar_model", Str_vehicle_model)
                                    i.putExtra("userLat", pickup_lat)
                                    i.putExtra("userLong", pickup_lon)
                                    i.putExtra("sourceLocation", source_address!!.text.toString())
                                    i.putExtra("destination_Location", Sselected_DestinationLocation.toString())
                                    i.putExtra("cat_type", "Ridehub")
                                    startActivity(i)
                                    activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                                } else {
                                    val intent = Intent(activity, TimerPage::class.java)
                                    intent.putExtra("Time", response_time)
                                    intent.putExtra("retry_count", try_value)
                                    intent.putExtra("ride_ID", riderId)
                                    startActivityForResult(intent, timer_request_code)
                                    activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                }
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            Alert(activity!!.getResources().getString(R.string.alert_label_title), Sresponse)
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()
            startTimer()
            cd = ConnectionDetector(activity!!)
            isInternetPresent = cd!!.isConnectingToInternet
            /* if (isInternetPresent) {
                 postRequest_recentRides(home_recent_reides_url)
             }*/
            mMap!!.clear()
            onMapReady(mMap)
            drawer_layout!!.setVisibility(View.VISIBLE)
            drawer_layout!!.setClickable(true)
            drawer_layout!!.setClickable(true)
            book_cardview_source_address_layout!!.setVisibility(View.GONE)
        })
        mDialog.show()
    }


    //-------------------AsynTask To get the current Address----------------
    private fun PostRequest(Url: String, latitude: Double, longitude: Double, Dlatitude: Double, Dlongitude: Double) {
        //loading_layout.setVisibility(View.VISIBLE);
        progressWheel!!.setVisibility(View.VISIBLE)
        //center_marker.setVisibility(View.GONE);
        println("--------------Book My ride url-------------------$Url")
        Sselected_latitude = latitude.toString()
        Sselected_longitude = longitude.toString()
        category_list.clear()

        println("--------------Book My ride UserID-------------------$UserID")
        println("--------------Book My ride latitude-------------------$latitude")
        println("--------------Book My ride longitude-------------------$longitude")
        println("--------------Book My ride CategoryID-------------------$CategoryID")
        println("--------------Book My ride DLatitude-------------------$Dlatitude")
        println("--------------Book My ride DLongitude-------------------$Dlongitude")


        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["pickup_lat"] = latitude.toString()
        jsonParams["pickup_lon"] = longitude.toString()
        jsonParams["drop_lat"] = Dlatitude.toString()
        jsonParams["drop_lon"] = Dlongitude.toString()
        jsonParams["category"] = CategoryID
        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Book My ride reponse-------------------$response")
                var fail_response = ""
                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {
                        if (`object`.getString("status").equals("1", ignoreCase = true)) {

                            val jobject = `object`.getJSONObject("response")
                            if (jobject.length() > 0) {
                                for (i in 0 until jobject.length()) {

                                    val check_driver_object = jobject.get("drivers")
                                    if (check_driver_object is JSONArray) {

                                        val driver_array = jobject.getJSONArray("drivers")
                                        if (driver_array.length() > 0) {
                                            driver_list.clear()

                                            for (j in 0 until driver_array.length()) {
                                                val driver_object = driver_array.getJSONObject(j)

                                                val pojo = HomePojo()
                                                pojo.setDriver_lat(driver_object.getString("lat"))
                                                pojo.setDriver_long(driver_object.getString("lon"))

                                                driver_list.add(pojo)
                                            }
                                            driver_status = true
                                        } else {
                                            driver_list.clear()
                                            driver_status = false
                                        }
                                    } else {
                                        driver_status = false
                                    }

                                    val check_ratecard_object = jobject.get("ratecard")
                                    if (check_ratecard_object is JSONObject) {

                                        val ratecard_object = jobject.getJSONObject("ratecard")
                                        if (ratecard_object.length() > 0) {
                                            ratecard_list.clear()
                                            val pojo = HomePojo()

                                            pojo.setRate_cartype(ratecard_object.getString("category"))
                                            pojo.setRate_note(ratecard_object.getString("note"))
                                            pojo.setCurrencyCode(jobject.getString("currency"))

                                            val farebreakup_object = ratecard_object.getJSONObject("farebreakup")
                                            if (farebreakup_object.length() > 0) {
                                                val minfare_object = farebreakup_object.getJSONObject("min_fare")
                                                if (minfare_object.length() > 0) {
                                                    pojo.setMinfare_amt(minfare_object.getString("amount"))
                                                    pojo.setMinfare_km(minfare_object.getString("text"))
                                                }

                                                val afterfare_object = farebreakup_object.getJSONObject("after_fare")
                                                if (afterfare_object.length() > 0) {
                                                    pojo.setAfterfare_amt(afterfare_object.getString("amount"))
                                                    pojo.setAfterfare_km(afterfare_object.getString("text"))
                                                }

                                                val otherfare_object = farebreakup_object.getJSONObject("other_fare")
                                                if (otherfare_object.length() > 0) {
                                                    pojo.setOtherfare_amt(otherfare_object.getString("amount"))
                                                    pojo.setOtherfare_km(otherfare_object.getString("text"))
                                                }
                                            }

                                            ratecard_list.add(pojo)
                                            ratecard_status = true
                                        } else {
                                            ratecard_list.clear()
                                            ratecard_status = false
                                        }
                                    } else {
                                        ratecard_status = false
                                    }

                                    val check_category_object = jobject.get("category")
                                    if (check_category_object is JSONArray) {
                                        val cat_array = jobject.getJSONArray("category")
                                        if (cat_array.length() > 0) {
                                            category_list.clear()
                                            for (k in 0 until cat_array.length()) {
                                                val cat_object = cat_array.getJSONObject(k)
                                                val pojo = HomePojo()
                                                pojo.setCat_name(cat_object.getString("name"))
                                                //pojo.setCat_time(cat_object.getString("eta"));
                                                pojo.setCat_id(cat_object.getString("id"))
                                                pojo.setMin_amount(cat_object.getString("min_amount"))
                                                pojo.setMax_amount(cat_object.getString("max_amount"))
                                                pojo.setCar_capacity_min(cat_object.getString("seatcapacity_min"))
                                                pojo.setCar_capacity_max(cat_object.getString("seatcapacity_max"))
                                                pojo.setIcon_normal(cat_object.getString("icon_normal"))
                                                pojo.setIcon_active(cat_object.getString("icon_active"))
                                                pojo.setSelected_Cat(jobject.getString("selected_category"))

                                                //Log.e("selected_subcategory",jobject.getString("selected_category")+"--"+jobject.getString("selected_subcategory"));
                                                if (cat_object.getString("id") == jobject.getString("selected_category")) {
                                                    CarAvailable = cat_object.getString("eta")
                                                    ScarType = cat_object.getString("name")
                                                    Log.e("CatCARAVAILABLE", "" + CarAvailable + "" + ScarType)

                                                    tv_selected_cab_estimate_time!!.setText(CarAvailable + " to closest ride")
                                                    tv_selected_cab__fare!!.setText("$" + cat_object.getString("max_amount"))


                                                    // tv_selected_cab_type!!.setText("Ridehub")

                                                }
                                                category_list.add(pojo)
                                            }

                                            category_status = true
                                            Log.e("CategorySTATUSIF", "" + category_status)
                                        } else {
                                            Log.e("CategorySTATUSELSE", "" + category_status)
                                            category_list.clear()
                                            category_status = false
                                        }


                                    } else {
                                        category_status = false
                                    }
                                }
                            }

                            main_response_status = true

                        } else {
                            fail_response = `object`.getString("response")
                            main_response_status = false
                        }

                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                // UberRideEstimateRequest()
                PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)


                if (main_response_status) {
                    Log.e("DRIVERSTAT", "$driver_status----------$main_response_status---------$category_status")
                    if (driver_status) {

                        currentLocation_image!!.setVisibility(View.GONE)
                        /***********Design Visibility Views********/
                        rl_continue_booking!!.visibility = View.VISIBLE
                        book_cardview_source_address_layout!!.visibility = View.GONE
                        for (i in driver_list.indices) {
                            val Dlatitude = java.lang.Double.parseDouble(driver_list[i].getDriver_lat())
                            val Dlongitude = java.lang.Double.parseDouble(driver_list[i].getDriver_long())

                            // create marker double Dlatitude = gps.getLatitude();
                            val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
                            val height = 80
                            val width = 45
                            val bitmapdraw = resources.getDrawable(R.mipmap.car_icon) as BitmapDrawable
                            val b = bitmapdraw.bitmap
                            val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                            marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                           // marker.rotation(100f)
                            // adding marker
                            mMap!!.addMarker(marker)
                        }
                        //To Draw the Polyline between latlng

                        Log.e("LOCATION_SOURCE_CHANGE", "$SselectedLatitude--$SselectedLongitude---$DselectedLatitude----$DselectedLongitude")
                        if (SselectedLatitude.equals("", ignoreCase = true)) {
                            SselectedLatitude = Sselected_latitude
                        }
                        if (SselectedLongitude.equals("", ignoreCase = true)) {
                            SselectedLongitude = Sselected_longitude
                        }
                        Log.e("LOCATION_SOURCE_CHANGE_ll", "$SselectedLatitude--$SselectedLongitude---$DselectedLatitude----$DselectedLongitude")
                        // drawRoutePolyline(SselectedLatitude, SselectedLongitude, DselectedLatitude, DselectedLongitude, "")

                    } else {

                        Log.e("DRIVERSTATELSE", "$driver_status----------$main_response_status---------$category_status")
                        Log.e("LOCATION_LAT", "$SselectedLatitude----$Sselected_longitude--$DselectedLatitude-----$DselectedLongitude")
                        rl_continue_booking!!.visibility = View.GONE
                        book_cardview_source_address_layout!!.visibility = View.GONE
                        mMap!!.clear()
                        if (SselectedLatitude.equals("", ignoreCase = true)) {
                            SselectedLatitude = Sselected_latitude
                        }
                        if (SselectedLongitude.equals("", ignoreCase = true)) {
                            SselectedLongitude = Sselected_longitude
                        }
                        try {
                            val lat_decimal = java.lang.Double.parseDouble(SselectedLatitude)
                            val lng_decimal = java.lang.Double.parseDouble(SselectedLongitude)
                            val pick_lat_decimal = java.lang.Double.parseDouble(DselectedLatitude)
                            val pick_lng_decimal = java.lang.Double.parseDouble(DselectedLongitude)
                            updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal, "nocars")

                            println("inside updategoogle1--------------updateGoogleMapTrackRide")
                        } catch (e: Exception) {
                            println("try--------------$e")
                        }

                    }

                    if (category_status) {
                        Log.e("category_list_RideHub", category_list.size.toString() + "")
                        progressWheel!!.setVisibility(View.GONE)


                        category_list.clear()
                        cat_images.clear()
                        cat_time.clear()
                        cat_time_sorted.clear()
                        //Uncomment this when working with uber
                        //uber_rides_ridehub()

                        adapter = BookMyRide_Adapter(activity!!, category_list)
                        lv_rydetypes!!.setAdapter(adapter)
                        adapter!!.notifyDataSetChanged()



                        if (!driver_status) {
                            Log.e("DRIVER_FALSE", "MANIBABU")
                            currentLocation_image!!.setVisibility(View.GONE)
                            if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                                Log.e("SETPICKUP", setpickuploc)
                            } else {
                                Log.e("SETPICKUP_no", setpickuploc)
                                //listview.setVisibility(View.VISIBLE)
                                currentLocation_image!!.setVisibility(View.GONE)
                            }

                        } else {
                            Log.e("DRIVER_TRUE", "MANIBABU")

                            Log.e("SETPICKUP_lll", setpickuploc)
                            if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                                Log.e("SETPICKUP_lll", setpickuploc)
                            } else {
                                Log.e("SETPICKUP_lll_no", setpickuploc)
                                //listview.setVisibility(View.VISIBLE)
                                currentLocation_image!!.setVisibility(View.GONE)
                            }
                        }

                    } else {
                        //listview.setVisibility(GONE)
                    }


                } else {

                }

                // SselectedAddress = address
                progressWheel!!.setVisibility(View.GONE)

            }

            override fun onErrorListener() {
                progressWheel!!.setVisibility(View.GONE)

            }
        })
    }


    override fun onDestroy() {
        activity!!.unregisterReceiver(logoutReciver)
        stoptimertask()
        super.onDestroy()

    }


    //*****************UBER RIDE REQUEST CODE********************


    private fun UberEstimate(sselectedLatitude: String, sselectedLongitude: String, dselectedLatitude: String, dselectedLongitude: String, prod_id: String) {

        progressWheel!!.setVisibility(View.VISIBLE)
        val jobj = JSONObject()
        jobj.put("start_latitude", sselectedLatitude)
        jobj.put("start_longitude", sselectedLongitude)
        jobj.put("end_latitude", dselectedLatitude)
        jobj.put("end_longitude", dselectedLongitude)
        jobj.put("product_id", prod_id)

        product_id_stg = prod_id

        uber_start_lat = sselectedLatitude
        uber_start_lng = sselectedLongitude
        uber_end_lat = dselectedLatitude
        uber_end_lng = dselectedLongitude

        uber_Request = object : StringRequest(Request.Method.POST, Iconstant.API_ESTI_stg,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {

                        println("-------------TOLLDATA Response----------------$response")
                        Log.e("estimation_data", "---" + response)

                        progressWheel!!.setVisibility(View.GONE)
                        try {
                            val main_obj = JSONObject(response)
                            val fare_obj = main_obj.getJSONObject("fare")
                            val breakdown_array = fare_obj.getJSONArray("breakdown")
                            for (n in 0 until breakdown_array.length()) {
                                val type_obj = breakdown_array.getJSONObject(n)
                                val type_stg = type_obj.getString("type")
                                val name_stg = type_obj.getString("name")
                                value_stg = type_obj.getString("value")

                                tv_selected_cab__fare!!.setText("$" + value_stg)
                            }
                            fare_id_stg = fare_obj.getString("fare_id")


                            val trip_obj = main_obj.getJSONObject("trip")
                            val pickup_estimate_obj = main_obj.getString("pickup_estimate")

                            trip_dist_unit_stg = trip_obj.getString("distance_unit")
                            trip_dur_unit_stg = trip_obj.getString("duration_estimate")
                            trip_dist_est_stg = trip_obj.getString("distance_estimate")

                            tv_selected_cab_estimate_time!!.setText(pickup_estimate_obj.toString() + " min " + " to closest ride")


                            // Toast.makeText(context, "Fare ID: " + fare_id_stg, Toast.LENGTH_SHORT).show()

                            rl_continue_booking!!.visibility = View.VISIBLE
                            book_cardview_source_address_layout!!.visibility = View.GONE

                        } catch (e: Exception) {

                        }


                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_error", "---" + error)
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                headers.put("Content-type", "application/json")
                /* headers["x-api-key"] = Iconstant.toll_api_key
                 headers["content-type"] = "application/json"*/
                return headers
            }


            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (jobj.toString() == null) null else jobj.toString().toByteArray()
            }

        }
        uber_Request.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Request.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Request)

    }

    private fun UberEstimateTime() {
        uber_Request = object : StringRequest(Request.Method.GET, Iconstant.API_PROD_TIME_stg + "?start_latitude=" + Sselected_latitude + "&start_longitude=" + Sselected_longitude,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {

                        println("-------------UBERESTIMATETIME Response----------------$response")
                        Log.e("estimation_time_data", "---" + response)
                        cat_time.clear()
                        //category_list.clear()
                        try {
                            val main_obj = JSONObject(response)
                            val breakdown_array = main_obj.getJSONArray("times")
                            for (n in 0 until breakdown_array.length()) {
                                val type_obj = breakdown_array.getJSONObject(n)
                                val localized_display_name_stg = type_obj.getString("localized_display_name")
                                val estimate_stg = type_obj.getString("estimate")
                                val display_name_stg = type_obj.getString("display_name")

                                val pojo = HomePojo()
                                pojo.setCat_time(estimate_stg)

                                cat_time.add(pojo)

                                cat_time_sorted.add(cat_images.get(n).getMin_amount()!!.toDouble())

                            }

                            Collections.sort(cat_time_sorted)
                            System.out.println("Sorted set: " + cat_time_sorted + "------------" + cat_time_sorted.subList(0, min(cat_time_sorted.size, 2)))

                            cat_time_sorted.subList(0, min(cat_time_sorted.size, 2))

                            for (k in 0 until cat_time_sorted.size) {

                                val pojo = HomePojo()
                                pojo.setCat_time("$" + cat_time_sorted[k].toString())
                                pojo.setCat_name(cat_images.get(k).getCat_name().toString())
                                pojo.setCat_id(cat_images.get(k).getCat_id().toString())
                                pojo.setCat_image(cat_images.get(k).getCat_image().toString())
                                pojo.setMain_type("Uber")

                                if (k >= 0 && k <= 1) {
                                    category_list.add(pojo)
                                }


                            }
                            main_response_status = true
                            Log.e("category_list_uber", (cat_time_sorted + "---" + category_list.size.toString() + "-------" + cat_images.size.toString() + "-----" + cat_time.size.toString()).toString())

                            adapter = BookMyRide_Adapter(activity!!, category_list)
                            lv_rydetypes!!.adapter = adapter
                            adapter!!.notifyDataSetChanged()


                        } catch (e: Exception) {

                        }


                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_error", "---" + error)
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Request.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Request.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Request)

    }

    private fun UberRideReq() {
        val v2GetRouteDirection = GMapV2GetRouteDirection()
        var document: Document
        val jobj = JSONObject()

        jobj.put("start_latitude", uber_start_lat)
        jobj.put("start_longitude", uber_start_lng)
        jobj.put("end_latitude", uber_end_lat)
        jobj.put("end_longitude", uber_end_lng)
        jobj.put("product_id", product_id_stg)
        jobj.put("fare_id", fare_id_stg)

        uber_Request = object : StringRequest(Request.Method.POST, Iconstant.API_REQ_stg,
                object : com.android.volley.Response.Listener<String> {
                    @SuppressLint("MissingPermission")
                    override fun onResponse(response: String) {

                        Log.e("req_data", "---" + response)

                        try {
                            val main_ride_obj = JSONObject(response)
                            val status_stg = main_ride_obj.getString("status")
                            var product_id = main_ride_obj.getString("product_id")
                            val destination_obj = main_ride_obj.getJSONObject("destination")
                            val pickup_obj = main_ride_obj.getJSONObject("pickup")
                            val latitude_stg = destination_obj.getString("latitude")
                            val longitude_stg = destination_obj.getString("longitude")

                            val latitude_pickup_stg = pickup_obj.getString("latitude")
                            val longitude_pickup_stg = pickup_obj.getString("longitude")

                            request_id_stg = main_ride_obj.getString("request_id")

                            session!!.setlyft_ride_id(request_id_stg)
                            Log.e("request_id_stg", request_id_stg)
                            if (status_stg.equals("processing")) {
                                timer_stop = "0"

                                UberCurrentReq()

                            } else {
                                var driver_stg = main_ride_obj.getString("driver")
                            }

                            var eta_stg = main_ride_obj.getString("eta")
                            var location_stg = main_ride_obj.getString("location")
                            var vehicle_stg = main_ride_obj.getString("vehicle")
                            var shared_stg = main_ride_obj.getString("shared")
                            Toast.makeText(context, "Ride Request Status: " + status_stg, Toast.LENGTH_LONG).show()


                            val data_pickup = LatLng(latitude_pickup_stg.toDouble(), longitude_pickup_stg.toDouble())
                            val data_drop = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())

                            document = v2GetRouteDirection.getDocument(data_drop, data_pickup, GMapV2GetRouteDirection.MODE_DRIVING)

                            mMap!!.clear()
                            try {
                                val directionPoint = v2GetRouteDirection.getDirection(document)
                                val rectLine = PolylineOptions().width(10f).color(
                                        resources.getColor(R.color.black)).geodesic(true)
                                for (i in directionPoint.indices) {
                                    rectLine.add(directionPoint.get(i))
                                }
                                // Adding route on the map
                                mMap!!.addPolyline(rectLine)
                                markerOptions!!.position(data_drop)
                                markerOptions!!.position(data_pickup)
                                markerOptions!!.draggable(true)
                                mMap!!.isMyLocationEnabled = false

                                //googleMap.addMarker(markerOptions);
                                mMap!!.addMarker(MarkerOptions()
                                        .position(data_drop)
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                                mMap!!.addMarker(MarkerOptions()
                                        .position(data_pickup)
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))

                                println("inside---------marker--------------")
                                //Show path in
                                val builder = LatLngBounds.Builder()
                                builder.include(data_drop)
                                builder.include(data_pickup)
                                val bounds = builder.build()
                                mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300))
                                // mMap.getMinZoomLevel();

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } catch (e: Exception) {

                        }


                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_error", "---" + error)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                headers.put("Content-type", "application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (jobj.toString() == null) null else jobj.toString().toByteArray()
            }

        }
        uber_Request.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Request.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Request)

    }

    private fun UberCurrentReq() {

        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_CURRENT_stg, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("req_id_data", "---" + response)
                val v2GetRouteDirection = GMapV2GetRouteDirection()
                var document: Document
                try {
                    val main_ride_obj = JSONObject(response)
                    val product_id_stg = main_ride_obj.getString("product_id")
                    val request_id_stg = main_ride_obj.getString("request_id")
                    val status_stg = main_ride_obj.getString("status")


                    val destination_obj = main_ride_obj.getJSONObject("destination")
                    val pickup_obj = main_ride_obj.getJSONObject("pickup")
                    val destination_latitude = destination_obj.getString("latitude")
                    val destination_longitude = destination_obj.getString("longitude")

                    val latitude_pickup_stg = pickup_obj.getString("latitude")
                    val longitude_pickup_stg = pickup_obj.getString("longitude")
                    session!!.setlyft_ride_id(request_id_stg)

                    if (status_stg.equals("processing")) {
                        Log.e("processing", "---" + "processing")
                        tv_ride_status.setText("processing Please wait...")
                        uber_dialog.show()
                        UberMap(session!!.getlyft_ride_id())
                    } else if (status_stg.equals("accepted")) {

                        stopUbertimertask()
                        uber_dialog.dismiss()
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true

                        Log.e("accepted", "---" + "accepted")
                        val driver_obj = main_ride_obj.getJSONObject("driver")
                        val vehicle_obj = main_ride_obj.getJSONObject("vehicle")
                        val driver_name = driver_obj.getString("name")
                        val phone_number = driver_obj.getString("phone_number")
                        val driver_rating = driver_obj.getString("rating")
                        val driver_picture_url = driver_obj.getString("picture_url")
                        val driver_sms_number = driver_obj.getString("sms_number")

                        val vehicle_make = vehicle_obj.getString("make")
                        val vehicle_picture_url = vehicle_obj.getString("picture_url")
                        val vehicle_model = vehicle_obj.getString("model")
                        val vehicle_license_plate = vehicle_obj.getString("license_plate")


                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")

                        Log.e("ACCEPTED_TRACKTRIDE", source_address!!.text.toString() + "-----" + Sselected_DestinationLocation)
                        //Move to ride Detail page
                        val i = Intent(activity, TrackYourRide::class.java)
                        i.putExtra("driverID", "")
                        i.putExtra("driverName", driver_name)
                        i.putExtra("driverImage", driver_picture_url)
                        i.putExtra("driverRating", driver_rating)
                        i.putExtra("driverLat", latitude_stg)
                        i.putExtra("driverLong", longitude_stg)
                        i.putExtra("driverTime", "")
                        i.putExtra("rideID", request_id_stg)
                        i.putExtra("driverMobile", phone_number)
                        i.putExtra("driverCar_no", vehicle_license_plate)
                        i.putExtra("driverCar_model", vehicle_model)
                        i.putExtra("source_Location", source_address!!.text.toString())
                        i.putExtra("userLat", latitude_pickup_stg)
                        i.putExtra("userLong", longitude_pickup_stg)
                        i.putExtra("cat_type", "Uber")
                        startActivity(i)
                        activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                        UberMap(session!!.getlyft_ride_id())

                    } else if (status_stg.equals("arriving")) {
                        stopUbertimertask()
                        //arriving
                        uber_dialog.dismiss()
                        drawer_layout!!.isEnabled = true
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        // UberMap(request_id_stg)
                        // startUberTimer()
                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                        UberMap(session!!.getlyft_ride_id())


                        val driver_obj = main_ride_obj.getJSONObject("driver")
                        val vehicle_obj = main_ride_obj.getJSONObject("vehicle")
                        val driver_name = driver_obj.getString("name")
                        val phone_number = driver_obj.getString("phone_number")
                        val driver_rating = driver_obj.getString("rating")
                        val driver_picture_url = driver_obj.getString("picture_url")
                        val driver_sms_number = driver_obj.getString("sms_number")

                        val vehicle_make = vehicle_obj.getString("make")
                        val vehicle_picture_url = vehicle_obj.getString("picture_url")
                        val vehicle_model = vehicle_obj.getString("model")
                        val vehicle_license_plate = vehicle_obj.getString("license_plate")

                        Log.e("ACCEPTED_TRACKTRIDE", source_address!!.text.toString() + "-----" + Sselected_DestinationLocation)
                        //Move to ride Detail page
                        val i = Intent(activity, TrackYourRide::class.java)
                        i.putExtra("driverID", "")
                        i.putExtra("driverName", driver_name)
                        i.putExtra("driverImage", driver_picture_url)
                        i.putExtra("driverRating", driver_rating)
                        i.putExtra("driverLat", latitude_stg)
                        i.putExtra("driverLong", longitude_stg)
                        i.putExtra("driverTime", "")
                        i.putExtra("rideID", request_id_stg)
                        i.putExtra("driverMobile", phone_number)
                        i.putExtra("driverCar_no", vehicle_license_plate)
                        i.putExtra("driverCar_model", vehicle_model)
                        i.putExtra("source_Location", source_address!!.text.toString())
                        i.putExtra("userLat", latitude_pickup_stg)
                        i.putExtra("userLong", longitude_pickup_stg)
                        i.putExtra("cat_type", "Uber")
                        startActivity(i)
                        activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)


                    } else if (status_stg.equals("in_progress")) {
                        stopUbertimertask()
                        uber_dialog.dismiss()
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true
                        //in_progress
                        UberMap(session!!.getlyft_ride_id())
                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")


                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)

                        val driver_obj = main_ride_obj.getJSONObject("driver")
                        val vehicle_obj = main_ride_obj.getJSONObject("vehicle")
                        val driver_name = driver_obj.getString("name")
                        val phone_number = driver_obj.getString("phone_number")
                        val driver_rating = driver_obj.getString("rating")
                        val driver_picture_url = driver_obj.getString("picture_url")
                        val driver_sms_number = driver_obj.getString("sms_number")

                        val vehicle_make = vehicle_obj.getString("make")
                        val vehicle_picture_url = vehicle_obj.getString("picture_url")
                        val vehicle_model = vehicle_obj.getString("model")
                        val vehicle_license_plate = vehicle_obj.getString("license_plate")

                        Log.e("ACCEPTED_TRACKTRIDE", source_address!!.text.toString() + "-----" + Sselected_DestinationLocation)
                        //Move to ride Detail page
                        val i = Intent(activity, TrackYourRide::class.java)
                        i.putExtra("driverID", "")
                        i.putExtra("driverName", driver_name)
                        i.putExtra("driverImage", driver_picture_url)
                        i.putExtra("driverRating", driver_rating)
                        i.putExtra("driverLat", latitude_stg)
                        i.putExtra("driverLong", longitude_stg)
                        i.putExtra("driverTime", "")
                        i.putExtra("rideID", request_id_stg)
                        i.putExtra("driverMobile", phone_number)
                        i.putExtra("driverCar_no", vehicle_license_plate)
                        i.putExtra("driverCar_model", vehicle_model)
                        i.putExtra("source_Location", source_address!!.text.toString())
                        i.putExtra("userLat", latitude_pickup_stg)
                        i.putExtra("userLong", longitude_pickup_stg)
                        i.putExtra("cat_type", "Uber")
                        startActivity(i)
                        activity!!.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)


                    } else if (status_stg.equals("completed")) {
                        //completed
                        UberMap(session!!.getlyft_ride_id())

                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true
                        stopUbertimertask()
                    }

                    val data_pickup = LatLng(latitude_pickup_stg.toDouble(), longitude_pickup_stg.toDouble())
                    val data_drop = LatLng(destination_latitude.toDouble(), destination_longitude.toDouble())
                    document = v2GetRouteDirection.getDocument(data_drop, data_pickup, GMapV2GetRouteDirection.MODE_DRIVING)
                    try {
                        val directionPoint = v2GetRouteDirection.getDirection(document)
                        val rectLine = PolylineOptions().width(10f).color(
                                resources.getColor(R.color.black)).geodesic(true)
                        for (i in directionPoint.indices) {
                            rectLine.add(directionPoint.get(i))
                        }
                        // Adding route on the map
                        mMap!!.addPolyline(rectLine)
                        markerOptions!!.position(data_drop)
                        markerOptions!!.position(data_pickup)
                        markerOptions!!.draggable(true)
                        mMap!!.isMyLocationEnabled = false

                        //googleMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_drop)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_pickup)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))

                        println("inside---------marker--------------")

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(data_drop)
                        builder.include(data_pickup)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300))
                        mMap!!.getMinZoomLevel();


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                    //Toast.makeText(context, "Ride Request in Current Status : " + status_stg, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_id_error", "---" + error.networkTimeMs + "---" + error.networkResponse)
                stopUbertimertask()
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }

    private fun UberMap(request_id_stg: String) {


        var latitude_pickup_stg = ""
        var longitude_pickup_stg = ""
        var latitude_stg = ""
        var longitude_stg = ""
        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_ID_stg + request_id_stg, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("UberMapRESP", "---" + response)
                val v2GetRouteDirection = GMapV2GetRouteDirection()
                var document: Document
                try {
                    val main_ride_obj = JSONObject(response)
                    if (main_ride_obj.has("product_id")) {
                        val product_id_stg = main_ride_obj.getString("product_id")
                    }
                    val request_id_stg = main_ride_obj.getString("request_id")
                    val status_stg_UBERMAP = main_ride_obj.getString("status")

                    if (main_ride_obj.has("destination")) {
                        val destination_obj = main_ride_obj.getJSONObject("destination")
                        latitude_stg = destination_obj.getString("latitude")
                        longitude_stg = destination_obj.getString("longitude")
                    }
                    if (main_ride_obj.has("pickup")) {
                        val pickup_obj = main_ride_obj.getJSONObject("pickup")

                        latitude_pickup_stg = pickup_obj.getString("latitude")
                        longitude_pickup_stg = pickup_obj.getString("longitude")
                    }



                    if (status_stg_UBERMAP.equals("processing")) {
                        Log.e("processing", "---" + "processing")
                        tv_ride_status.setText("processing Please wait...")
                        uber_dialog.show()
                    } else if (status_stg_UBERMAP.equals("accepted")) {

                        Log.e("UBERCOMPLETED", "ACCEPTED")
                        uber_dialog.dismiss()
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true

                        Log.e("accepted", "---" + "accepted")
                        val driver_obj = main_ride_obj.getJSONObject("driver")
                        val vehicle_obj = main_ride_obj.getJSONObject("vehicle")
                        val driver_name = driver_obj.getString("name")
                        val phone_number = driver_obj.getString("phone_number")
                        val driver_rating = driver_obj.getString("rating")
                        val driver_picture_url = driver_obj.getString("picture_url")
                        val driver_sms_number = driver_obj.getString("sms_number")

                        val vehicle_make = vehicle_obj.getString("make")
                        val vehicle_picture_url = vehicle_obj.getString("picture_url")
                        val vehicle_model = vehicle_obj.getString("model")
                        val vehicle_license_plate = vehicle_obj.getString("license_plate")


                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)

                    } else if (status_stg_UBERMAP.equals("arriving")) {
                        //arriving
                        Log.e("UBERCOMPLETED", "ARRIVING")
                        uber_dialog.dismiss()
                        drawer_layout!!.isEnabled = true
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                    } else if (status_stg_UBERMAP.equals("in_progress")) {

                        Log.e("UBERCOMPLETED", "INPROGRESS")

                        uber_dialog.dismiss()
                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true
                        //in_progress
                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")


                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                    } else if (status_stg_UBERMAP.equals("completed")) {
                        //completed

                        Log.e("UBERCOMPLETED", "COMPLETED")

                        book_cardview_source_address_layout!!.setVisibility(View.GONE)
                        drawer_layout!!.isEnabled = true
                        UberRecipet(request_id_stg)
                        stopUbertimertask()
                    }

                    Log.e("AFTERCOMPLETED", status_stg_UBERMAP)

                    val data_pickup = LatLng(latitude_pickup_stg.toDouble(), longitude_pickup_stg.toDouble())
                    val data_drop = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())
                    document = v2GetRouteDirection.getDocument(data_drop, data_pickup, GMapV2GetRouteDirection.MODE_DRIVING)
                    try {
                        val directionPoint = v2GetRouteDirection.getDirection(document)
                        val rectLine = PolylineOptions().width(10f).color(
                                resources.getColor(R.color.black)).geodesic(true)
                        for (i in directionPoint.indices) {
                            rectLine.add(directionPoint.get(i))
                        }
                        // Adding route on the map
                        mMap!!.addPolyline(rectLine)
                        markerOptions!!.position(data_drop)
                        markerOptions!!.position(data_pickup)
                        markerOptions!!.draggable(true)
                        mMap!!.isMyLocationEnabled = false

                        //googleMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_drop)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_pickup)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))

                        println("inside---------marker--------------")

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(data_drop)
                        builder.include(data_pickup)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300))
                        mMap!!.getMinZoomLevel();


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (timer_stop.equals("0")) {
                        timer_stop = "1"
                        startUberTimer()
                    }

                    //Toast.makeText(context, "Ride Request in Current Status : " + status_stg, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_id_error", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }

    fun startUberTimer() {
        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent) {
            uber_timer = Timer()
            //initialize the TimerTask's job
            initializeUberTimerTask()
            //schedule the timer, to wake up every 1 second
            uber_timer!!.schedule(timerTask_uber, 3000, 5000)
        }//
    }


    fun initializeUberTimerTask() {
        timerTask_uber = object : TimerTask() {
            override fun run() {
                try {
                    Log.e("TIMER", "arm")
                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet
                    if (isInternetPresent) {
                        if (!session!!.getlyft_ride_id().equals("")) {
                            lyft_ride_id = session!!.getlyft_ride_id()
                            Log.e("lyft_ride_id_lll", lyft_ride_id)
                        }
                        if (main_type_stg.equals("Uber")) {
                            UberCurrentReq()
                            Log.e("lyft_ride_id_lll", "UBER")
                            //UberMap(session!!.getlyft_ride_id())
                        } else if (main_type_stg.equals("Lyft")) {
                            Log.e("lyft_ride_id_lll", "LYFT")
                            postRequest_tripdetails_lyft(session!!.getlyft_ride_id())
                        } else {
                            UberCurrentReq()
                            //UberMap(session!!.getlyft_ride_id())
                            postRequest_tripdetails_lyft(session!!.getlyft_ride_id())
                        }
                        Log.e("tier", "timer_arm_arm")
                    }
                } catch (e: Exception) {
                    Log.e("TIMER_EXCEPTION", e.toString())
                }

            }
        }
    }

    fun stopUbertimertask() {
        //stop the timer, if it's not already null
        if (uber_timer != null) {
            uber_timer!!.cancel()
            uber_timer = null
        }
    }

    fun Ubermarker(latitude_stg: String, longitude_stg: String, bearing_stg: String) {
        mMap!!.clear()

        if (old_lat_long.equals("old")) {
            old_lat_long = ""
        } else {
            old_lat_long = "old"
        }
        var Dlatitude = 0.00
        var Dlongitude = 0.00
        if (first_time_long.equals("old")) {
            Dlatitude = java.lang.Double.parseDouble(latitude_stg)
            Dlongitude = java.lang.Double.parseDouble(longitude_stg)
            first_time_long = ""
        } else {
            Dlatitude = java.lang.Double.parseDouble(second_lat)
            Dlongitude = java.lang.Double.parseDouble(second_long)
        }

        //val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
        val marker = mMap!!.addMarker(MarkerOptions()
                .position(LatLng(Dlatitude, Dlongitude))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon))
                .anchor(0.5f, 0.5f)
                .flat(true))

        if (marker != null) {

            Log.e("ANIMATEMARKER", "ANIMATE")
            val startPosition = marker.position
            val endPosition = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())


            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 5000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction

                    val lng = v * endPosition.longitude + (1 - v) * startPosition.longitude
                    val lat = v * endPosition.latitude + (1 - v) * startPosition.latitude
                    val newPos = LatLng(lat, lng)
                    marker.setPosition(newPos)
                    marker.setAnchor(0.5f, 0.5f)
                    marker.rotation = getBearing(startPosition, newPos)
                    mMap!!.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition.Builder()
                                    .target(newPos)
                                    .bearing(bearing_stg.toFloat())
                                    .zoom(15.5f)
                                    .build()))

                    val line = mMap!!.addPolyline(PolylineOptions().add(startPosition, newPos)
                            .width(10f).color(resources.getColor(R.color.spinner_arrow_color)))

                    if (old_lat_long.equals("old")) {
                        second_lat = latitude_stg
                        second_long = longitude_stg
                    } else {
                        second_lat = latitude_stg
                        second_long = longitude_stg
                    }

                } catch (ex: Exception) {
                    //I don't care atm..
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                }
            })
            valueAnimator.start()
        }

    }

    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return -1f
    }


    //Uber Reciept

    private fun UberRecipet(request_id_stg: String) {
        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_ID_stg + request_id_stg + "/receipt", object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("UBERRECEIPTRES", "---" + response)
                try {
                    val recipet_obj = JSONObject(response)
                    val subtotal_stg = recipet_obj.getString("subtotal")
                    val total_charged_stg = recipet_obj.getString("total_charged")
                    val total_owed_stg = recipet_obj.getString("total_owed")
                    val total_fare_stg = recipet_obj.getString("total_fare")
                    val currency_code_stg = recipet_obj.getString("currency_code")
                    val duration_stg = recipet_obj.getString("duration")
                    val distance_stg = recipet_obj.getString("distance")
                    val request_id = recipet_obj.getString("request_id")
                    val distance_label_stg = recipet_obj.getString("distance_label")


                    uber_receipt_dialog.show()

                    fare_breakup_total_amount_textview.setText(total_fare_stg)
                    //fare_breakup_discount_amount_uber.setText()
                    fare_breakup_duration_uber.setText(duration_stg)
                    fare_breakup_uber_distance.setText(distance_stg + " " + distance_label_stg)
                    fare_breakup_subtotal_uber.setText(currency_code_stg + " " + subtotal_stg)


                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_id_error_rec", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //headers.put("Authorization", "Bearer " + access_token_stg_final)
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }


    //-------------------Delete Ride Post Request----------------

    private fun DeleteRideRequest(Url: String) {

        dialog = Dialog(activity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val loading = dialog.findViewById(R.id.custom_loading_textview) as TextView
        loading.text = resources.getString(R.string.action_pleasewait)

        println("--------------Delete Ride url-------------------$Url")


        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = riderId

        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Delete Ride reponse-------------------$response")

                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {
                        val status = `object`.getString("status")
                        val response_value = `object`.getString("response")
                        if (status.equals("1", ignoreCase = true)) {
                            riderId = ""
                            Alert(activity!!.getResources().getString(R.string.action_success), response_value)
                        } else {
                            Alert(activity!!.getResources().getString(R.string.alert_label_title), response_value)
                        }


                        //---------Hiding the bottom layout after cancel request--------
                        val animFadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out)



                        lv_rydetypes!!.setVisibility(View.GONE)
                        // listview_subcategory.setVisibility(View.VISIBLE);
                        // rideLater_textview.setText(getResources().getString(R.string.home_label_ride_later));

                        currentLocation_image!!.setClickable(true)
                        currentLocation_image!!.setVisibility(View.GONE)
                        drawer_layout!!.setEnabled(true)

                        NavigationDrawer.enableSwipeDrawer()


                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    private fun UberRideEstimateRequest() {

        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_PROD_stg + "?latitude=" + SselectedLatitude + "&longitude=" + SselectedLongitude, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("req_prod_id_rec", "---" + response)

                cat_images.clear()
                try {

                    val main_obj = JSONObject(response)
                    val main_array = main_obj.getJSONArray("products")
                    for (k in 0 until main_array.length()) {
                        val prod_obj = main_array.getJSONObject(k)

                        val product_id_stg = prod_obj.getString("product_id")
                        val image_stg = prod_obj.getString("image")
                        val product_group_stg = prod_obj.getString("product_group")

                        val price_details_obj = prod_obj.getJSONObject("price_details")

                        val pojo = HomePojo()
                        pojo.setCat_name(product_group_stg)
                        pojo.setCat_id(product_id_stg)
                        pojo.setCat_image(image_stg)
                        pojo.setMain_type("Uber")

                        cat_images.add(pojo)

                    }

                    //Toast.makeText(context, "Ride products : " + response, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {

                }
                UberEstimateTime()


            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_id_error", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)


    }

    //***********************LYFT API CALLS*******************************//
    ///lyft Process//

    /**Method for generating access token for Lyft api,
     * use access_token as authorization key for all api calls */
    private fun postRequest_auth_lyft(currentLocation: LatLng) {

        val client_id: String = Iconstant.Lyft_client_id
        val client_secret: String = Iconstant.Lyft_client_secret_key
        //body
        val jsonBody = JSONObject()
        jsonBody.put("grant_type", "client_credentials")
        /* "public" = ride eta, estimates, ride types.
         * "profile" = user profile info.
         * "rides.read" =  user's ride details present, past.
         * "rides.request" = requesting a ride.
         **/
        jsonBody.put("scope", "public")

        val requestBody = jsonBody.toString()

        lyft_ClientRequest = object : StringRequest(Request.Method.POST, Iconstant.LyftAuthUrl,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft auth Response---$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            token_type = jObject.get("token_type").toString()
                            public_access_token = jObject.get("access_token").toString()
                            println("lyft auth token---$token_type----$public_access_token")

                            postRequest_ride_type_lyft(currentLocation)    // ride types service

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft auth error-----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val client_credentials: String = client_id + ":" + client_secret
                //basic authorization
                val auth = ("Basic " + android.util.Base64.encodeToString(client_credentials.toByteArray(), android.util.Base64.NO_WRAP))
                headers.put("Authorization", auth)
                headers.put("Content-Type", "application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (requestBody == null) null else requestBody.toByteArray()
            }

        }
        lyft_ClientRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_ClientRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_ClientRequest)

    }

    /* method for getting ride types api calls*/
    private fun postRequest_ride_type_lyft(currentLocation: LatLng) {

        val start_lat = currentLocation.latitude
        val start_lng = currentLocation.longitude
        val req_url: String = Iconstant.LyftBaseUrl + "v1/ridetypes?lat=" + start_lat + "&lng=" + start_lng

        lyft_RideTypesRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft type Response----$response")
                        val jObject: JSONObject = JSONObject(response)
                        val jsonArray = jObject.getJSONArray("ride_types")
                        var ride_Types_Pojo: Lyft_Ride_Types_Pojo
                        ride_type_list_lyft.clear()

                        for (i in 0 until jsonArray.length()) {
                            val pojo = HomePojo()
                            val jobject = jsonArray.getJSONObject(i)
                            val display_name = jobject.getString("display_name")
                            val category_key = jobject.getString("category_key")
                            val image_url = jobject.getString("image_url")
                            val pricing_details = jobject.getJSONObject("pricing_details")  //inner object

                            val service_fee_description = pricing_details.getString("service_fee_description")
                            val base_charge = pricing_details.getString("base_charge")
                            val cost_per_mile = pricing_details.getString("cost_per_mile")
                            val cancel_penalty_amount = pricing_details.getString("cancel_penalty_amount")
                            val cost_per_minute = pricing_details.getString("cost_per_minute")
                            val cost_minimum = pricing_details.getString("cost_minimum")
                            val trust_and_service = pricing_details.getString("trust_and_service")

                            val seats = jobject.getString("seats")
                            val ride_type = jobject.getString("ride_type")
                            ride_Types_Pojo = Lyft_Ride_Types_Pojo(display_name, category_key, image_url, service_fee_description, base_charge, cost_per_mile, cancel_penalty_amount, cost_per_minute, cost_minimum, trust_and_service, seats, ride_type)
                            ride_type_list_lyft.add(ride_Types_Pojo)


                        }

                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft type error----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                    // Toast.makeText(activity, "No drivers available!", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // oAuth 2.0 authorization
                headers.put("Authorization", token_type!! + " " + public_access_token!!)
                return headers
            }


        }
        lyft_RideTypesRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_RideTypesRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_RideTypesRequest)
    }


    /* method for getting ride estimate api calls*/
    private fun postRequest_ride_estimate_lyft(currentLocation: LatLng, endLocation: LatLng) {

        val start_lat = currentLocation.latitude
        val start_lng = currentLocation.longitude
        val end_lat = endLocation.latitude
        val end_lng = endLocation.longitude

        val req_url: String = Iconstant.LyftBaseUrl + "v1/cost?start_lat=" + start_lat + "&start_lng=" + start_lng + "&end_lat=" + end_lat + "&end_lng=" + end_lng
        Log.e("REQUIREDLYFTLOCATION", req_url)

        lyft_RideEstimateRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft estimate Response------$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            val jsonArray = jObject.getJSONArray("cost_estimates")
                            var estimate_Pojo: Lyft_Ride_Estimate_Pojo

                            //category_list.clear()
                            estimate_list_lyft.clear()
                            for (i in 0 until jsonArray.length()) {

                                val jobject = jsonArray.getJSONObject(i)
                                val ride_type = jobject.getString("ride_type")
                                val display_name = jobject.getString("display_name")
                                val primetime_percentage = jobject.getString("primetime_percentage")
                                val primetime_confirmation_token = jobject.getString("primetime_confirmation_token")
                                val cost_token = jobject.getString("cost_token")
                                val price_quote_id = jobject.getString("price_quote_id")
                                val price_group_id = jobject.getString("price_group_id")
                                val is_valid_estimate = jobject.getString("is_valid_estimate")
                                val estimated_duration_seconds = jobject.getString("estimated_duration_seconds")
                                val estimated_distance_miles = jobject.getString("estimated_distance_miles")
                                val estimated_cost_cents_min = jobject.getString("estimated_cost_cents_min")
                                val estimated_cost_cents_max = jobject.getString("estimated_cost_cents_max")
                                val can_request_ride = jobject.getString("can_request_ride")

                                estimate_Pojo = Lyft_Ride_Estimate_Pojo(ride_type, display_name, primetime_percentage, primetime_confirmation_token, cost_token, price_quote_id, price_group_id, is_valid_estimate, estimated_duration_seconds, estimated_distance_miles, estimated_cost_cents_min, estimated_cost_cents_max, can_request_ride)
                                estimate_list_lyft.add(estimate_Pojo)

                            }

                            if (estimate_list_lyft.size > 0) {
                                for (j in 0 until estimate_list_lyft.size) {
                                    val pojo = HomePojo()
                                    pojo.setCat_image(ride_type_list_lyft[j].image_url)
                                    pojo.setCat_name(estimate_list_lyft[j].display_name)
                                    pojo.setCat_id(estimate_list_lyft[j].ride_type)
                                    pojo.setMin_amount("$" + (java.lang.Double.valueOf(estimate_list_lyft[j].estimated_cost_cents_min) / 100).toString())
                                    pojo.setMax_amount((java.lang.Double.valueOf(estimate_list_lyft[j].estimated_cost_cents_max) / 100).toString())
                                    pojo.setMain_type("Lyft")
                                    category_list.add(pojo)
                                }

                            } else {
                                Toast.makeText(activity, "not avaialble!", Toast.LENGTH_SHORT).show()
                            }


                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft estimate error---$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", token_type!! + " " + public_access_token!!)
                return headers
            }


        }
        lyft_RideEstimateRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_RideEstimateRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_RideEstimateRequest)
    }


    /* method for getting ride estimate api calls*/
    private fun ride_estimate_lyft_single(sselectedLatitude: String, sselectedLongitude: String, dselectedLatitude: String, dselectedLongitude: String, cat_type: String) {

        val start_lat = sselectedLatitude
        val start_lng = sselectedLongitude
        val end_lat = dselectedLatitude
        val end_lng = dselectedLongitude

        val req_url: String = Iconstant.LyftBaseUrl + "v1/cost?start_lat=" + start_lat + "&start_lng=" + start_lng + "&end_lat=" + end_lat + "&end_lng=" + end_lng + "&ride_type=" + cat_type

        lyft_RideEstimateRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft estimate single Response------$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            val jsonArray = jObject.getJSONArray("cost_estimates")
                            var estimate_Pojo: Lyft_Ride_Estimate_Pojo

                            //category_list.clear()
                            estimate_list_lyft.clear()
                            for (i in 0 until jsonArray.length()) {

                                val jobject = jsonArray.getJSONObject(i)
                                val ride_type = jobject.getString("ride_type")
                                val display_name = jobject.getString("display_name")
                                val primetime_percentage = jobject.getString("primetime_percentage")
                                val primetime_confirmation_token = jobject.getString("primetime_confirmation_token")
                                val cost_token = jobject.getString("cost_token")
                                val price_quote_id = jobject.getString("price_quote_id")
                                val price_group_id = jobject.getString("price_group_id")
                                val is_valid_estimate = jobject.getString("is_valid_estimate")
                                val estimated_duration_seconds = jobject.getString("estimated_duration_seconds")
                                val estimated_distance_miles = jobject.getString("estimated_distance_miles")
                                val estimated_cost_cents_min = jobject.getString("estimated_cost_cents_min")
                                val estimated_cost_cents_max = jobject.getString("estimated_cost_cents_max")
                                val can_request_ride = jobject.getString("can_request_ride")

                                fare_id_stg = ride_type

                                tv_selected_cab_estimate_time!!.setText(estimated_duration_seconds.toString() + " sec " + " to closest ride")
                                tv_selected_cab__fare!!.setText("US$" + estimated_cost_cents_max)
                            }



                            rl_continue_booking!!.visibility = View.VISIBLE
                            book_cardview_source_address_layout!!.visibility = View.GONE

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft estimate error---$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", token_type + " " + public_access_token)
                return headers
            }


        }
        lyft_RideEstimateRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_RideEstimateRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_RideEstimateRequest)
    }


    private fun postRequest_nearby_drivers_eta_lyft(currentLocation: LatLng) {
        val start_lat = currentLocation.latitude
        val start_lng = currentLocation.longitude
        //  val ride_type = "lyft"

        val req_url: String = Iconstant.LyftBaseUrl + "v1/nearby-drivers-pickup-etas?lat=" + start_lat + "&lng=" + start_lng /*+ "&ride_type=" + ride_type*/

        lyft_NearbyDriversRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft drivers Response------$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            val jResponse = jObject.getJSONArray("nearby_drivers_pickup_etas")
                            val drivers = ArrayList<LatLng>()
                            var nearby_Drivers_Pojo: Lyft_Nearby_Drivers_Pojo

                            driver_list.clear()
                            nearby_drivers_list_lyft.clear()
                            for (i in 0 until jResponse.length()) {
                                val jobject = jResponse.getJSONObject(i)

                                val rangeObj = jobject.getJSONObject("pickup_duration_range")
                                val duration_ms = rangeObj.getString("duration_ms")
                                val range_ms = rangeObj.getString("range_ms")
                                val unrounded_range_ms = rangeObj.getString("unrounded_range_ms")
                                val unrounded_duration_ms = rangeObj.getString("unrounded_duration_ms")

                                val display_name = jobject.getString("display_name")

                                val ride_type = jobject.getString("ride_type")

                                val nearby_drivers = jobject.getJSONArray("nearby_drivers")
                                for (k in 0 until nearby_drivers.length()) {
                                    val driverObj = nearby_drivers.getJSONObject(k)

                                    val locArray = driverObj.getJSONArray("locations")
                                    if (locArray.length() > 0) {
                                        drivers.clear()
                                        for (l in 0 until locArray.length()) {
                                            val latlngObj = locArray.getJSONObject(0)

                                            // TODO add only one LatLng from locations object

                                            val pojo = HomePojo()
                                            val lat = latlngObj.getString("lat")
                                            val lng = latlngObj.getString("lng")

                                            pojo.setDriver_lat(lat)
                                            pojo.setDriver_long(lng)

                                            drivers.add(LatLng(lat.toDouble(), lng.toDouble()))
                                            driver_list.add(pojo)
                                        }
                                        nearby_Drivers_Pojo = Lyft_Nearby_Drivers_Pojo(ride_type, drivers)
                                        nearby_drivers_list_lyft.add(nearby_Drivers_Pojo)
                                    } else {
                                        //  if drivers are not available
                                        Toast.makeText(activity, "No vehicles available!", Toast.LENGTH_LONG).show()
                                    }
                                }

                                val pickup_time_range = jobject.getJSONObject("pickup_time_range")
                                val pickup_range_ms = pickup_time_range.getString("range_ms")
                                val pickup_timestamp_ms = pickup_time_range.getString("timestamp_ms")

                            }

                            if (driver_list.size > 0) {
                                for (j in driver_list.indices) {
                                    val Dlatitude = java.lang.Double.parseDouble(driver_list[j].getDriver_lat())
                                    val Dlongitude = java.lang.Double.parseDouble(driver_list[j].getDriver_long())

                                    val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
                                    val height = 80
                                    val width = 45
                                    val bitmapdraw = resources.getDrawable(R.mipmap.car_icon) as BitmapDrawable
                                    val b = bitmapdraw.bitmap
                                    val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                                    marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                                    marker.rotation(100f)
                                    // adding marker
                                    mMap!!.addMarker(marker)
                                }
                            } else {
                                mMap!!.clear()
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft drivers error---$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", token_type!! + " " + public_access_token!!)
                return headers
            }


        }
        lyft_NearbyDriversRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_NearbyDriversRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_NearbyDriversRequest)
    }


    /* method for getting access_token after user allows permission*/
    private fun postRequest_retrieve_access_lyft(authorization_code: String) {

        val client_id: String = Iconstant.Lyft_client_id
        val client_secret: String = Iconstant.Lyft_client_secret_key
        //body
        val jsonBody = JSONObject()
        jsonBody.put("grant_type", "authorization_code")
        jsonBody.put("code", authorization_code)

        val requestBody = jsonBody.toString()

        lyft_UserAuthRequest = object : StringRequest(Request.Method.POST, Iconstant.LyftAuthUrl,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft user auth Response---$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            user_token_type = jObject.getString("token_type")
                            user_access_token = jObject.getString("access_token")
                            user_key_expires_in = jObject.getString("expires_in")
                            user_refresh_token = jObject.getString("refresh_token")
                            val user_scope = jObject.getString("scope")

                            session!!.userAccessToken(user_access_token!!)
                            session!!.refreshToken(user_refresh_token!!)

                            //  postRequest_userprofile_lyft(user_token_type!!, user_access_token!!)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft user auth error-----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val client_credentials: String = client_id + ":" + client_secret
                //basic authorization
                val auth = ("Basic " + android.util.Base64.encodeToString(client_credentials.toByteArray(), android.util.Base64.NO_WRAP))
                headers.put("Authorization", auth)
                headers.put("Content-Type", "application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (requestBody == null) null else requestBody.toByteArray()
            }

        }
        lyft_UserAuthRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_UserAuthRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_UserAuthRequest)
    }

    /* new access key request */
    private fun postRequest_refresh_access_token_lyft(refresh_token: String) {

        val refresh_token1 = refresh_token
        val client_id: String = Iconstant.Lyft_client_id
        val client_secret: String = Iconstant.Lyft_client_secret_key

        //body
        val jsonBody = JSONObject()
        jsonBody.put("grant_type", "refresh_token")
        jsonBody.put("refresh_token", refresh_token1)

        val requestBody = jsonBody.toString()

        lyft_RefreshTokenRequest = object : StringRequest(Request.Method.POST, Iconstant.LyftAuthUrl,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft user auth refresh Response---$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            user_token_type = jObject.getString("token_type")
                            user_access_token = jObject.getString("access_token")
                            user_key_expires_in = jObject.getString("expires_in")  //3600
                            val user_scope = jObject.getString("scope")

                            session!!.userAccessToken(user_access_token!!)

                            if (!user_key_expires_in.equals("")) {
                                val countDownTimer = object : CountDownTimer((user_key_expires_in!!.toLong() * 1000), 1) {
                                    override fun onTick(millisUntilFinished: Long) {
                                        // Log.i("timer: ticks: ", millisUntilFinished.toString())
                                    }

                                    override fun onFinish() {
                                        //Log.i("timer: ", "finished")
                                        postRequest_refresh_access_token_lyft(refresh_token1)
                                    }
                                }
                                countDownTimer.start()
                            }


                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft user auth refresh error-----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val client_credentials: String = client_id + ":" + client_secret
                //basic authorization
                val auth = ("Basic " + android.util.Base64.encodeToString(client_credentials.toByteArray(), android.util.Base64.NO_WRAP))
                headers.put("Authorization", auth)
                headers.put("Content-Type", "application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (requestBody == null) null else requestBody.toByteArray()
            }

        }
        lyft_RefreshTokenRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_RefreshTokenRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_RefreshTokenRequest)
    }


    /* method for getting ride types api calls*/
    private fun postRequest_ride_req_lyft(sselectedLatitude: String, sselectedLongitude: String, dselectedLatitude: String, dselectedLongitude: String, fare_id_stg: String) {

        val start_lat = sselectedLatitude
        val start_lng = sselectedLongitude
        val end_lat = dselectedLatitude
        val end_lng = dselectedLongitude

        val originLatLngObject = JSONObject() // origin latlng
        val destLatLngObject = JSONObject() // destination latlng
        val jsonBody = JSONObject() // body

        originLatLngObject.put("lat", start_lat)
        originLatLngObject.put("lng", start_lng)
        destLatLngObject.put("lat", end_lat)
        destLatLngObject.put("lng", end_lng)

        jsonBody.put("ride_type", fare_id_stg) // ride type
        jsonBody.put("origin", originLatLngObject)
        jsonBody.put("destination", destLatLngObject)

        val requestBody = jsonBody.toString()

        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides"
        // val req_url: String = ""
        Log.e("main_lyft_ride", "ride------")

        lyft_RideRequest = object : StringRequest(Request.Method.POST, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft ride Response----$response")

                        try {
                            val jobj = JSONObject(response)
                            val status = jobj.getString("status")
                            lyft_ride_id = jobj.getString("ride_id")
                            val _ride_type = jobj.getString("ride_type")

                            /* val passenger = jobj.getJSONObject("passenger")
                             val rating = passenger.getString("rating")
                             val first_name = passenger.getString("first_name")
                             val last_name = passenger.getString("last_name")
                             val image_url = passenger.getString("image_url")
                             val user_id = passenger.getString("user_id")

                             val destination = jobj.getJSONObject("destination")
                             val destination_lat = destination.getString("lat")
                             val destination_lng = destination.getString("lng")
                             val destination_address = destination.getString("address")

                             val origin = jobj.getJSONObject("origin")
                             val origin_lat = origin.getString("lat")
                             val origin_lng = origin.getString("lng")
                             val origin_address = origin.getString("address")*/


                            session!!.setlyft_ride_id(lyft_ride_id)

                            if (status.equals("pending")) {
                                tv_ride_status.setText("processing Please wait...")
                                uber_dialog.show()
                                postRequest_tripdetails_lyft(session!!.getlyft_ride_id())
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }


                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft ride error----$error")


                try {

                    val err_response = error.networkResponse
                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        //val resp = err_reponse.data.toString()
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")
                        /* val primetime_percentage = response.getString("primetime_percentage")
                         val primetime_multiplier = response.getString("primetime_multiplier")
                         val cost_token = response.getString("cost_token")
                         val token_duration = response.getString("token_duration")
                         Log.e("error response:", "$error_description ----- $error_code ----- $primetime_multiplier ----- " +
                                 " ----- $cost_token ----- $token_duration")*/

                        println("-----lyft_ride_error_stg----$error_description----$error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    } else {

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // oAuth 2.0 authorization
                headers.put("Authorization", token_type + " " + session!!.getUserAccessToken())
                // headers.put("Content-Type","application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {

                return if (requestBody == null) null else requestBody.toByteArray()
            }

            @Throws(AuthFailureError::class)
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }

        lyft_RideRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_RideRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_RideRequest)
    }

    /* method for getting trip details api, should run every 10 seconds, limit - 1000 calls per min */
    private fun postRequest_tripdetails_lyft(ride_id: String) {
        val v2GetRouteDirection = GMapV2GetRouteDirection()
        var document: Document

        var latitude_pickup_stg = ""
        var longitude_pickup_stg = ""
        var latitude_stg = ""
        var longitude_stg = ""
        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides/" + ride_id

        lyft_TripDetailsRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft trip Response----$response")

                        // status: pending....accepted....arrived....pickedUp....droppedOff

                        if (timer_stop.equals("0")) {
                            timer_stop = "1"
                            //startUberTimer()
                        }

                        val jObject = JSONObject(response)
                        //val _id = jObject.getString("ride_id")
                        val status = jObject.getString("status")
                        // val ride_type = jObject.getString("ride_type")

                        if (status.equals("pending")) {

                            val origin = jObject.getJSONObject("origin")
                            latitude_pickup_stg = origin.getString("lat")
                            longitude_pickup_stg = origin.getString("lng")
                            //val origin_address = origin.getString("address")

                            val destination = jObject.getJSONObject("destination")
                            latitude_stg = destination.getString("lat")
                            longitude_stg = destination.getString("lng")
                            // val destination_address = destination.getString("address")

                            /*val passenger = jObject.getJSONObject("passenger")
                            val passenger_user_id = passenger.getString("user_id")
                            val passenger_first_name = passenger.getString("first_name")
                            val passenger_last_name = passenger.getString("last_name")
                            val passenger_image_url = passenger.getString("image_url")
                            val passenger_rating = passenger.getString("rating")*/

                        } else {
                            if (status.equals("accepted")) {

                                val origin = jObject.getJSONObject("origin")
                                latitude_pickup_stg = origin.getString("lat")
                                longitude_pickup_stg = origin.getString("lng")
                                //  val origin_address = origin.getString("address")

                                val destination = jObject.getJSONObject("destination")
                                latitude_stg = destination.getString("lat")
                                longitude_stg = destination.getString("lng")
                                /* val destination_address = destination.getString("address")
                                 val origin_eta_seconds = origin.getString("eta_seconds")   // Available after ride status changes to accepted and until arrived

                                 val destination_lat = destination.getString("lat")
                                 val destination_lng = destination.getString("lng")
                                 val destination_eta_seconds = destination.getString("eta_seconds") */ // Available after ride status changes to pickedUp and until droppedOff

                                val driver = jObject.getJSONObject("driver")
                                //val driver_user_id = driver.getString("user_id")
                                val driver_first_name = driver.getString("first_name")
                                /*val driver_image_url = driver.getString("image_url")
                                val driver_rating = driver.getString("rating")
                                val driver_phone_number = driver.getString("phone_number")*/

                                val vehicle = jObject.getJSONObject("vehicle")
                                //val vehicle_make = vehicle.getString("make")
                                val vehicle_model = vehicle.getString("model")
                                /*val vehicle_year = vehicle.getString("year")
                                val vehicle_license_plate = vehicle.getString("license_plate")
                                val vehicle_license_plate_state = vehicle.getString("license_plate_state")
                                val vehicle_color = vehicle.getString("color")
                                val vehicle_image_url = vehicle.getString("image_url")
*/
                                val location = jObject.getJSONObject("location")
                                val location_lat = location.getString("lat")
                                val location_lng = location.getString("lng")
                                val location_bearing = location.getString("bearing")

                                val can_cancel = jObject.getJSONArray("can_cancel")

                                Log.e("lyft_details", "" + vehicle + "----" + vehicle_model + "----" + driver_first_name)
                                Ubermarker(location_lat, location_lng, location_bearing)

                            } else if (status.equals("arrived")) {


                                val origin = jObject.getJSONObject("origin")
                                /*val origin_lat = origin.getString("lat")
                                val origin_lng = origin.getString("lng")
                                val origin_address = origin.getString("address")
                                val origin_eta_seconds = origin.getString("eta_seconds")  */ // Available after ride status changes to accepted and until arrived

                                latitude_pickup_stg = origin.getString("lat")
                                longitude_pickup_stg = origin.getString("lng")
                                val destination = jObject.getJSONObject("destination")
                                latitude_stg = destination.getString("lat")
                                longitude_stg = destination.getString("lng")

                                /* val destination_lat = destination.getString("lat")
                                 val destination_lng = destination.getString("lng")
                                 val destination_address = destination.getString("address")
                                 val destination_eta_seconds = destination.getString("eta_seconds") */ // Available after ride status changes to pickedUp and until droppedOff

                                val location = jObject.getJSONObject("location")
                                val location_lat = location.getString("lat")
                                val location_lng = location.getString("lng")
                                val location_bearing = location.getString("bearing")

                                Ubermarker(location_lat, location_lng, location_bearing)

                            } else if (status.equals("pickedUp")) {
                                val origin = jObject.getJSONObject("origin")
                                latitude_pickup_stg = origin.getString("lat")
                                longitude_pickup_stg = origin.getString("lng")
                                val destination = jObject.getJSONObject("destination")
                                latitude_stg = destination.getString("lat")
                                longitude_stg = destination.getString("lng")


                                /* val cancellation_price = jObject.getJSONObject("cancellation_price")
                                 val cancellation_amount = cancellation_price.getString("amount")
                                 val cancellation_currency = cancellation_price.getString("currency")
                                 val cancellation_token = cancellation_price.getString("token")
                                 val cancellation_token_duration = cancellation_price.getString("token_duration")

                                 val pickup = jObject.getJSONObject("pickup")
                                 val pickup_lat = pickup.getString("lat")
                                 val pickup_lng = pickup.getString("lng")
                                 val pickup_address = pickup.getString("address")*/

                                val location = jObject.getJSONObject("location")
                                val location_lat = location.getString("lat")
                                val location_lng = location.getString("lng")
                                val location_bearing = location.getString("bearing")
                                Ubermarker(location_lat, location_lng, location_bearing)
                            } else if (status.equals("droppedOff")) {
                                val origin = jObject.getJSONObject("origin")
                                latitude_pickup_stg = origin.getString("lat")
                                longitude_pickup_stg = origin.getString("lng")
                                val destination = jObject.getJSONObject("destination")
                                latitude_stg = destination.getString("lat")
                                longitude_stg = destination.getString("lng")

                                /* val line_items = jObject.getJSONArray("line_items") // Available after ride status changes to droppedOff and the passenger has rated and paid for the ride, plus some processing time.
                                 for (i in 0 until line_items.length()) {
                                     val obj = line_items.getJSONObject(i)
                                     val amount = obj.getString("amount")
                                     val currency = obj.getString("currency")
                                     val type = obj.getString("type")
                                 }

                                 val primetime_percentage = jObject.getJSONObject("primetime_percentage")
                                 val distance_miles = jObject.getJSONObject("distance_miles")    // Available after ride status changes to droppedOff
                                 val duration_seconds = jObject.getJSONObject("duration_seconds")    // Available after ride status changes to droppedOff

                                 val price = jObject.getJSONObject("price")  // Available after ride status changes to droppedOff and the passenger has rated and paid for the ride, plus some processing time
                                 val price_amount = price.getString("amount")
                                 val price_currency = price.getString("currency")
                                 val price_description = price.getString("description")

                                 val charges = jObject.getJSONArray("charges")
                                 for (j in 0 until charges.length()) {
                                     val obJ = charges.getJSONObject(j)
                                     val charges_amount = obJ.getString("amount")
                                     val charges_currency = obJ.getString("currency")
                                     val payment_method = obJ.getString("payment_method")
                                 }

                                 val rating = jObject.getString("rating")
                                 val feedback = jObject.getString("feedback")*/
                                stopUbertimertask()
                            } else if (status.equals("canceled")) {

                                val origin = jObject.getJSONObject("origin")
                                latitude_pickup_stg = origin.getString("lat")
                                longitude_pickup_stg = origin.getString("lng")
                                val destination = jObject.getJSONObject("destination")
                                latitude_stg = destination.getString("lat")
                                longitude_stg = destination.getString("lng")

                                /*  val origin_lat = origin.getString("lat")
                                  val origin_lng = origin.getString("lng")
                                  val origin_address = origin.getString("address")
                                  val origin_eta_seconds = origin.getString("eta_seconds")   // Available after ride status changes to accepted and until arrived

                                  val destination_lat = destination.getString("lat")
                                  val destination_lng = destination.getString("lng")
                                  val destination_address = destination.getString("address")
                                  val destination_eta_seconds = destination.getString("eta_seconds")  // Available after ride status changes to pickedUp and until droppedOff

                                  val driver = jObject.getJSONObject("driver")
                                  val driver_user_id = driver.getString("user_id")
                                  val driver_first_name = driver.getString("first_name")
                                  val driver_image_url = driver.getString("image_url")
                                  val driver_rating = driver.getString("rating")
                                  val driver_phone_number = driver.getString("phone_number")

                                  val vehicle = jObject.getJSONObject("vehicle")
                                  val vehicle_make = vehicle.getString("make")
                                  val vehicle_model = vehicle.getString("model")
                                  val vehicle_year = vehicle.getString("year")
                                  val vehicle_license_plate = vehicle.getString("license_plate")
                                  val vehicle_license_plate_state = vehicle.getString("license_plate_state")
                                  val vehicle_color = vehicle.getString("color")
                                  val vehicle_image_url = vehicle.getString("image_url")

                                  val line_items = jObject.getJSONArray("line_items") // Available after ride status changes to droppedOff and the passenger has rated and paid for the ride, plus some processing time.
                                  for (i in 0 until line_items.length()) {
                                      val obj = line_items.getJSONObject(i)
                                      val amount = obj.getString("amount")
                                      val currency = obj.getString("currency")
                                      val type = obj.getString("type")
                                  }

                                  val primetime_percentage = jObject.getJSONObject("primetime_percentage")
                                  val distance_miles = jObject.getJSONObject("distance_miles")    // Available after ride status changes to droppedOff
                                  val duration_seconds = jObject.getJSONObject("duration_seconds")    // Available after ride status changes to droppedOff

                                  val price = jObject.getJSONObject("price")  // Available after ride status changes to droppedOff and the passenger has rated and paid for the ride, plus some processing time
                                  val price_amount = price.getString("amount")
                                  val price_currency = price.getString("currency")
                                  val price_description = price.getString("description")

                                  val charges = jObject.getJSONArray("charges")
                                  for (j in 0 until charges.length()) {
                                      val obJ = charges.getJSONObject(j)
                                      val charges_amount = obJ.getString("amount")
                                      val charges_currency = obJ.getString("currency")
                                      val payment_method = obJ.getString("payment_method")
                                  }*/

                                /* val passenger = jObject.getJSONObject("passenger")
                                 val passenger_user_id = passenger.getString("user_id")
                                 val passenger_first_name = passenger.getString("first_name")
                                 val passenger_last_name = passenger.getString("last_name")
                                 val passenger_image_url = passenger.getString("image_url")
                                 val passenger_rating = passenger.getString("rating")

                                 val canceled_by = jObject.getString("canceled_by")
                                 val route_url = jObject.getString("route_url")*/
                                stopUbertimertask()
                            }

                            /*val driver = jObject.getJSONObject("driver")
                            val driver_user_id = driver.getString("user_id")
                            val driver_first_name = driver.getString("first_name")
                            val driver_image_url = driver.getString("image_url")
                            val driver_rating = driver.getString("rating")
                            val driver_phone_number = driver.getString("phone_number")

                            val vehicle = jObject.getJSONObject("vehicle")
                            val vehicle_make = vehicle.getString("make")
                            val vehicle_model = vehicle.getString("model")
                            val vehicle_year = vehicle.getString("year")
                            val vehicle_license_plate = vehicle.getString("license_plate")
                            val vehicle_license_plate_state = vehicle.getString("license_plate_state")
                            val vehicle_color = vehicle.getString("color")
                            val vehicle_image_url = vehicle.getString("image_url")*/

                            /*track_your_ride_driver_carmodel.text = vehicle_make
                            track_your_ride_driver_carNo.text = vehicle_license_plate
                            track_your_ride_driver_name.text = driver_first_name
                            track_your_ride_driver_rating.text = driver_rating + "*"

                            Picasso.with(context)
                                    .load(vehicle_image_url)
                                    .into(track_your_ride_carimage)

                            Picasso.with(context)
                                    .load(driver_image_url)
                                    .into(track_your_ride_driverimage)

                            ll_driver_details_id.visibility = View.VISIBLE*/

                        }


                        /* Ubermarker(latitude_stg, longitude_stg, "")*/
                        val data_pickup = LatLng(latitude_pickup_stg.toDouble(), longitude_pickup_stg.toDouble())
                        val data_drop = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())
                        document = v2GetRouteDirection.getDocument(data_drop, data_pickup, GMapV2GetRouteDirection.MODE_DRIVING)
                        try {
                            val directionPoint = v2GetRouteDirection.getDirection(document)
                            val rectLine = PolylineOptions().width(10f).color(
                                    resources.getColor(R.color.colorAccent)).geodesic(true)
                            for (i in directionPoint.indices) {
                                rectLine.add(directionPoint.get(i))
                            }
                            // Adding route on the map
                            mMap!!.addPolyline(rectLine)
                            markerOptions!!.position(data_drop)
                            markerOptions!!.position(data_pickup)
                            markerOptions!!.draggable(true)
                            mMap!!.isMyLocationEnabled = false
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft trip error----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // OAuth 2.0 authorization
                headers.put("Authorization", token_type + " " + user_access_token)
                return headers
            }

        }
        lyft_TripDetailsRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_TripDetailsRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_TripDetailsRequest)
    }

    /* method for getting trip receipt api calls*/
    private fun postRequest_tripreceipt_lyft(ride_id: String) {

        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides/" + ride_id + "/receipt"

        lyft_TripReceiptRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft receipt Response----$response")
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            val _id = jObject.getString("ride_id")
                            val ride_profile = jObject.getString("ride_profile")
                            val requested_at = jObject.getString("requested_at")

                            val price = jObject.getJSONObject("price")
                            val total_amount = price.getString("amount")
                            val total_currency = price.getString("currency")
                            val description = price.getString("description")

                            val line_items = jObject.getJSONArray("line_items")
                            for (i in 0 until line_items.length()) {
                                val obj = line_items.getJSONObject(i)
                                val amount = obj.getString("amount")
                                val currency = obj.getString("currency")
                                val type = obj.getString("type")
                            }

                            val charges = jObject.getJSONArray("charges")
                            for (j in 0 until charges.length()) {
                                val obJ = charges.getJSONObject(j)
                                val charges_amount = obJ.getString("amount")
                                val charges_currency = obJ.getString("currency")
                                val payment_method = obJ.getString("payment_method")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft receipt error----$error")

                try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("error response:", "$error_description ----- $error_code")
                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // OAuth 2.0 authorization
                headers.put("Authorization", token_type + " " + session!!.getUserAccessToken())
                return headers
            }

        }
        lyft_TripReceiptRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_TripReceiptRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_TripReceiptRequest)
    }


    /* method for cancelling trip api calls*/
    private fun postRequest_cancel_lyft(ride_id: String, cancel_token: String) {

        var error_id = ""
        var amount = ""
        var currency = ""
        var token = ""
        var token_duration = ""
        var requestBody = ""
        val jsonBody = JSONObject() // body

        if (!cancel_token.equals("")) {

            jsonBody.put("cancel_confirmation_token", cancel_token) // cancellation token

        }
        requestBody = jsonBody.toString()

        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides/" + ride_id + "/cancel"

        lyft_TripCancelRequest = object : StringRequest(Request.Method.POST, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft cancel Response----$response")
                        try {


                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                println("-----lyft cancel error----$error")

                val err_response = error.networkResponse

                if (err_response.data != null && err_response.statusCode != null) {
                    val resp = String(err_response.data, Charset.forName("utf-8"))
                    val response = JSONObject(resp)

                    error_id = response.getString("error")
                    val error_detail = response.getJSONArray("error_detail")
                    for (i in 0 until error_detail.length()) {
                        val detail = error_detail.getJSONObject(i)
                        val cancel_confirmation = detail.getString("cancel_confirmation")
                    }
                    amount = response.getString("amount")   // cancellation charges
                    currency = response.getString("currency")
                    token = response.getString("token") // token to be sent again for cancelllation
                    token_duration = response.getString("token_duration")   // 60 sec token expiration
                }

                if (error_id.equals("cancel_confirmation_required")) {
                    if (!amount.equals("") || amount != null) {
                        val mDialog = PkDialog(activity!!)
                        mDialog.setDialogTitle("Charges")
                        mDialog.setDialogMessage("Cancellation charges $amount")
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            postRequest_cancel_lyft(session!!.getlyft_ride_id(), token)
                        })
                        mDialog.setNegativeButton(resources.getString(R.string.action_cancel), View.OnClickListener {
                            mDialog.dismiss()
                        })
                        mDialog.show()
                    }
                }
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // OAuth 2.0 authorization
                headers["Authorization"] = token_type + " " + session!!.getUserAccessToken()
                return headers
            }


            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return if (requestBody == null) null else requestBody.toByteArray()
            }

        }
        lyft_TripCancelRequest!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_TripCancelRequest!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_TripCancelRequest)
    }


    fun uber_rides_ridehub() {

        // uber_products_ridehub_API()
        Log.e("SELSECTEDLATLNG", "---" + SselectedLatitude + "----" + SselectedLongitude + "----" + DselectedLatitude + "----" + DselectedLongitude)

        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_ESTI_PRICE_stg + "?start_latitude=" + SselectedLatitude + "&start_longitude=" + SselectedLongitude + "&end_latitude=" + DselectedLatitude + "&end_longitude=" + DselectedLongitude, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("req_prod_id_rec", "---" + response)

                try {

                    val main_obj = JSONObject(response)
                    val main_array = main_obj.getJSONArray("prices")
                    for (k in 0 until main_array.length()) {
                        val prod_obj = main_array.getJSONObject(k)

                        val localized_display_name_stg = prod_obj.getString("localized_display_name")
                        val distance_stg = prod_obj.getString("distance")
                        val display_name_stg = prod_obj.getString("display_name")
                        val product_id_stg = prod_obj.getString("product_id")
                        val estimate_stg = prod_obj.getString("estimate")
                        val high_estimate_stg = prod_obj.getString("high_estimate")
                        val low_estimate_stg = prod_obj.getString("low_estimate")

                        val pojo = HomePojo()
                        pojo.setCat_name(localized_display_name_stg)
                        pojo.setCat_id(product_id_stg)
                        pojo.setMin_amount(low_estimate_stg)
                        pojo.setMax_amount(high_estimate_stg)
                        pojo.setMain_type("Uber")

                        cat_images.add(pojo)

                    }

                } catch (e: Exception) {

                }
                UberEstimateTime()


            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("estimation_id_error_price", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // headers.put("Authorization", "Bearer " + access_token_stg_final)
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }


}
