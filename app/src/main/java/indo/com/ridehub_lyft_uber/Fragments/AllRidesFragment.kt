package indo.com.ridehub_lyft_uber.Fragments

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ListView
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.AppController
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.volley.VolleyErrorResponse
import indo.com.ridehub_lyft_uber.Activities.MyRidesDetail
import indo.com.ridehub_lyft_uber.Adapters.MyRidesAdapter
import indo.com.ridehub_lyft_uber.PojoResponse.MyRidesPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class AllRidesFragment : Fragment() {
    lateinit var fragmentName: Fragment
    internal lateinit var dialog: Dialog
    var uri: Uri? = null
    internal lateinit var my_loader: Dialog
    lateinit var my_rides_listview: ListView
    internal var paylink: String? = ""
    internal lateinit var sessionManager: SessionManager

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null

    private var UserID = ""

    public var itemlist_all: ArrayList<MyRidesPojo> = ArrayList<MyRidesPojo>()


    private var isRideAvailable = false

    internal lateinit var adapter: MyRidesAdapter
    lateinit var uber_Req_current: StringRequest
    lateinit var lyft_Req_History: StringRequest

    val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
    val stf = SimpleDateFormat("HH:mm a", Locale.getDefault())

    val type_lyft = "LYFT"
    val type_ridehub = "RIDEHUB"
    val type_uber = "UBER"
    private var UserAccessToken = ""

    companion object {
        val TAG: String = AllRidesFragment::class.java.simpleName
        fun newInstance() = AllRidesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.myridesallfragment, container, false)
        sessionManager = SessionManager(this.activity!!)

        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet

        val user = sessionManager!!.getUserDetails()
        UserID = user[SessionManager!!.KEY_USERID].toString()
        if (!sessionManager.getUserAccessToken().equals("")) {
            UserAccessToken = sessionManager.getUserAccessToken()
        }

        my_rides_listview = rootView.findViewById<ListView>(R.id.my_rides_listview)

        if (isInternetPresent!!) {
            //Uberhistory()
            postRequest_MyRides(Iconstant.myrides_url)
        } else {
            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
        }



        my_rides_listview!!.setOnItemClickListener { parent, view, position, id ->

            val intent = Intent(context, MyRidesDetail::class.java)
            intent.putExtra("RideID", itemlist_all[position].getRide_id())
            intent.putExtra("RideType", itemlist_all[position].getType())
            activity!!.startActivity(intent)
            //overridePendingTransition(R.anim.enter, R.anim.exit)

        }

        return rootView
    }


    //-----------------------MyRides Post Request-----------------
    private fun postRequest_MyRides(Url: String) {
        dialog = Dialog(activity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)


        println("-------------MyRides Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["type"] = "all"

        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                dialog.dismiss()
                println("-------------MyRides Response----------------$response")

                var Sstatus = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val check_rides_object = response_object.get("rides")
                            if (check_rides_object is JSONArray) {
                                val ride_array = response_object.getJSONArray("rides")
                                if (ride_array.length() > 0) {
                                    //itemlist_all.clear()

                                    for (i in 0 until ride_array.length()) {
                                        val ride_object = ride_array.getJSONObject(i)

                                        val pojo = MyRidesPojo()
                                        pojo.setRide_id(ride_object.getString("ride_id"))
                                        pojo.setRide_time(ride_object.getString("ride_time"))
                                        if (ride_object.getString("ride_date").contains("/")){
                                            val replaceDate = ride_object.getString("ride_date").toString().replace("/","-")
                                            pojo.setRide_date(replaceDate)
                                        }else{
                                            pojo.setRide_date(ride_object.getString("ride_date"))
                                        }
                                        pojo.setPickup(ride_object.getString("pickup"))
                                        pojo.setRide_status(ride_object.getString("ride_status"))
                                        pojo.setGroup(ride_object.getString("group"))
                                        pojo.setDatetime(ride_object.getString("datetime"))
                                        pojo.setType(type_ridehub)

                                        itemlist_all.add(pojo)

                                       /* if (ride_object.getString("group").equals("upcoming", ignoreCase = true)) {
                                            itemlist_upcoming.add(pojo)
                                        } else if (ride_object.getString("group").equals("completed", ignoreCase = true)) {
                                            itemlist_completed.add(pojo)
                                        } else if (ride_object.getString("group").equals("all", ignoreCase = true) && ride_object.getString("ride_status").equals("Cancelled", ignoreCase = true)) {
                                            itemlist_cancelled.add(pojo)
                                        }*/

                                    }
                                    isRideAvailable = true
                                } else {
                                    LyftHistory()
                                    isRideAvailable = false
                                }
                            } else {
                                isRideAvailable = false
                            }
                        }

                    }

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        if (isRideAvailable) {

                            Collections.sort(itemlist_all, MyRidesPojo().dateWiseComparator)
                            for (str in itemlist_all)
                            {
                                System.out.println("----MYRIDESDATESRIDEHUB----" + str)
                            }
                            LyftHistory()

                            adapter = MyRidesAdapter(activity!!, itemlist_all)
                            my_rides_listview!!.adapter = adapter
                        } else {
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                    dialog.dismiss()
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                    dialog.dismiss()
                }

            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }



    /***********uber history ********/
    private fun Uberhistory() {

        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_HISTORY_stg, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("uberhistory_all_resp", "---" + response)
                itemlist_all.clear()

                try {

                    val jObject: JSONObject = JSONObject(response)
                    /* val count = jObject.getString("count")
                     val limit = jObject.getString("limit")
                     val offset = jObject.getString("offset")*/


                    val history = jObject.getJSONArray("history")
                    if (history.length() > 0) {
                        for (i in 0 until history.length()) {
                            val pojo = MyRidesPojo()
                            val jobject: JSONObject = history.getJSONObject(i)
                            val status = jobject.getString("status")
                            // val distance = jobject.getString("distance")
                            // val product_id = jobject.getString("product_id")
                            val start_time = jobject.getString("start_time")
                            //  val end_time = jobject.getString("end_time")
                            val request_id = jobject.getString("request_id")
                            //  val request_time = jobject.getString("request_time")

                            val start_city = jobject.getJSONObject("start_city")
                             val latitude = start_city.getString("latitude")
                            val display_name = start_city.getString("display_name")
                              val longitude = start_city.getString("longitude")

                            val date = Date( java.lang.Long.parseLong(start_time)  * 1000L)
                            val ride_date = sdf.format(date)
                            val ride_time = stf.format(date)

                            Log.v("date_uber", ride_date + "----" + ride_time  )
                            pojo.setRide_id(request_id)
                            pojo.setRide_time(ride_time)
                            pojo.setRide_date(ride_date)
                            pojo.setPickup(display_name)
                            pojo.setRide_status(status)
                            pojo.setGroup("")
                            pojo.setDatetime("$ride_date $ride_time")
                            pojo.setType(type_uber)
                            pojo.setSource_lat(latitude)
                            pojo.setSource_lng(longitude)
                            itemlist_all.add(pojo)

                        }

                        Collections.sort(itemlist_all, MyRidesPojo().dateWiseComparator)

                        for (str in itemlist_all)
                                {
                                    System.out.println("----MYRIDESDATESUBER----" + str)
                                }
                        postRequest_MyRides(Iconstant.myrides_url)
                    }else{
                        postRequest_MyRides(Iconstant.myrides_url)
                    }


                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                postRequest_MyRides(Iconstant.myrides_url)
                VolleyErrorResponse.volleyError(activity, error)
                Log.e("uber_history_error_all", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = Iconstant.API_UBER_AUTH_KEY_stg
                return headers
            }

        }
        uber_Req_current.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }


    /*********** Lyft history ********/
    private fun LyftHistory() {

        var pickup_time = ""
        var ride_date = ""
        var ride_time = ""
        val start_time = "2019-01-01T00:00:00" // select start date

        val limit = 20 // default = 10
        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides?start_time=" + start_time + "&limit=" + limit

        lyft_Req_History = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    override fun onResponse(response: String) {
                        println("----lyft history Response_all----$response")
                        try {
                            var distance_miles = ""
                            var duration_seconds = ""
                            var rating = ""
                            val jObject: JSONObject = JSONObject(response)
                            val history = jObject.getJSONArray("ride_history")
                            if (history.length() > 0) {
                                for (i in 0 until history.length()) {
                                    val pojo = MyRidesPojo()
                                    val obj = history.getJSONObject(i)
                                    val ride_id = obj.getString("ride_id")
                                    val status = obj.getString("status")
                                    // val ride_type = obj.getString("ride_type")

                                    val origin = obj.getJSONObject("origin")
                                     val origin_lat = origin.getString("lat")
                                     val origin_lng = origin.getString("lng")
                                     /* val origin_address = origin.getString("address")*/

                                    if (!status.equals("canceled")) {
                                        if (obj.has("pickup")) {
                                            val pickup = obj.getJSONObject("pickup")
                                            /*val pickup_lat = pickup.getString("lat")
                                            val pickup_lng = pickup.getString("lng")
                                            val pickup_address = pickup.getString("address")*/
                                            pickup_time = pickup.getString("time")
                                        }
                                        if (obj.has("dropoff")) {
                                            val dropoff = obj.getJSONObject("dropoff")
                                            /*val dropoff_lat = dropoff.getString("lat")
                                            val dropoff_lng = dropoff.getString("lng")
                                           val  dropoff_address = dropoff.getString("address")
                                            val dropoff_time = dropoff.getString("time")*/
                                        }
                                        if (obj.has("distance_miles")) {
                                            distance_miles = obj.getString("distance_miles")
                                        }
                                        if (obj.has("duration_seconds")) {
                                             duration_seconds = obj.getString("duration_seconds")
                                        }

                                        ride_date = sdf.format(pickup_time)
                                        ride_time = stf.format(pickup_time)
                                    }


                                    /* val destination = obj.getJSONObject("destination")
                                     val destination_lat = destination.getString("lat")
                                     val destination_lng = destination.getString("lng")
                                     val destination_address = destination.getString("address")


                                     val passenger = obj.getJSONObject("passenger")
                                     val passenger_user_id = passenger.getString("user_id")
                                     val passenger_first_name = passenger.getString("first_name")
                                     val passenger_last_name = passenger.getString("last_name")
                                     val passenger_image_url = passenger.getString("image_url")
                                     val passenger_rating = passenger.getString("rating")

                                     val driver = obj.getJSONObject("driver")
                                     val driver_first_name = driver.getString("first_name")
                                     val driver_image_url = driver.getString("image_url")
                                     val driver_rating = driver.getString("rating")

                                     val vehicle = obj.getJSONObject("vehicle")
                                     val make = vehicle.getString("make")
                                     val model = vehicle.getString("model")
                                     val year = vehicle.getString("year")
                                     val license_plate = vehicle.getString("license_plate")
                                     val license_plate_state = vehicle.getString("license_plate_state")
                                     val color = vehicle.getString("color")
                                     val image_url = vehicle.getString("image_url")

                                     if (obj.has("primetime_percentage")) {
                                         val primetime_percentage = obj.getString("primetime_percentage")
                                     }
                                     if (obj.has("rating")) {
                                         val rating = obj.getString("rating")
                                     }
                                     if (obj.has("feedback")) {
                                         val feedback = obj.getString("feedback")
                                     }

                                     val price = obj.getJSONObject("price")
                                     val price_amount = price.getString("amount")
                                     val price_currency = price.getString("currency")
                                     val price_description = price.getString("description")

                                     val line_items = obj.getJSONArray("line_items")
                                     for (j in 0 until line_items.length()) {
                                         val obJ = line_items.getJSONObject(j)
                                         val line_items_amount = obJ.getString("amount")
                                         val line_items_currency = obJ.getString("currency")
                                         val line_items_type = obJ.getString("type")
                                     }

                                     val requested_at = obj.getString("requested_at")
                                     val ride_profile = obj.getString("ride_profile")
                                     val pricing_details_url = obj.getString("pricing_details_url")
                                     val canceled_by = obj.getString("canceled_by")*/

                                    if (obj.has("rating")) {
                                         rating = obj.getString("rating")
                                    }

                                    Log.e("date_lyft_all", "$ride_date ---- $ride_time")
                                    pojo.setRide_id(ride_id)
                                    pojo.setRide_time(ride_time)
                                    pojo.setRide_date(ride_date)
                                    pojo.setPickup("lyft")
                                    pojo.setRide_status(status)
                                    pojo.setGroup("")
                                    pojo.setType(type_lyft)
                                    pojo.setDatetime("$ride_date $ride_time")
                                    pojo.setDistance(distance_miles)
                                    pojo.setDuration(duration_seconds)
                                    pojo.setSource_lat(origin_lat)
                                    pojo.setSource_lng(origin_lng)

                                    itemlist_all.add(pojo)
                                }
                            }

                            if (isRideAvailable) {
                                Collections.sort(itemlist_all, MyRidesPojo().dateWiseComparator)    // re arranging arraylist based on date
                                /*for (str in itemlist_all)
                                {
                                    System.out.println("----test----" + str)
                                }*/
                                adapter = MyRidesAdapter(activity!!, itemlist_all)
                                my_rides_listview!!.adapter = adapter
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(activity, error)

                Log.e("lyft_history_error_all", "---" + error.networkTimeMs + "---" + error.networkResponse)
                /*try {
                    val err_response = error.networkResponse

                    if (err_response.statusCode != null && err_response.data != null) {
                        val resp = String(err_response.data, Charset.forName("utf-8"))
                        val response = JSONObject(resp)
                        val error_description = response.getString("error_description")
                        val error_code = response.getString("error")

                        Log.e("err resp history_all", "$error_description ----- $error_code")
//                        Toast.makeText(activity, error_description, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }*/
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                // OAuth 2.0 authorization
                headers.put("Authorization", "Bearer $UserAccessToken")
                return headers
            }

        }
        lyft_Req_History.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_Req_History.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_Req_History)
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


}