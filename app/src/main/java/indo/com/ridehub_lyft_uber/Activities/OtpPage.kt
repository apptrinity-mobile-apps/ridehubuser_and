package indo.com.ridehub_lyft_uber.Activities

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.*
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*


class OtpPage : ActivityHockeyApp() {
    private var context: Context? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null

    private var back: RelativeLayout? = null
    private var Eotp: EditText? = null
    private var send: Button? = null
    private var tv_otp_message: CustomTextView? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog

    private var Susername = ""
    private var Semail = ""
    private var Spassword = ""
    private var Sphone = ""
    private var ScountryCode = ""
    private var SreferalCode = ""
    private var SgcmId = ""
    private var Sotp_Status = ""
    private var Sotp = ""

    private val PERMISSION_REQUEST_ID = 100;
    private val TAG = "OtpPage"
    private var mSmsBroadcastReceiver: SmsBroadcastReceiver? = null;
    private val BROADCAST_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private var intentFilter: IntentFilter? = null;

    //----------------------Code for TextWatcher-------------------------
    private val EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Eotp!!.text.length > 0) {
                Eotp!!.error = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_page)



        mSmsBroadcastReceiver = SmsBroadcastReceiver()
        intentFilter = IntentFilter()
        intentFilter!!.addAction(BROADCAST_ACTION)
        requestRuntimePermissions(
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.SEND_SMS
        )


        context = getApplicationContext()
        initialize()

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        send!!.setOnClickListener {
            if (Eotp!!.text.toString().length == 0) {
                erroredit(Eotp!!, getResources().getString(R.string.otp_label_alert_otp))
            } else if (Sotp != Eotp!!.text.toString()) {
                erroredit(Eotp!!, getResources().getString(R.string.otp_label_alert_invalid))
            } else {
                cd = ConnectionDetector(this@OtpPage)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    PostRequest(Iconstant.register_otp_url)
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            }
        }


        Eotp!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(Eotp!!.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
            false
        }
    }

    private fun initialize() {
        session = SessionManager(this@OtpPage)
        cd = ConnectionDetector(this@OtpPage)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.otp_header_back_layout) as RelativeLayout
        Eotp = findViewById(R.id.otp_password_editText) as EditText
        send = findViewById(R.id.otp_submit_button) as Button
        tv_otp_message = findViewById(R.id.tv_otp_message) as CustomTextView

        Eotp!!.addTextChangedListener(EditorWatcher)

        val intent = getIntent()
        Susername = intent.getStringExtra("UserName")
        Semail = intent.getStringExtra("Email")
        Spassword = intent.getStringExtra("Password")
        Sphone = intent.getStringExtra("Phone")
        ScountryCode = intent.getStringExtra("CountryCode")
        SreferalCode = intent.getStringExtra("ReferalCode")
        SgcmId = intent.getStringExtra("GcmID")
        Sotp_Status = intent.getStringExtra("Otp_Status")
        Sotp = intent.getStringExtra("Otp")

        tv_otp_message!!.setText(Sotp)

        if (Sotp_Status.equals("development", ignoreCase = true)) {
            Eotp!!.setText(Sotp)
        } else {
            Eotp!!.setText("")
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@OtpPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@OtpPage, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {
        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@OtpPage.finish()
            this@OtpPage.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }


    // -------------------------code for Login Post Request----------------------------------

    private fun PostRequest(Url: String) {

        dialog = Dialog(this@OtpPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_otp))

        println("--------------Otp url-------------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_name"] = Susername
        jsonParams["email"] = Semail
        jsonParams["password"] = Spassword
        jsonParams["phone_number"] = Sphone
        jsonParams["country_code"] = ScountryCode
        jsonParams["referal_code"] = SreferalCode
        jsonParams["gcm_id"] = SgcmId

        mRequest = ServiceRequest(this@OtpPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {

                println("--------------Otp reponse-------------------$response")


                var Sstatus = ""
                var Smessage = ""
                var Suser_image = ""
                var Suser_id = ""
                var Suser_name = ""
                var Semail = ""
                var Scountry_code = ""
                var SphoneNo = ""
                var Sreferal_code = ""
                var Scategory = ""
                var Subcategory = ""
                var SsecretKey = ""
                var SwalletAmount = ""
                var ScurrencyCode = ""
                var sCurrencySymbol = ""

                var gcmId = ""

                try {

                    val `object` = JSONObject(response)

                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("message")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Suser_image = `object`.getString("user_image")
                        Suser_id = `object`.getString("user_id")
                        Suser_name = `object`.getString("user_name")
                        Semail = `object`.getString("email")
                        Scountry_code = `object`.getString("country_code")
                        SphoneNo = `object`.getString("phone_number")
                        Sreferal_code = `object`.getString("referal_code")
                        Scategory = `object`.getString("category")
                        Subcategory = `object`.getString("subcategory")
                        SsecretKey = `object`.getString("sec_key")
                        SwalletAmount = `object`.getString("wallet_amount")
                        ScurrencyCode = `object`.getString("currency")
                        gcmId = `object`.getString("key")

                        //is_alive_other = object.getString("is_alive_other");

                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (Sstatus.equals("1", ignoreCase = true)) {
                    SingUpAndSignIn.activty.finish()
                    println("--------SsecretKey-----------$SsecretKey")
                    session!!.createLoginSession(Semail, Suser_id, Suser_name, Suser_image, Scountry_code, SphoneNo, Sreferal_code, Scategory, Subcategory, gcmId)
                    session!!.createWalletAmount(sCurrencySymbol + SwalletAmount)
                    session!!.setXmppKey(Suser_id, SsecretKey)

                    //starting XMPP service
                    ChatService.startUserAction(this@OtpPage)

                    val intent = Intent(context, SingUpAndSignIn::class.java)
                    startActivity(intent)
                    finish()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {

                    val mDialog = PkDialog(this@OtpPage)
                    mDialog.setDialogTitle(getResources().getString(R.string.action_error))
                    mDialog.setDialogMessage(Smessage)
                    mDialog.setCancelOnTouchOutside(false)
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), OnClickListener { mDialog.dismiss() })
                    mDialog.show()
                }

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(Eotp!!.windowToken, 0)

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    private fun requestRuntimePermissions(vararg permissions: String) {
        for (perm in permissions) {

            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, arrayOf(perm), PERMISSION_REQUEST_ID)

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ID) {

            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted
                mSmsBroadcastReceiver!!.bindListener(object : SmsListener {
                    override fun messageReceived(sender: String, messageText: String) {
                        Log.e("Text", messageText)
                        Toast.makeText(this@OtpPage, "Message: $messageText", Toast.LENGTH_LONG).show()
                        val mystring = messageText
                        val arr = mystring.split(" ".toRegex(), 2).toTypedArray()

                        val firstWord = arr[0]   //the
                        val theRest = arr[1]
                        Eotp!!.setText(firstWord)


                    }
                })

            } else {
                Log.e(TAG, "Permission not granted")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "Registered receiver")
        registerReceiver(mSmsBroadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        Log.e(TAG, "Unregistered receiver")
        unregisterReceiver(mSmsBroadcastReceiver)

    }


}
