package indo.com.ridehub_lyft_uber.Activities

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.CurrencySymbolConverter
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class InviteAndEarn : ActivityHockeyApp(), View.OnClickListener {
    override fun onClick(v: View?) {

    }

    internal val PERMISSION_REQUEST_CODE = 111
    internal lateinit var Rl_whatsApp: LinearLayout
    internal lateinit var Rl_messenger: LinearLayout
    internal lateinit var Rl_sms: LinearLayout
    internal lateinit var Rl_email: LinearLayout
    internal lateinit var Rl_twitter: LinearLayout
    internal lateinit var Rl_facebook: LinearLayout
    internal lateinit var ll_shareinvitecode: CustomTextView
    internal var sCurrencySymbol = ""
    internal lateinit var tv_invite_now: CustomTextView
    internal lateinit var tv_shareride: CustomTextView
    private var back: RelativeLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var Tv_friends_earn: TextView? = null
    private var Tv_you_earn: TextView? = null
    private var Tv_referral_code: TextView? = null
    //private RelativeLayout Rl_whatsApp, Rl_messenger, Rl_sms, Rl_email, Rl_twitter, Rl_facebook;
    private var mRequest: ServiceRequest? = null
    private var UserID = ""
    private var isdataPresent = false
    private var Sstatus = ""
    private var friend_earn_amount = ""
    private var you_earn_amount = ""
    private var friends_rides = ""
    private var ScurrencyCode = ""
    private var referral_code = ""
    private var sShareLink = ""
    private val package_name = "com.indobytes.app.ridehubuser"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.freeride_ride_hub);
        val config = getResources().getConfiguration()
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.freeride_ride_hub)
        } else {
            setContentView(R.layout.freeride_ride_hub)
        }
        initialize()

        //Start XMPP Chat Service
        ChatService.startUserAction(this@InviteAndEarn)

        back!!.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }


    }

    private fun initialize() {
        session = SessionManager(this@InviteAndEarn)
        cd = ConnectionDetector(this@InviteAndEarn)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.invite_earn_header_back_layout) as RelativeLayout
        Tv_friends_earn = findViewById(R.id.invite_earn_friend_earn_textview) as TextView
        Tv_you_earn = findViewById(R.id.invite_earn_you_earn_textview) as TextView
        Tv_referral_code = findViewById(R.id.invite_earn_referral_code_textview) as TextView
       // tv_invite_now = findViewById(R.id.tv_invite_now) as CustomTextView
        ll_shareinvitecode = findViewById(R.id.ll_shareinvitecode) as CustomTextView
        tv_shareride = findViewById(R.id.tv_shareride) as CustomTextView


        val relative = SpannableString("Share the Ride Hub love and give friends free rides to try Ride, Worth up to $10 each!")
        Log.e("length_relative", "" + relative.length)
        relative.setSpan(RelativeSizeSpan(2f), relative.length - 9, relative.length - 6, 0)
        relative.setSpan(ForegroundColorSpan(getResources().getColor(R.color.border_color)), relative.length - 9, relative.length - 6, 0)// set color
        /*relative.setSpan(new RelativeSizeSpan(0.5f), relative.length() - 14, relative.length() - 7,
                0);*/
        tv_shareride.setText(relative)



// get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        if (isInternetPresent!!) {
            displayInvite_Request(Iconstant.invite_earn_friends_url)
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
        }


        ll_shareinvitecode.setOnClickListener(this)

        ll_shareinvitecode.setOnClickListener {
            val text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy! " + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
            whatsApp_sendMsg(text)
        }



    }

   /* override fun onClick(v: View) {
        if (isdataPresent) {
            val text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
            if (v === tv_invite_now) {
                whatsApp_sendMsg(text)
            } else if (v === Rl_messenger) {
                messenger_sendMsg(text)
            } else if (v === Rl_sms) {
                sms_sendMsg(text)
            } else if (v === Rl_email) {
                sendEmail(text)
            } else if (v === Rl_twitter) {
                val twitter_text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
                var imageUri: Uri? = null
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car_new), null, null))
                } catch (e: NullPointerException) {
                }

                shareTwitter(twitter_text, imageUri)
            } else if (v === Rl_facebook) {
                val facebook_text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
                var imageUri: Uri? = null
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car_new), null, null))
                } catch (e: NullPointerException) {
                }

                if (sShareLink.length > 0) {
                    shareFacebookLink(sShareLink)
                } else {
                    shareFacebook(facebook_text, imageUri)
                }

            }
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_problem_server))
        }
    }*/

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@InviteAndEarn)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------Sending message on WhatsApp Method------
    private fun whatsApp_sendMsg(text: String) {
        val pm = this@InviteAndEarn.getPackageManager()
        /* try {
           Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            //waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_whatsApp_not_installed));
        }*/
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, text)
            sendIntent.type = "text/plain"
            Intent.createChooser(sendIntent, "Share via")
            startActivity(sendIntent)
        } catch (e: Exception) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_whatsApp_not_installed))
        }


    }

    //--------Sending message on Facebook Messenger Method------
    private fun messenger_sendMsg(text: String) {
        val pm = this@InviteAndEarn.getPackageManager()
        try {
            val waIntent = Intent(Intent.ACTION_SEND)
            waIntent.type = "text/plain"
            val info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA)  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.facebook.orca")
            waIntent.putExtra(Intent.EXTRA_TEXT, text)
            startActivity(Intent.createChooser(waIntent, "Share with"))
        } catch (e: PackageManager.NameNotFoundException) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_messenger_not_installed))
        }

    }

    //--------Sending message on SMS Method------
    private fun sms_sendMsg(text: String) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkSmsPermission()) {
                requestPermission()
            } else {
                val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this@InviteAndEarn) //Need to change the build to API 19
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, text)
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName)
                }
                startActivity(sendIntent)
            }
        } else {
            val sendIntent = Intent(Intent.ACTION_VIEW)
            sendIntent.putExtra("sms_body", text)
            sendIntent.type = "vnd.android-dir/mms-sms"
            startActivity(sendIntent)
        }
    }


    //----------Sending message on Email Method--------
    protected fun sendEmail(text: String) {
        val TO = arrayOf("")
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "message/rfc822"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Rideys app Invitation")
        emailIntent.putExtra(Intent.EXTRA_TEXT, text)
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed))
        }

    }


    //----------Share Image and Text on Twitter Method--------
    protected fun shareTwitter(text: String, image: Uri?) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, text)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_STREAM, image)
        intent.type = "image/jpeg"
        intent.setPackage("com.twitter.android")

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_twitter_not_installed))
        }

    }

    //----------Share Link on Method--------
    private fun shareFacebookLink(link: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, link)
        val pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.orca")
        val pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana")
        val pacakage3 = getPackageManager().getLaunchIntentForPackage("com.example.facebook")
        val pacakage4 = getPackageManager().getLaunchIntentForPackage("com.facebook.android")
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca")
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana")
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook")
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android")
        } else {
            intent.setPackage("com.facebook.orca")
        }

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed))
        }

    }

    //----------Share Image and Text on Facebook Method--------
    protected fun shareFacebook(text: String, image: Uri?) {

        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        //intent.setType("text/plain");
        intent.type = "image/*"
        //intent.putExtra(Intent.EXTRA_TEXT, "http://project.dectar.com/fortaxi/rider/signup?ref=QVJPQ0tJQQ==");
        intent.putExtra(Intent.EXTRA_STREAM, image)
        val pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.orca")
        val pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana")
        val pacakage3 = getPackageManager().getLaunchIntentForPackage("com.example.facebook")
        val pacakage4 = getPackageManager().getLaunchIntentForPackage("com.facebook.android")
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca")
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana")
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook")
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android")
        } else {
            intent.setPackage("com.facebook.orca")
        }

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed))
        }

    }


    //-----------------------Display Invite Amount Post Request-----------------
    private fun displayInvite_Request(Url: String) {
        val dialog = Dialog(this@InviteAndEarn)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(this@InviteAndEarn.getResources().getString(R.string.action_pleasewait))

        println("-------------displayInvite_Request Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID

        mRequest = ServiceRequest(this@InviteAndEarn)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                println("-------------displayInvite_Request Response----------------$response")

                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (`object`.length() > 0) {
                        val response_Object = `object`.getJSONObject("response")
                        if (response_Object.length() > 0) {
                            val detail_object = response_Object.getJSONObject("details")
                            if (detail_object.length() > 0) {
                                friend_earn_amount = detail_object.getString("friends_earn_amount")
                                you_earn_amount = detail_object.getString("your_earn_amount")
                                friends_rides = detail_object.getString("your_earn")
                                referral_code = detail_object.getString("referral_code")
                                ScurrencyCode = detail_object.getString("currency")
                                sShareLink = detail_object.getString("url")

                                isdataPresent = true
                            } else {
                                isdataPresent = false
                            }
                        } else {
                            isdataPresent = false
                        }
                    } else {
                        isdataPresent = false
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (isdataPresent) {
                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)

                    val earn_text = "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_earn) + "</font>" + "<font color=#ef5e27>" + " " + sCurrencySymbol + "" + you_earn_amount + "</font>"

                    Tv_friends_earn!!.text = Html.fromHtml(earn_text)
                    // Tv_friends_earn.setText(getResources().getString(R.string.invite_earn_label_friends_earn) + " " + sCurrencySymbol + "" + friend_earn_amount);
                    val code_text = "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_ride) + ", " + getResources().getString(R.string.invite_earn_label_friend_ride) + "</font>" + "<font color=#ef5e27>" + sCurrencySymbol + "" + you_earn_amount + "</font>"
                    Tv_you_earn!!.text = Html.fromHtml(code_text)
                    //Tv_you_earn.setText(getResources().getString(R.string.invite_earn_label_friends_ride) + "," + getResources().getString(R.string.invite_earn_label_friend_ride) + " " + sCurrencySymbol + "" + you_earn_amount);
                    Tv_referral_code!!.text = referral_code
                }
                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    private fun checkSmsPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                val text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " " + getResources().getString(R.string.invite_earn_label_share_messgae3)

                var defaultSmsPackageName: String? = null //Need to change the build to API 19
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this)
                }

                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, text)
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName)
                }
                startActivity(sendIntent)
            } else {
            }
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    override  fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }

}
