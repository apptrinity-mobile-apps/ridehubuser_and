package indo.com.ridehub_lyft_uber.Activities

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.wang.avi.AVLoadingIndicatorView
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.gps.GPSTracker
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class UpdateUserLocation : Activity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    internal lateinit var session: SessionManager
    internal lateinit var gps: GPSTracker

    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var mGoogleApiClient: GoogleApiClient
    internal lateinit var result: PendingResult<LocationSettingsResult>
    private val avLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var userID = ""
    private var sLatitude = ""
    private var sLongitude = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var mRequest: ServiceRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.update_user_location)
        initialize()

        Handler().postDelayed({ setLocation() }, 2000)

    }

    private fun initialize() {
        cd = ConnectionDetector(this@UpdateUserLocation)
        session = SessionManager(applicationContext)
        gps = GPSTracker(applicationContext)

        mGoogleApiClient = GoogleApiClient.Builder(this@UpdateUserLocation)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        mGoogleApiClient.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        // enableGpsService();
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }


    private fun setLocation() {
        cd = ConnectionDetector(this@UpdateUserLocation)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            if (gps.isgpsenabled() && gps.canGetLocation()) {

                val user = session.getUserDetails()
                userID = user[SessionManager.KEY_USERID]!!
                sLatitude = (gps.getLatitude().toString())
                sLongitude = (gps.getLongitude().toString())

                postRequest_SetUserLocation(Iconstant.setUserLocation)

            } else {
                enableGpsService()
            }
        } else {

            val mDialog = PkDialog(this@UpdateUserLocation)
            mDialog.setDialogTitle(resources.getString(R.string.alert_nointernet))
            mDialog.setDialogMessage(resources.getString(R.string.alert_nointernet_message))
            mDialog.setPositiveButton(resources.getString(R.string.timer_label_alert_retry), View.OnClickListener {
                mDialog.dismiss()
                setLocation()
            })
            mDialog.show()

        }
    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@UpdateUserLocation, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {

                    Handler().postDelayed({
                        session = SessionManager(applicationContext)
                        gps = GPSTracker(this@UpdateUserLocation)

                        val user = session.getUserDetails()
                        userID = user[SessionManager.KEY_USERID]!!
                        sLatitude = (gps.getLatitude().toString())
                        sLongitude = (gps.getLongitude().toString())

                        postRequest_SetUserLocation(Iconstant.setUserLocation)
                    }, 2000)
                }
                Activity.RESULT_CANCELED -> {
                    enableGpsService()
                }
                else -> {
                }
            }
        }
    }


    //-----------------------User Current Location Post Request-----------------
    private fun postRequest_SetUserLocation(Url: String) {

        println("----------sLatitude----------$sLatitude")
        println("----------sLongitude----------$sLongitude")
        println("----------userID----------$userID")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["latitude"] = sLatitude
        jsonParams["longitude"] = sLongitude

        println("-------------UpdateUserLocation UserLocation Url----------------$Url")
        mRequest = ServiceRequest(this@UpdateUserLocation)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {

                println("-------------UpdateUserLocation UserLocation Response----------------$response")

                var Str_status = ""
                var sCategoryID = ""
                var assign_as = ""
                try {
                    val `object` = JSONObject(response)
                    Str_status = `object`.getString("status")
                    sCategoryID = `object`.getString("category_id")
                    assign_as = `object`.getString("assign_as")

                    if (Str_status.equals("1", ignoreCase = true)) {
                        session.setCategoryID(sCategoryID)
                        session.setAgent(assign_as)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                println("-------------UpdateUserLocation UserLocation Response1----------------$response")
                val intent = Intent(this@UpdateUserLocation, NavigationDrawer::class.java)
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)

                println("-------------UpdateUserLocation UserLocation Response2----------------$response")
            }

           override fun onErrorListener() {
                val intent = Intent(this@UpdateUserLocation, NavigationDrawer::class.java)
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
        })
    }

    companion object {
        internal val REQUEST_LOCATION = 199
    }

}