package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ForgotPassword : ActivityHockeyApp() {
    private var back: RelativeLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog

    private var Et_email: EditText? = null
    private var Bt_submit: Button? = null

    internal var Sstatus = ""
    internal var Smessage = ""
    internal var Ssms_status = ""
    internal var SverificationCode = ""
    internal var SemailAddress = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.forgotpassword)
        initialize()

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }


        Bt_submit!!.setOnClickListener {
            cd = ConnectionDetector(this@ForgotPassword)
            isInternetPresent = cd!!.isConnectingToInternet

            if (Et_email!!.text.toString().equals("", ignoreCase = true)) {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.emergencycontact_lable_enteremail_id_textview))
            } else if (!isValidEmail(Et_email!!.text.toString())) {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.forgot_password_email_label_enter_valid_email))
            } else {

                if (isInternetPresent!!) {
                    PostRequest(Iconstant.forgot_password_url)
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            }
        }

        Et_email!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_email!!)
            }
            false
        }
    }

    private fun initialize() {

        back = findViewById(R.id.forgot_password_email_header_back_layout) as RelativeLayout
        Et_email = findViewById(R.id.forgot_password_email_email_editText) as EditText
        Bt_submit = findViewById(R.id.forgot_password_email_submit_button) as Button

        Bt_submit!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")
    }


    //--------------Close Keyboard Method-----------
    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ForgotPassword)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    // -------------------------code for Forgot Password Post Request----------------------------------

    private fun PostRequest(Url: String) {

        dialog = Dialog(this@ForgotPassword)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_verifying))

        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = Et_email!!.text.toString()

        mRequest = ServiceRequest(this@ForgotPassword)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Forgot Password reponse-------------------$Url$response")

                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("response")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Ssms_status = `object`.getString("sms_status")
                        SverificationCode = `object`.getString("verification_code")
                        SemailAddress = `object`.getString("email_address")
                    }


                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val mDialog = PkDialog(this@ForgotPassword)
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.forgot_password_email_label_success))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            val i = Intent(this@ForgotPassword, ForgotPasswordOtp::class.java)
                            i.putExtra("Intent_Otp_Status", Ssms_status)
                            i.putExtra("Intent_verificationCode", SverificationCode)
                            i.putExtra("Intent_email", SemailAddress)
                            startActivity(i)
                            finish()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                        mDialog.show()

                    } else {
                        Alert(getResources().getString(R.string.action_error), Smessage)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(Et_email!!.windowToken, 0)

                dialog.dismiss()
            }

           override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
   override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
            return true
        }
        return false
    }

    companion object {

        //-------------------------code to Check Email Validation-----------------------
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

}
