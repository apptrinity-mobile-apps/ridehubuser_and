package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class ChangePassword : ActivityHockeyApp() {
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var session: SessionManager? = null
    private var back: RelativeLayout? = null
    private var Et_old_password: EditText? = null
    private var Et_new_password: EditText? = null
    private var Et_confirm_password: EditText? = null
    private var UserID = ""
    private var mRequest: ServiceRequest? = null
    private var Bt_submit: Button? = null
    internal lateinit var dialog: Dialog

    //----------------------Code for TextWatcher-------------------------
    private val EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            if (Et_old_password!!.text.length > 0) {
                Et_old_password!!.error = null
            }
            if (Et_new_password!!.text.length > 0) {
                Et_new_password!!.error = null
            }
            if (Et_confirm_password!!.text.length > 0) {
                Et_confirm_password!!.error = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password)
        context = getApplicationContext()
        initialize()

        back!!.setOnClickListener {
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        Bt_submit!!.setOnClickListener {
            if (Et_old_password!!.text.toString().length == 0) {
                erroredit(Et_old_password!!, getResources().getString(R.string.changepassword_label_alert_oldpassword))
            } else if (!isValidPassword(Et_new_password!!.text.toString())) {
                erroredit(Et_new_password!!, getResources().getString(R.string.changepassword_label_alert_newpassword))
            } else if (!isValidPassword(Et_confirm_password!!.text.toString())) {
                erroredit(Et_confirm_password!!, getResources().getString(R.string.changepassword_label_alert_newpassword))
            } else if (Et_new_password!!.text.toString() != Et_confirm_password!!.text.toString()) {
                erroredit(Et_confirm_password!!, getResources().getString(R.string.changepassword_lable_confirm_notmatch_edittext))
            } else {

                cd = ConnectionDetector(this@ChangePassword)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    postRequest_changePassword(Iconstant.changePassword_url)

                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            }
        }


        Et_old_password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_old_password!!)
            }
            false
        }

        Et_new_password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_new_password!!)
            }
            false
        }

        Et_confirm_password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_confirm_password!!)
            }
            false
        }


    }

    private fun initialize() {
        session = SessionManager(this@ChangePassword)

        Bt_submit = findViewById(R.id.change_password_submitbutton) as Button
        back = findViewById(R.id.changepassword_header_back_layout) as RelativeLayout
        Et_old_password = findViewById(R.id.change_password_enter_old_password_editText) as EditText
        Et_new_password = findViewById(R.id.change_password_enter_new_password_edittext) as EditText
        Et_confirm_password = findViewById(R.id.change_password_confirm_editText) as EditText

        // Bt_submit.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf"));

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        Et_old_password!!.addTextChangedListener(EditorWatcher)
        Et_new_password!!.addTextChangedListener(EditorWatcher)
        Et_confirm_password!!.addTextChangedListener(EditorWatcher)
    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@ChangePassword, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    //--------------Close KeyBoard Method-----------
    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ChangePassword)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    // ---validating password with retype password---
    private fun isValidPassword(pass: String): Boolean {
        return if (pass.length < 6) {
            false
        } else {
            true
        }/*
			 * else if(!pass.matches("(.*[A-Z].*)")) { return false; }
			 *//* else if (!pass.matches("(.*[a-z].*)")) {
            return false;
        } else if (!pass.matches("(.*[0-9].*)")) {
            return false;
        }*//*
			 * else if(!pass.matches(
			 * "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)")) {
			 * return false; }
			 */

    }

    //-----------------------Change Password Post Request-----------------
    private fun postRequest_changePassword(Url: String) {
        dialog = Dialog(this@ChangePassword)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_otp))

        println("-------------change password Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["password"] = Et_old_password!!.text.toString()
        jsonParams["new_password"] = Et_new_password!!.text.toString()

        mRequest = ServiceRequest(this@ChangePassword)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                println("-------------change password Response----------------$response")

                var Sstatus = ""
                var Smessage = ""
                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("response")

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
                if (Sstatus.equals("1", ignoreCase = true)) {

                    val mDialog = PkDialog(this@ChangePassword)
                    mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                    mDialog.setDialogMessage(getResources().getString(R.string.changepassword_label_changed_success))
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                        mDialog.dismiss()
                        onBackPressed()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    })
                    mDialog.show()
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage)
                }
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }
}
