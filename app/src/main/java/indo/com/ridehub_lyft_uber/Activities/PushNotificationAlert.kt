package indo.com.ridehub_lyft_uber.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.RelativeLayout
import android.widget.TextView
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.subclass.ActivitySubClass

class PushNotificationAlert : ActivitySubClass() {
    internal lateinit var Tv_ok: TextView
    internal lateinit var Tv_title: TextView
    internal lateinit var Rl_ok: RelativeLayout
    internal lateinit var message: TextView
    private var Str_message: String? = null
    private var Str_action: String? = null
    private var SrideId_intent: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pushnotification_alert)
        initialize()

        Rl_ok.setOnClickListener {
            if (Str_action!!.equals(Iconstant.PushNotification_RideCancelled_Key, ignoreCase = true) || Str_action!!.equals(Iconstant.PushNotification_RideCompleted_Key, ignoreCase = true)) {
                val broadcastIntent = Intent()
                broadcastIntent.action = "com.pushnotification.updateBottom_view"
                sendBroadcast(broadcastIntent)
            }

            if (Str_action!!.equals(Iconstant.PushNotification_PaymentPaid_Key, ignoreCase = true)) {
                val intent = Intent(this@PushNotificationAlert, MyRideRating::class.java)
                intent.putExtra("RideID", SrideId_intent)
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)
            } else {
                finish()
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }
    }

    private fun initialize() {
        Tv_ok = findViewById(R.id.pushnotification_alert_ok_textview) as TextView
        Tv_title = findViewById(R.id.pushnotification_alert_messge_label) as TextView
        message = findViewById(R.id.pushnotification_alert_messge_textview) as TextView
        Rl_ok = findViewById(R.id.pushnotification_alert_ok_layout) as RelativeLayout

        val intent = getIntent()
        Str_message = intent.getStringExtra("message")
        Str_action = intent.getStringExtra("Action")
        if (getIntent().getExtras().containsKey("RideID")) {
            SrideId_intent = intent.getStringExtra("RideID")
        }
        message.text = Str_message

        if (Str_action!!.equals(Iconstant.PushNotification_RideCancelled_Key, ignoreCase = true)) {
            Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_cancelled_sorry))
        } else if (Str_action!!.equals(Iconstant.PushNotification_CabArrived_Key, ignoreCase = true)) {
            Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label))
        } else if (Str_action!!.equals(Iconstant.PushNotification_RideCompleted_Key, ignoreCase = true)) {
            Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_completed_thanks))
        } else if (Str_action!!.equals(Iconstant.PushNotification_PaymentPaid_Key, ignoreCase = true)) {
            Tv_title.setText(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success))
        }

        /* try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    //-----------------Move Back on pressed phone back button------------------
   override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            //Do nothing
            true
        } else false
    }
}
