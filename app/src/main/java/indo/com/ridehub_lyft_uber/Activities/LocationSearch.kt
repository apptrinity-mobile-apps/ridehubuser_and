package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.graphics.drawable.AnimatedVectorDrawableCompat
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.Adapters.PlaceSearchAdapter
import indo.com.ridehub_lyft_uber.Fragments.Fragment_HomePage
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.PojoResponse.EstimateDetailPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.iconstant.Iconstant.profile_getFavouriteaddress_url
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.CustomProgress
import indo.com.ridehub_lyft_uber.utils.ProgressListener
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LocationSearch : ActivityHockeyApp(), ProgressListener {

    private var back: RelativeLayout? = null
    private var et_desti_search: EditText? = null
    private var et_source_search: EditText? = null
    private var listview: ListView? = null
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var tv_emptyText: TextView? = null
    private var progresswheel: ProgressBar? = null
    private var fav_progresswheel: ProgressBar? = null
    private var iv_source_clear: ImageView? = null
    private var iv_destination_clear: ImageView? = null
    private var iv_favedit_clear: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var context: Context
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal lateinit var adapter: PlaceSearchAdapter
    private var isdataAvailable = false
    private val isEstimateAvailable = false

    private var Slatitude = ""
    private var Dlatitude = ""
    private var Slongitude = ""
    private var Dlongitude = ""
    private var Sselected_location = ""

    internal var dialog: Dialog? = null
    internal var ratecard_list = ArrayList<EstimateDetailPojo>()
    internal lateinit var source_address: String
    internal var destination_address: String? = null
    internal lateinit var source_address2: String
    internal lateinit var setpickuploc: String
    internal var identi = 0
    internal lateinit var session: SessionManager
    private var UserID: String? = null
    internal var Str_destination_status = "no"
    internal lateinit var ET_source_status: String
    internal var ET_destination_status = ""
    private var ll_fav_workplace: LinearLayout? = null
    private var ll_fav_homeplace: LinearLayout? = null
    private var tv_fav_workplace: TextView? = null
    private var tv_fav_homeplace: TextView? = null
    private var tv_fav_headworkplace: TextView? = null
    private var tv_fav_headhomeplace: TextView? = null
    internal var Fav_selected_Location: String? = null
    private var fav_alert_layout: RelativeLayout? = null
    private var fav_back: RelativeLayout? = null
    private var et_fav_search: EditText? = null
    private var fav_listview: ListView? = null
    private var fav_tv_emptyText: TextView? = null
    private var fav_alert_textview: TextView? = null
    private var ll_mainsearch: LinearLayout? = null
    private var ll_fav_search: LinearLayout? = null
    internal lateinit var fav_home: String
    internal var fav_work = "inactive"
    internal lateinit var Fav_Home: String
    internal lateinit var Fav_Work: String
    internal lateinit var fragment_homePage: Fragment_HomePage
    internal lateinit var recent_source: String
    internal lateinit var recent_destination: String
    //internal lateinit var recent_rides_array: ArrayList<RecentRidesPojo>
    internal lateinit var reccent_rides_listview: RecyclerView

    lateinit var avd: AnimatedVectorDrawableCompat
    lateinit var iv_line: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.place_search)
        context = applicationContext
        initialize()

        fragment_homePage = Fragment_HomePage()
        session = SessionManager(applicationContext)
        Slatitude = intent.getStringExtra("Slatitude")
        Slongitude = intent.getStringExtra("Slongitude")
        source_address = intent.getStringExtra("source_address")
        setpickuploc = intent.getStringExtra("setpickuploc")


        if (!source_address.equals("", ignoreCase = true)) {
            et_source_search!!.setText(source_address)
            listview!!.clearTextFilter()
            listview!!.visibility = View.GONE
        }

        et_desti_search!!.requestFocus()
        Log.e("SOURCEADDRESS", source_address)


        val data = source_address.toLowerCase().replace(" ", "%20")
        CitySearchRequest2(Iconstant.place_search_url + data)

        postRequest_getFavourite(profile_getFavouriteaddress_url)
        listview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            ll_fav_homeplace!!.visibility = View.VISIBLE
            ll_fav_workplace!!.visibility = View.VISIBLE

            Toast.makeText(context, "" + identi, Toast.LENGTH_SHORT).show()
            Log.e("listitem", "$ET_source_status---$identi---$ET_destination_status")
            Sselected_location = itemList_location[position]

            if (ET_source_status.equals("active", ignoreCase = true)) {
                source_address = Sselected_location
            }

            if (identi == 0) {
                et_source_search!!.setText(Sselected_location)
            }
            cd = ConnectionDetector(this@LocationSearch)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                Log.e("LISTVIEW", itemList_placeId[position])
                LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position])
            } else {
                alert_layout!!.visibility = View.VISIBLE
                alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
            }
        }

        et_desti_search!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                ET_source_status = "inactive"
                et_source_search!!.setText(source_address)
                ll_fav_homeplace!!.visibility = View.VISIBLE
                ll_fav_workplace!!.visibility = View.VISIBLE

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                // et_source_search.setText(source_address);
                ET_destination_status = "active"
                listview!!.visibility = View.VISIBLE
                ll_fav_homeplace!!.visibility = View.GONE
                ll_fav_workplace!!.visibility = View.GONE
            }

            override fun afterTextChanged(s: Editable) {
                identi = 1
                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    listview!!.visibility = View.VISIBLE
                    val data = et_desti_search!!.text.toString().toLowerCase().replace(" ", "%20")
                    CitySearchRequest(Iconstant.place_search_url + data)

                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                }

            }
        })

        et_desti_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_desti_search!!)
                // et_source_search.setText(source_address);
                ll_fav_homeplace!!.visibility = View.VISIBLE
                ll_fav_workplace!!.visibility = View.VISIBLE
            }
            false
        }

        back!!.setOnClickListener {
            // close keyboard


            val i = Intent(this@LocationSearch, Fragment_HomePage::class.java)
            startActivity(i)

            fragment_homePage.startTimer()
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        et_desti_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_desti_search, 0)
        }, 200)


        et_source_search!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                // Log.e("ETSEARCH_CITY", "Cesr destination");
                if (ET_destination_status.equals("active", ignoreCase = true)) {
                    et_desti_search!!.setText("")
                    et_desti_search!!.hint = "Where to ?"
                }
                val et_quantity = v as EditText
                et_quantity.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                        ET_source_status = "active"
                        ET_destination_status = "inactive"

                    }

                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                        identi = 0
                        cd = ConnectionDetector(this@LocationSearch)
                        isInternetPresent = cd!!.isConnectingToInternet
                        ll_fav_homeplace!!.visibility = View.GONE
                        ll_fav_workplace!!.visibility = View.GONE

                        if (isInternetPresent!!) {
                            if (mRequest != null) {
                                mRequest!!.cancelRequest()
                            }
                            listview!!.visibility = View.VISIBLE
                            val data = et_source_search!!.text.toString().toLowerCase().replace(" ", "%20")
                            Log.e("ETSEARCH_CITY11", data)
                            CitySearchRequest(Iconstant.place_search_url + data)
                        } else {
                            alert_layout!!.visibility = View.VISIBLE
                            alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                        }
                    }

                    override fun afterTextChanged(s: Editable) {}
                })
            } else {

            }
        }



        et_desti_search!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                ll_fav_homeplace!!.visibility = View.VISIBLE
                ll_fav_workplace!!.visibility = View.VISIBLE

            } else {
            }
        }


        /*et_source_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ET_source_status = "active";
                ET_destination_status = "inactive";

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                identi = 0;
                cd = new ConnectionDetector(LocationSearch.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    String data = et_source_search.getText().toString().toLowerCase().replace(" ", "%20");
                    Log.e("ETSEARCH_CITY", data);
                    CitySearchRequest(Iconstant.place_search_url + data);

                } else {
                    alert_layout.setVisibility(View.VISIBLE);
                    alert_textview.setText(getResources().getString(R.string.alert_nointernet));
                }

            }
        });
*/
        et_source_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_source_search!!)

            }
            false
        }


        et_source_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_source_search, 0)
        }, 200)


        iv_source_clear!!.setOnClickListener {
            et_source_search!!.text.clear()
            et_desti_search!!.text.clear()
            ll_fav_homeplace!!.visibility = View.GONE
            ll_fav_workplace!!.visibility = View.GONE
        }

        iv_destination_clear!!.setOnClickListener {
            et_desti_search!!.text.clear()
            ll_fav_homeplace!!.visibility = View.VISIBLE
            ll_fav_workplace!!.visibility = View.VISIBLE
        }

        iv_favedit_clear!!.setOnClickListener { et_fav_search!!.text.clear() }


        ll_fav_homeplace!!.setOnClickListener {
            /* Intent intent = new Intent(LocationSearch.this, LocationSearch_Favourite.class);
        intent.putExtra("home", "home");
        startActivity(intent);*/
            //
            fav_home = "home"
            Fav_Home = tv_fav_homeplace!!.text.toString()
            if (!Fav_Home.equals("Add Home", ignoreCase = true)) {
                Log.e("FAV_HOME", Fav_Home)
                et_source_search!!.setText(source_address)
                et_desti_search!!.setText(Fav_Home)
                ll_fav_search!!.visibility = View.GONE
                ll_mainsearch!!.visibility = View.VISIBLE
                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    val data = Fav_Home.toLowerCase().replace(" ", "%20")
                    Sselected_location = tv_fav_homeplace!!.text.toString()
                    Log.e("ETSEARCH_CITY1111111", data)
                    CitySearchRequest2(Iconstant.place_search_url + data)
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                }
            } else {
                Log.e("FAV_HOME_ELSE", Fav_Home)
                et_fav_search!!.text.clear()
                ll_mainsearch!!.visibility = View.GONE
                ll_fav_search!!.visibility = View.VISIBLE
                fav_listview!!.visibility = View.VISIBLE
            }
        }

        ll_fav_workplace!!.setOnClickListener {
            /* Intent intent = new Intent(LocationSearch.this, LocationSearch_Favourite.class);
        intent.putExtra("home", "work");
        startActivity(intent);*/
            fav_home = "work"
            Fav_Work = tv_fav_workplace!!.text.toString()
            if (!Fav_Work.equals("Add Work", ignoreCase = true)) {
                Log.e("FAV_WORK", Fav_Work)
                et_source_search!!.setText(source_address)
                et_desti_search!!.setText(Fav_Work)
                ll_fav_search!!.visibility = View.GONE
                ll_mainsearch!!.visibility = View.VISIBLE
                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    val data = Fav_Work.toLowerCase().replace(" ", "%20")
                    Sselected_location = tv_fav_workplace!!.text.toString()
                    Log.e("ETSEARCH_CITY222222", data)
                    CitySearchRequest2(Iconstant.place_search_url + data)
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                }
            } else {
                Log.e("FAV_HOME_ELSE", Fav_Work)
                et_fav_search!!.text.clear()
                ll_mainsearch!!.visibility = View.GONE
                ll_fav_search!!.visibility = View.VISIBLE
                fav_listview!!.visibility = View.VISIBLE
            }
        }


        et_fav_search!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    val data = et_fav_search!!.text.toString().toLowerCase().replace(" ", "%20")
                    CitySearchRequest(Iconstant.place_search_url + data)
                } else {
                    fav_alert_layout!!.visibility = View.VISIBLE
                    fav_alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                }

            }
        })


        et_fav_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_fav_search!!)
            }
            false
        }

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        et_fav_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_fav_search, 0)
        }, 200)

        fav_listview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            Toast.makeText(context, "" + identi, Toast.LENGTH_SHORT).show()

            Sselected_location = itemList_location[position]
            Log.e("listitem", "$ET_source_status---$identi---$ET_destination_status-----$Sselected_location")
            /* if (ET_source_status.equalsIgnoreCase("active")) {
            source_address = Sselected_location;
        }*/
            // if (identi == 0) {
            source_address2 = Sselected_location
            et_fav_search!!.setText(Sselected_location)

            //tv_fav_homeplace.setText(Sselected_location);
            ll_fav_search!!.visibility = View.GONE
            ll_mainsearch!!.visibility = View.VISIBLE

            if (fav_home.equals("home", ignoreCase = true)) {
                tv_fav_homeplace!!.text = Sselected_location
                tv_fav_headhomeplace!!.visibility = View.VISIBLE
                tv_fav_headhomeplace!!.text = "Home"
                listview!!.visibility = View.GONE
            } else if (fav_home.equals("work", ignoreCase = true)) {
                tv_fav_workplace!!.text = Sselected_location
                tv_fav_headworkplace!!.visibility = View.VISIBLE
                tv_fav_headworkplace!!.text = "Work"
                listview!!.visibility = View.GONE
            }


            // }
            /* cd = new ConnectionDetector(LocationSearch.this);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId.get(position));
        } else {
            fav_alert_layout.setVisibility(View.VISIBLE);
            fav_alert_textview.setText(getResources().getString(R.string.alert_nointernet));
        }*/
        }

        fav_back!!.setOnClickListener {
            // close keyboard
            /* InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
mgr.hideSoftInputFromWindow(back.getWindowToken(), 0);

LocationSearch.this.finish();
LocationSearch.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);*/
            ll_fav_search!!.visibility = View.GONE
            ll_mainsearch!!.visibility = View.VISIBLE
        }


        CustomProgress.getInstance().setListener(this)
    }

    private fun initialize() {
        alert_layout = findViewById(R.id.location_search_alert_layout) as RelativeLayout
        alert_textview = findViewById(R.id.location_search_alert_textView) as TextView
        back = findViewById(R.id.location_search_back_layout) as RelativeLayout
        et_desti_search = findViewById(R.id.location_destinat_search_editText) as EditText
        listview = findViewById(R.id.location_search_listView) as ListView
        progresswheel = findViewById(R.id.location_search_progressBar) as ProgressBar
        tv_emptyText = findViewById(R.id.location_search_empty_textview) as TextView

        et_source_search = findViewById(R.id.location_source_search_editText) as EditText
        iv_source_clear = findViewById(R.id.iv_source_clear) as ImageView
        iv_favedit_clear = findViewById(R.id.iv_favedit_clear) as ImageView
        iv_destination_clear = findViewById(R.id.iv_destination_clear) as ImageView
        ll_fav_workplace = findViewById(R.id.ll_fav_workplace) as LinearLayout
        ll_fav_homeplace = findViewById(R.id.ll_fav_homeplace) as LinearLayout
        tv_fav_workplace = findViewById(R.id.tv_fav_workplace) as TextView
        tv_fav_homeplace = findViewById(R.id.tv_fav_homeplace) as TextView
        tv_fav_headhomeplace = findViewById(R.id.tv_fav_headhomeplace) as TextView
        tv_fav_headworkplace = findViewById(R.id.tv_fav_headworkplace) as TextView

        fav_listview = findViewById(R.id.location_search_listView_fav) as ListView
        fav_tv_emptyText = findViewById(R.id.location_search_empty_textview_fav) as TextView
        fav_alert_textview = findViewById(R.id.location_search_alert_textView_fav) as TextView
        fav_alert_layout = findViewById(R.id.location_search_alert_layout_fav) as RelativeLayout
        fav_back = findViewById(R.id.location_search_back_layout_fav) as RelativeLayout
        et_fav_search = findViewById(R.id.location_search_editText) as EditText
        fav_progresswheel = findViewById(R.id.location_search_progressBar_fav) as ProgressBar

        ll_mainsearch = findViewById(R.id.ll_mainsearch) as LinearLayout
        ll_fav_search = findViewById(R.id.ll_fav_search) as LinearLayout

        session = SessionManager(this@LocationSearch)
        val user = session.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID)

        reccent_rides_listview = findViewById(R.id.reccent_rides_listview) as RecyclerView
        reccent_rides_listview.isNestedScrollingEnabled = false

        // recent_rides_array = ArrayList<RecentRidesPojo>()

        //postRequest_recentRides(home_recent_reides_url)


        iv_line = findViewById(R.id.iv_line)
        avd = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_line)!!
        iv_line.background = avd
        iv_line.visibility = View.GONE

    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@LocationSearch)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //-------------------Search Place Request----------------
    private fun CitySearchRequest(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        CustomProgress.getInstance().changeState(true)
        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(Url, Request.Method.GET, null, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Search city  reponse-------------------$response")
                var status = ""
                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {

                        status = `object`.getString("status")
                        val place_array = `object`.getJSONArray("predictions")
                        if (status.equals("OK", ignoreCase = true)) {
                            if (place_array.length() > 0) {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                for (i in 0 until place_array.length()) {
                                    val place_object = place_array.getJSONObject(i)
                                    itemList_location.add(place_object.getString("description"))
                                    itemList_placeId.add(place_object.getString("place_id"))
                                }
                                isdataAvailable = true
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        } else {
                            itemList_location.clear()
                            itemList_placeId.clear()
                            isdataAvailable = false
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                CustomProgress.getInstance().changeState(false)
                progresswheel!!.visibility = View.INVISIBLE
                alert_layout!!.visibility = View.GONE
                if (isdataAvailable) {
                    tv_emptyText!!.visibility = View.GONE
                } else {
                    tv_emptyText!!.visibility = View.VISIBLE
                    ll_fav_homeplace!!.visibility = View.VISIBLE
                    ll_fav_workplace!!.visibility = View.VISIBLE
                }
                adapter = PlaceSearchAdapter(this@LocationSearch, itemList_location)
                adapter.setProgress(iv_line)
                listview!!.adapter = adapter
                fav_listview!!.adapter = adapter
                adapter.notifyDataSetChanged()
            }

            override fun onErrorListener() {
                CustomProgress.getInstance().changeState(false)
                progresswheel!!.visibility = View.INVISIBLE
                alert_layout!!.visibility = View.GONE

                // close keyboard
                CloseKeyboard(et_desti_search!!)
            }
        })
    }


    private fun CitySearchRequest2(Url: String) {

        progresswheel!!.visibility = View.GONE
        CustomProgress.getInstance().changeState(true)
        println("--------------Search city2 url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(Url, Request.Method.GET, null, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Search city2  reponse-------------------$response")
                var status = ""
                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {

                        status = `object`.getString("status")
                        val place_array = `object`.getJSONArray("predictions")
                        if (status.equals("OK", ignoreCase = true)) {
                            if (place_array.length() > 0) {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                for (i in 0 until place_array.length()) {
                                    val place_object = place_array.getJSONObject(i)
                                    itemList_location.add(place_object.getString("description"))
                                    itemList_placeId.add(place_object.getString("place_id"))
                                }
                                isdataAvailable = true

                                LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[0])

                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        } else {
                            itemList_location.clear()
                            itemList_placeId.clear()
                            isdataAvailable = false
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                CustomProgress.getInstance().changeState(false)
                progresswheel!!.visibility = View.INVISIBLE
                alert_layout!!.visibility = View.GONE
                if (isdataAvailable) {
                    tv_emptyText!!.visibility = View.GONE
                    fav_tv_emptyText!!.visibility = View.GONE
                } else {
                    tv_emptyText!!.visibility = View.VISIBLE
                    fav_tv_emptyText!!.visibility = View.VISIBLE
                }
                adapter = PlaceSearchAdapter(this@LocationSearch, itemList_location)
                adapter.setProgress(iv_line)
                listview!!.adapter = adapter
                fav_listview!!.adapter = adapter
                adapter.notifyDataSetChanged()
            }

            override fun onErrorListener() {
                CustomProgress.getInstance().changeState(false)
                progresswheel!!.visibility = View.INVISIBLE
                alert_layout!!.visibility = View.GONE

                // close keyboard
                CloseKeyboard(et_desti_search!!)
            }
        })
    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private fun LatLongRequest(Url: String) {

        dialog = Dialog(this@LocationSearch)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))

        println("--------------LatLong url-------------------$Url-----$identi")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(Url, Request.Method.GET, null, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------LatLong  reponse-------------------$response")
                var status = ""
                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {

                        status = `object`.getString("status")
                        val place_object = `object`.getJSONObject("result")
                        if (status.equals("OK", ignoreCase = true)) {
                            if (place_object.length() > 0) {
                                val geometry_object = place_object.getJSONObject("geometry")
                                if (geometry_object.length() > 0) {
                                    val location_object = geometry_object.getJSONObject("location")
                                    if (location_object.length() > 0) {

                                        if (identi == 0) {
                                            Slatitude = location_object.getString("lat")
                                            Slongitude = location_object.getString("lng")
                                            Log.e("LOCATION_CLASS_0", "$Slatitude--------$Slongitude")

                                        } else {
                                            Dlatitude = location_object.getString("lat")
                                            Dlongitude = location_object.getString("lng")
                                            Log.e("LOCATION_CLASS_1", "$Dlatitude--------$Dlongitude")
                                        }

                                        isdataAvailable = true
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        } else {
                            isdataAvailable = false
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (isdataAvailable) {
                    dialog!!.dismiss()


                    if (!et_desti_search!!.text.toString().equals("", ignoreCase = true)) {
                        Log.e("Intant", Slatitude + "--" + Slongitude + "--" + Dlatitude + "--------" + Dlongitude + "---------" + Sselected_location + "--------" + et_source_search!!.text.toString() + "" + setpickuploc)
                        val returnIntent = Intent()
                        returnIntent.putExtra("Selected_Latitude", Slatitude)
                        returnIntent.putExtra("Selected_Longitude", Slongitude)
                        returnIntent.putExtra("Selected_DLatitude", Dlatitude)
                        returnIntent.putExtra("Selected_DLongitude", Dlongitude)
                        returnIntent.putExtra("Selected_Location", Sselected_location)
                        returnIntent.putExtra("Selected_SourceLocation", et_source_search!!.text.toString())
                        returnIntent.putExtra("setpickuploc", setpickuploc)
                        returnIntent.putExtra("searchlocation", "yes")

                        val searchValue = "yes"
                        session.setLocationSearchValue(searchValue)

                        setResult(RESULT_OK, returnIntent)
                        onBackPressed()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }

                } else {
                    dialog!!.dismiss()
                    Alert(getResources().getString(R.string.alert_label_title), status)
                }
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }


    private fun postRequest_getFavourite(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        println("---------------GET Favourite Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams.put("user_id", UserID!!)



        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("---------------GET Favourite Response-----------------$response")
                var Sstatus = ""
                val Smessage = ""

                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        progresswheel!!.visibility = View.GONE
                        val Location_aray = `object`.getJSONArray("locations")
                        for (i in 0 until Location_aray.length()) {
                            val address_object = Location_aray.getJSONObject(i)

                            val address = address_object.getString("address")
                            val title = address_object.getString("title")
                            val location_key = address_object.getString("location_key")

                            if (title.equals("home", ignoreCase = true)) {
                                tv_fav_headhomeplace!!.visibility = View.VISIBLE
                                tv_fav_headhomeplace!!.text = "Home"
                                tv_fav_homeplace!!.visibility = View.VISIBLE
                                tv_fav_homeplace!!.text = address
                                Sselected_location = address

                                //tv_home_loc_delete.setVisibility(View.VISIBLE);
                            } else if (title.equals("work", ignoreCase = true)) {
                                tv_fav_headworkplace!!.visibility = View.VISIBLE
                                tv_fav_headworkplace!!.text = "Work"
                                tv_fav_workplace!!.visibility = View.VISIBLE
                                tv_fav_workplace!!.text = address
                                Sselected_location = address
                                //tv_work_loc_delete.setVisibility(View.VISIBLE);

                            }
                            Log.e("ADDRESS_RES", "$address------$title")

                        }

                        // Alert(getResources().getString(R.string.action_success), getResources().getString(R.string.profile_lable_username_success));
                    } else {
                        //Alert(getResources().getString(R.string.action_error), Smessage);
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }


            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    /*  private fun postRequest_recentRides(Url: String) {
          println("---------------Recent Rides Url-----------------$Url")

          val jsonParams = HashMap<String, String>()
          jsonParams["user_id"] = UserID
          mRequest = ServiceRequest(this)
          mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener() {
              override fun onCompleteListener(response: String) {
                  println("---------------Recent Rides Response-----------------$response")
                  var Sstatus = ""
                  val Smessage = ""
                  try {
                      val `object` = JSONObject(response)
                      Sstatus = `object`.getString("status")
                      if (Sstatus.equals("1", ignoreCase = true)) {
                          val jsonResponse = `object`.getJSONObject("response")
                          val listArray = jsonResponse.getJSONArray("list")
                          recent_rides_array = ArrayList<RecentRidesPojo>()
                          recent_rides_array.clear()
                          for (i in 0 until listArray.length()) {
                              val place_object = listArray.getJSONObject(i)
                              recent_source = place_object.getString("source")
                              recent_destination = place_object.getString("destination")
                              Log.e("RECENTSOU&DEST", "$recent_source--------$recent_destination")

                              val recentRidesPojo = RecentRidesPojo(recent_destination)
                              recent_rides_array.add(recentRidesPojo)
                          }

                          reccent_rides_listview.setHasFixedSize(true)
                          val layoutManager = LinearLayoutManager(context)
                          //,LinearLayoutManager.HORIZONTAL, false
                          reccent_rides_listview.layoutManager = layoutManager
                          reccent_rides_listview.itemAnimator = DefaultItemAnimator()
                          val recentRidesAdapter = RecentRidesAdapter(context, recent_rides_array)
                          reccent_rides_listview.adapter = recentRidesAdapter

                      }

                  } catch (e: JSONException) {
                      e.printStackTrace()
                  }

                  dialog!!.dismiss()

              }

              fun onErrorListener() {
                  dialog!!.dismiss()
              }
          })
      }*/


/*
    internal inner class RecentRidesAdapter(private val mContext: Context, var recent_rides_array: ArrayList<RecentRidesPojo>) : RecyclerView.Adapter<RecentRidesAdapter.MyViewHolder>() {

        internal inner class MyViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
            var tv_recent_source: CustomTextView? = null
            var tv_recent_destination: CustomTextView
            var ll_recent_ride_listitem: LinearLayout

            init {
                // tv_recent_source = (CustomTextView) rowView.findViewById(R.id.tv_recent_source);
                tv_recent_destination = rowView.findViewById(R.id.tv_recent_destination) as CustomTextView
                ll_recent_ride_listitem = rowView.findViewById(R.id.ll_recent_ride_listitem) as LinearLayout

            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentRidesAdapter.MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recent_rides_list_layout, parent, false)

            return RecentRidesAdapter.MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: RecentRidesAdapter.MyViewHolder, position: Int) {
            try {

                val recentRidesPojo = recent_rides_array[position]

                holder.tv_recent_destination.setText(recentRidesPojo.getDestination())

                holder.ll_recent_ride_listitem.setOnClickListener {
                    val recent_ride_dest = recentRidesPojo.getDestination().toString()
                    et_desti_search!!.setText(recent_ride_dest)

                    if (isInternetPresent!!) {
                        val data = recent_ride_dest.toLowerCase().replace(" ", "%20")
                        Sselected_location = recent_ride_dest
                        Log.e("ETSEARCH_CITY222222", data)
                        CitySearchRequest2(Iconstant.place_search_url + data)
                    } else {
                        alert_layout!!.visibility = View.VISIBLE
                        alert_textview!!.setText(getResources().getString(R.string.alert_nointernet))
                    }
                }

            } catch (e: Exception) {
                //Log.e("AdapterError", e.toString());
            }

        }

        override fun getItemCount(): Int {
            return recent_rides_array.size
        }

    }
*/


    private fun startAnimation(isStart: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isStart) {
                repeatAnimation()
            } else {
                handler.postDelayed({
                    iv_line.removeCallbacks(action)
                    avd.stop()
                    iv_line.visibility = View.GONE
                    iv_line.invalidate()
                }, 800)

            }
        }
    }

    private val action = Runnable { repeatAnimation() }
    internal var handler = Handler()
    private fun repeatAnimation() {
        if (avd != null) {
            Thread(Runnable {

                handler.post {
                    iv_line.visibility = View.VISIBLE
                    avd.start()
                    iv_line.postDelayed(action, 1000)
                    iv_line.invalidate()
                }
            }).start()

        }
    }

    override fun onProgressChanged(state: Boolean) {
        startAnimation(state)
    }
}
