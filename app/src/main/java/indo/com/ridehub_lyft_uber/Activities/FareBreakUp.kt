package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.Request
import com.squareup.picasso.Picasso
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.subclass.ActivitySubClass
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*


class FareBreakUp : ActivitySubClass() {
    private var Tv_baseFare: CustomTextView? = null
    private var Tv_discount: CustomTextView? = null
    private var Tv_duration: CustomTextView? = null
    private var Tv_waiting: CustomTextView? = null
    private var Tv_timeTravel: CustomTextView? = null
    private var Tv_serviceTax_Title: CustomTextView? = null
    private var Rl_payment: RelativeLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var SrideId_intent = ""
    private var ScurrencyCode = ""
    private var StotalAmount = ""
    private var total_and_refer = ""
    private var points_conversion = ""
    private var fianl_conversion = ""
    private var Sduation = ""
    private var SwaitingTime = ""
    private var StravelDistance = ""
    internal var currencycode: Currency? = null
    private var Im_DriverImage: RoundedImageView? = null
    private var Tv_DriverName: CustomTextView? = null
    private var Tv_SubTotal: CustomTextView? = null
    private var Tv_TripTotal: CustomTextView? = null

    //------Tip Declaration-----
    private var Et_tip_Amount: EditText? = null
    private var Tv_Referpoints_Amount: CustomTextView? = null
    private var tv_note_referpoints: CustomTextView? = null
    private var Bt_tip_Apply: Button? = null
    private var Rl_tip: RelativeLayout? = null
    private var Rl_referPoints: RelativeLayout? = null
    private var Tv_tip: CustomTextView? = null
    private var Ll_TipAmount: LinearLayout? = null
    private var Ll_referralpoints: LinearLayout? = null
    private var sSelectedTipAmount = ""
    private var Rb_driver: RatingBar? = null
    private var Tv_serviceTax: CustomTextView? = null
    private var Tv_referralpoints: CustomTextView? = null

    private var sDriverName = ""
    private var sDriverImage = ""
    private var sDriverRating = ""
    private var sDriverLat = ""
    private var sDriverLong = ""
    private var sUserLat = ""
    private var sUserLong = ""
    private var sSubTotal = ""
    private var sServiceTax = ""
    private var sServiceTax_percentage = ""
    private var sTotalPayment = ""
    private var CouponDiscount = ""
    private var ReferPoints_Total = ""
    private var number_of_points = ""
    private var amount_per_point = ""

    private var mRequest: ServiceRequest? = null
    private var Cb_tip: CheckBox? = null
    private var Cb_referpoints: CheckBox? = null
    internal lateinit var dialog: Dialog
    private var session: SessionManager? = null
    internal lateinit var UserID: String

    private var is_Checked_points = "No"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.farebreak_up_2)
        farebreakup_class = this@FareBreakUp
        initialize()

        Rl_payment!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {

                Log.e("MAKEPAYMENT_REFERRAL",ReferPoints_Total+"-----"+is_Checked_points)
                if (ReferPoints_Total.toFloat() >= 0) {
                    if (is_Checked_points.equals("No")) {

                        val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                        passIntent.putExtra("RideID", SrideId_intent)
                        passIntent.putExtra("TotalAmount", total_and_refer)
                        startActivity(passIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    } else {
                        Log.e("CHECKEDVALUE",is_Checked_points)
                        postRequest_PayByREFERPoints(Iconstant.pay_by_referpoints)
                    }

                } else {
                    Log.e("MAKEPAYMENT_REFERRAL",ReferPoints_Total+"-----"+is_Checked_points)

                    val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                    passIntent.putExtra("RideID", SrideId_intent)
                    passIntent.putExtra("TotalAmount", total_and_refer)
                    startActivity(passIntent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }


            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }


        Bt_tip_Apply!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet

            if (Et_tip_Amount!!.text.toString().length > 0 && java.lang.Double.parseDouble(Et_tip_Amount!!.text.toString()) > 0.0) {
                if (isInternetPresent!!) {
                    postRequest_Tip(Iconstant.tip_add_url, "Apply")
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.my_rides_detail_tip_empty_label))
            }
        }


        Ll_RemoveTip!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                postRequest_Tip(Iconstant.tip_remove_url, "Remove")
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }


        Cb_tip!!.setOnClickListener { v ->
            if ((v as CheckBox).isChecked) {

                Rl_tip!!.visibility = View.VISIBLE
            } else {
                Rl_tip!!.visibility = View.GONE
            }
        }

        Cb_referpoints!!.setOnClickListener { v ->

            if ((v as CheckBox).isChecked) {
                is_Checked_points = "Yes"
                total_and_refer = (StotalAmount.toFloat() - ReferPoints_Total.toFloat()).toString()
                Log.e("REAMAINGBALNCE", total_and_refer)
                Ll_referralpoints!!.visibility = View.VISIBLE


                val fianl_conversion = sTotalPayment.toFloat() - points_conversion.toFloat()

                if(sTotalPayment.toFloat() > points_conversion.toFloat()){
                    Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                }else{
                    Tv_SubTotal!!.text = currencycode!!.symbol + "0"
                }
                Log.e("FINALCONVERSION", fianl_conversion.toString())

            } else {
                is_Checked_points = "No"
                Ll_referralpoints!!.visibility = View.GONE
                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
            }
        }


    }

    private fun initialize() {

        session = SessionManager(this@FareBreakUp)
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        Tv_baseFare = findViewById(R.id.fare_breakup_total_amount_textview) as CustomTextView
        Tv_discount = findViewById(R.id.fare_breakup_discount_amount_textview) as CustomTextView
        Tv_duration = findViewById(R.id.fare_breakup_duration_textview) as CustomTextView
        Tv_waiting = findViewById(R.id.fare_breakup_waiting_textview) as CustomTextView
        Tv_timeTravel = findViewById(R.id.fare_breakup_timetravel_textview) as CustomTextView
        Rl_payment = findViewById(R.id.fare_breakup_payment_layout) as RelativeLayout

        Im_DriverImage = findViewById(R.id.fare_breakup_imageview) as RoundedImageView
        Tv_DriverName = findViewById(R.id.fare_breakup_driver_name_textView) as CustomTextView
        Tv_SubTotal = findViewById(R.id.fare_breakup_subtotal_textView) as CustomTextView
        Tv_TripTotal = findViewById(R.id.fare_breakup_trip_total_textView) as CustomTextView

        Et_tip_Amount = findViewById(R.id.fare_breakup_tip_editText) as EditText
        Tv_Referpoints_Amount = findViewById(R.id.fare_breakup_referpoints_TextView) as CustomTextView
        tv_note_referpoints = findViewById(R.id.tv_note_referpoints) as CustomTextView
        Bt_tip_Apply = findViewById(R.id.fare_breakup_tip_apply_button) as Button
        Rl_tip = findViewById(R.id.fare_breakup_tip_layout) as RelativeLayout
        Cb_tip = findViewById(R.id.fare_breakup_tip_checkBox) as CheckBox
        Cb_referpoints = findViewById(R.id.fare_breakup_referpoints_checkBox) as CheckBox

        Tv_tip = findViewById(R.id.fare_breakup_tip_amount_textView) as CustomTextView
        Ll_TipAmount = findViewById(R.id.fare_breakup_tip_amount_layout) as LinearLayout
        Ll_RemoveTip = findViewById(R.id.fare_breakup_tip_amount_remove_layout) as LinearLayout
        Rl_TipMain = findViewById(R.id.fare_breakup_tip_top_layout) as RelativeLayout
        Rl_ReferMain = findViewById(R.id.fare_breakup_referpoints_layout) as RelativeLayout
        Rb_driver = findViewById(R.id.fare_breakup_driver_ratingBar) as RatingBar
        Tv_serviceTax_Title = findViewById(R.id.fare_breakup_serviceTax_label) as CustomTextView
        Tv_serviceTax = findViewById(R.id.fare_breakup_serviceTax_textView) as CustomTextView


        Ll_referralpoints = findViewById(R.id.fare_breakup_referralPoints_layout) as LinearLayout
        Tv_referralpoints = findViewById(R.id.fare_breakup_referralPoints_textView) as CustomTextView

        val intent = getIntent()
        SrideId_intent = intent.getStringExtra("RideID")
        ScurrencyCode = intent.getStringExtra("CurrencyCode")
        StotalAmount = intent.getStringExtra("TotalAmount")
        StravelDistance = intent.getStringExtra("TravelDistance")
        Sduation = intent.getStringExtra("Duration")
        SwaitingTime = intent.getStringExtra("WaitingTime")
        sDriverName = intent.getStringExtra("DriverName")
        sDriverImage = intent.getStringExtra("DriverImage")
        sDriverRating = intent.getStringExtra("DriverRating")
        sDriverLat = intent.getStringExtra("DriverLatitude")
        sDriverLong = intent.getStringExtra("DriverLongitude")
        sUserLat = intent.getStringExtra("UserLatitude")
        sUserLong = intent.getStringExtra("UserLongitude")
        sSubTotal = intent.getStringExtra("SubTotal")
        sServiceTax = intent.getStringExtra("ServiceTax")
        sTotalPayment = intent.getStringExtra("TotalPayment")
        CouponDiscount = intent.getStringExtra("CouponDiscount")
        sServiceTax_percentage = intent.getStringExtra("ServiceTax_Percent")
        ReferPoints_Total = intent.getStringExtra("ReferPoints_Total")
        number_of_points = intent.getStringExtra("number_of_points")
        amount_per_point = intent.getStringExtra("amount_per_point")

        Log.e("TOTALREFERPOINTS", ReferPoints_Total + "------" + number_of_points + "------" + amount_per_point)




        currencycode = Currency.getInstance(getLocale(ScurrencyCode))

        Picasso.with(this@FareBreakUp).invalidate(sDriverImage)
        Picasso.with(this@FareBreakUp).load(sDriverImage).into(Im_DriverImage)
        Tv_DriverName!!.text = sDriverName
        if (sDriverRating.length > 0) {
            Rb_driver!!.rating = java.lang.Float.parseFloat(sDriverRating)
        }
        //StotalAmount
        Tv_baseFare!!.text = currencycode!!.symbol + sSubTotal
        Tv_discount!!.text = CouponDiscount
        Tv_duration!!.text = Sduation
        Tv_waiting!!.text = SwaitingTime
        Tv_timeTravel!!.text = StravelDistance
        //sSubTotal

        if (is_Checked_points.equals("Yes")) {
            fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
            Log.e("FINALCONVERSION", fianl_conversion.toString())
            Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
        } else {
            Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
        }


        Tv_serviceTax!!.text = currencycode!!.symbol + sServiceTax
        Tv_TripTotal!!.text = currencycode!!.symbol + (java.lang.Double.parseDouble(sTotalPayment) + java.lang.Double.parseDouble(CouponDiscount))

        Tv_serviceTax_Title!!.text = "Service Tax ($sServiceTax_percentage%)"


        if (ReferPoints_Total.equals("")) {
            Rl_ReferMain!!.visibility = View.GONE
            Ll_referralpoints!!.visibility = View.GONE
        } else {
            Tv_Referpoints_Amount!!.text = ReferPoints_Total
            // Ll_referralpoints!!.visibility = View.VISIBLE
            points_conversion = (ReferPoints_Total.toFloat() / number_of_points.toFloat()).toString()
            val used_points = (sTotalPayment.toFloat() * amount_per_point.toFloat()).toString()
            if(used_points.toFloat() >= ReferPoints_Total.toFloat()){
                Tv_referralpoints!!.setText(ReferPoints_Total)
            }else{
                Tv_referralpoints!!.setText(used_points)
            }

           // Tv_referralpoints!!.setText("US $" + ReferPoints_Total.toFloat())

        }

        if (number_of_points.equals("")) {
            Rl_ReferMain!!.visibility = View.GONE
        } else {
            tv_note_referpoints!!.text = "Note : " + number_of_points + " points equals to " + " $ " + amount_per_point
        }


    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@FareBreakUp)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Tip Post Request-----------------
    private fun postRequest_Tip(Url: String, tipStatus: String) {
        dialog = Dialog(this@FareBreakUp)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))


        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = SrideId_intent
        if (tipStatus.equals("Apply", ignoreCase = true)) {
            jsonParams["tips_amount"] = Et_tip_Amount!!.text.toString()
        }

        mRequest = ServiceRequest(this@FareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                var sStatus = ""
                var sResponse = ""
                var sTipAmount = ""
                try {

                    val `object` = JSONObject(response)
                    sStatus = `object`.getString("status")
                    if (sStatus.equals("1", ignoreCase = true)) {

                        val response_Object = `object`.getJSONObject("response")
                        sTipAmount = response_Object.getString("tips_amount")
                        sTotalPayment = response_Object.getString("total")
                        if (tipStatus.equals("Apply", ignoreCase = true)) {
                            sSelectedTipAmount = sTipAmount
                            Tv_tip!!.text = currencycode!!.symbol + sTipAmount
                            Tv_TripTotal!!.text = currencycode!!.symbol + sTotalPayment

                            if (is_Checked_points.equals("Yes")) {
                                fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
                                Log.e("FINALCONVERSION", fianl_conversion.toString())
                                Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                            } else {
                                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            }
                            // Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            Rl_TipMain!!.visibility = View.GONE
                            Rl_tip!!.visibility = View.GONE
                            Ll_TipAmount!!.visibility = View.VISIBLE
                        } else {
                            Tv_TripTotal!!.text = currencycode!!.symbol + sTotalPayment
                            // Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            if (is_Checked_points.equals("Yes")) {
                                fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
                                Log.e("FINALCONVERSION", fianl_conversion.toString())
                                Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                            } else {
                                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            }

                            Cb_tip!!.isChecked = false
                            Et_tip_Amount!!.setText("")
                            Rl_TipMain!!.visibility = View.VISIBLE
                            Ll_TipAmount!!.visibility = View.GONE
                        }

                    } else {
                        sResponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), sResponse)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.fare_breakup_label_complete_payment))
            return true
        }
        return false
    }

    companion object {

        lateinit var farebreakup_class: FareBreakUp
        private var Ll_RemoveTip: LinearLayout? = null
        private var Rl_TipMain: RelativeLayout? = null
        private var Rl_ReferMain: RelativeLayout? = null

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }


        fun invisibleTips() {
            Ll_RemoveTip!!.visibility = View.INVISIBLE
            Rl_TipMain!!.visibility = View.INVISIBLE
        }
    }


    //-----------------------Tip Post Request-----------------
    private fun postRequest_PayByREFERPoints(Url: String) {
        dialog = Dialog(this@FareBreakUp)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))

        System.out.println("PayByREFERPointsPARAMS " + UserID + "----" + SrideId_intent + "----" + total_and_refer + "----" + is_Checked_points)

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["pay_by_points"] = is_Checked_points


        mRequest = ServiceRequest(this@FareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {


                System.out.println("PayByREFERPointsResponse " + response)

                var Sstatus = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("0", ignoreCase = true)) {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    } else if (Sstatus.equals("1", ignoreCase = true)) {
                        //Updating wallet amount on Navigation Drawer Slide

                        NavigationDrawer.navigationNotifyChange()
                        val mDialog = PkDialog(this@FareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_wallet_success))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            FareBreakUp.farebreakup_class.finish()
                            val intent = Intent(this@FareBreakUp, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else if (Sstatus.equals("2", ignoreCase = true)) {

                        FareBreakUp.invisibleTips()

                        val unbill_amount = `object`.getString("unbill_amount")
                        //Updating wallet amount on Navigation Drawer Slide

                        val mDialog = PkDialog(this@FareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                            passIntent.putExtra("RideID", SrideId_intent)
                            passIntent.putExtra("TotalAmount", unbill_amount)
                            startActivity(passIntent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


}
