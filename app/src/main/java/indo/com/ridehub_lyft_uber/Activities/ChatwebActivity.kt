package indo.com.ridehub_lyft_uber.Activities

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.util.Log
import android.view.View
import android.webkit.SslErrorHandler
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import indo.com.ridehub_lyft_uber.R



class ChatwebActivity : AppCompatActivity() {

    private val TAG = "WebViewLog"
    private var progressBar: ProgressDialog? = null
    private var webview: WebView? = null
    private var editText: EditText? = null
    private var button: Button? = null
    lateinit var data: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatwebjava)
        webview = findViewById<View>(R.id.webView) as WebView?
        editText = findViewById<View>(R.id.editText) as EditText
        button = findViewById<View>(R.id.button) as Button
        button!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                editText!!.setText("")
            }
        })

        data = "<script>\n" +
                "(function(t,a,l,k,j,s){\n" +
                "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
                ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
                ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
                "</script>\n" +
                "\n" +
                "<!-- container element in which TalkJS will display a chat UI -->\n" +
                "<div id=\"talkjs-container\" style=\"width: 100%; margin: 0px; height: 500px\"><i>Loading chat...</i></div>\n" +
                "\n" +
                "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
                "<script>\n" +
                "Talk.ready.then(function() {\n" +
                "    var me = new Talk.User({\n" +
                "        id: \"654321\",\n" +
                "        name: \"Sebastian\",\n" +
                "        role: \"user\",\n" +
                "        email: \"Sebastian@example.com\",\n" +
                "        photoUrl: \"https://demo.talkjs.com/img/sebastian.jpg\",\n" +
                "        welcomeMessage: \"Hey, how can I help?\"\n" +
                "    });\n" +
                "    window.talkSession = new Talk.Session({\n" +
                "        appId: \"w4zp7QVv\",\n" +
                "        me: me\n" +
                "    });\n" +
                "    var other = new Talk.User({\n" +
                "        id: \"123456\",\n" +
                "        name: \"Alice\",\n" +
                "        role: \"user\",\n" +
                "        email: \"alice@example.com\",\n" +
                "        photoUrl: \"https://demo.talkjs.com/img/alice.jpg\",\n" +
                "        welcomeMessage: \"Hey there! How are you? :-)\"\n" +
                "    });\n" +
                "\n" +
                "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
                "    conversation.setParticipant(me);\n" +
                "    conversation.setParticipant(other);\n" +
                "    var inbox = talkSession.createInbox({selected: conversation});\n" +
                "    inbox.mount(document.getElementById(\"talkjs-container\"));\n" +
                "});\n" +
                "</script>\n"


        val settings = webview!!.settings
        settings.javaScriptEnabled = true
        webview!!.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        progressBar = ProgressDialog.show(this@ChatwebActivity, "WebView Android", "Loading...")

        webview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                Log.i(TAG, "shouldOverrideUrlLoading() DEPRECATED Processing webview url click...")
                writeToLog("Processing webview url $url")
                if (url.contains("moneyintoken")) {
                    Log.i(TAG, "****contiene token ")
                }

                view.loadUrl(url)

                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    writeToLog("shouldOverrideUrlLoading() Processing webview url " + request.getUrl().toString())

                    if (request.getUrl().toString().contains("moneyintoken")) {
                        Log.i(TAG, "2 ****contiene token ")
                    }

                } else {
                    writeToLog("Processing webview url " + request.toString())

                    if (request.toString().contains("moneyintoken")) {
                        Log.i(TAG, "2 ****contiene token ")
                    }
                }


                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.i(TAG, "Finished loading URL: $url")
                writeToLog("Finished loading URL: $url")
                if (progressBar!!.isShowing()) {
                    progressBar!!.dismiss()
                }
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Log.e(TAG, "onReceivedError DEPRECATED Error: $description")
                Toast.makeText(applicationContext, "Oh no! $description", Toast.LENGTH_SHORT).show()
                writeToLog("Error: $description", 1)
            }


            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
                super.onPageStarted(view, url, favicon)
                val newUrl = view.url
                Log.i(TAG, "onPageStarted() URL: $url newURL: $newUrl")
                writeToLog("onPageStarted() URL: $url newURL: $newUrl")
            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                var message = "SSL Certificate error."
                when (error.getPrimaryError()) {
                    SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                    SslError.SSL_EXPIRED -> message = "The certificate has expired."
                    SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                    SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
                }
                message += "\"SSL Certificate Error\" Do you want to continue anyway?.. YES"

                writeToLog(message, 1)
                handler.proceed()

                Log.e(TAG, "onReceivedError DEPRECATED Error: $message")
            }

        }
        // webview.loadUrl(url);

        webview!!.loadDataWithBaseURL("https://talkjs.com/dashboard/login", data, "text/html", "UTF-8", "")
    }

    private fun writeToLog(msg: String) {
        writeToLog(msg, 0)
    }

    private fun writeToLog(msg: String, error: Int) {
        var color = "#00AA00"
        if (error == 1) color = "#00AA00"
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            editText!!.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText!!.getText().toString() + "<br>", Html.FROM_HTML_MODE_LEGACY))
        } else {
            editText!!.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText!!.getText().toString() + "<br>"))
        }
    }

}
