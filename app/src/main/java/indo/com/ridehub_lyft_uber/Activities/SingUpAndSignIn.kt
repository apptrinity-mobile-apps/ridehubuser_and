package indo.com.ridehub_lyft_uber.Activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.widget.Button
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R

class SingUpAndSignIn : ActivityHockeyApp() {
    private var signin: Button? = null
    private var register: Button? = null

    private var Str_Hash = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signin_and_signup)
        activty = this
        initialize()
        signin!!.setOnClickListener {
            signin!!.setTextColor(getResources().getColor(R.color.border_color))
            register!!.setTextColor(getResources().getColor(R.color.white))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                signin!!.background = getResources().getDrawable(R.drawable.button_curve_background_blue2)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                register!!.background = getResources().getDrawable(R.drawable.button_curve_login_background_blue)
            }
            val i = Intent(this@SingUpAndSignIn, LoginPage::class.java)
            i.putExtra("HashKey", Str_Hash)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        register!!.setOnClickListener {
            register!!.setTextColor(getResources().getColor(R.color.border_color))
            signin!!.setTextColor(getResources().getColor(R.color.white))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                signin!!.background = getResources().getDrawable(R.drawable.button_curve_login_background_blue)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                register!!.background = getResources().getDrawable(R.drawable.button_curve_background_blue2)
            }
            val i = Intent(this@SingUpAndSignIn, RegisterPage::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    private fun initialize() {
        signin = findViewById(R.id.signin_main_button) as Button
        register = findViewById(R.id.register_main_button) as Button

        val i = getIntent()

        if(i.getStringExtra("HashKey")!= null){
            Str_Hash = i.getStringExtra("HashKey")
            println("Str_Hash--------------------${Str_Hash}")
        }


        signin!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")
        register!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")
    }

    companion object {
        lateinit var activty: SingUpAndSignIn
    }

}