package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.ridehub_lyft_uber.Adapters.MyRideStripeCardListAdapter
import indo.com.ridehub_lyft_uber.PojoResponse.StripeCardListPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.subclass.ActivitySubClass
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class StripeCardsListFareBreakUp : ActivitySubClass() {

    internal lateinit var UserID: String
    internal lateinit var EmailID: String
    internal lateinit var mobilenum: String
    private var session: SessionManager? = null
    private var back: RelativeLayout? = null
    private var lv_allcards: ListView? = null
    private var tv_add_new_card: ImageView? = null
    private var tv_nocards: CustomTextView? = null
    private var cd: ConnectionDetector? = null
    private var isInternetPresent: Boolean? = false
    private var dialog: Dialog? = null
    private var mRequest: ServiceRequest? = null

    private var Str_mobileID = ""
    private var SrideId_intent = ""
    private var card_id = ""

    private var itemStripeCardlist: ArrayList<StripeCardListPojo>? = null

    private var isPaymentAvailable = false
    private var adapter: MyRideStripeCardListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.stripe_cardslist)

        myride_paymentStripe_class = this@StripeCardsListFareBreakUp
        initialize()
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()
        EmailID = user[SessionManager.KEY_EMAIL].toString()
        mobilenum = user[SessionManager.KEY_PHONENO].toString()
        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        tv_add_new_card!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@StripeCardsListFareBreakUp, FareBreakUpPaymentWebView::class.java)
            intent.putExtra("MobileID", Str_mobileID)
            intent.putExtra("RideID", SrideId_intent)
            startActivity(intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        })


    }


    private fun initialize() {
        session = SessionManager(this@StripeCardsListFareBreakUp)
        cd = ConnectionDetector(this@StripeCardsListFareBreakUp)
        isInternetPresent = cd!!.isConnectingToInternet
        back = findViewById(R.id.my_rides_payment_header_back_layout) as RelativeLayout
        lv_allcards = findViewById(R.id.lv_allcards) as ListView
        tv_add_new_card = findViewById(R.id.tv_add_new_card) as ImageView
        tv_nocards = findViewById(R.id.tv_nocards) as CustomTextView
        itemStripeCardlist = ArrayList<StripeCardListPojo>()

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        val i = intent
        Str_mobileID = i.getStringExtra("MobileID")
        SrideId_intent = i.getStringExtra("RideID")


        if (isInternetPresent!!) {
            postRequest_StripeCardList(Iconstant.makepayment_stripe_cardList)
        } else {
            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
        }


        lv_allcards!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            card_id = itemStripeCardlist!![position].getCard_id().toString()
            Log.e("CARDID", "" + card_id)

            MakePayment_Stripe(Iconstant.makepayment_autoDetect_url)
        }


    }


    private fun postRequest_StripeCardList(Url: String) {
        dialog = Dialog(this@StripeCardsListFareBreakUp)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------StripeCardList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(this@StripeCardsListFareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {
                println("-------------StripeCardList Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")


                        if (response_object.length() > 0) {
                            val card_array = response_object.getJSONArray("cardlist")
                            if (card_array.length() > 0) {
                                itemStripeCardlist!!.clear()
                                for (i in 0 until card_array.length()) {
                                    val reason_object = card_array.getJSONObject(i)
                                    val pojo = StripeCardListPojo()
                                    pojo.setLast4_digits(reason_object.getString("last4_digits"))
                                    pojo.setCredit_card_type(reason_object.getString("credit_card_type"))
                                    pojo.setExp_year(reason_object.getString("exp_year"))
                                    pojo.setExp_month(reason_object.getString("exp_month"))
                                    pojo.setCard_id(reason_object.getString("card_id"))

                                    itemStripeCardlist!!.add(pojo)
                                }
                                adapter = MyRideStripeCardListAdapter(this@StripeCardsListFareBreakUp, itemStripeCardlist!!)
                                lv_allcards!!.adapter = adapter
                                isPaymentAvailable = true
                            } else {

                                tv_nocards!!.visibility = View.VISIBLE
                                isPaymentAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                    if (Sstatus.equals("1", ignoreCase = true) && isPaymentAvailable) {

                    }

                } catch (e: JSONException) {
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@StripeCardsListFareBreakUp)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    private fun MakePayment_Stripe(Url: String) {
        dialog = Dialog(this@StripeCardsListFareBreakUp)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)
        println("-------------MakePayment Auto-Detect Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["stripeToken"] = card_id
        mRequest = ServiceRequest(this@StripeCardsListFareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {
                println("-------------MakePayment Auto-Detect Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog(this@StripeCardsListFareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            StripeCardsListFareBreakUp.myride_paymentStripe_class.finish()
                            FareBreakUp.farebreakup_class.finish()
                            FareBreakUpPaymentList.myride_paymentList_class.finish()
                            val intent = Intent(this@StripeCardsListFareBreakUp, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                }

                dialog!!.dismiss()
            }

           override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    companion object {

        lateinit var myride_paymentStripe_class: StripeCardsListFareBreakUp
    }


}

