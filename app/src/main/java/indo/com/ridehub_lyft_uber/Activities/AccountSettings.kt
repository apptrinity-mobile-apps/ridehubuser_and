package indo.com.ridehub_lyft_uber.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.SessionManager


class AccountSettings : AppCompatActivity() {
    private var Iv_close: ImageView? = null
    private var Iv_banner: ImageView? = null
    private var Tv_title: TextView? = null
    private var Tv_message: TextView? = null
    private var toggle_ridehub: Switch? = null
    private var toggle_uber: Switch? = null
    private var toggle_lyft: Switch? = null
    private var toggle_publictransit: Switch? = null
    private var ll_invite_a_frnd: LinearLayout? = null
    private var ll_intro_screens: LinearLayout? = null
    private var ll_feedbackform: LinearLayout? = null
    private var Vi_space: View? = null

    private var sTitle = ""
    private var sMessage = ""
    private var mBanner = ""

    private var ridehubservice: Boolean = false
    private var uberservice: Boolean = false
    private var lyftservice: Boolean = false
    private var publictransitservice: Boolean = false

    private var session: SessionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        initialize()


    }

    private fun initialize() {

        session = SessionManager(this@AccountSettings)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {

            onBackPressed() }

        toggle_ridehub = findViewById(R.id.toggle_ridehub) as Switch
        toggle_uber = findViewById(R.id.toggle_uber) as Switch
        toggle_lyft = findViewById(R.id.toggle_lyft) as Switch
        toggle_publictransit = findViewById(R.id.toggle_publictransit) as Switch
        ll_invite_a_frnd = findViewById(R.id.ll_invite_a_frnd) as LinearLayout
        ll_intro_screens = findViewById(R.id.ll_intro_screens) as LinearLayout
        ll_feedbackform = findViewById(R.id.ll_feedbackform) as LinearLayout


        ridehubservice  = session!!.getRidehubService()
        uberservice  = session!!.getUberService()
        lyftservice  = session!!.getLyftService()
        publictransitservice  = session!!.getPublicTransitService()

        toggle_ridehub!!.isChecked = ridehubservice
        toggle_uber!!.isChecked = uberservice
        toggle_lyft!!.isChecked = lyftservice
        toggle_publictransit!!.isChecked = publictransitservice


        toggle_ridehub!!.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                Log.e("toggle_ridehub",""+isChecked)
                session!!.setRidehubService(isChecked)


            }
        })
        toggle_uber!!.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                Log.e("toggle_uber",""+isChecked)
                session!!.setUberService(isChecked)


            }
        })
        toggle_lyft!!.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                Log.e("toggle_lyft",""+isChecked)
                session!!.setLyftService(isChecked)

            }
        })
        toggle_publictransit!!.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                Log.e("toggle_publictransit",""+isChecked)
                session!!.setPublicTransitService(isChecked)

            }
        })

        ll_invite_a_frnd!!.setOnClickListener {
            val settings_invite_intent = Intent(this@AccountSettings, InviteAndEarn::class.java)
            startActivity(settings_invite_intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        ll_intro_screens!!.setOnClickListener {
            val settings_intro_intent = Intent(this@AccountSettings, SettingsLauncherActivity::class.java)
            startActivity(settings_intro_intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        ll_feedbackform!!.setOnClickListener {
            val settings_feedback_intent = Intent(this@AccountSettings, FeedBackActivity::class.java)
            startActivity(settings_feedback_intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }


    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            return true
        }
        return false
    }
}
