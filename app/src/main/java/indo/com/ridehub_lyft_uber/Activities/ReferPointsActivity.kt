package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.Adapters.ReferPointsAdapter
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.PojoResponse.ReferPointsPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class ReferPointsActivity : ActivityHockeyApp(), View.OnClickListener {
    override fun onClick(v: View?) {

    }

    internal val PERMISSION_REQUEST_CODE = 111
    internal lateinit var Rl_whatsApp: LinearLayout
    internal lateinit var Rl_messenger: LinearLayout
    internal lateinit var Rl_sms: LinearLayout
    internal lateinit var Rl_email: LinearLayout
    internal lateinit var Rl_twitter: LinearLayout
    internal lateinit var Rl_facebook: LinearLayout
    internal lateinit var ll_shareinvitecode: CustomTextView
    internal var sCurrencySymbol = ""
    internal lateinit var tv_invite_now: CustomTextView
    internal lateinit var tv_shareride: CustomTextView
    private var back: RelativeLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var Tv_friends_earn: TextView? = null
    private var Tv_you_earn: TextView? = null
    private var Tv_referral_code: TextView? = null
    //private RelativeLayout Rl_whatsApp, Rl_messenger, Rl_sms, Rl_email, Rl_twitter, Rl_facebook;
    private var mRequest: ServiceRequest? = null
    private var UserID = ""
    private var isdataPresent = false
    private var Sstatus = ""
    private var friend_earn_amount = ""
    private var you_earn_amount = ""
    private var friends_rides = ""
    private var ScurrencyCode = ""
    private var referral_code = ""
    private var sShareLink = ""
    private val package_name = "com.indobytes.app.ridehubuser"

    internal lateinit var refer_adapter: ReferPointsAdapter
    internal lateinit var refer_points_listview: ListView
    internal lateinit var refer_points_subtotal_textView: CustomTextView
    internal lateinit var tv_nodata: CustomTextView

    public var itemlist_referpoints: ArrayList<ReferPointsPojo> = ArrayList<ReferPointsPojo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.freeride_ride_hub);
        val config = getResources().getConfiguration()
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.refer_points_ridehub)
        } else {
            setContentView(R.layout.refer_points_ridehub)
        }
        initialize()

        //Start XMPP Chat Service
        ChatService.startUserAction(this@ReferPointsActivity)

        back!!.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }


    }

    private fun initialize() {
        session = SessionManager(this@ReferPointsActivity)
        cd = ConnectionDetector(this@ReferPointsActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.invite_earn_header_back_layout) as RelativeLayout
        refer_points_listview = findViewById(R.id.refer_points_listview) as ListView
        tv_nodata = findViewById(R.id.tv_nodata) as CustomTextView
        refer_points_subtotal_textView = findViewById(R.id.refer_points_subtotal_textView) as CustomTextView

        /* back = findViewById(R.id.invite_earn_header_back_layout) as RelativeLayout
         Tv_friends_earn = findViewById(R.id.invite_earn_friend_earn_textview) as TextView
         Tv_you_earn = findViewById(R.id.invite_earn_you_earn_textview) as TextView
         Tv_referral_code = findViewById(R.id.invite_earn_referral_code_textview) as TextView
        // tv_invite_now = findViewById(R.id.tv_invite_now) as CustomTextView
         ll_shareinvitecode = findViewById(R.id.ll_shareinvitecode) as CustomTextView
         tv_shareride = findViewById(R.id.tv_shareride) as CustomTextView*/


// get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        if (isInternetPresent!!) {
            displayInvite_Request(Iconstant.refer_points_url)
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
        }


    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ReferPointsActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }


    //-----------------------Display Invite Amount Post Request-----------------
    private fun displayInvite_Request(Url: String) {
        val dialog = Dialog(this@ReferPointsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(this@ReferPointsActivity.getResources().getString(R.string.action_pleasewait))

        println("-------------Referpoints Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID

        mRequest = ServiceRequest(this@ReferPointsActivity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------Referpoints Response----------------$response")

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (`object`.length() > 0) {

                        val data_array = `object`.getJSONArray("data")

                        for (i in 0 until data_array.length()) {
                            val ride_object = data_array.getJSONObject(i)

                            val pojo = ReferPointsPojo()
                            pojo.setRide_id(ride_object.getString("ride_id"))
                            pojo.setDate(ride_object.getString("date"))
                            pojo.setRefer_from(ride_object.getString("refer_from"))
                            pojo.setRefer_to(ride_object.getString("refer_to"))
                            pojo.setPoints(ride_object.getString("points"))
                            pojo.setReferrer_name(ride_object.getString("referrer_name"))
                            pojo.setTrans_type(ride_object.getString("trans_type"))
                            pojo.setType(ride_object.getString("type"))
                            pojo.setLevel(ride_object.getString("level"))

                            itemlist_referpoints.add(pojo)

                        }


                        refer_adapter = ReferPointsAdapter(this@ReferPointsActivity!!, itemlist_referpoints)
                        refer_points_listview!!.adapter = refer_adapter

                        val total_points = `object`.getString("total_points")
                        refer_points_subtotal_textView.text = total_points

                        /*for (n in 0 until data_array.length()) {
                            val type_obj = data_array.getJSONObject(n)
                            val refer_from = type_obj.getString("refer_from")
                            val refer_to = type_obj.getString("refer_to")
                            val points = type_obj.getString("points")
                            val referrer_name = type_obj.getString("referrer_name")
                            val type = type_obj.getString("type")
                            val ride_id = type_obj.getString("ride_id")
                            val trans_type = type_obj.getString("trans_type")
                            val level = type_obj.getString("level")

                            Log.e("RFEREPOINTS_LOG",refer_from+""+refer_to+""+points+""+referrer_name+""+type+""+ride_id+""+trans_type+""+level)


                        }*/


                    } else {
                        isdataPresent = false
                        tv_nodata.visibility = View.VISIBLE
                        tv_nodata.setText("No Points Available")
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }

}
