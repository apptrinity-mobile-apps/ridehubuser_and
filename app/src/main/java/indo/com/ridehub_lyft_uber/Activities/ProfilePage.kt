
package indo.com.ridehub_lyft_uber.Activities
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import com.countrycodepicker.CountryPicker
import com.countrycodepicker.CountryPickerListener
import com.squareup.picasso.Picasso
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.facebook.Util
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.HockeyApp.FragmentActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ProfilePage : FragmentActivityHockeyApp() {

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var session: SessionManager? = null
    private var layout_changePassword: RelativeLayout? = null
    private var back: RelativeLayout? = null
    private var logout: RelativeLayout? = null
    private var profile_name: TextView? = null
    private var tv_email: TextView? = null
    private var profile_icon: RoundedImageView? = null
    private var Et_name: EditText? = null
    private var UserID = ""
    private var UserName = ""
    private var UserMobileno = ""
    private var UserCountyCode = ""
    private var UserEmail = ""
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal lateinit var picker: CountryPicker

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.profilepage)
        context = getApplicationContext()
        initialize()

        //Start XMPP Chat Service
        ChatService.startUserAction(this@ProfilePage)

        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()
        }


        logout!!.setOnClickListener {
            cd = ConnectionDetector(this@ProfilePage)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {

                val mDialog = PkDialog(this@ProfilePage)
                mDialog.setDialogTitle(getResources().getString(R.string.profile_lable_logout_title))
                mDialog.setDialogMessage(getResources().getString(R.string.profile_lable_logout_message))
                mDialog.setPositiveButton(getResources().getString(R.string.profile_lable_logout_yes), View.OnClickListener {
                    mDialog.dismiss()
                    postRequest_Logout(Iconstant.logout_url)
                })
                mDialog.setNegativeButton(getResources().getString(R.string.profile_lable_logout_no), View.OnClickListener { mDialog.dismiss() })
                mDialog.show()

            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }

        layout_changePassword!!.setOnClickListener {
            val intent = Intent(this@ProfilePage, ChangePassword::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        Et_name!!.setOnEditorActionListener { v, actionId, event ->
            var handled = false

            if (actionId == EditorInfo.IME_ACTION_SEND) {
                cd = ConnectionDetector(this@ProfilePage)
                isInternetPresent = cd!!.isConnectingToInternet
                CloseKeyboard(Et_name!!)

                if (Et_name!!.text.toString().length == 0) {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_lable_error_name))
                } else {
                    if (isInternetPresent!!) {
                        postRequest_editUserName(Iconstant.profile_edit_userName_url)
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                    }
                }
                handled = true
            }
            handled
        }

        /* Et_country_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
               @Override
               public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                   boolean handled = false;
                   if (actionId == EditorInfo.IME_ACTION_NEXT ) {
                       Et_mobileno.requestFocus();
                       handled = true;
                   }
                   return handled;
               }
           });*/


        Rl_country_code!!.setOnClickListener { picker.show(getSupportFragmentManager(), "COUNTRY_PICKER") }

        picker.setListener(object : CountryPickerListener {
            override fun onSelectCountry(name: String, code: String, dialCode: String) {
                picker.dismiss()
                Tv_countryCode!!.text = dialCode.replace("+", "")
                Et_mobileno!!.requestFocus()
            }
        })

        Et_mobileno!!.setOnEditorActionListener { v, actionId, event ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                cd = ConnectionDetector(this@ProfilePage)
                isInternetPresent = cd!!.isConnectingToInternet
                CloseKeyboard(Et_name!!)

                if (!isValidPhoneNumber(Et_mobileno!!.text.toString())) {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_lable_error_mobile))
                } else /*if (Tv_countryCode!!.text.toString().length == 0) {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_lable_error_mobilecode))
                } else*/ {
                    if (isInternetPresent!!) {
                        postRequest_editMobileNumber(Iconstant.profile_edit_mobileNo_url)
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                    }
                }
                handled = true
            }
            handled
        }

    }

    private fun initialize() {
        session = SessionManager(this@ProfilePage)
        picker = CountryPicker.newInstance("Select Country")

        tv_email = findViewById(R.id.myprofile_emailid_textview) as TextView
        Rl_country_code = findViewById(R.id.myprofile_textView_country_code_layout) as RelativeLayout
        profile_icon = findViewById(R.id.profile_icon) as RoundedImageView
        Tv_countryCode = findViewById(R.id.myprofile_country_code_textview) as TextView
        back = findViewById(R.id.myprofile_header_back_layout) as RelativeLayout
        Et_mobileno = findViewById(R.id.myprofile_edit_phoneno_editText) as EditText
        Et_name = findViewById(R.id.myprofile_username_editText) as EditText
        layout_changePassword = findViewById(R.id.myprofile_changepassword_layout) as RelativeLayout
        logout = findViewById(R.id.myprofile_logout_button) as RelativeLayout
        profile_name = findViewById(R.id.tv_profile_name) as TextView

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()
        UserName = user[SessionManager.KEY_USERNAME].toString()
        UserMobileno = user[SessionManager.KEY_PHONENO].toString()
        UserEmail = user[SessionManager.KEY_EMAIL].toString()
        UserCountyCode = user[SessionManager.KEY_COUNTRYCODE].toString()
        val UserprofileImage = user[SessionManager.KEY_USERIMAGE]


        Picasso.with(context).load(UserprofileImage.toString()).placeholder(R.drawable.no_profile_image_avatar_icon).into(profile_icon)

        //Et_name.setImeActionLabel("Send", KeyEvent.);

        tv_email!!.text = UserEmail
        Et_name!!.setText(UserName)
        profile_name!!.setText(UserName)
        Et_mobileno!!.setText(UserMobileno)
        Tv_countryCode!!.text = UserCountyCode.replace("+", "")

        //----Code to make EditText Cursor at End of the Text------
        Et_name!!.setSelection(Et_name!!.text.length)
        Et_mobileno!!.setSelection(Et_mobileno!!.text.length)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ProfilePage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------------Close KeyBoard Method-----------
    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Show Dialog Method-----------
    private fun showDialog(data: String) {
        dialog = Dialog(this@ProfilePage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = data
    }

    //-----------------------Edit UserName Request-----------------
    private fun postRequest_editUserName(Url: String) {
        showDialog(getResources().getString(R.string.action_updating))
        println("---------------Edit Username Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["user_name"] = Et_name!!.text.toString()

        mRequest = ServiceRequest(this@ProfilePage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                println("---------------Edit Username Response-----------------$response")
                var Sstatus = ""
                var Smessage = ""
                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("response")

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()

                if (Sstatus.equals("1", ignoreCase = true)) {
                    session!!.setUserNameUpdate(Et_name!!.text.toString())
                    Alert(getResources().getString(R.string.action_success), getResources().getString(R.string.profile_lable_username_success))
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage)
                }
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------Edit MobileNumber Request-----------------
    private fun postRequest_editMobileNumber(Url: String) {
        showDialog(getResources().getString(R.string.action_updating))
        println("---------------Edit MobileNumber Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["country_code"] = "+" + Tv_countryCode!!.text.toString()
        //jsonParams["country_code"] = "+" + "91"
        jsonParams["phone_number"] = Et_mobileno!!.text.toString()
        jsonParams["otp"] = ""

        mRequest = ServiceRequest(this@ProfilePage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("---------------Edit MobileNumber Response-----------------$response")
                var Sstatus = ""
                var Smessage = ""
                var Sotp = ""
                var Sotp_status = ""
                var Scountry_code = ""
                var Sphone_number = ""
                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("response")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Sotp = `object`.getString("otp")
                        Sotp_status = `object`.getString("otp_status")
                        Scountry_code = `object`.getString("country_code")
                        Sphone_number = `object`.getString("phone_number")
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
                if (Sstatus.equals("1", ignoreCase = true)) {
                    val intent = Intent(this@ProfilePage, ProfileOtpPage::class.java)
                    intent.putExtra("Otp", Sotp)
                    intent.putExtra("Otp_Status", Sotp_status)
                    intent.putExtra("CountryCode", Scountry_code)
                    intent.putExtra("Phone", Sphone_number)
                    intent.putExtra("UserID", UserID)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage)
                }
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    fun logoutFromFacebook() {
        Util.clearCookies(this@ProfilePage)
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }


    //-----------------------Logout Request-----------------
    private fun postRequest_Logout(Url: String) {
        showDialog(getResources().getString(R.string.action_logging_out))
        println("---------------LogOut Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["device"] = "ANDROID"

        mRequest = ServiceRequest(this@ProfilePage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("---------------LogOut Response-----------------$response")
                var Sstatus = ""
                var Sresponse = ""
                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Sresponse = `object`.getString("response")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
                if (Sstatus.equals("1", ignoreCase = true)) {
                    logoutFromFacebook()
                    session!!.logoutUser()
                    val local = Intent()
                    local.action = "com.app.logout"
                    this@ProfilePage.sendBroadcast(local)

                    onBackPressed()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse)
                }
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override  fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            return true
        }
        return false
    }

    companion object {
        private var Et_mobileno: EditText? = null
        private var Rl_country_code: RelativeLayout? = null
        private var Tv_countryCode: TextView? = null

        // validating Phone Number
        fun isValidPhoneNumber(target: CharSequence?): Boolean {
            return if (target == null || TextUtils.isEmpty(target) || target.length <= 9) {
                false
            } else {
                android.util.Patterns.PHONE.matcher(target).matches()
            }
        }

        //--------------Update Mobile Number From Profile OTP Page Method-----------
        fun updateMobileDialog(code: String, phone: String) {
            Et_mobileno!!.setText(phone)
            Tv_countryCode!!.text = code.replace("+", "")
        }
    }
}