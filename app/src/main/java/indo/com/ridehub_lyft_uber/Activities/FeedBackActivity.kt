package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.widget.Button
import indo.com.mylibrary.widgets.CustomEdittext
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.SessionManager

class FeedBackActivity : AppCompatActivity() {


    internal lateinit var sessionManager: SessionManager
    internal lateinit var et_user_name: CustomEdittext
    internal lateinit var et_email: CustomEdittext
    internal lateinit var et_comments: CustomEdittext
    internal lateinit var tv_submit: Button
    internal lateinit var my_loader: Dialog

    internal lateinit var user_name_stg: String
    internal lateinit var email_stg: String
    internal lateinit var comments_stg: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)


        et_user_name = findViewById(R.id.et_user_name) as CustomEdittext
        et_email = findViewById(R.id.et_email) as CustomEdittext
        et_comments = findViewById(R.id.et_comments) as CustomEdittext
        tv_submit = findViewById(R.id.tv_submit) as Button


        val toolbar = findViewById(R.id.toolbar) as Toolbar

       // myloading()

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        sessionManager = SessionManager(this)
        if (sessionManager.isLoggedIn) {

            //et_user_name.setText(sessionManager.isUserName)
          //  et_email.setText(sessionManager.isUserId)


        }else{
            /*val home_intent = Intent(this@ContactUsActivity, ContactUsActivity::class.java)
            finish()
            startActivity(home_intent)*/
        }

       /* tv_submit.setOnClickListener {

            if (sessionManager.isLoggedIn) {
                et_user_name.setText(sessionManager.isUserName)
                et_email.setText(sessionManager.isEmail)
                comments_stg = et_comments!!.getText().toString()
                if(hasValidCredentials2()){
                    callContactCommentsAPI(sessionManager.isUserName,sessionManager.isEmail,comments_stg)
                }


            }else {
                user_name_stg = et_user_name.getText().toString()
                email_stg = et_email!!.getText().toString()
                comments_stg = et_comments!!.getText().toString()

                if (hasValidCredentials()) {
                    callContactCommentsAPI(user_name_stg,email_stg,comments_stg)
                }



            }


        }*/


    }


    /*private fun callContactCommentsAPI(name: String, email: String, comments: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postContactComment(name, email,comments)
        Log.d("ContactComments", name + "------" + email+"------"+comments)
        call.enqueue(object : Callback<getNewsListResponse> {
            override fun onResponse(call: Call<getNewsListResponse>, response: retrofit2.Response<getNewsListResponse>?) {
                if (response != null && response.body()!!.status!!.equals("1")) {
                    my_loader.dismiss()
                    Toast.makeText(this@FeedBackActivity, response!!.body()!!.result.toString(), Toast.LENGTH_SHORT).show()

                }else{
                    Toast.makeText(this@FeedBackActivity, response!!.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<getNewsListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }*/


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(user_name_stg))
            et_user_name!!.setError("Name Required")
        else if (TextUtils.isEmpty(email_stg))
            et_email!!.setError("Email Required")
        else if (TextUtils.isEmpty(comments_stg))
            et_comments!!.setError("Comments Required")
        else
            return true
        return false
    }

    private fun hasValidCredentials2(): Boolean {
        if (TextUtils.isEmpty(comments_stg))
            et_comments!!.setError("Comments Required")
        else
            return true
        return false
    }

    /*private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }*/

}
