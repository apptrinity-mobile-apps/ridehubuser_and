package indo.com.ridehub_lyft_uber.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.Adapters.HomeMenuListAdapter
import indo.com.ridehub_lyft_uber.Fragments.Fragment_HomePage
import indo.com.ridehub_lyft_uber.HockeyApp.ActionBarActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class NavigationDrawer : ActionBarActivityHockeyApp() {
    internal lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private var mDrawerList: ListView? = null
    private var UserID = ""
    private var UserName = ""
    private var UserMobileno = ""
    private var UserCountyCode = ""
    private var UserEmail = ""
    private var UserorAgent = ""
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    private var title: Array<String>? = null
    private var icon: IntArray? = null
    private var session: SessionManager? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    internal var bookmyride: Fragment = Fragment_HomePage()

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.navigation_drawer)
        context = applicationContext
        session = SessionManager(this@NavigationDrawer)
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]!!
        UserName = user[SessionManager.KEY_USERNAME]!!
        UserMobileno = user[SessionManager.KEY_PHONENO]!!
        UserEmail = user[SessionManager.KEY_EMAIL]!!
        UserCountyCode = user[SessionManager.KEY_COUNTRYCODE]!!
        UserorAgent = user[SessionManager.KEY_AGENT]!!
        println("---------------UserorAgent-----------------$UserorAgent")
        drawerLayout = findViewById(R.id.navigation_drawer) as DrawerLayout
        mDrawer = findViewById(R.id.drawer) as RelativeLayout
        mDrawerList = findViewById(R.id.drawer_listview) as ListView

        /* View decorView = getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
*/

        /*MaterialRippleLayout.on(mDrawerList)
                .rippleColor(R.color.ripple_blue_color)
                .create();*/
        if (savedInstanceState == null) {
            val tx = supportFragmentManager.beginTransaction()
            tx.replace(R.id.content_frame, Fragment_HomePage())
            tx.commit()
        }

        actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name)
        drawerLayout.setDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
             mDrawer!!.background = resources.getDrawable(R.drawable.bg_menu_ridehub2)
         }*/

        /*  if(UserorAgent.equalsIgnoreCase("agent")){
            title = new String[]{"username", getResources().getString(R.string.navigation_label_bookmyride),
                    getResources().getString(R.string.navigation_label_myride),
                    getResources().getString(R.string.navigation_label_money),
                    getResources().getString(R.string.navigation_label_invite),
                    getResources().getString(R.string.navigation_label_ratecard),
                    getResources().getString(R.string.navigation_label_commissions),
                    getResources().getString(R.string.navigation_label_emergency),
                    //getResources().getString(R.string.navigation_label_report_issue),
                    getResources().getString(R.string.navigation_label_aboutus),
                    getResources().getString(R.string.profile_lable_logout_title),
            };


            icon = new int[]{R.drawable.no_profile_image_avatar_icon, R.mipmap.ic_track_live_trip_icon,
                    R.mipmap.ic_mytrips_icon, R.mipmap.payment_icon,
                    R.mipmap.ic_refer_earn_icon, R.mipmap.ic_rate_icon,R.mipmap.ic_pricing_icon, R.mipmap.ic_contactus_icon,R.mipmap.ic_aboutus_icon,R.mipmap.ic_logout_icon};
        }else {*/
        title = arrayOf("username", /*getResources().getString(R.string.navigation_label_bookmyride),*/
                resources.getString(R.string.navigation_label_bookmyride), resources.getString(R.string.navigation_label_myride), /*resources.getString(R.string.navigation_label_wallet),*/ resources.getString(R.string.navigation_label_freerides),resources.getString(R.string.navigation_label_referpoints), /*resources.getString(R.string.navigation_label_money),*/ resources.getString(R.string.navigation_label_invite))
        icon = intArrayOf(R.mipmap.payment_icon, R.mipmap.book_a_ride,
                /*R.mipmap.payment_icon,*/
                R.mipmap.youttrips_icon,/* R.mipmap.payment_icon,*/ R.mipmap.freerides_icon,R.mipmap.freerides_icon,/* R.mipmap.sendgift_icon,*/ R.mipmap.settings_icon)
        //}

        //R.drawable.report_issue_icon,
        mMenuAdapter = HomeMenuListAdapter(context!!, title!!, icon!!)
        mDrawerList!!.adapter = mMenuAdapter
        mMenuAdapter!!.notifyDataSetChanged()


        mDrawerList!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            cd = ConnectionDetector(this@NavigationDrawer)
            isInternetPresent = cd!!.isConnectingToInternet

            val ft = supportFragmentManager.beginTransaction()
            when (position) {

                0 -> {
                     val intent = Intent(this@NavigationDrawer, ProfilePage::class.java)
                     startActivity(intent)
                     overridePendingTransition(R.anim.enter, R.anim.exit)
                }
                1 -> {
                    val tx = supportFragmentManager.beginTransaction()
                    tx.replace(R.id.content_frame, Fragment_HomePage())
                    tx.commit()
                }

                2 -> if (isInternetPresent!!) {
                    val myRide_intent = Intent(this@NavigationDrawer, MyRides::class.java)
                    startActivity(myRide_intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
                /*3 -> if (isInternetPresent!!) {
                    *//* Intent emergency_intent = new Intent(NavigationDrawer.this, CabilyMoney.class);
                    startActivity(emergency_intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);*//*
                    val emergency_intent = Intent(this@NavigationDrawer, InviteAndEarn::class.java)
                    startActivity(emergency_intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    Toast.makeText(context, "Send a Gift", Toast.LENGTH_SHORT).show()
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }*/

                3 -> if (isInternetPresent!!) {
                    val emergency_intent = Intent(this@NavigationDrawer, InviteAndEarn::class.java)
                    startActivity(emergency_intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    //Toast.makeText(context, "Free Rides", Toast.LENGTH_SHORT).show();
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }

                4 -> if (isInternetPresent!!) {
                    val emergency_intent = Intent(this@NavigationDrawer, ReferPointsActivity::class.java)
                    startActivity(emergency_intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    //Toast.makeText(context, "Free Rides", Toast.LENGTH_SHORT).show();
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }

                /*6 -> if (isInternetPresent!!) {
                    *//* Intent emergency_intent = new Intent(NavigationDrawer.this, CabilyMoney.class);
                    startActivity(emergency_intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);*//*
                    Toast.makeText(context, "Help", Toast.LENGTH_SHORT).show()
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }*/
                5 -> if (isInternetPresent!!) {
                    val invite_intent = Intent(this@NavigationDrawer, AccountSettings::class.java)
                    startActivity(invite_intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            }
            ft.commit()
            mDrawerList!!.setItemChecked(position, true)
            drawerLayout!!.closeDrawer(mDrawer!!)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

	        //noinspection SimplifiableIfStatement
	        if (id == R.id.action_settings) {
	            return true;
	        }*/

        return super.onOptionsItemSelected(item)
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@NavigationDrawer)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //----------Method to Send Email--------
    protected fun sendEmail() {
        val TO = arrayOf(resources.getString(R.string.action_email_toAddress))
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "message/rfc822"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message")
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this@NavigationDrawer, "There is no email client installed.", Toast.LENGTH_SHORT).show()
        }

    }


    //-----------------------Logout Request-----------------
    private fun postRequest_Logout(Url: String) {
        showDialog(resources.getString(R.string.action_logging_out))
        println("---------------LogOut Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["device"] = "ANDROID"

        mRequest = ServiceRequest(this@NavigationDrawer)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("---------------LogOut Response-----------------$response")
                var Sstatus = ""
                var Sresponse = ""
                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Sresponse = `object`.getString("response")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
                if (Sstatus.equals("1", ignoreCase = true)) {
                    logoutFromFacebook()
                    session!!.logoutUser()
                    val local = Intent()
                    local.action = "com.app.logout"
                    this@NavigationDrawer.sendBroadcast(local)

                    onBackPressed()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else {
                    Alert(resources.getString(R.string.action_error), Sresponse)
                }
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //--------------Show Dialog Method-----------
    private fun showDialog(data: String) {
        dialog = Dialog(this@NavigationDrawer)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = data
    }

    fun logoutFromFacebook() {
        // Util.clearCookies(this@NavigationDrawer)
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }

    companion object {
        internal lateinit var drawerLayout: DrawerLayout
        internal var mDrawer: RelativeLayout? = null
        internal var mMenuAdapter: HomeMenuListAdapter? = null

        private var context: Context? = null

        fun openDrawer() {
            drawerLayout.openDrawer(mDrawer!!)
        }

        fun disableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        fun enableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }


        fun navigationNotifyChange() {
            mMenuAdapter!!.notifyDataSetChanged()
        }
    }




}