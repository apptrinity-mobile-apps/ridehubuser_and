package indo.com.ridehub_lyft_uber.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import indo.com.mylibrary.gps.GPSTracker
import indo.com.mylibrary.latlnginterpolation.LatLngInterpolator
import indo.com.mylibrary.latlnginterpolation.MarkerAnimation
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.PojoResponse.CancelTripPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.util.*

class MyRideDetailTrackRide : FragmentActivity(), View.OnClickListener, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private var tv_done: TextView? = null
    private var tv_drivername: TextView? = null
    private var tv_carModel: TextView? = null
    private var tv_carNo: TextView? = null
    private var tv_rating: TextView? = null
    private var driver_image: RoundedImageView? = null
    private var rl_callDriver: ImageView? = null
    private var rl_endTrip: CustomTextView? = null
    private var mMap: GoogleMap? = null
    private val marker: MarkerOptions? = null
    private var gps: GPSTracker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var driverID = ""
    private var driverName = ""
    private var driverImage = ""
    private var driverRating = ""
    private var driverLat: String? = ""
    private var driverLong: String? = ""
    private var driverTime = ""
    private var rideID = ""
    private var driverMobile: String? = ""
    private var driverCar_no = ""
    private var driverCar_model = ""
    private var userLat = ""
    private var userLong = ""
    private var sRideStatus = ""
    private var isReasonAvailable = false
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var session: SessionManager? = null
    private var UserID = ""
    private var itemlist_reason: ArrayList<CancelTripPojo>? = null
    private var Tv_headerTitle: TextView? = null
    private var fromPosition: LatLng? = null
    private var toPosition: LatLng? = null
    private var markerOptions: MarkerOptions? = null
    internal lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    private var currentLocation: Location? = null
    private var refreshReceiver: RefreshReceiver? = null
    private val realTimeHandler = Handler()


    internal var sPickUpLocation = ""
    internal var sPickUpLatitude = ""
    internal var sPickUpLongitude = ""
    internal var sDropLocation = ""
    internal var sDropLatitude = ""
    internal var sDropLongitude = ""
    private var isRidePickUpAvailable = false
    private var isRideDropAvailable = false

    internal val PERMISSION_REQUEST_CODE = 111
    internal val PERMISSION_REQUEST_CODES = 222
    private var panic_btn: CardView? = null
    private var sSelectedPanicNumber = ""

    internal var mLatLngInterpolator: LatLngInterpolator? = null

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        this.currentLocation = location
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()

    }

    override fun onResume() {
        super.onResume()
        ChatService.startUserAction(this@MyRideDetailTrackRide)
        startLocationUpdates()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        //mMap.setOnCameraMoveStartedListener(this);
        // mMap.setOnCameraIdleListener(this);
        //mMap.setOnCameraMoveListener(this);
        // mMap.setOnCameraMoveCanceledListener(this);

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //mMap.setMyLocationEnabled(true);
            val styleManager = MapStyleManager.attachToMap(this, mMap!!)
            styleManager.addStyle(0f, R.raw.map_silver_json)
            styleManager.addStyle(10f, R.raw.map_silver_json)
            styleManager.addStyle(12f, R.raw.map_silver_json)

        }
        if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()
            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude
            // Move the camera to last position with a zoom level
            val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(18f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        } else {
            Alert(resources.getString(R.string.action_error), resources.getString(R.string.alert_gpsEnable))
        }


    }

    inner class RefreshReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver") {
                println("triparrived----------------------")
                Tv_headerTitle!!.text = resources.getString(R.string.action_driver_arrived)
                rl_endTrip!!.visibility = View.GONE
            } else if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip") {
                println("tripbegin----------------------")
                Tv_headerTitle!!.text = resources.getString(R.string.action_enjy_your_ride)
                rl_endTrip!!.visibility = View.GONE
                panic_btn!!.visibility = View.VISIBLE
            } else if (intent.action == "com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver") {
            }

            println("check--------------")
            if (intent.extras != null && intent.extras!!.containsKey("drop_lat") && intent.extras!!.containsKey("drop_lng")) {
                val lat = intent.extras!!.get("drop_lat") as String
                val lng = intent.extras!!.get("drop_lng") as String
                val pickUp_lat = intent.extras!!.get("pickUp_lat") as String
                val pickUp_lng = intent.extras!!.get("pickUp_lng") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    val pick_lat_decimal = java.lang.Double.parseDouble(pickUp_lat)
                    val pick_lng_decimal = java.lang.Double.parseDouble(pickUp_lng)
                    updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal)
                    println("inside updategoogle1--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

            }
            println("out else--------------")
            if (intent.extras != null && intent.extras!!.containsKey(Iconstant.isContinousRide)) {
                val lat = intent.extras!!.get("latitude") as String
                val lng = intent.extras!!.get("longitude") as String
                val ride_id = intent.extras!!.get("ride_id") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    updateDriverOnMap(lat_decimal, lng_decimal)
                } catch (e: Exception) {
                }

            }
        }
    }

    private fun updateDriverOnMap(lat_decimal: kotlin.Double, lng_decimal: kotlin.Double) {
        /* LatLng dropLatLng = new LatLng(lat_decimal, lng_decimal);
        if (curentDriverMarker != null) {
            curentDriverMarker.remove();
            curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                    .position(dropLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));
        }*/

        val latLng = LatLng(lat_decimal, lng_decimal)

        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }

        if (curentDriverMarker != null) {
            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)

            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)

        } else {

            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove))
                    .anchor(0.5f, 0.5f)
                    .flat(true))

            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
        }
    }

    private fun updateGoogleMapTrackRide(lat_decimal: kotlin.Double, lng_decimal: kotlin.Double, pick_lat_decimal: kotlin.Double, pick_lng_decimal: kotlin.Double) {
        if (mMap != null) {
            mMap!!.clear()
        }
        val dropLatLng = LatLng(lat_decimal, lng_decimal)
        val pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)
        /*if(currentLocation  != null){
            pickUpLatLng = new  LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
        }else{
            pickUpLatLng = new  LatLng(MyCurrent_lat,MyCurrent_long);
        }*/
        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng, pickUpLatLng)
        draw_route_asyncTask.execute()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_detail_track_ride)
        trackyour_ride_class = this@MyRideDetailTrackRide
        initialize()
        try {
            setLocationRequest()
            buildGoogleApiClient()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initializeMap()

        panic_btn!!.setOnClickListener {
            println("----------------panic onclick method-----------------")
            panic()
        }
        //Start XMPP Chat Service
    }


    private fun initialize() {
        cd = ConnectionDetector(this@MyRideDetailTrackRide)
        isInternetPresent = cd!!.isConnectingToInternet
        gps = GPSTracker(this@MyRideDetailTrackRide)
        session = SessionManager(this@MyRideDetailTrackRide)
        itemlist_reason = ArrayList()
        markerOptions = MarkerOptions()

        tv_done = findViewById(R.id.myride_detail_track_your_ride_done_textview) as TextView
        tv_drivername = findViewById(R.id.myride_detail_track_your_ride_driver_name) as TextView
        tv_carModel = findViewById(R.id.myride_detail_track_your_ride_driver_carmodel) as TextView
        Tv_headerTitle = findViewById(R.id.myride_detail_track_your_ride_track_label) as TextView
        tv_carNo = findViewById(R.id.myride_detail_track_your_ride_driver_carNo) as TextView
        tv_rating = findViewById(R.id.myride_detail_track_your_ride_driver_rating) as TextView
        driver_image = findViewById(R.id.myride_detail_track_your_ride_driverimage) as RoundedImageView
        rl_callDriver = findViewById(R.id.myride_detail_track_your_ride_calldriver_layout) as ImageView
        rl_endTrip = findViewById(R.id.myride_detail_track_your_ride_endtrip_layout) as CustomTextView
        Tv_headerTitle = findViewById(R.id.myride_detail_track_your_ride_track_label) as TextView
        panic_btn = findViewById(R.id.myride_detail_track_your_ride_panic_cardview_layout) as CardView


        val intent = intent
        if (intent != null) {
            driverID = intent.getStringExtra("driverID")
            driverName = intent.getStringExtra("driverName")
            driverImage = intent.getStringExtra("driverImage")
            driverRating = intent.getStringExtra("driverRating")
            driverLat = intent.getStringExtra("driverLat")
            driverLong = intent.getStringExtra("driverLong")
            driverTime = intent.getStringExtra("driverTime")
            rideID = intent.getStringExtra("rideID")
            driverMobile = intent.getStringExtra("driverMobile")
            driverCar_no = intent.getStringExtra("driverCar_no")
            driverCar_model = intent.getStringExtra("driverCar_model")
            userLat = intent.getStringExtra("userLat")
            userLong = intent.getStringExtra("userLong")
            sRideStatus = intent.getStringExtra("rideStatus")

            if (intent.hasExtra("PickUpLocation")) {
                sPickUpLocation = intent.getStringExtra("PickUpLocation")
                sPickUpLatitude = intent.getStringExtra("PickUpLatitude")
                sPickUpLongitude = intent.getStringExtra("PickUpLongitude")

                isRidePickUpAvailable = true
            } else {
                isRidePickUpAvailable = false
            }

            if (intent.hasExtra("DropLocation")) {
                sDropLocation = intent.getStringExtra("DropLocation")
                sDropLatitude = intent.getStringExtra("DropLatitude")
                sDropLongitude = intent.getStringExtra("DropLongitude")

                isRideDropAvailable = true
            } else {
                isRideDropAvailable = false
            }


        }


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")
        registerReceiver(refreshReceiver, intentFilter)


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        tv_drivername!!.text = driverName
        tv_carNo!!.text = driverCar_no
        tv_carModel!!.text = driverCar_model
        tv_rating!!.text = driverRating
        Picasso.with(this)
                .load(driverImage)
                .into(driver_image)
        tv_done!!.setOnClickListener(this)
        rl_callDriver!!.setOnClickListener(this)
        rl_endTrip!!.setOnClickListener(this)
    }

    private fun panic() {
        println("----------------panic method-----------------")
        val view = View.inflate(this@MyRideDetailTrackRide, R.layout.panic_page, null)
        val dialog = MaterialDialog(this@MyRideDetailTrackRide)
        dialog.setContentView(view).setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
        ) { dialog.dismiss() }.show()
        val call_police = view.findViewById(R.id.call_police_button) as Button
        val call_fire = view.findViewById(R.id.call_fireservice_button) as Button
        val call_ambulance = view.findViewById(R.id.call_ambulance_button) as Button
        call_police.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+100"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "+100")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "+100")
                startActivity(callIntent)
            }
        }

        call_fire.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+101"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "101")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "101")
                startActivity(callIntent)
            }
        }
        call_ambulance.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+108"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "108")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "108")
                startActivity(callIntent)
            }
        }

    }


    private fun initializeMap() {
        if (mMap == null) {
            // mMap = ((SupportMapFragment) getSu().findFragmentById(R.id.myride_detail_track_your_ride_mapview)).getMap();
            val mapFragment = supportFragmentManager.findFragmentById(R.id.myride_detail_track_your_ride_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                Toast.makeText(this@MyRideDetailTrackRide, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show()
            }
        }


        if (sRideStatus.equals("Onride", ignoreCase = true)) {
            Tv_headerTitle!!.text = resources.getString(R.string.action_enjy_your_ride)
            rl_endTrip!!.visibility = View.GONE
            panic_btn!!.visibility = View.VISIBLE
        } else if (sRideStatus.equals("arrived", ignoreCase = true)) {
            Tv_headerTitle!!.text = resources.getString(R.string.action_driver_arrived)
            rl_endTrip!!.visibility = View.GONE
        } else if (sRideStatus.equals("Confirmed", ignoreCase = true)) {
            Tv_headerTitle!!.text = resources.getString(R.string.track_your_ride_label_track)
            rl_endTrip!!.visibility = View.GONE
        }

        if (isRideDropAvailable && sRideStatus.equals("Onride", ignoreCase = true)) {
            println("--------------inside isRideDropAvailable if -----------------------")

            try {
                val lat_decimal = java.lang.Double.parseDouble(sDropLatitude)
                val lng_decimal = java.lang.Double.parseDouble(sDropLongitude)
                val pick_lat_decimal = java.lang.Double.parseDouble(sPickUpLatitude)
                val pick_lng_decimal = java.lang.Double.parseDouble(sPickUpLongitude)
                updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal)
            } catch (e: Exception) {
            }

        } else {
            //set marker for driver location.
            if (driverLat != null && driverLong != null) {
                fromPosition = LatLng(java.lang.Double.parseDouble(userLat), java.lang.Double.parseDouble(userLong))
                toPosition = LatLng(java.lang.Double.parseDouble(driverLat), java.lang.Double.parseDouble(driverLong))
                if (fromPosition != null && toPosition != null) {

                    println("--------------inside isRideDropAvailable else fromPosition-----------------------" + fromPosition!!)
                    println("--------------inside isRideDropAvailable else toPosition-----------------------" + toPosition!!)

                    val draw_route_asyncTask = GetRouteTask()
                    draw_route_asyncTask.execute()
                }
            }
        }
    }

    override fun onClick(v: View) {
        if (v === tv_done) {
            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)
            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        } else if (v === rl_callDriver) {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile!!)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile!!)
                    startActivity(callIntent)
                }
            } else {
                Alert(this@MyRideDetailTrackRide.resources.getString(R.string.alert_label_title), this@MyRideDetailTrackRide.resources.getString(R.string.track_your_ride_alert_content1))
            }
        } else if (v === rl_endTrip) {
            val mDialog = PkDialog(this@MyRideDetailTrackRide)
            mDialog.setDialogTitle(resources.getString(R.string.my_rides_detail_cancel_ride_alert_title))
            mDialog.setDialogMessage(resources.getString(R.string.my_rides_detail_cancel_ride_alert))
            mDialog.setPositiveButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_yes), View.OnClickListener {
                mDialog.dismiss()
                cd = ConnectionDetector(this@MyRideDetailTrackRide)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    //postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url);
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            })
            mDialog.setNegativeButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_no), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()

        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@MyRideDetailTrackRide)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values

            document = v2GetRouteDirection.getDocument(toPosition, fromPosition, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map
                    mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(fromPosition!!)
                    markerOptions!!.position(toPosition!!)
                    markerOptions!!.draggable(true)

                    println("-------------inside getRoute---------------------")


                    //googleMap.addMarker(markerOptions);
                    mMap!!.addMarker(MarkerOptions()
                            .position(fromPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.search_dot)))
                    mMap!!.addMarker(MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.search_dot)))
                    curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)))

                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(fromPosition!!)
                    builder.include(toPosition!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private fun postRequest_CancelRides_Reason(Url: String) {
        dialog = Dialog(this@MyRideDetailTrackRide)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------MyRide Cancel Reason Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(this@MyRideDetailTrackRide)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
          override  fun onCompleteListener(response: String) {
                println("-------------MyRide Cancel Reason Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val reason_array = response_object.getJSONArray("reason")
                            if (reason_array.length() > 0) {
                                itemlist_reason!!.clear()
                                for (i in 0 until reason_array.length()) {
                                    val reason_object = reason_array.getJSONObject(i)
                                    val pojo = CancelTripPojo()
                                    pojo.setReason(reason_object.getString("reason"))
                                    pojo.setReasonId(reason_object.getString("id"))
                                    itemlist_reason!!.add(pojo)
                                }
                                isReasonAvailable = true
                            } else {
                                isReasonAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                    if (Sstatus.equals("1", ignoreCase = true) && isReasonAvailable) {
                        val finish_timerPage = Intent()
                        finish_timerPage.action = "com.pushnotification.finish.TimerPage"
                        sendBroadcast(finish_timerPage)
                        val passIntent = Intent(this@MyRideDetailTrackRide, TrackRideCancelTrip::class.java)
                        val bundleObject = Bundle()
                        bundleObject.putSerializable("Reason", itemlist_reason)
                        passIntent.putExtras(bundleObject)
                        passIntent.putExtra("RideID", rideID)
                        startActivity(passIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

            return true
        }
        return false
    }

    public override fun onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver)
        ChatService.disableChat()
        super.onDestroy()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var currentLocation: LatLng? = null
        private var endLocation: LatLng? = null

        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng) {
            this.currentLocation = currentLocation
            this.endLocation = endLocation
        }

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(endLocation, currentLocation, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(
                            resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map
                    mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(endLocation!!)
                    markerOptions!!.position(currentLocation!!)
                    markerOptions!!.draggable(true)

                    //mMap.addMarker(markerOptions);
                    mMap!!.addMarker(MarkerOptions()
                            .position(endLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.search_dot)))
                    mMap!!.addMarker(MarkerOptions()
                            .position(currentLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.search_dot)))

                    curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                            .position(endLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)))

                    println("inside---------marker--------------")

                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(endLocation!!)
                    builder.include(currentLocation!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODE)
    }


    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODES)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + driverMobile!!)
                startActivity(callIntent)
            }


            PERMISSION_REQUEST_CODES ->

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                    startActivity(callIntent)
                }
        }
    }

    companion object {
        lateinit var trackyour_ride_class: MyRideDetailTrackRide
        private var curentDriverMarker: Marker? = null
        private val movingMarker: Marker? = null
        internal val REQUEST_LOCATION = 199

        internal var latLngInterpolator: LatLngInterpolator = LatLngInterpolator.Spherical()

        fun updateMap(latLng: LatLng) {
            MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator)
        }
    }
}
