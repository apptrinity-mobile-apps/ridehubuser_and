package indo.com.ridehub_lyft_uber.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.format.DateUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.AppController
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.volley.VolleyErrorResponse
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.PojoResponse.CancelTripPojo
import indo.com.ridehub_lyft_uber.PojoResponse.MyRideDetailPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

class MyRidesDetail : FragmentActivity(), OnMapReadyCallback {
    private var Iv_panic: ImageView? = null
    private val panic_dialog: MaterialDialog? = null

    private var rl_ridedetail_locations: RelativeLayout? = null
    private var back: RelativeLayout? = null
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var UserID = ""
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal lateinit var itemlist: ArrayList<MyRideDetailPojo>
    internal lateinit var itemlist_reason: ArrayList<CancelTripPojo>

    // private var Tv_carType: TextView? = null
    private var Tv_carNo_CRN: CustomTextView? = null
    // private var Tv_carNo: TextView? = null
    private var Tv_rideStatus: TextView? = null
    //private var Tv_address: TextView? = null
    private var Tv_dropaddress: TextView? = null
    private var Tv_pickup: TextView? = null
    private var Tv_drop: TextView? = null
    private var Tv_rideDistance: TextView? = null
    private var Tv_timeTaken: TextView? = null
    private var Tv_waitTime: TextView? = null
    private var Tv_totalBill: TextView? = null
    private var Tv_paymentType: TextView? = null
    private var Tv_totalPaid: TextView? = null
    private var Tv_couponDiscount: TextView? = null
    private var Tv_walletUsuage: TextView? = null
    private var Tv_totalpoints: TextView? = null
    // private var Tv_ownercarNo: TextView? = null
    private var Rl_favorite: RelativeLayout? = null
    private var Rl_priceBottom: RelativeLayout? = null
    private var Rl_button: RelativeLayout? = null
    //private var Rl_address: RelativeLayout? = null
    private var Rl_dropaddress: RelativeLayout? = null
    private var Rl_pickup: RelativeLayout? = null
    private var Iv_favorite: ImageView? = null
    private var Ll_cancelTrip: LinearLayout? = null
    private var Ll_payment: LinearLayout? = null
    private var Ll_mailInvoice: LinearLayout? = null
    private var Ll_reportIssue: LinearLayout? = null
    private var Ll_trackRide: LinearLayout? = null
    private var Ll_share_Ride: LinearLayout? = null

    private var mMap: GoogleMap? = null
    private var SrideId_intent = ""
    private var isPickUpAvailable = false
    private var isDropAvailable = false
    private var isSummaryAvailable = false
    private var isReasonAvailable = false
    private var isFareAvailable = false
    private var isTrackRideAvailable = false
    private var completejob_dialog: MaterialDialog? = null
    private var Et_share_trip_mobileno: EditText? = null

    private var Str_LocationLatitude = ""
    private var Str_LocationLongitude = ""
    private var currencySymbol = ""
    private var TotalAmount = ""

    //------Invoice Dialog Declaration-----
    private var Et_dialog_InvoiceEmail: EditText? = null
    private var invoice_dialog: MaterialDialog? = null

    //------Favourite Dialog Declaration-----
    private var Et_dialog_FavouriteTitle: EditText? = null
    private var favourite_dialog: MaterialDialog? = null

    //------Tip Declaration-----
    private var Et_tip_Amount: EditText? = null
    private var Bt_tip_Apply: Button? = null
    private var Rl_tip: RelativeLayout? = null
    private var Rl_main_tip: RelativeLayout? = null
    private var Cb_tip: CheckBox? = null
    private var Tv_tipAmount: TextView? = null
    private var Ll_deleteTip: LinearLayout? = null

    private var isRidePickUpAvailable = false
    private var isRideDropAvailable = false
    internal lateinit var panic_cardView: CardView

    internal val PERMISSION_REQUEST_CODE = 111
    private var sSelectedPanicNumber = ""

    internal lateinit var tv_pick_up_id: CustomTextView
    internal lateinit var tv_source_id: CustomTextView

    private var refreshReceiver: RefreshReceiver? = null

    lateinit var uber_TripReceiptRequest: StringRequest
    lateinit var lyft_TripReceiptRequest: StringRequest
    private var Sridetype_intent = ""
    private var Sridedistance_intent = ""
    private var Srideduration_intent = ""
    private var Sridedate_intent = ""
    private var Sridepickuploc_intent = ""
    private var Sridelong_intent = ""
    private var Sridelat_intent = ""
    val type_lyft = "LYFT"
    val type_ridehub = "RIDEHUB"
    val type_uber = "UBER"
    private var UserAccessToken = ""
    private var Iv_ridetype: ImageView? = null


    //----------------------Code for TextWatcher-------------------------
    private val mailInvoice_EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Et_dialog_InvoiceEmail!!.text.length > 0) {
                Et_dialog_InvoiceEmail!!.error = null
            }
        }
    }

    private val favourite_EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Et_dialog_FavouriteTitle!!.text.length > 0) {
                Et_dialog_FavouriteTitle!!.error = null
            }
        }
    }

    inner class RefreshReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_REFRESH") {
                if (isInternetPresent!!) {
                    postRequest_MyRides(Iconstant.myride_details_url)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myrides_detail)
        myrideDetail_class = this@MyRidesDetail
        initialize()
        initializeMap()

        //Start XMPP Chat Service
        ChatService.startUserAction(this@MyRidesDetail)

        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()
        }

        Ll_cancelTrip!!.setOnClickListener {
            val mDialog = PkDialog(this@MyRidesDetail)
            mDialog.setDialogTitle(resources.getString(R.string.my_rides_detail_cancel_ride_alert_title))
            mDialog.setDialogMessage(resources.getString(R.string.my_rides_detail_cancel_ride_alert))
            mDialog.setPositiveButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_yes), View.OnClickListener {
                mDialog.dismiss()
                cd = ConnectionDetector(this@MyRidesDetail)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url)
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            })
            mDialog.setNegativeButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_no), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()
        }

        Ll_payment!!.setOnClickListener {
            cd = ConnectionDetector(this@MyRidesDetail)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                val passIntent = Intent(this@MyRidesDetail, MyRidePaymentList::class.java)
                passIntent.putExtra("RideID", SrideId_intent)
                passIntent.putExtra("TotalAmount", TotalAmount)
                startActivity(passIntent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }
        }

        Ll_mailInvoice!!.setOnClickListener { mailInvoice() }

        Ll_reportIssue!!.setOnClickListener { sendEmail() }

        Ll_trackRide!!.setOnClickListener {
            cd = ConnectionDetector(this@MyRidesDetail)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                postRequest_TrackRide(Iconstant.myride_details_track_your_ride_url)
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }
        }


        Ll_share_Ride!!.setOnClickListener { shareTrip() }


        Iv_favorite!!.setOnClickListener {
            if (itemlist[0].getIsFavLocation().equals("0")) {
                favouriteAddress()
            }
        }


        Bt_tip_Apply!!.setOnClickListener {
            cd = ConnectionDetector(this@MyRidesDetail)
            isInternetPresent = cd!!.isConnectingToInternet

            if (Et_tip_Amount!!.text.toString().length > 0) {
                if (isInternetPresent!!) {
                    postRequest_Tip(Iconstant.tip_add_url, "Apply")
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.my_rides_detail_tip_empty_label))
            }
        }

        Cb_tip!!.setOnClickListener { v ->
            if ((v as CheckBox).isChecked) {
                Rl_tip!!.visibility = View.VISIBLE
            } else {
                Rl_tip!!.visibility = View.GONE
            }
        }


        Ll_deleteTip!!.setOnClickListener {
            cd = ConnectionDetector(this@MyRidesDetail)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                postRequest_Tip(Iconstant.tip_remove_url, "Remove")
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }
        }
        Iv_panic!!.setOnClickListener {
            println("----------------panic onclick method-----------------")
            panic()
        }

    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        //mMap.setOnCameraMoveStartedListener(this);
        // mMap.setOnCameraIdleListener(this);
        //mMap.setOnCameraMoveListener(this);
        // mMap.setOnCameraMoveCanceledListener(this);

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //mMap.setMyLocationEnabled(true);
            val styleManager = MapStyleManager.attachToMap(this, mMap!!)
            styleManager.addStyle(0F, R.raw.map_silver_json)
            styleManager.addStyle(10F, R.raw.map_silver_json)
            styleManager.addStyle(12F, R.raw.map_silver_json)

        }

    }


    private fun initializeMap() {

        if (mMap == null) {
            //mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.book_my_ride_mapview));
            val mapFragment = supportFragmentManager.findFragmentById(R.id.my_rides_detail_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                // Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private fun initialize() {
        session = SessionManager(this@MyRidesDetail)
        cd = ConnectionDetector(this@MyRidesDetail)
        isInternetPresent = cd!!.isConnectingToInternet
        itemlist = ArrayList()
        itemlist_reason = ArrayList()


        //new UI
        Iv_ridetype = findViewById(R.id.iv_ridedetail_cabtype) as ImageView

        back = findViewById(R.id.my_rides_detail_header_back_layout) as RelativeLayout
        // Tv_carType = findViewById(R.id.my_rides_detail_car_type) as TextView
        Tv_carNo_CRN = findViewById(R.id.tv_myridedetail_crn) as CustomTextView
        // Tv_carNo = findViewById(R.id.my_rides_detail_car_number) as TextView
        // Tv_ownercarNo = findViewById(R.id.my_rides_detail_owner_car_number) as TextView
        Tv_rideStatus = findViewById(R.id.my_rides_detail_ride_status) as TextView
        //Tv_address = findViewById(R.id.my_rides_detail_location_textview) as TextView
        Tv_dropaddress = findViewById(R.id.my_rides_detail_droplocation_textview) as TextView
        Tv_pickup = findViewById(R.id.my_rides_detail_pickup_time_textview) as TextView
        Tv_drop = findViewById(R.id.my_rides_detail_drop_time_textview) as TextView
        Tv_rideDistance = findViewById(R.id.my_rides_detail_ride_distance_textview) as TextView
        Tv_timeTaken = findViewById(R.id.my_rides_detail_time_taken_textview) as TextView
        Tv_waitTime = findViewById(R.id.my_rides_detail_wait_time_textview) as TextView
        Tv_totalBill = findViewById(R.id.my_rides_detail_total_bill_textview) as TextView
        Tv_paymentType = findViewById(R.id.my_rides_detail_payment_type_textview) as TextView
        Tv_totalPaid = findViewById(R.id.my_rides_detail_total_paid_textview) as TextView
        Tv_couponDiscount = findViewById(R.id.my_rides_detail_coupon_discount_textview) as TextView
        Tv_walletUsuage = findViewById(R.id.my_rides_detail_wallet_usuage_textview) as TextView
        Tv_totalpoints = findViewById(R.id.my_rides_detail_total_points_textview) as TextView
        Ll_share_Ride = findViewById(R.id.my_rides_detail_share_layout) as LinearLayout


        Rl_favorite = findViewById(R.id.my_rides_detail_favorite_layout) as RelativeLayout
        Rl_priceBottom = findViewById(R.id.my_rides_detail_price_layout) as RelativeLayout
        Rl_button = findViewById(R.id.my_rides_detail_button_layout) as RelativeLayout
        Iv_favorite = findViewById(R.id.my_rides_detail_favorite_imageView) as ImageView
        // Rl_address = findViewById(R.id.my_rides_detail_address_layout) as RelativeLayout
        Rl_pickup = findViewById(R.id.my_rides_detail_time_layout) as RelativeLayout
        Rl_dropaddress = findViewById(R.id.my_rides_detail_dropaddress_layout) as RelativeLayout


        Ll_cancelTrip = findViewById(R.id.my_rides_detail_cancel_trip_layout) as LinearLayout
        Ll_payment = findViewById(R.id.my_rides_detail_payment_layout) as LinearLayout
        Ll_mailInvoice = findViewById(R.id.my_rides_detail_mail_invoice_layout) as LinearLayout
        Ll_reportIssue = findViewById(R.id.my_rides_detail_report_issue_layout) as LinearLayout
        Ll_trackRide = findViewById(R.id.my_rides_detail_track_ride_layout) as LinearLayout

        Et_tip_Amount = findViewById(R.id.my_rides_detail_tip_editText) as EditText
        Bt_tip_Apply = findViewById(R.id.my_rides_detail_tip_apply_button) as Button
        Rl_main_tip = findViewById(R.id.my_rides_detail_tip_top_layout) as RelativeLayout
        Rl_tip = findViewById(R.id.my_rides_detail_tip_layout) as RelativeLayout
        Cb_tip = findViewById(R.id.my_rides_detail_tip_checkBox) as CheckBox
        Tv_tipAmount = findViewById(R.id.my_rides_detail_tip_amount_textView) as TextView
        Ll_deleteTip = findViewById(R.id.my_rides_detail_tip_amount_remove_layout) as LinearLayout

        panic_cardView = findViewById(R.id.my_rides_detail_panic_cardview_layout) as CardView
        Iv_panic = findViewById(R.id.panic_image) as ImageView


        tv_pick_up_id = findViewById(R.id.tv_pick_up_id) as CustomTextView
        tv_source_id = findViewById(R.id.tv_source_id) as CustomTextView

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_REFRESH")
        registerReceiver(refreshReceiver, intentFilter)

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager!!.KEY_USERID!!].toString()
        if (!session!!.getUserAccessToken().equals("")) {
            UserAccessToken = session!!.getUserAccessToken()
        }
        val intent = intent
        SrideId_intent = intent.getStringExtra("RideID")
        Sridetype_intent = intent.getStringExtra("RideType")
        Srideduration_intent = intent.getStringExtra("Duration")
        Sridedistance_intent = intent.getStringExtra("Distance")
        Sridedate_intent = intent.getStringExtra("Date")
        Sridepickuploc_intent = intent.getStringExtra("PickUp")
        Sridelat_intent = intent.getStringExtra("Latitude")
        Sridelong_intent = intent.getStringExtra("Longitude")
        if (isInternetPresent!!) {
//            postRequest_MyRides(Iconstant.myride_details_url)
            if (Sridetype_intent.equals(type_uber)) {

                Iv_ridetype!!.setImageResource(R.drawable.uber)
                UberReceipet(SrideId_intent)

            } else if (Sridetype_intent.equals(type_ridehub)) {

                Iv_ridetype!!.setImageResource(R.drawable.cab_type_ridehub)
                postRequest_MyRides(Iconstant.myride_details_url)

            } else if (Sridetype_intent.equals(type_lyft)) {

                Iv_ridetype!!.setImageResource(R.drawable.lyft_icon)
                LyftReceipt(SrideId_intent)

            }
        } else {
            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
        }
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@MyRidesDetail)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()

            if (completejob_dialog != null) {
                completejob_dialog!!.dismiss()
            }
        })
        mDialog.show()

    }

    private fun shareTrip() {
        completejob_dialog = MaterialDialog(this@MyRidesDetail)
        val view = LayoutInflater.from(this@MyRidesDetail).inflate(R.layout.share_trip_popup, null)
        Et_share_trip_mobileno = view.findViewById(R.id.sharetrip_mobilenoEt)
        val Bt_Submit = view.findViewById(R.id.jsharetrip_popup_submit) as Button
        val Bt_Cancel = view.findViewById(R.id.sharetrip_popup_cancel) as Button
        completejob_dialog!!.setView(view).show()
        Bt_Submit.setOnClickListener {
            if (Et_share_trip_mobileno!!.text.toString().length == 0) {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.profile_lable_error_mobile))
            } else {
                if (isInternetPresent!!) {
                    share_trip_postRequest_MyRides(this@MyRidesDetail, Iconstant.share_trip_url, "jobcomplete")
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            }
        }

        Bt_Cancel.setOnClickListener { completejob_dialog!!.dismiss() }

    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@MyRidesDetail, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    //----------Method to Send Email--------
    protected fun sendEmail() {
        val TO = arrayOf("info@zoplay.com")
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "message/rfc822"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message")
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this@MyRidesDetail, "There is no email client installed.", Toast.LENGTH_SHORT).show()
        }

    }

    //----------Method for Invoice Email--------
    private fun mailInvoice() {
        val view = View.inflate(this@MyRidesDetail, R.layout.mail_invoice_dialog, null)
        invoice_dialog = MaterialDialog(this@MyRidesDetail)
        invoice_dialog!!.setContentView(view)
                .setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
                ) { invoice_dialog!!.dismiss() }
                .setPositiveButton(resources.getString(R.string.my_rides_detail_send)
                ) {
                    cd = ConnectionDetector(this@MyRidesDetail)
                    isInternetPresent = cd!!.isConnectingToInternet

                    if (!isValidEmail(Et_dialog_InvoiceEmail!!.text.toString())) {
                        erroredit(Et_dialog_InvoiceEmail!!, resources.getString(R.string.register_label_alert_email))
                    } else {
                        if (isInternetPresent!!) {
                            postRequest_EmailInvoice(Iconstant.myride_details_inVoiceEmail_url, Et_dialog_InvoiceEmail!!.text.toString(), SrideId_intent)
                        } else {
                            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                        }
                    }
                }
                .show()

        Et_dialog_InvoiceEmail = view.findViewById(R.id.mail_invoice_email_edittext) as EditText
        Et_dialog_InvoiceEmail!!.addTextChangedListener(mailInvoice_EditorWatcher)

        Et_dialog_InvoiceEmail!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(Et_dialog_InvoiceEmail!!.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
            false
        }
    }

    private fun panic() {
        println("----------------panic method-----------------")
        val view = View.inflate(this@MyRidesDetail, R.layout.panic_page, null)
        val dialog = MaterialDialog(this@MyRidesDetail)
        dialog.setContentView(view).setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
        ) { dialog.dismiss() }.show()
        val call_police = view.findViewById(R.id.call_police_button) as Button
        val call_fire = view.findViewById(R.id.call_fireservice_button) as Button
        val call_ambulance = view.findViewById(R.id.call_ambulance_button) as Button
        call_police.setOnClickListener {
            dialog.dismiss()

            sSelectedPanicNumber = "+100"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "+100")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "+100")
                startActivity(callIntent)
            }
        }

        call_fire.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+101"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "101")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "101")
                startActivity(callIntent)
            }
        }
        call_ambulance.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+108"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "108")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "108")
                startActivity(callIntent)
            }
        }

    }

    //----------Method for Favourite Address--------
    private fun favouriteAddress() {
        val view = View.inflate(this@MyRidesDetail, R.layout.myride_detail_favourite_dialog, null)
        favourite_dialog = MaterialDialog(this@MyRidesDetail)
        favourite_dialog!!.setContentView(view)
                .setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
                ) { favourite_dialog!!.dismiss() }
                .setPositiveButton(resources.getString(R.string.my_rides_detail_favourite_apply)
                ) {
                    cd = ConnectionDetector(this@MyRidesDetail)
                    isInternetPresent = cd!!.isConnectingToInternet

                    if (Et_dialog_FavouriteTitle!!.text.toString().length == 0) {
                        erroredit(Et_dialog_FavouriteTitle!!, resources.getString(R.string.my_rides_detail_favourite_alert_title))
                    } else {
                        if (isInternetPresent!!) {
                            postRequest_FavoriteSave(Iconstant.favoritelist_add_url)
                        } else {
                            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                        }
                    }
                }
                .show()

        Et_dialog_FavouriteTitle = view.findViewById(R.id.myride_detail_favourite_title_edittext) as EditText
        Et_dialog_FavouriteTitle!!.addTextChangedListener(favourite_EditorWatcher)

        Et_dialog_FavouriteTitle!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(Et_dialog_FavouriteTitle!!.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
            false
        }

    }


    //-----------------------MyRide Detail Post Request-----------------
    private fun postRequest_MyRides(Url: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------MyRide Detail Url----------------$Url")
        println("-------------MyRide Detail user_id----------------$UserID")
        println("-------------MyRide Detail ride_id----------------$SrideId_intent")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------MyRide Detail Response----------------$response")

                var Sstatus = ""
                val currencycode: Currency

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {

                            val check_details_object = response_object.get("details")
                            if (check_details_object is JSONObject) {

                                val detail_object = response_object.getJSONObject("details")
                                if (detail_object.length() > 0) {
                                    itemlist.clear()
                                    val pojo = MyRideDetailPojo()

                                    currencycode = Currency.getInstance(getLocale(detail_object.getString("currency")))
                                    currencySymbol = currencycode.symbol

                                    pojo.setCurrrencySymbol(currencycode.symbol)
                                    pojo.setCarType(detail_object.getString("cab_type"))
                                    pojo.setRideId(detail_object.getString("ride_id"))
                                    pojo.setRideStatus(detail_object.getString("ride_status"))
                                    pojo.setDisplayStatus(detail_object.getString("disp_status"))
                                    pojo.setDoCancelAction(detail_object.getString("do_cancel_action"))
                                    pojo.setDoTrackAction(detail_object.getString("do_track_action"))
                                    pojo.setIsFavLocation(detail_object.getString("is_fav_location"))
                                    pojo.setPay_status(detail_object.getString("pay_status"))
                                    pojo.setDistance_unit(detail_object.getString("distance_unit"))
                                    pojo.setVehicle_number(detail_object.getString("vehicle_number"))
                                    pojo.setPayment_type(detail_object.getString("payment_type"))
                                    pojo.setPickup(resources.getString(R.string.my_rides_detail_pickup_textview) + " " + detail_object.getString("pickup_date"))
                                    println("-------------MyRide Detail Response1----------------" + detail_object.toString())

                                    val check_pickup_object = detail_object.get("pickup")
                                    if (check_pickup_object is JSONObject) {


                                        val pickup_object = detail_object.getJSONObject("pickup")
                                        val drop_object = detail_object.getJSONObject("drop")

                                        if (pickup_object.length() > 0) {
                                            tv_pick_up_id.setText(pickup_object.getString("location"))
                                        }
                                        if (drop_object.length() > 0) {
                                            tv_source_id.setText(drop_object.getString("location"))
                                        }

                                        if (pickup_object.length() > 0) {
                                            pojo.setAddress(pickup_object.getString("location"))
                                            //tv_pick_up_id.text = pickup_object.getString("location")
                                            val latlong_object = pickup_object.getJSONObject("latlong")
                                            if (latlong_object.length() > 0) {
                                                pojo.setLocationLat(latlong_object.getString("lat"))
                                                pojo.setLocationLong(latlong_object.getString("lon"))
                                            }

                                            isPickUpAvailable = true
                                        } else {
                                            isPickUpAvailable = false
                                        }
                                    }
                                    println("-------------MyRide Detail Response2----------------" + check_pickup_object.toString())
                                    val check_drop_object = detail_object.get("drop")
                                    if (check_drop_object is JSONObject) {

                                        val drop_object = detail_object.getJSONObject("drop")
                                        if (drop_object.length() > 0) {
                                            pojo.setDropAddress(drop_object.getString("location"))


                                            // JSONObject latlong_object = drop_object.getJSONObject("latlong");
                                            /* if (latlong_object.length() > 0) {
                                                pojo.setLocationLat(latlong_object.getString("lat"));
                                                pojo.setLocationLong(latlong_object.getString("lon"));
                                            }*/

                                            isDropAvailable = true
                                        } else {
                                            isDropAvailable = false
                                        }
                                    }

                                    println("-------------MyRide Detail Response3----------------" + check_drop_object.toString())


                                    if (detail_object.getString("ride_status").equals("Completed", ignoreCase = true) || detail_object.getString("ride_status").equals("Finished", ignoreCase = true)) {
                                        pojo.setDrop(resources.getString(R.string.my_rides_detail_drop_textview) + " " + " " + detail_object.getString("drop_date"))

                                        println("-------------MyRide Detail Response4----------------" + detail_object.toString())
                                        val check_summary_object = detail_object.get("summary")
                                        if (check_summary_object is JSONObject) {

                                            val summary_object = detail_object.getJSONObject("summary")
                                            val pickup_object = detail_object.getJSONObject("pickup")
                                            val drop_object = detail_object.getJSONObject("drop")

                                            if (pickup_object.length() > 0) {
                                                tv_pick_up_id.setText(pickup_object.getString("location"))
                                            }
                                            if (drop_object.length() > 0) {
                                                tv_source_id.setText(drop_object.getString("location"))
                                            }


                                            if (summary_object.length() > 0) {
                                                pojo.setRideDistance(summary_object.getString("ride_distance"))
                                                pojo.setTimeTaken(summary_object.getString("ride_duration"))
                                                pojo.setWaitTime(summary_object.getString("waiting_duration"))

                                                isSummaryAvailable = true
                                            } else {
                                                isSummaryAvailable = false
                                            }
                                        }


                                        val check_fare_object = detail_object.get("fare")
                                        if (check_fare_object is JSONObject) {

                                            val fare_object = detail_object.getJSONObject("fare")
                                            if (fare_object.length() > 0) {
                                                TotalAmount = fare_object.getString("grand_bill")
                                                pojo.setTotalBill("" + (java.lang.Double.valueOf(fare_object.getString("grand_bill"))!! + java.lang.Double.valueOf(fare_object.getString("coupon_discount"))!!))
                                                pojo.setTotalPaid(fare_object.getString("total_paid"))
                                                pojo.setCouponDiscount(fare_object.getString("coupon_discount"))
                                                pojo.setWalletUsuage(fare_object.getString("wallet_usage"))
                                                pojo.setPoints_usage(fare_object.getString("points_usage"))
                                                pojo.setTip_amount(fare_object.getString("tips_amount"))

                                                isFareAvailable = true
                                            } else {
                                                isFareAvailable = false
                                            }
                                        }

                                    }

                                    itemlist.add(pojo)
                                }
                            }

                        }
                    }


                    //------------OnPost Execute------------
                    if (Sstatus.equals("1", ignoreCase = true)) {

                        if (itemlist.size > 0) {
                            // Rl_address!!.visibility = View.GONE
                            Rl_dropaddress!!.visibility = View.GONE
                            Rl_pickup!!.visibility = View.GONE

                            //  Tv_carType!!.text = itemlist[0].getCarType() + " " + resources.getString(R.string.my_rides_detail_ride_textview)
                            // Tv_carNo!!.text = resources.getString(R.string.my_rides_detail_crn_textview) + " " + itemlist[0].getRideId()

                            Tv_carNo_CRN!!.text = itemlist[0].getCarType() + " " + resources.getString(R.string.my_rides_detail_ride_textview) + "  " + resources.getString(R.string.my_rides_detail_crn_textview) + ":" + itemlist[0].getRideId()

                            Tv_rideStatus!!.text = itemlist[0].getRideStatus()
                            //  Tv_ownercarNo!!.text = "Vehicle Number : " + itemlist[0].getVehicle_number()

                            println("-------------MyRide Detail Response4----------------" + "jdfdsfhsdf")
                            if (isPickUpAvailable) {
                                // Tv_address!!.text = itemlist[0].getAddress()
                                Tv_dropaddress!!.text = itemlist[0].getDropAddress()
                                Tv_pickup!!.text = itemlist[0].getPickup()

                                //set marker for User location.
                                if (itemlist[0].getLocationLat() != null && itemlist[0].getLocationLong() != null) {

                                    Str_LocationLatitude = itemlist[0]!!.getLocationLat().toString()
                                    Str_LocationLongitude = itemlist[0]!!.getLocationLong().toString()

                                    mMap!!.addMarker(MarkerOptions()
                                            .position(LatLng(java.lang.Double.parseDouble(itemlist[0].getLocationLat()), java.lang.Double.parseDouble(itemlist[0].getLocationLong())))
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))

                                    // Move the camera to last position with a zoom level

                                    val cameraPosition = CameraPosition.Builder().target(LatLng(java.lang.Double.parseDouble(itemlist[0].getLocationLat()), java.lang.Double.parseDouble(itemlist[0].getLocationLong()))).zoom(15f).build()
                                    val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                                    mMap!!.moveCamera(camUpdate)

                                }
                            }


                            if (itemlist[0].getRideStatus().equals("Completed") || itemlist[0].getRideStatus().equals("Finished")) {
                                Tv_drop!!.visibility = View.VISIBLE
                                Tv_drop!!.text = itemlist[0].getDrop()

                                if (isSummaryAvailable) {
                                    Tv_rideDistance!!.text = itemlist[0].getRideDistance() + " " + itemlist[0].getDistance_unit()
                                    Tv_timeTaken!!.text = itemlist[0].getTimeTaken() + " " + resources.getString(R.string.my_rides_detail_mins_textview)
                                    Tv_waitTime!!.text = itemlist[0].getWaitTime() + " " + resources.getString(R.string.my_rides_detail_mins_textview)
                                }

                                if (isFareAvailable) {
                                    Tv_totalBill!!.text = itemlist[0].getCurrrencySymbol() + itemlist[0].getTotalBill()
                                    Tv_paymentType!!.text = itemlist[0].getPayment_type()
                                    Tv_totalPaid!!.text = itemlist[0].getCurrrencySymbol() + itemlist[0].getTotalPaid()
                                    Tv_walletUsuage!!.text = itemlist[0].getCurrrencySymbol() + itemlist[0].getWalletUsuage()
                                    Tv_totalpoints!!.text =  "$ "+itemlist[0].getPoints_usage()
                                    Tv_tipAmount!!.text = itemlist[0].getCurrrencySymbol() + itemlist[0].getTip_amount()

                                    if (itemlist[0].getCouponDiscount().equals("0")) {
                                        Tv_couponDiscount!!.visibility = View.GONE
                                    } else {
                                        Tv_couponDiscount!!.visibility = View.VISIBLE
                                        Tv_couponDiscount!!.text = resources.getString(R.string.my_rides_detail_coupon_discount_textview) + " " + itemlist[0].getCurrrencySymbol() + itemlist[0].getCouponDiscount()
                                    }

                                    if (itemlist[0].getWalletUsuage().equals("0")) {
                                        Tv_walletUsuage!!.visibility = View.GONE
                                    } else {
                                        Tv_walletUsuage!!.visibility = View.VISIBLE
                                        Tv_walletUsuage!!.text = resources.getString(R.string.my_rides_detail_wallet_usuage_textview) + " " + itemlist[0].getCurrrencySymbol() + itemlist[0].getWalletUsuage()
                                    }
                                    if (itemlist[0].getPoints_usage().equals("0")) {
                                        Tv_totalpoints!!.visibility = View.GONE
                                    } else {
                                        Tv_totalpoints!!.visibility = View.VISIBLE
                                        Tv_totalpoints!!.text = "$ "+itemlist[0].getPoints_usage()
                                    }


                                    if (itemlist[0].getTip_amount().equals("0")) {
                                        Ll_deleteTip!!.visibility = View.GONE
                                        Rl_main_tip!!.visibility = View.VISIBLE
                                        Rl_tip!!.visibility = View.GONE
                                    } else {
                                        Ll_deleteTip!!.visibility = View.VISIBLE
                                        Rl_main_tip!!.visibility = View.GONE
                                        Rl_tip!!.visibility = View.GONE
                                    }
                                }

                                Rl_priceBottom!!.visibility = View.VISIBLE
                            } else {
                                Rl_priceBottom!!.visibility = View.GONE
                                Tv_drop!!.visibility = View.GONE
                            }

                            //------------Panic Button Change Function-------
                            if (itemlist[0].getRideStatus().equals("Onride")) {
                                panic_cardView.visibility = View.VISIBLE
                            } else {
                                panic_cardView.visibility = View.GONE
                            }


                            //------------Button Change Function-------
                            if (itemlist[0].getPay_status().equals("Pending") || itemlist[0].getPay_status().equals("Processing")) {
                                Ll_cancelTrip!!.visibility = View.GONE
                                Ll_payment!!.visibility = View.VISIBLE
                                Ll_mailInvoice!!.visibility = View.GONE
                                Ll_reportIssue!!.visibility = View.GONE
                                Rl_favorite!!.visibility = View.GONE
                            }

                            if (itemlist[0].getDoCancelAction().equals("1")) {
                                Ll_cancelTrip!!.visibility = View.VISIBLE
                                Ll_payment!!.visibility = View.GONE
                                Ll_mailInvoice!!.visibility = View.GONE
                                Ll_reportIssue!!.visibility = View.GONE
                                Rl_favorite!!.visibility = View.GONE
                                Rl_main_tip!!.visibility = View.GONE
                                Rl_tip!!.visibility = View.GONE
                                Ll_deleteTip!!.visibility = View.GONE
                                Cb_tip!!.isChecked = false
                            }

                            if (itemlist[0].getRideStatus().equals("Completed")) {
                                Ll_cancelTrip!!.visibility = View.GONE
                                Ll_payment!!.visibility = View.GONE
                                Ll_mailInvoice!!.visibility = View.VISIBLE
                                Ll_reportIssue!!.visibility = View.VISIBLE
                                Rl_favorite!!.visibility = View.VISIBLE
                                Rl_main_tip!!.visibility = View.GONE
                                Rl_tip!!.visibility = View.GONE
                                Ll_deleteTip!!.visibility = View.GONE
                                Cb_tip!!.isChecked = false

                                //Programmatically making textView to left of layout
                                val params = RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                                        RelativeLayout.LayoutParams.WRAP_CONTENT)
                                params.addRule(RelativeLayout.LEFT_OF, R.id.my_rides_detail_favorite_layout)
                                params.addRule(RelativeLayout.CENTER_VERTICAL, R.id.my_rides_detail_favorite_layout)
                                Tv_rideStatus!!.layoutParams = params
                            }


                            //------Show and Hide the Button Layout------
                            if (itemlist[0].getPay_status().equals("Pending") || itemlist[0].getPay_status().equals("Processing") || itemlist[0].getDoCancelAction().equals("1") || itemlist[0].getRideStatus().equals("Completed") || itemlist[0].getRideStatus().equals("Onride")) {
                                Rl_button!!.visibility = View.VISIBLE
                            } else {
                                Rl_button!!.visibility = View.GONE
                            }


                            //---------Changing Favourite Color-----
                            if (itemlist[0].getIsFavLocation().equals("1")) {
                                Iv_favorite!!.setImageResource(R.drawable.heart_red_icon)
                                Iv_favorite!!.isEnabled = false
                            } else {
                                Iv_favorite!!.setImageResource(R.drawable.heart_grey_icon)
                                Iv_favorite!!.isEnabled = true
                            }

                            //------Show and Hide Track Ride Button Layout------
                            if (itemlist[0].getDoTrackAction().equals("1")) {
                                Ll_trackRide!!.visibility = View.VISIBLE
                                Ll_share_Ride!!.visibility = View.VISIBLE
                            } else {
                                Ll_trackRide!!.visibility = View.GONE
                                Ll_share_Ride!!.visibility = View.GONE
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private fun postRequest_CancelRides_Reason(Url: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)


        println("-------------MyRide Cancel Reason Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------MyRide Cancel Reason Response----------------$response")

                var Sstatus = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {

                            val check_reason_object = response_object.get("reason")
                            if (check_reason_object is JSONArray) {

                                val reason_array = response_object.getJSONArray("reason")
                                if (reason_array.length() > 0) {
                                    itemlist_reason.clear()
                                    for (i in 0 until reason_array.length()) {
                                        val reason_object = reason_array.getJSONObject(i)
                                        val pojo = CancelTripPojo()
                                        pojo.setReason(reason_object.getString("reason"))
                                        pojo.setReasonId(reason_object.getString("id"))

                                        itemlist_reason.add(pojo)
                                    }

                                    isReasonAvailable = true
                                } else {
                                    isReasonAvailable = false
                                }
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                    if (Sstatus.equals("1", ignoreCase = true) && isReasonAvailable) {

                        val passIntent = Intent(this@MyRidesDetail, MyRideCancelTrip::class.java)
                        val bundleObject = Bundle()
                        bundleObject.putSerializable("Reason", itemlist_reason)
                        passIntent.putExtras(bundleObject)
                        passIntent.putExtra("RideID", SrideId_intent)
                        startActivity(passIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------MyRide Email Invoice Post Request-----------------
    private fun postRequest_EmailInvoice(Url: String, Semail: String, SrideId: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_sending_invoice)

        println("-------------MyRide Email Invoice Url----------------$Url"+"-----"+SrideId+"-----"+Semail)
        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = Semail
        jsonParams["ride_id"] = SrideId

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                var Sstatus = ""
                var Sresponse = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Sresponse = `object`.getString("response")
                    println("-------------MyRide Email Invoice Url----------------$Sstatus$Sresponse")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        invoice_dialog!!.dismiss()
                        Alert(resources.getString(R.string.action_success), Sresponse)
                    } else {
                        Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }
                    invoice_dialog!!.dismiss()
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------Favourite Save Post Request-----------------
    private fun postRequest_FavoriteSave(Url: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_saving)


        println("-------------Favourite Save Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["title"] = Et_dialog_FavouriteTitle!!.text.toString()
        jsonParams["latitude"] = Str_LocationLatitude
        jsonParams["longitude"] = Str_LocationLongitude
        jsonParams["address"] = itemlist[0].getAddress().toString()

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------Favourite Save Response----------------$response")
                var Sstatus = ""
                var Smessage = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("message")

                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(Et_dialog_FavouriteTitle!!.windowToken, 0)

                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val mDialog = PkDialog(this@MyRidesDetail)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(Smessage)
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            favourite_dialog!!.dismiss()
                            Iv_favorite!!.setImageResource(R.drawable.heart_red_icon)
                            itemlist[0].setIsFavLocation("1")
                        })
                        mDialog.show()

                    } else {
                        Alert(resources.getString(R.string.alert_label_title), Smessage)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------Track Ride Post Request-----------------
    private fun postRequest_TrackRide(Url: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)

        println("-------------Track Ride Url----------------$Url")
        println("-------------Track Ride ride_id----------------$SrideId_intent")

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = SrideId_intent

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------Track Ride Response----------------$response")
                var Sstatus = ""
                var driverID = ""
                var driverName = ""
                var driverImage = ""
                var driverRating = ""
                var driverLat = ""
                var driverLong = ""
                var driverTime = ""
                var rideID = ""
                var driverMobile = ""
                var driverCar_no = ""
                var driverCar_model = ""
                var userLat = ""
                var userLong = ""
                var sRideStatus = ""

                var sPickUpLocation = ""
                var sPickUpLatitude = ""
                var sPickUpLongitude = ""
                var sDropLocation = ""
                var sDropLatitude = ""
                var sDropLongitude = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val driver_profile_object = response_object.getJSONObject("driver_profile")
                            if (driver_profile_object.length() > 0) {
                                driverID = driver_profile_object.getString("driver_id")
                                driverName = driver_profile_object.getString("driver_name")
                                driverImage = driver_profile_object.getString("driver_image")
                                driverRating = driver_profile_object.getString("driver_review")
                                driverLat = driver_profile_object.getString("driver_lat")
                                driverLong = driver_profile_object.getString("driver_lon")
                                driverTime = driver_profile_object.getString("min_pickup_duration")
                                rideID = driver_profile_object.getString("ride_id")
                                driverMobile = driver_profile_object.getString("phone_number")
                                driverCar_no = driver_profile_object.getString("vehicle_number")
                                driverCar_model = driver_profile_object.getString("vehicle_model")
                                userLat = driver_profile_object.getString("rider_lat")
                                userLong = driver_profile_object.getString("rider_lon")
                                sRideStatus = driver_profile_object.getString("ride_status")

                                val check_pickUp_object = driver_profile_object.get("pickup")
                                if (check_pickUp_object is JSONObject) {
                                    val pickup_object = driver_profile_object.getJSONObject("pickup")
                                    if (pickup_object.length() > 0) {
                                        sPickUpLocation = pickup_object.getString("location")
                                        val latLong_object = pickup_object.getJSONObject("latlong")
                                        if (latLong_object.length() > 0) {
                                            sPickUpLatitude = latLong_object.getString("lat")
                                            sPickUpLongitude = latLong_object.getString("lon")

                                            isRidePickUpAvailable = true
                                        } else {
                                            isRidePickUpAvailable = false
                                        }
                                    } else {
                                        isRidePickUpAvailable = false
                                    }
                                } else {
                                    isRidePickUpAvailable = false
                                }


                                val check_drop_object = driver_profile_object.get("drop")
                                if (check_drop_object is JSONObject) {
                                    val drop_object = driver_profile_object.getJSONObject("drop")
                                    if (drop_object.length() > 0) {
                                        sDropLocation = drop_object.getString("location")
                                        val latLong_object = drop_object.getJSONObject("latlong")
                                        if (latLong_object.length() > 0) {
                                            sDropLatitude = latLong_object.getString("lat")
                                            sDropLongitude = latLong_object.getString("lon")

                                            isRideDropAvailable = true
                                        } else {
                                            isRideDropAvailable = false
                                        }
                                    } else {
                                        isRideDropAvailable = false
                                    }
                                } else {
                                    isRideDropAvailable = false
                                }

                                isTrackRideAvailable = true
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                    if (Sstatus.equals("1", ignoreCase = true) && isTrackRideAvailable) {

                        val i = Intent(this@MyRidesDetail, MyRideDetailTrackRide::class.java)
                        i.putExtra("driverID", driverID)
                        i.putExtra("driverName", driverName)
                        i.putExtra("driverImage", driverImage)
                        i.putExtra("driverRating", driverRating)
                        i.putExtra("driverLat", driverLat)
                        i.putExtra("driverLong", driverLong)
                        i.putExtra("driverTime", driverTime)
                        i.putExtra("rideID", rideID)
                        i.putExtra("driverMobile", driverMobile)
                        i.putExtra("driverCar_no", driverCar_no)
                        i.putExtra("driverCar_model", driverCar_model)
                        i.putExtra("userLat", userLat)
                        i.putExtra("userLong", userLong)
                        i.putExtra("rideStatus", sRideStatus)

                        if (isRidePickUpAvailable) {
                            i.putExtra("PickUpLocation", sPickUpLocation)
                            i.putExtra("PickUpLatitude", sPickUpLatitude)
                            i.putExtra("PickUpLongitude", sPickUpLongitude)
                        }

                        if (isRideDropAvailable) {
                            i.putExtra("DropLocation", sDropLocation)
                            i.putExtra("DropLatitude", sDropLatitude)
                            i.putExtra("DropLongitude", sDropLongitude)
                        }

                        startActivity(i)
                        overridePendingTransition(R.anim.enter, R.anim.exit)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //----------------------------------Share Trip post reques------------------------
    private fun share_trip_postRequest_MyRides(mContext: Context, url: String, key: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("------------- ride_id----------------$SrideId_intent")
        println("------------- mobile_no----------------" + Et_share_trip_mobileno!!.text.toString())

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["mobile_no"] = Et_share_trip_mobileno!!.text.toString()

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {

            override fun onCompleteListener(response: String) {
                Log.e("share trip", response)

                var Str_status = ""
                var Str_response = ""
                println("sharetrip response-------------$response")

                try {
                    val `object` = JSONObject(response)
                    Str_status = `object`.getString("status")
                    Str_response = `object`.getString("response")

                    if (Str_status.equals("1", ignoreCase = true)) {

                        Alert(resources.getString(R.string.action_success), Str_response)

                    } else {
                        Alert(resources.getString(R.string.action_error), Str_response)

                    }

                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {

                dialog.dismiss()

            }


        })

    }

    //-----------------------Tip Post Request-----------------
    private fun postRequest_Tip(Url: String, tipStatus: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)


        println("-------------tip Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = SrideId_intent
        if (tipStatus.equals("Apply", ignoreCase = true)) {
            jsonParams["tips_amount"] = Et_tip_Amount!!.text.toString()
        }

        mRequest = ServiceRequest(this@MyRidesDetail)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------tip Response----------------$response")
                var sStatus = ""
                var sResponse = ""
                var sTipAmount = ""
                var sTotalBill = ""
                try {

                    val `object` = JSONObject(response)
                    sStatus = `object`.getString("status")
                    if (sStatus.equals("1", ignoreCase = true)) {

                        val response_Object = `object`.getJSONObject("response")
                        sTipAmount = response_Object.getString("tips_amount")
                        sTotalBill = response_Object.getString("total")

                        if (tipStatus.equals("Apply", ignoreCase = true)) {
                            Tv_tipAmount!!.text = currencySymbol + sTipAmount
                            Tv_totalBill!!.text = currencySymbol + sTotalBill
                            Rl_main_tip!!.visibility = View.GONE
                            Rl_tip!!.visibility = View.GONE
                            Ll_deleteTip!!.visibility = View.VISIBLE
                        } else {

                            Tv_totalBill!!.text = currencySymbol + sTotalBill
                            Tv_tipAmount!!.text = currencySymbol + sTipAmount
                            Cb_tip!!.isChecked = false
                            Et_tip_Amount!!.setText("")
                            Rl_main_tip!!.visibility = View.VISIBLE
                            Ll_deleteTip!!.visibility = View.GONE
                        }

                    } else {
                        sResponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), sResponse)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            return true
        }
        return false
    }


    public override fun onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver)
        super.onDestroy()
    }


    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODE)
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                startActivity(callIntent)
            }
        }
    }

    companion object {
        private var isInternetPresent: Boolean? = false
        lateinit internal var myrideDetail_class: MyRidesDetail

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }

        //code to Check Email Validation
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }


    /*******uber reciept******/
    private fun UberReceipet(request_id_stg: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        uber_TripReceiptRequest = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_ID_stg + request_id_stg + "/receipt", object : com.android.volley.Response.Listener<String> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(response: String) {
                Log.e("uber_receipt", "---$response")
                val currencycode: Currency
                var name = ""
                var type = ""
                var amount = ""
                var date = ""
                var pickup = ""
                var lat = ""
                var lng = ""
                try {

                    val receipt_obj = JSONObject(response)
                    val charge_adjustments = receipt_obj.getJSONArray("charge_adjustments")

                    currencycode = Currency.getInstance(getLocale(receipt_obj.getString("currency_code")))
                    currencycode.symbol

                    val subtotal_stg = receipt_obj.getString("subtotal")
                    val total_charged_stg = receipt_obj.getString("total_charged")
                    val total_owed_stg = receipt_obj.getString("total_owed")
                    val total_fare_stg = receipt_obj.getString("total_fare")
                    // val currency_code_stg = receipt_obj.getString("currency_code")
                    val duration_stg = receipt_obj.getString("duration")
                    val distance_stg = receipt_obj.getString("distance")
                    val distance_label_stg = receipt_obj.getString("distance_label")

                    for (i in 0 until charge_adjustments.length()) {
                        val jObj = charge_adjustments.getJSONObject(i)
                        amount = jObj.getString("amount")
                        type = jObj.getString("type")
                        name = jObj.getString("name")
                    }

                    val request_id = receipt_obj.getString("request_id")

                    if (Sridedate_intent.equals("") || Sridedate_intent.equals(null)) {
                        date = ""
                    } else {
                        if (!Sridedate_intent.equals("00-00-0000")) {
                            date = Sridedate_intent
                        } else {
                            date = ""
                        }
                    }

                    if (Sridepickuploc_intent.equals("") || Sridepickuploc_intent.equals(null)) {
                        pickup = ""
                    } else {
                        pickup = Sridepickuploc_intent
                    }

                    Rl_priceBottom!!.visibility = View.VISIBLE
                    Rl_button!!.visibility = View.VISIBLE
                    Tv_couponDiscount!!.visibility = View.GONE
                    Tv_walletUsuage!!.visibility = View.GONE
                    Ll_mailInvoice!!.visibility = View.VISIBLE
                    Ll_reportIssue!!.visibility = View.VISIBLE
                    Ll_share_Ride!!.visibility = View.GONE

                    tv_pick_up_id.text = pickup
                    Tv_rideDistance!!.text = "$distance_stg mi"
                    Tv_timeTaken!!.text = duration_stg + " " + resources.getString(R.string.my_rides_detail_mins_textview)
                    Tv_waitTime!!.text = "0 " + resources.getString(R.string.my_rides_detail_mins_textview)
                    Tv_paymentType!!.text = ""
                    Tv_totalBill!!.text = total_fare_stg
                    Tv_tipAmount!!.text = currencycode.symbol + "0"
                    Tv_carNo_CRN!!.text = resources.getString(R.string.my_rides_detail_crn_textview) + ":" + request_id
                    Tv_totalPaid!!.text = total_fare_stg
                    Tv_pickup!!.text = date

                    if (Sridelat_intent != "" && Sridelong_intent != "") {
                        lat = Sridelat_intent
                        lng = Sridelong_intent

                        mMap!!.addMarker(MarkerOptions()
                                .position(LatLng(java.lang.Double.parseDouble(lat), java.lang.Double.parseDouble(lng)))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))

                        // Move the camera to last position with a zoom level

                        val cameraPosition = CameraPosition.Builder().target(LatLng(java.lang.Double.parseDouble(lat), java.lang.Double.parseDouble(lng))).zoom(15f).build()
                        val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                        mMap!!.moveCamera(camUpdate)
                    }

                } catch (e: Exception) {

                }
                dialog.dismiss()
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(this@MyRidesDetail, error)
                dialog.dismiss()
                Log.e("error_receipt_uber", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = Iconstant.API_UBER_AUTH_KEY_stg
                return headers
            }

        }
        uber_TripReceiptRequest.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_TripReceiptRequest.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_TripReceiptRequest)
    }


    /********* Lyft receipt **************/
    private fun LyftReceipt(ride_id: String) {
        dialog = Dialog(this@MyRidesDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        val req_url: String = Iconstant.LyftBaseUrl + "v1/rides/" + ride_id + "/receipt"

        lyft_TripReceiptRequest = object : StringRequest(Request.Method.GET, req_url,
                object : com.android.volley.Response.Listener<String> {
                    @SuppressLint("SetTextI18n")
                    override fun onResponse(response: String) {
                        println("----lyft_receipt----$response")
                        var payment_method = ""
                        var duration = ""
                        var distance = ""
                        var date = ""
                        var pickup = ""
                        var lat = ""
                        var lng = ""
                        val currencycode: Currency
                        try {
                            val jObject: JSONObject = JSONObject(response)
                            val _id = jObject.getString("ride_id")
                            //val ride_profile = jObject.getString("ride_profile")
                            //val requested_at = jObject.getString("requested_at")

                            val price = jObject.getJSONObject("price")
                            val total_amount = price.getString("amount")
                            val total_currency = price.getString("currency")
                            /*val description = price.getString("description")*/

                            /* val line_items = jObject.getJSONArray("line_items")
                             for (i in 0 until line_items.length()) {
                                 val obj = line_items.getJSONObject(i)
                                 val amount = obj.getString("amount")
                                 val currency = obj.getString("currency")
                                 val type = obj.getString("type")
                             }*/

                            val charges = jObject.getJSONArray("charges")
                            for (j in 0 until charges.length()) {
                                val obJ = charges.getJSONObject(j)
                                // val charges_amount = obJ.getString("amount")
                                // val charges_currency = obJ.getString("currency")
                                payment_method = obJ.getString("payment_method")
                            }
                            currencycode = Currency.getInstance(getLocale(total_currency))

                            if (Sridedistance_intent.equals("") || Sridedistance_intent.equals(null)) {
                                distance = "0"
                            } else {
                                distance = Sridedistance_intent
                            }

                            if (Srideduration_intent.equals("") || Srideduration_intent.equals(null)) {
                                duration = "0"
                            } else {
                                duration = DateUtils.formatElapsedTime(Srideduration_intent.toLong())
                            }

                            if (Sridepickuploc_intent.equals("") || Sridepickuploc_intent.equals(null)) {
                                pickup = ""
                            } else {
                                pickup = Sridepickuploc_intent
                            }


                            if (Sridedate_intent.equals("") || Sridedate_intent.equals(null)) {
                                date = ""
                            } else {
                                if (!Sridedate_intent.equals("00-00-0000")) {
                                    date = Sridedate_intent
                                } else {
                                    date = ""
                                }
                            }

                            Rl_priceBottom!!.visibility = View.VISIBLE
                            Rl_button!!.visibility = View.VISIBLE
                            Tv_couponDiscount!!.visibility = View.GONE
                            Tv_walletUsuage!!.visibility = View.GONE
                            Ll_mailInvoice!!.visibility = View.VISIBLE
                            Ll_reportIssue!!.visibility = View.VISIBLE
                            Ll_share_Ride!!.visibility = View.GONE

                            tv_pick_up_id.text = pickup
                            Tv_rideDistance!!.text = "$distance mi"
                            Tv_timeTaken!!.text = duration + " " + resources.getString(R.string.my_rides_detail_mins_textview)
                            Tv_waitTime!!.text = "0 " + resources.getString(R.string.my_rides_detail_mins_textview)
                            Tv_carNo_CRN!!.text = resources.getString(R.string.my_rides_detail_crn_textview) + ":" + _id
                            Tv_paymentType!!.text = payment_method
                            Tv_totalBill!!.text = currencycode.symbol + total_amount
                            Tv_tipAmount!!.text = currencycode.symbol + "0"
                            Tv_totalPaid!!.text = currencycode.symbol + total_amount
                            Tv_pickup!!.text = date

                            if (Sridelat_intent != "" && Sridelong_intent != "") {
                                lat = Sridelat_intent
                                lng = Sridelong_intent

                                mMap!!.addMarker(MarkerOptions()
                                        .position(LatLng(java.lang.Double.parseDouble(lat), java.lang.Double.parseDouble(lng)))
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))

                                // Move the camera to last position with a zoom level

                                val cameraPosition = CameraPosition.Builder().target(LatLng(java.lang.Double.parseDouble(lat), java.lang.Double.parseDouble(lng))).zoom(15f).build()
                                val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                                mMap!!.moveCamera(camUpdate)
                            }

                        } catch (e: Exception) {
                            println(e)
                        }
                        dialog.dismiss()

                    }

                }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(this@MyRidesDetail, error)
                dialog.dismiss()
                Log.e("error_receipt_lyft", "---" + error.networkTimeMs + "---" + error.networkResponse)
                /* val err_response = error.networkResponse

                 if (e_response.statusCode != null && err_response.data != null) {
                     val resp = String(err_response.data, Charset.forName("utf-8"))
                     val response = JSONObject(resp)
                     val error_description = response.getString("error_description")
                     val error_code = response.getString("error")

                     Log.e("error response:", "$error_description ----- $error_code")
                     Toast.makeText(this@MyRidesDetail, error_description, Toast.LENGTH_SHORT).show()
                 }*/
            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer $UserAccessToken"
                return headers
            }

        }
        lyft_TripReceiptRequest.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        lyft_TripReceiptRequest.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(lyft_TripReceiptRequest)
    }

}
