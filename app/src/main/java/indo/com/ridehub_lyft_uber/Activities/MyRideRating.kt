package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.android.volley.Request
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.squareup.picasso.Picasso
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.ridehub_lyft_uber.Adapters.RatingAdapter
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.PojoResponse.RatingPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class MyRideRating : ActivityHockeyApp() {
    private var skip: RelativeLayout? = null
    private var submit: RelativeLayout? = null
    private var tv_rating_driver_name: CustomTextView? = null
    private var tv_driver_rating_count: CustomTextView? = null
    private var rate_driver_imageview: RoundedImageView? = null
    private var Et_comment: EditText? = null
    private var iv_cab_type_rating: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var UserID = ""

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal var itemlist: ArrayList<RatingPojo>? = null
    internal lateinit var adapter: RatingAdapter
    private var listview: ExpandableHeightListView? = null
    private var SrideId_intent = ""
    private var isDataAvailable = false



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_rating)
        initialize()

        skip!!.setOnClickListener {
            /*val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            this@MyRideRating.sendBroadcast(broadcastIntent)
            finish()
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)*/
            println("------------Submit Rating Response----------------SKIP")
            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            println("------------SKIP--"+broadcastIntent.action)
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

            val intent = Intent(this@MyRideRating, NavigationDrawer::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)


        }

        submit!!.setOnClickListener {
            var isRatingEmpty = false

            if (itemlist != null) {
                for (i in itemlist!!.indices) {
                    if (itemlist!![i].getRatingcount()?.length == 0 || itemlist!![i].getRatingcount().equals("0.0")) {
                        isRatingEmpty = true
                    }
                }


                if (!isRatingEmpty) {
                    if (isInternetPresent!!) {

                        println("------------ride_id-------------$SrideId_intent")
                        println("------------comments-------------" + Et_comment!!.text.toString())
                        println("------------ratingsFor-------------" + "driver")
                        /*if (Et_comment!!.text.toString().length > 0) {*/
                            val jsonParams = HashMap<String, String>()
                            jsonParams["comments"] = Et_comment!!.text.toString()
                            jsonParams["ratingsFor"] = "driver"
                            jsonParams["ride_id"] = SrideId_intent
                            for (i in itemlist!!.indices) {
                                jsonParams["ratings[$i][option_id]"] = itemlist!![i].getRatingId().toString()
                                jsonParams["ratings[$i][option_title]"] = itemlist!![i].getRatingName().toString()
                                jsonParams["ratings[$i][rating]"] = itemlist!![i].getRatingcount().toString()
                            }
                            println("------------jsonParams-------------$jsonParams")
                            postRequest_SubmitRating(Iconstant.myride_rating_submit_url, jsonParams)
                        /*} else {
                            Alert(getResources().getString(R.string.my_rides_rating_header_sorry_textview), getResources().getString(R.string.my_rides_rating_header_comment_textview))
                        }*/
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                    }
                } else {
                    Alert(getResources().getString(R.string.my_rides_rating_header_sorry_textview), getResources().getString(R.string.my_rides_rating_header_enter_all))
                }

            }
        }
    }

    private fun initialize() {
        session = SessionManager(this@MyRideRating)
        cd = ConnectionDetector(this@MyRideRating)
        isInternetPresent = cd!!.isConnectingToInternet
        itemlist = ArrayList<RatingPojo>()
        skip = findViewById(R.id.my_rides_rating_header_skip_layout) as RelativeLayout
        listview = findViewById(R.id.my_rides_rating_listView) as ExpandableHeightListView
        submit = findViewById(R.id.my_rides_rating_submit_layout) as RelativeLayout
        Et_comment = findViewById(R.id.my_rides_rating_comment_edittext) as EditText
        tv_rating_driver_name = findViewById(R.id.tv_rating_driver_name) as CustomTextView
        tv_driver_rating_count = findViewById(R.id.tv_driver_rating_count) as CustomTextView
        rate_driver_imageview = findViewById(R.id.rate_driver_imageview) as RoundedImageView
        iv_cab_type_rating = findViewById(R.id.iv_cab_type_rating) as ImageView
        Et_comment!!.imeOptions = EditorInfo.IME_ACTION_DONE
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()
        val intent = getIntent()
        SrideId_intent = intent.getStringExtra("RideID")
        if (isInternetPresent!!) {
            postRequest_RatingList(Iconstant.myride_rating_url)
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
        }



    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@MyRideRating)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-----------------------Rating List Post Request-----------------
    private fun postRequest_RatingList(Url: String) {
        dialog = Dialog(this@MyRideRating)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))


        println("-------------Rating List Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["optionsFor"] = "driver"
        jsonParams["ride_id"] = SrideId_intent

        println("rideid-----------$SrideId_intent")

        mRequest = ServiceRequest(this@MyRideRating)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
          override  fun onCompleteListener(response: String) {

                Log.e("rateing", response)

                println("-------------Rating List Response----------------$response")

                var Sstatus = ""
                var SRating_status = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    val driver_name = `object`.getString("driver_name")
                    val driver_image = `object`.getString("driver_image")
                    val driver_review = `object`.getString("driver_review")

                    if(driver_review.equals("")){
                        tv_driver_rating_count!!.setText("4.0")
                    }else{
                        tv_driver_rating_count!!.setText(driver_review)
                    }

                    tv_rating_driver_name!!.setText(driver_name)
                    Picasso.with(this@MyRideRating).load("https://ridehub.co/"+driver_image).into(rate_driver_imageview)

                    SRating_status = `object`.getString("ride_ratting_status")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val payment_array = `object`.getJSONArray("review_options")
                        if (payment_array.length() > 0) {
                            itemlist!!.clear()
                            for (i in 0 until payment_array.length()) {
                                val reason_object = payment_array.getJSONObject(i)
                                val pojo = RatingPojo()
                                pojo.setRatingId(reason_object.getString("option_id"))
                                pojo.setRatingName(reason_object.getString("option_title"))
                                pojo.setRatingcount("")
                                itemlist!!.add(pojo)
                            }
                            isDataAvailable = true
                        } else {
                            isDataAvailable = false
                        }
                    }

                    if (Sstatus.equals("1", ignoreCase = true) && isDataAvailable) {
                        if (SRating_status.equals("1", ignoreCase = true)) {
                            Toast.makeText(getApplicationContext(), "Already submitted your rating", Toast.LENGTH_SHORT).show()
                        } else {
                            adapter = RatingAdapter(this@MyRideRating, itemlist!!)
                            listview!!.adapter = adapter
                            listview!!.isExpanded = true
                        }
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------Submit Rating Post Request-----------------
    private fun postRequest_SubmitRating(Url: String, jsonParams: HashMap<String, String>) {
        dialog = Dialog(this@MyRideRating)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))


        println("-------------Submit Rating Url----------------$Url")

        mRequest = ServiceRequest(this@MyRideRating)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("------------Submit Rating Response----------------$response")

                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val broadcastIntent = Intent()
                        broadcastIntent.action = "com.pushnotification.updateBottom_view"
                        sendBroadcast(broadcastIntent)

                        val mDialog = PkDialog(this@MyRideRating)
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_rating_submit_successfully))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()



                            val broadcastIntent = Intent()
                            broadcastIntent.action = "com.pushnotification.updateBottom_view"
                            println("------------Submit Rating Response----------------OKDONE"+broadcastIntent.action)
                            sendBroadcast(broadcastIntent)
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                            finish()


                            val intent = Intent(this@MyRideRating, NavigationDrawer::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.enter, R.anim.exit)

                            /*val finish_timerPage = Intent()
                            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
                            sendBroadcast(finish_timerPage)
                            val broadcastIntent = Intent()
                            broadcastIntent.action = "com.pushnotification.updateBottom_view"
                            sendBroadcast(broadcastIntent)
                            finish()
                            onBackPressed()
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)*/

                            /* finish();
                        onBackPressed();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override  fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            //Do nothing
            true
        } else false
    }

}

