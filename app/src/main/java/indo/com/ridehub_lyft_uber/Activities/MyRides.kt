package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.Fragments.AllRidesFragment
import indo.com.ridehub_lyft_uber.Fragments.CancelledRidesFragment
import indo.com.ridehub_lyft_uber.Fragments.UpcomingRidesFragment
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import kotlinx.android.synthetic.main.activity_myrides.*
import java.util.*


class MyRides : AppCompatActivity() {
    private var back: RelativeLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var UserID = ""
    internal lateinit var dialog: Dialog
    lateinit var viewPager: ViewPager
   private lateinit var tabLayout: TabLayout
    lateinit var iv_filter_myrides: ImageView
    internal lateinit var dialog_list: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myrides)
        myride_class = this@MyRides
        initialize()

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        //Start XMPP Chat Service
        ChatService.startUserAction(this@MyRides)

    }

    private fun initialize() {

        session = SessionManager(this@MyRides)
        cd = ConnectionDetector(this@MyRides)
        isInternetPresent = cd!!.isConnectingToInternet


        viewPager = findViewById(R.id.container) as ViewPager
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs) as TabLayout
        iv_filter_myrides = findViewById(R.id.iv_filter_myrides) as ImageView
        tabLayout.setupWithViewPager(viewPager);
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager!!.KEY_USERID].toString()




//TODO
        dialog_list = Dialog(this)
        dialog = Dialog(this)

        val window = dialog_list.getWindow();
        //window.setGravity(Gravity.CENTER_VERTICAL);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_filter_myrides)
       // val iv_payforservicedown = dialog_list.findViewById(R.id.iv_payforservicedown) as LinearLayout

        iv_filter_myrides.setOnClickListener {

            if (!dialog_list.isShowing())
                dialog_list.show()
        }



    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@MyRides)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }



    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            return true
        }
        return false
    }

    companion object {
        lateinit var myride_class: MyRides
    }




    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(AllRidesFragment(), "All")
        adapter.addFrag(UpcomingRidesFragment(), "Upcoming")
        adapter.addFrag(CancelledRidesFragment(), "Cancelled")

        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        for (i in 0 until tabs.tabCount) {
            val view = tabs.getTabAt(i) as TextView
            view.setTextColor(resources.getColor(R.color.colorPrimary))
            if(i==1){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.colorPrimary))
            }else if(i==2){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.colorPrimary))
            }else if(i==3){
                val view = tabs.getTabAt(i) as TextView
                view.setTextColor(resources.getColor(R.color.colorPrimary))
            }

        }
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList.get(position)
        }
    }





}

