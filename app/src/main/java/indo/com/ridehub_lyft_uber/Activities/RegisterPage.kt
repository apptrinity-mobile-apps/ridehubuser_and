package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.countrycodepicker.CountryPicker
import com.countrycodepicker.CountryPickerListener
import com.google.android.gms.gcm.GoogleCloudMessaging
import com.widget.CustomTextView
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.gps.GPSTracker
import indo.com.mylibrary.pushnotification.GCMInitializer
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.HockeyApp.FragmentActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.CountryDialCode
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*




class RegisterPage : FragmentActivityHockeyApp() {
    private var back: RelativeLayout? = null
    private var Eusername: EditText? = null
    private var Epassword: EditText? = null
    private var Eemail: EditText? = null
    private var EphoneNo: EditText? = null
    private var Ereferalcode: EditText? = null
    private var submit: Button? = null
    private var help: ImageView? = null
    private var Rl_countryCode: RelativeLayout? = null
    private var Tv_countryCode: TextView? = null
    private var tv_alreadylogin: CustomTextView? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal lateinit var mHandler: Handler
    //------------------GCM Initialization------------------
    private val gcm: GoogleCloudMessaging? = null
    private var GCM_Id = ""

    internal lateinit var picker: CountryPicker
    private var gpsTracker: GPSTracker? = null

    //----------------------Code for TextWatcher-------------------------
    private val loginEditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Eusername!!.text.length > 0) {
                Eusername!!.error = null
            }
            if (Epassword!!.text.length > 0) {
                Epassword!!.error = null
            }
            if (Eemail!!.text.length > 0) {
                Eemail!!.error = null
            }
            if (EphoneNo!!.text.length > 0) {
                EphoneNo!!.error = null
            }

        }
    }

    //--------Handler Method------------
    internal var dialogRunnable: Runnable = Runnable {
        dialog = Dialog(this@RegisterPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_verifying))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registerpage)
        context = getApplicationContext()
        initialize()

        help!!.setOnClickListener { Referral_information() }

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        Rl_countryCode!!.setOnClickListener { picker.show(getSupportFragmentManager(), "COUNTRY_PICKER") }

        picker.setListener(object : CountryPickerListener {
            override fun onSelectCountry(name: String, code: String, dialCode: String) {
                picker.dismiss()
                Tv_countryCode!!.text = dialCode

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(Rl_countryCode!!.windowToken, 0)
            }
        })

        submit!!.setOnClickListener {

            if (Eusername!!.text.toString().length == 0) {
                erroredit(Eusername!!, getResources().getString(R.string.register_label_alert_username))
            }else if (!isValidEmail(Eemail!!.text.toString())) {
                erroredit(Eemail!!, getResources().getString(R.string.register_label_alert_email))
            } else if (!isValidPhoneNumber(EphoneNo!!.text.toString())) {
                erroredit(EphoneNo!!, getResources().getString(R.string.register_label_alert_phoneNo))
            }else if (!isValidPassword(Epassword!!.text.toString())) {
                erroredit(Epassword!!, getResources().getString(R.string.register_label_alert_password))
            }    else if (Tv_countryCode!!.text.toString().equals("code", ignoreCase = true)) {
                erroredit(EphoneNo!!, getResources().getString(R.string.register_label_alert_country_code))
            } else {

                cd = ConnectionDetector(this@RegisterPage)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {

                    mHandler.post(dialogRunnable)

                    //---------Getting GCM Id----------
                    val initializer = GCMInitializer(this@RegisterPage, object : GCMInitializer.CallBack {
                       override fun onRegisterComplete(registrationId: String) {

                            GCM_Id = registrationId
                            PostRequest(Iconstant.register_url)
                        }

                        override fun onError(errorMsg: String) {
                            PostRequest(Iconstant.register_url)
                        }
                    })
                    initializer.init()

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message))
                }
            }
        }


        Eusername!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Eusername!!)
            }
            false
        }


        Epassword!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Epassword!!)
            }
            false
        }

        Eemail!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Eemail!!)
            }
            false
        }


        EphoneNo!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(EphoneNo!!)
            }
            false
        }
        Ereferalcode!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Ereferalcode!!)
            }
            false
        }

        /*tv_alreadylogin!!.setOnEditorActionListener { v, actionId, event ->
            val intent = Intent(context, LoginPage::class.java)
            startActivity(intent)
            false
        }*/


    }

    private fun initialize() {
        cd = ConnectionDetector(this@RegisterPage)
        isInternetPresent = cd!!.isConnectingToInternet
        mHandler = Handler()
        picker = CountryPicker.newInstance("Select Country")

        back = findViewById(R.id.register_header_back_layout) as RelativeLayout
        Eusername = findViewById(R.id.register_username_editText) as EditText
        Epassword = findViewById(R.id.register_password_editText) as EditText
        Eemail = findViewById(R.id.register_email_editText) as EditText
        EphoneNo = findViewById(R.id.register_phone_editText) as EditText
        Ereferalcode = findViewById(R.id.register_referalcode_editText) as EditText
        help = findViewById(R.id.register_referalcode_help_image) as ImageView
        Rl_countryCode = findViewById(R.id.register_country_code_layout) as RelativeLayout
        Tv_countryCode = findViewById(R.id.register_country_code_textview) as TextView
        //tv_alreadylogin = findViewById(R.id.tv_alreadylogin) as CustomTextView
        submit = findViewById(R.id.register_submit_button) as Button
        submit!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")

        //code to make password editText as dot
        Epassword!!.transformationMethod = PasswordTransformationMethod()

        Eusername!!.addTextChangedListener(loginEditorWatcher)
        Epassword!!.addTextChangedListener(loginEditorWatcher)


        gpsTracker = GPSTracker(this@RegisterPage)
        if (gpsTracker!!.canGetLocation() && gpsTracker!!.isgpsenabled()) {

            val MyCurrent_lat = gpsTracker!!.getLatitude()
            val MyCurrent_long = gpsTracker!!.getLongitude()

            val geocoder = Geocoder(this, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1)
                if (addresses != null && !addresses.isEmpty()) {

                    val Str_getCountryCode = addresses[0].countryCode
                    if (Str_getCountryCode.length > 0 && Str_getCountryCode != null && Str_getCountryCode != "null") {
                       val Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode)
                        Tv_countryCode!!.setText(Str_countyCode)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

    }

    private fun Referral_information() {
        val dialog = MaterialDialog(this@RegisterPage)
        val view = LayoutInflater.from(this).inflate(R.layout.register_referalcode_dialog, null)

        val tv_ok = view.findViewById(R.id.referral_code_popup_text_ok) as TextView
        tv_ok.setOnClickListener { dialog.dismiss() }
        dialog.setView(view).show()
    }

    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@RegisterPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@RegisterPage, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    // validating password with retype password
    private fun isValidPassword(pass: String): Boolean {
        return if (pass.length < 6) {
            false
        } else {
            true
        }/*
             * else if(!pass.matches("(.*[A-Z].*)")) { return false; }
			 *//*else if (!pass.matches("(.*[a-z].*)")) {
            return false;
        } else if (!pass.matches("(.*[0-9].*)")) {
            return false;
        }*//*
             * else if(!pass.matches(
			 * "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)")) {
			 * return false; }
			 */

    }


    // -------------------------code for Login Post Request----------------------------------
    private fun PostRequest(Url: String) {

        println("--------------register url-------------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_name"] = Eusername!!.text.toString()
        jsonParams["email"] = Eemail!!.text.toString()
        jsonParams["password"] = Epassword!!.text.toString()
        jsonParams["phone_number"] = EphoneNo!!.text.toString()
        jsonParams["country_code"] = Tv_countryCode!!.text.toString()
        jsonParams["referal_code"] = Ereferalcode!!.text.toString()
        jsonParams["gcm_id"] = GCM_Id

        Log.e("REGISTERPARAMS",Eusername!!.text.toString()+"----"+Eemail!!.text.toString()+"----"+Epassword!!.text.toString()+"----"+EphoneNo!!.text.toString()+"----"+Tv_countryCode!!.text.toString()+"----"+Ereferalcode!!.text.toString()+"----"+GCM_Id)

        mRequest = ServiceRequest(this@RegisterPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
          override  fun onCompleteListener(response: String) {

                Log.e("registr", response)

                println("--------------register reponse-------------------$response")

                var Sstatus = ""
                var Smessage = ""
                var Sotp_status = ""
                var Sotp = ""

                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("message")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Sotp_status = `object`.getString("otp_status")
                        Sotp = `object`.getString("otp")
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }


                if (Sstatus.equals("1", ignoreCase = true)) {
                    val intent = Intent(context, OtpPage::class.java)
                    intent.putExtra("Otp_Status", Sotp_status)
                    intent.putExtra("Otp", Sotp)
                    intent.putExtra("UserName", Eusername!!.text.toString())
                    intent.putExtra("Email", Eemail!!.text.toString())
                    intent.putExtra("Password", Epassword!!.text.toString())
                    intent.putExtra("Phone", EphoneNo!!.text.toString())
                    intent.putExtra("CountryCode", Tv_countryCode!!.text.toString())
                    intent.putExtra("ReferalCode", Ereferalcode!!.text.toString())
                    intent.putExtra("GcmID", GCM_Id)

                    println("gcm---------$GCM_Id")

                    startActivity(intent)
                    finish()
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

                } else {
                    Alert(getResources().getString(R.string.login_label_alert_register_failed), Smessage)
                }

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(Eusername!!.windowToken, 0)

                dialog.dismiss()
            }

           override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override  fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@RegisterPage.finish()
            this@RegisterPage.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }

    companion object {


        //-------------------------code to Check Email Validation-----------------------
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        // validating Phone Number
        fun isValidPhoneNumber(target: CharSequence?): Boolean {
            return if (target == null || TextUtils.isEmpty(target) || target.length <= 9) {
                false
            } else {
                android.util.Patterns.PHONE.matcher(target).matches()
            }
        }
    }
}
