package indo.com.ridehub_lyft_uber.Activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import at.grabner.circleprogress.CircleProgressView
import at.grabner.circleprogress.TextMode
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.subclass.ActivitySubClass
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class TimerPage : ActivitySubClass() {
    internal lateinit var mCircleView: CircleProgressView
    //private CountDownTimer timer;
    internal var seconds = 0
    private var retry: String? = ""
    private var rideID = ""
    private var userID = ""

    internal lateinit var updateReciver: BroadcastReceiver
    private var sessionManager: SessionManager? = null

    private var Iv_cancelRide: ImageView? = null
    private var Ll_cancelRide_message: LinearLayout? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var mRequest: ServiceRequest? = null
    private var toast: Toast? = null
    private var mdialog: PkDialog? = null
    internal lateinit var mHandler: Handler
    internal var count = 0


    internal var mRunnable: Runnable = object : Runnable {
        override fun run() {
            if (count < seconds) {
                count++
                mCircleView.setText(Math.abs(seconds - count).toString())
                mCircleView.setTextMode(TextMode.TEXT)
                mCircleView.setValueAnimated(count.toFloat(), 500)
                mHandler.postDelayed(this, 1000)
            } else {
                mHandler.removeCallbacks(this)

                if (mRequest != null) {
                    mRequest!!.cancelRequest()
                }

                val returnIntent = Intent()
                returnIntent.putExtra("Accepted_or_Not", "not")
                returnIntent.putExtra("Retry_Count", retry)
                setResult(RESULT_OK, returnIntent)
                finish()
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.timerpage)
        initialize()

        //Start XMPP Chat Service
        ChatService.startUserAction(this@TimerPage)

        // Receiving the data from broadcast
        val filter = IntentFilter()
        filter.addAction("com.app.pushnotification.RideAccept")
        updateReciver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                println("----------message--------------" + intent.getStringExtra("message"))

                if (intent.getStringExtra("Action").equals("ride_confirmed", ignoreCase = true)) {
                    val i = Intent(this@TimerPage, TrackYourRide::class.java)
                    i.putExtra("driverID", intent.getStringExtra("driverID"))
                    i.putExtra("driverName", intent.getStringExtra("driverName"))
                    i.putExtra("driverImage", intent.getStringExtra("driverImage"))
                    i.putExtra("driverRating", intent.getStringExtra("driverRating"))
                    i.putExtra("driverLat", intent.getStringExtra("driverLat"))
                    i.putExtra("driverLong", intent.getStringExtra("driverLong"))
                    i.putExtra("driverTime", intent.getStringExtra("driverTime"))
                    i.putExtra("rideID", intent.getStringExtra("rideID"))
                    i.putExtra("driverMobile", intent.getStringExtra("driverMobile"))
                    i.putExtra("driverCar_no", intent.getStringExtra("driverCar_no"))
                    i.putExtra("driverCar_model", intent.getStringExtra("driverCar_model"))
                    i.putExtra("source_Location", intent.getStringExtra("source_Location"))
                    i.putExtra("userLat", intent.getStringExtra("userLatitude"))
                    i.putExtra("userLong", intent.getStringExtra("userLongitude"))
                    i.putExtra("cat_type", intent.getStringExtra("cab_type"))
                    i.putExtra("destination_Location", intent.getStringExtra("destination_Location"))
                    startActivity(i)
                    mHandler.removeCallbacks(mRunnable)
                    //timer.cancel();

                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }

                    finish()
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }

            }
        }
        registerReceiver(updateReciver, filter)


        Iv_cancelRide!!.setOnClickListener {
            mdialog = PkDialog(this@TimerPage)
            mdialog!!.setDialogTitle(getResources().getString(R.string.timer_label_alert_cancel_ride))
            mdialog!!.setDialogMessage(getResources().getString(R.string.timer_label_alert_cancel_ride_message))
            mdialog!!.setPositiveButton(getResources().getString(R.string.timer_label_alert_yes), View.OnClickListener {
                mdialog!!.dismiss()
                cd = ConnectionDetector(this@TimerPage)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    toast!!.cancel()
                    DeleteRideRequest(Iconstant.delete_ride_url)
                } else {
                    toast!!.setText(getResources().getString(R.string.alert_nointernet_message))
                    toast!!.show()
                }
            })
            mdialog!!.setNegativeButton(getResources().getString(R.string.timer_label_alert_no), View.OnClickListener { mdialog!!.dismiss() })
            mdialog!!.show()
        }

    }

    private fun initialize() {
        toast = Toast(this@TimerPage)
        sessionManager = SessionManager(this@TimerPage)

        Iv_cancelRide = findViewById(R.id.timer_cancel_ride_image) as ImageView
        Ll_cancelRide_message = findViewById(R.id.timer_cancel_ride_layout) as LinearLayout
        mCircleView = findViewById(R.id.timer_circleView) as CircleProgressView
        mCircleView.isEnabled = false
        mCircleView.isFocusable = false

        val userDetail = sessionManager!!.getUserDetails()
        userID = userDetail.get(SessionManager.KEY_USERID).toString()

        val intent = getIntent()
        seconds = Integer.parseInt(intent.getStringExtra("Time")) + 1
        retry = intent.getStringExtra("retry_count")
        rideID = intent.getStringExtra("ride_ID")

        if (retry != null && retry!!.length > 0) {
            retry = "2"
        } else {
            retry = "1"
        }


        //value setting
        mCircleView.setMaxValue(seconds.toFloat())
        mCircleView.setValueAnimated(0f)

        //show unit
        // mCircleView.setUnit("");
        // mCircleView.setShowUnit(true);

        //text sizes
        mCircleView.textSize = 50
        // mCircleView.setUnitSize(40); // if i set the text size i also have to set the unit size

        // enable auto text size, previous values are overwritten
        mCircleView.isAutoTextSize = true

        //if you want the calculated text sizes to be bigger/smaller you can do so via
        //mCircleView.setUnitScale(0.9f);
        mCircleView.textScale = 0.6f

        //colors of text and unit can be set via
        mCircleView.textColor = getResources().getColor(R.color.white)


        /*timer =new CountDownTimer((seconds*1000), 500)
        {
            public void onTick(long millisUntilFinished)
            {
                long sec = millisUntilFinished/1000;

                mCircleView.setText(String.valueOf(sec));
                mCircleView.setTextMode(TextMode.TEXT);
                mCircleView.setValueAnimated(sec, 500);
            }
            public void onFinish()
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Accepted_or_Not", "not");
                returnIntent.putExtra("Retry_Count", retry);
                setResult(RESULT_OK, returnIntent);
                onBackPressed();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        };
        timer.start();*/

        println("Seconds $seconds")

        mHandler = Handler()
        mHandler.post(mRunnable)
    }


    //-------------------Delete Ride Post Request----------------

    private fun DeleteRideRequest(Url: String) {

        Iv_cancelRide!!.visibility = View.GONE
        Ll_cancelRide_message!!.visibility = View.VISIBLE

        println("--------------Timer Delete Ride url-------------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["ride_id"] = rideID

        mRequest = ServiceRequest(this@TimerPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("--------------Timer Delete Ride reponse-------------------$response")

                try {
                    val `object` = JSONObject(response)
                    if (`object`.length() > 0) {
                        val status = `object`.getString("status")
                        val response_value = `object`.getString("response")
                        if (status.equals("1", ignoreCase = true)) {

                            Iv_cancelRide!!.visibility = View.VISIBLE
                            Ll_cancelRide_message!!.visibility = View.GONE
                            mHandler.removeCallbacks(mRunnable)


                            val mDialog = PkDialog(this@TimerPage)
                            mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                            mDialog.setDialogMessage(response_value)
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                                mDialog.dismiss()

                                val returnIntent = Intent()
                                returnIntent.putExtra("Accepted_or_Not", "Cancelled")
                                returnIntent.putExtra("Retry_Count", retry)
                                setResult(RESULT_OK, returnIntent)
                                finish()
                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                            })
                            mDialog.show()

                        } else {
                            toast!!.setText(response_value)
                            toast!!.show()
                        }
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                Iv_cancelRide!!.visibility = View.VISIBLE
                Ll_cancelRide_message!!.visibility = View.GONE
            }

            override fun onErrorListener() {
                Iv_cancelRide!!.visibility = View.VISIBLE
                Ll_cancelRide_message!!.visibility = View.GONE
            }
        })
    }


    protected override fun onResume() {
        ChatService.startUserAction(this@TimerPage)
        super.onResume()
    }

    override fun onDestroy() {
        //timer.cancel();

        if (mdialog != null) {
            mdialog!!.dismiss()
        }

        mHandler.removeCallbacks(mRunnable)
        unregisterReceiver(updateReciver)
        super.onDestroy()
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            //Do nothing
            true
        } else false
    }

}
