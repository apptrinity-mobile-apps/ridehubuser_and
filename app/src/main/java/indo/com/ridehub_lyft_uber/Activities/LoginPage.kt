package indo.com.ridehub_lyft_uber.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.pushnotification.GCMInitializer
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.AppInfoSessionManager
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.CurrencySymbolConverter
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.util.*

class LoginPage : ActivityHockeyApp() {
    private var back: RelativeLayout? = null
    private var forgotPwd: RelativeLayout? = null
    private var username: EditText? = null
    private var password: EditText? = null
    private var tv_signup: CustomTextView? = null
    private var submit: Button? = null
    private var Tv_or: TextView? = null

    private var facebooklayout: Button? = null
    // Instance of FaceBook Class
    /*private val facebook = Facebook(APP_ID)
    internal var mAsyncRunner: AsyncFacebookRunner*/
    private var mPrefs: SharedPreferences? = null
    private var email = ""
    private var profile_image = ""
    private var username1 = ""
    private var userid = ""
    private val jsonObjReq: JsonObjectRequest? = null
    private val postrequest: StringRequest? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var session: SessionManager? = null

    private var appinfo_session: AppInfoSessionManager? = null

    private var Str_Hash = ""

    private var mHandler: Handler? = null
    private var sCurrencySymbol = ""
    private var android_id: String? = null

    private var GCM_Id = ""

    private var sMediaId = ""



    private var Str_FacebookId: String? = ""

    //----------------------Code for TextWatcher-------------------------
    private val loginEditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (username!!.text.length > 0) {
                username!!.error = null
            }
            if (password!!.text.length > 0) {
                password!!.error = null
            }
        }
    }

    //--------Handler Method------------
    internal var dialogRunnable: Runnable = Runnable {
        dialog = Dialog(this@LoginPage)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    //--------Handler Method------------
    internal var dialogFacebookRunnable: Runnable = Runnable {
        dialog = Dialog(this@LoginPage)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


      /*  requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.loginpage)
        context = applicationContext
        //mAsyncRunner = AsyncFacebookRunner(facebook)
        android_id = Settings.Secure.getString(context!!.contentResolver,
                Settings.Secure.ANDROID_ID)
        initialize()

        try {
            val info = packageManager.getPackageInfo(
                    "your.package",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }



      /*  back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)
            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }*/

        forgotPwd!!.setOnClickListener {
          val i = Intent(this@LoginPage, ForgotPassword::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        tv_signup!!.setOnClickListener {
             val i = Intent(this@LoginPage, RegisterPage::class.java)
               startActivity(i)
               overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        submit!!.setOnClickListener {
            if (username!!.text.toString().length == 0) {
                erroredit(username!!, resources.getString(R.string.login_label_alert_username))
            } else if (!isValidEmail(username!!.text.toString())) {
                erroredit(username!!, resources.getString(R.string.login_label_alert_email_invalid))
            } else if (password!!.text.toString().length == 0) {
                erroredit(password!!, resources.getString(R.string.login_label_alert_password))
            } else {
                cd = ConnectionDetector(this@LoginPage)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    mHandler!!.post(dialogRunnable)
                    //---------Getting GCM Id----------
                    val initializer = GCMInitializer(this@LoginPage, object : GCMInitializer.CallBack {
                        override fun onRegisterComplete(registrationId: String) {
                            GCM_Id = registrationId
                            PostRequest(Iconstant.loginurl)
                        }

                        override fun onError(errorMsg: String) {
                            PostRequest(Iconstant.loginurl)
                        }
                    })
                    initializer.init()

                } else {
                    Alert(resources.getString(R.string.alert_nointernet), resources.getString(R.string.alert_nointernet_message))
                }
            }
        }


        username!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(username!!)
            }
            false
        }


        password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(password!!)
            }
            false
        }

        /*facebooklayout!!.setOnClickListener {
            logoutFromFacebook()
            loginToFacebook()
        }*/
    }

    private fun initialize() {
        session = SessionManager(this@LoginPage)
        appinfo_session = AppInfoSessionManager(this@LoginPage)
        cd = ConnectionDetector(this@LoginPage)
        isInternetPresent = cd!!.isConnectingToInternet
        mHandler = Handler()

       /* val user = appinfo_session.getAppInfo()
        Str_FacebookId = user.get(AppInfoSessionManager.KEY_FACEBOOK_ID)*/


        val i = intent
        Str_Hash = i.getStringExtra("HashKey")

        println("Str_Hash------------$Str_Hash")

        //Tv_or = findViewById(R.id.login_or_label) as TextView
       // back = findViewById(R.id.login_header_back_layout) as RelativeLayout
        forgotPwd = findViewById(R.id.login_forgotpwd_layout) as RelativeLayout
        username = findViewById(R.id.login_email_editText) as EditText
        tv_signup = findViewById(R.id.tv_signup) as CustomTextView


        password = findViewById(R.id.login_password_editText) as EditText
        submit = findViewById(R.id.login_submit_button) as Button

        //facebooklayout = findViewById(R.id.login_facebook_button) as Button


        /*if (Str_FacebookId != null && Str_FacebookId!!.length > 0) {
            facebooklayout!!.visibility = View.VISIBLE
            Tv_or!!.visibility = View.VISIBLE

        } else {
            facebooklayout!!.visibility = View.GONE
            Tv_or!!.visibility = View.GONE
        }*/

        submit!!.typeface = Typeface.createFromAsset(assets, "fonts/Poppins-Medium.ttf")

        //code to make password editText as dot
        password!!.transformationMethod = PasswordTransformationMethod()

        username!!.addTextChangedListener(loginEditorWatcher)
        password!!.addTextChangedListener(loginEditorWatcher)
    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(edittext.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@LoginPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@LoginPage, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }


    // -------------------------code for Login Post Request----------------------------------

    private fun PostRequest(Url: String) {

        println("-------GCM_Id-------$GCM_Id")

        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = username!!.text.toString()
        jsonParams["password"] = password!!.text.toString()
        jsonParams["gcm_id"] = GCM_Id

        mRequest = ServiceRequest(this@LoginPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                Log.e("login", response)

                println("--------------Login reponse-------------------$response")
                var Sstatus = ""
                var Smessage = ""
                var Suser_image = ""
                var Suser_id = ""
                var Suser_name = ""
                var Semail = ""
                var Scountry_code = ""
                var SphoneNo = ""
                var Sreferal_code = ""
                var Scategory = ""
                val Subcategory = ""
                var SsecretKey = ""
                var SwalletAmount = ""
                var ScurrencyCode = ""

                var is_alive_other = ""

                var gcmId = ""


                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("message")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Suser_image = `object`.getString("user_image")
                        Suser_id = `object`.getString("user_id")
                        Suser_name = `object`.getString("user_name")
                        Semail = `object`.getString("email")
                        Scountry_code = `object`.getString("country_code")
                        SphoneNo = `object`.getString("phone_number")
                        Sreferal_code = `object`.getString("referal_code")
                        Scategory = `object`.getString("category")
                        //Subcategory = object.getString("subcategory");
                        SsecretKey = `object`.getString("sec_key")
                        SwalletAmount = `object`.getString("wallet_amount")
                        gcmId = `object`.getString("key")
                        ScurrencyCode = `object`.getString("currency")
                        is_alive_other = `object`.getString("is_alive_other")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (Sstatus.equals("1", ignoreCase = true)) {
                    session!!.createLoginSession(Semail, Suser_id, Suser_name, Suser_image, Scountry_code, SphoneNo, Sreferal_code, Scategory, Subcategory, gcmId)
                    session!!.createWalletAmount(sCurrencySymbol + SwalletAmount)

                    Log.e("login_response", "$Suser_id==$SsecretKey")
                    session!!.setXmppKey(Suser_id, SsecretKey)

                    println("insidesession gcm--------------$gcmId")

                    if (is_alive_other.equals("Yes", ignoreCase = true)) {

                        val mDialog = PkDialog(this@LoginPage)
                        mDialog.setDialogTitle(resources.getString(R.string.app_name))
                        mDialog.setDialogMessage(resources.getString(R.string.alert_multiple_login))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()

                            ChatService.startUserAction(this@LoginPage)
                            SingUpAndSignIn.activty.finish()
                            val intent = Intent(context, UpdateUserLocation::class.java)
                            //val intent = Intent(context, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                        mDialog.show()

                    } else {
                        ChatService.startUserAction(this@LoginPage)
                        SingUpAndSignIn.activty.finish()
                        val intent = Intent(context, UpdateUserLocation::class.java)
                        //val intent = Intent(context, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                    }

                    /*  //starting XMPP service
                    if("No".equalsIgnoreCase(is_alive_other)){
                        ChatService.startUserAction(LoginPage.this);
                        SingUpAndSignIn.activty.finish();
                        Intent intent = new Intent(context, UpdateUserLocation.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);

                    }else{
                        Alert(getResources().getString(R.string.alert_multiple_login), Smessage);
                    }*/

                } else {
                    Alert(resources.getString(R.string.login_label_alert_signIn_failed), Smessage)
                }
                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(username!!.windowToken, 0)

                if (dialog != null) {
                    dialog!!.dismiss()
                }
            }

           override fun onErrorListener() {
                if (dialog != null) {
                    dialog!!.dismiss()
                }
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

           /* // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)*/

            this@LoginPage.finish()
            this@LoginPage.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }


    //--------------------------------code for faceBook------------------------------

    /*fun loginToFacebook() {

        println("---------------facebook login1-----------------------")
        mPrefs = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE)
        val access_token = mPrefs!!.getString("access_token", null)
        val expires = mPrefs!!.getLong("access_expires", 0)

        if (access_token != null) {
            facebook.setAccessToken(access_token)
        }


        println("---------------facebook expires-----------------------$expires")

        if (expires != 0L) {
            facebook.setAccessExpires(expires)
        }

        System.out.println("---------------facebook isSessionValid-----------------------" + facebook.isSessionValid())
        if (!facebook.isSessionValid()) {
            facebook.authorize(this@LoginPage,
                    arrayOf("email"),
                    object : Facebook.DialogListener() {

                        fun onCancel() {
                            // Function to handle cancel event
                        }

                        fun onComplete(values: Bundle) {
                            // Function to handle complete event
                            // Edit Preferences and update facebook acess_token
                            val editor = mPrefs!!.edit()
                            editor.putString("access_token",
                                    facebook.getAccessToken())
                            editor.putLong("access_expires", facebook.getAccessExpires())
                            editor.commit()
                            val accessToken = facebook.getAccessToken()

                            mHandler!!.post(dialogFacebookRunnable)
                            //---------Getting GCM Id----------
                            val initializer = GCMInitializer(this@LoginPage, object : GCMInitializer.CallBack() {
                                fun onRegisterComplete(registrationId: String) {
                                    GCM_Id = registrationId
                                    //getProfileInformation();

                                    val accessToken1 = facebook.getAccessToken()
                                    JsonRequest("https://graph.facebook.com/me?fields=id,name,picture,email&access_token=$accessToken1")
                                }

                                fun onError(errorMsg: String) {
                                    //getProfileInformation();

                                    val accessToken1 = facebook.getAccessToken()
                                    JsonRequest("https://graph.facebook.com/me?fields=id,name,picture,email&access_token=$accessToken1")
                                }
                            })
                            initializer.init()
                        }

                        fun onError(error: DialogError) {
                            // Function to handle error

                        }

                        fun onFacebookError(fberror: FacebookError) {
                            // Function to handle Facebook errors

                        }
                    })
        }
    }

    fun getProfileInformation() {
        mAsyncRunner.request("me", object : AsyncFacebookRunner.RequestListener() {
            fun onComplete(response: String, state: Any) {
                try {

                    println("----------facebook response----------------$response")

                    // Facebook Profile JSON data
                    val profile = JSONObject(response)
                    sMediaId = profile.getString("id")
                    this@LoginPage.runOnUiThread { PostRequest_facebook(Iconstant.social_check_url) }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }

            fun onIOException(e: IOException, state: Any) {}

            fun onFileNotFoundException(e: FileNotFoundException,
                                        state: Any) {
            }

            fun onMalformedURLException(e: MalformedURLException,
                                        state: Any) {
            }

            fun onFacebookError(e: FacebookError, state: Any) {}
        })
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        facebook.authorizeCallback(requestCode, resultCode, data)
    }

    fun logoutFromFacebook() {
        Util.clearCookies(this@LoginPage)
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }*/

   /* private fun PostRequest_facebook(Url: String) {


        val progress: ProgressDialog?
        progress = ProgressDialog(this@LoginPage)
        progress.setMessage(resources.getString(R.string.action_pleasewait))
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progress.isIndeterminate = false
        progress.show()

        println("-----------media_id 1------------$sMediaId")
        println("-----------deviceToken 1------------" + "")
        println("-----------gcm_id 1------------$GCM_Id")
        println("-----------email 1------------$email")

        val jsonParams = HashMap<String, String>()
        jsonParams["media_id"] = sMediaId
        jsonParams["deviceToken"] = ""
        jsonParams["gcm_id"] = GCM_Id
        jsonParams["email"] = email

        mRequest = ServiceRequest(this@LoginPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener() {
            override fun onCompleteListener(response: String) {

                println("--------------Login reponse-------------------$response")

                var Sstatus = ""
                var Smessage = ""
                var Suser_image = ""
                var Suser_id = ""
                var Suser_name = ""
                var Semail = ""
                var Scountry_code = ""
                var SphoneNo = ""
                var Sreferal_code = ""
                var Scategory = ""
                var Subcategory = ""
                var SsecretKey = ""
                var SwalletAmount = ""
                var ScurrencyCode = ""

                var gcmId = ""
                var is_alive_other = ""

                try {

                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("message")
                    println("---------Sstatus--------$Sstatus")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Suser_image = `object`.getString("user_image")
                        Suser_id = `object`.getString("user_id")
                        Suser_name = `object`.getString("user_name")
                        Semail = `object`.getString("email")
                        Scountry_code = `object`.getString("country_code")
                        SphoneNo = `object`.getString("phone_number")
                        Sreferal_code = `object`.getString("referal_code")
                        Scategory = `object`.getString("category")
                        Subcategory = `object`.getString("subcategory")
                        SsecretKey = `object`.getString("sec_key")
                        SwalletAmount = `object`.getString("wallet_amount")

                        gcmId = `object`.getString("key")
                        is_alive_other = `object`.getString("is_alive_other")

                        ScurrencyCode = `object`.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (Sstatus.equals("1", ignoreCase = true)) {
                    session!!.createLoginSession(Semail, Suser_id, Suser_name, Suser_image, Scountry_code, SphoneNo, Sreferal_code, Scategory, Subcategory, gcmId)
                    session!!.createWalletAmount(sCurrencySymbol + SwalletAmount)
                    session!!.setXmppKey(Suser_id, SsecretKey)

                    //starting XMPP service
                    ChatService.startUserAction(this@LoginPage)
                    SingUpAndSignIn.activty.finish()
                    val intent = Intent(context, UpdateUserLocation::class.java)
                    startActivity(intent)
                    finish()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else if (Sstatus.equals("2", ignoreCase = true)) {

                    val intent = Intent(this@LoginPage, RegisterFacebook::class.java)
                    intent.putExtra("userId", userid)
                    intent.putExtra("userName", username1)
                    intent.putExtra("userEmail", email)
                    intent.putExtra("media", sMediaId)
                    startActivity(intent)
                    finish()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(username!!.windowToken, 0)

                } else {
                    Alert(resources.getString(R.string.login_label_alert_signIn_failed), Smessage)
                }

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(username!!.windowToken, 0)
                progress.dismiss()
            }

            fun onErrorListener() {
                progress?.dismiss()
            }
        })
    }

    private fun JsonRequest(Url: String) {

        val jsonParams = HashMap<String, String>()
        mRequest = ServiceRequest(this@LoginPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.GET, jsonParams, object : ServiceRequest.ServiceListener() {
            fun onCompleteListener(response: String) {

                println("--------------access token reponse-------------------$response")

                try {

                    val `object` = JSONObject(response)
                    println("---------facebook profile------------$response")


                    sMediaId = `object`.getString("id")
                    userid = `object`.getString("id")
                    profile_image = "https://graph.facebook.com/" + `object`.getString("id") + "/picture?type=large"
                    username1 = `object`.getString("name")
                    username1 = username1.replace("\\s+".toRegex(), "")


                    if (`object`.has("email")) {
                        email = `object`.getString("email")
                    } else {
                        email = ""
                    }
                    println("-------sMediaId------------------$sMediaId")
                    println("-------email------------------$email")
                    println("-----------------userid-------------------------------$userid")
                    println("----------------profile_image-----------------$profile_image")
                    println("-----------username----------$username1")

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                //post execute
                dialog!!.dismiss()

                PostRequest_facebook(Iconstant.social_check_url)
            }

            fun onErrorListener() {
                if (dialog != null) {
                    dialog!!.dismiss()
                }
            }
        })
    }*/

    companion object {
        // Your FaceBook APP ID
        //private static String APP_ID = "468945646630814";
        private val APP_ID = "335037517017419"


        //-------------------------code to Check Email Validation-----------------------
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }


        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }


}
