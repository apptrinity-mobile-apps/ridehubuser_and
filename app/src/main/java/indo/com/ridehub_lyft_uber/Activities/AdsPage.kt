package indo.com.ridehub_lyft_uber.Activities

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import indo.com.ridehub_lyft_uber.R


class AdsPage : Activity() {
    private var Iv_close: ImageView? = null
    private var Iv_banner: ImageView? = null
    private var Tv_title: TextView? = null
    private var Tv_message: TextView? = null
    private var Vi_space: View? = null

    private var sTitle = ""
    private var sMessage = ""
    private var mBanner = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ads_page)
        initialize()

        Iv_close!!.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    private fun initialize() {
        Iv_close = findViewById(R.id.ads_page_close_imageView) as ImageView
        Iv_banner = findViewById(R.id.ads_page_banner_image) as ImageView
        Tv_title = findViewById(R.id.ads_page_title) as TextView
        Tv_message = findViewById(R.id.ads_page_message) as TextView
        Vi_space = findViewById(R.id.ads_page_view) as View

        val intent = intent

        if (intent.hasExtra("AdsTitle")) {
            sTitle = intent.getStringExtra("AdsTitle")
        }

        if (intent.hasExtra("AdsMessage")) {
            sMessage = intent.getStringExtra("AdsMessage")
        }

        if (intent.hasExtra("AdsBanner")) {
            mBanner = intent.getStringExtra("AdsBanner")
        }

        if (mBanner.length > 0) {
            Iv_banner!!.visibility = View.VISIBLE
            Vi_space!!.visibility = View.GONE

            Picasso.with(this@AdsPage).load(mBanner).into(Iv_banner)

        } else {
            Iv_banner!!.visibility = View.GONE
            Vi_space!!.visibility = View.VISIBLE
        }

        Tv_title!!.text = sTitle
        Tv_message!!.text = sMessage

    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            return true
        }
        return false
    }
}
