package indo.com.ridehub_lyft_uber.Activities

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import indo.com.mylibrary.gps.GPSTracker
import indo.com.mylibrary.latlnginterpolation.LatLngInterpolator
import indo.com.mylibrary.latlnginterpolation.MarkerAnimation
import indo.com.mylibrary.volley.AppController
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.mylibrary.volley.VolleyErrorResponse
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.mylibrary.xmpp.ChatService
import indo.com.ridehub_lyft_uber.PojoResponse.CancelTripPojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.util.*

class TrackYourRide : FragmentActivity(), OnMapReadyCallback, View.OnClickListener, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private var tv_done: TextView? = null
    private var tv_drivername: TextView? = null
    private var tv_carModel: TextView? = null
    private var tv_driver_estTime: CustomTextView? = null
    private var tv_carNo: TextView? = null
    private var tv_rating: TextView? = null
    private var tv_track_cab_type: ImageView? = null
    private var driver_image: RoundedImageView? = null
    private var rl_callDriver: ImageView? = null
    private var track_your_ride_carimage: ImageView? = null
    private var iv_userchat: ImageView? = null
    private var rl_endTrip: CustomTextView? = null
    //private GoogleMap googleMap;
    private val marker: MarkerOptions? = null
    private var gps: GPSTracker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var driverID = ""
    private var desti_location = ""
    private var source_Location = ""
    private var driverName = ""
    private var driverImage = ""
    private var driverRating = ""
    private var driverLat: String? = ""
    private var driverLong: String? = ""
    private var driverTime = ""
    private var rideID = ""
    private var driverMobile: String? = ""
    private var driverCar_no = ""
    private var driverCar_model = ""
    private var driverEst_time = ""
    private var userLat = ""
    private var userLong = ""
    private var cat_type = ""
    private var cab_type_img = ""
    private var isReasonAvailable = false
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var session: SessionManager? = null
    private var UserID = ""
    private var itemlist_reason: ArrayList<CancelTripPojo>? = null
    private var Tv_headerTitle: TextView? = null
    private var track_your_ride_view1: View? = null
    private var fromPosition: LatLng? = null
    private var toPosition: LatLng? = null
    private var markerOptions: MarkerOptions? = null
    internal lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    private var currentLocation: Location? = null
    private var currentLocationn: LatLng? = null
    private var  dropLatLng: LatLng? = null
    private var pickUpLatLng: LatLng? = null
    private var refreshReceiver: RefreshReceiver? = null
    private val realTimeTrackYourRideHandler = Handler()

    internal val PERMISSION_REQUEST_CODE = 111

    internal val PERMISSION_REQUEST_CODES = 222
    private var sSelectedPanicNumber = ""
    private var panic_btn: CardView? = null
    private var current_location_track: CardView? = null
    private var tv_trackride_source_loc: CustomTextView? = null
    private var tv_trackride_desti_loc: CustomTextView? = null
    private var ll_confirm_call_or_sms: LinearLayout? = null
    private var ll_source_destination: LinearLayout? = null
    private var mMap: GoogleMap? = null
    private var lat: Double = 0.toDouble()
    private var lng: Double = 0.toDouble()

    var old_lat_long = "old"
    var first_time_long = "old"

    var second_lat = ""
    var second_long = ""

    internal var mLatLngInterpolator: LatLngInterpolator? = null


    //UBER
    lateinit var uber_Req_current: StringRequest
    var uber_timer: Timer? = null
    var timerTask_uber: TimerTask? = null
    var timer_stop = "0"

    private var lyft_ride_id: String = ""
    private  var polyline: Polyline? = null
    private  var firsttimepolyline: Boolean? = true


    //UBERRECIEPT
    lateinit var uber_receipt_dialog: Dialog
    lateinit var ll_fare_breakup_total_amount: LinearLayout
    lateinit var ll_farebreakup_discount_uber: LinearLayout
    lateinit var ll_fare_breakup_duration_uber: LinearLayout
    lateinit var ll_fare_breakup_uberdistance: LinearLayout
    lateinit var ll_subtotal_uber: LinearLayout
    lateinit var fare_breakup_total_amount_textview: CustomTextView
    lateinit var fare_breakup_discount_amount_uber: CustomTextView
    lateinit var fare_breakup_duration_uber: CustomTextView
    lateinit var fare_breakup_uber_distance: CustomTextView
    lateinit var fare_breakup_subtotal_uber: CustomTextView
    lateinit var btn_uberreceipt_ok_dialog: Button


    var isFirstPosition = true
    var startPositionn: LatLng? = null
    var endPosition: LatLng? = null


    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        this.currentLocation = location
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()

    }

    override fun onResume() {
        super.onResume()
        ChatService.startUserAction(this@TrackYourRide)
        startLocationUpdates()
    }


    override fun onMapReady(googleMap: GoogleMap) {
        //enableGpsService();
        mMap = googleMap

        if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()

            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude

            mMap!!.projection
            mMap!!.cameraPosition
            // Move the camera to last position with a zoom level
            val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f).build()
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            mMap!!.isMyLocationEnabled = false

            mMap!!.uiSettings.isTiltGesturesEnabled
            mMap!!.uiSettings.isMyLocationButtonEnabled
            mMap!!.uiSettings.isMapToolbarEnabled
            mMap!!.uiSettings.isCompassEnabled
            mMap!!.mapType

            mMap!!.uiSettings.isZoomControlsEnabled
            mMap!!.focusedBuilding
            mMap!!.uiSettings.isRotateGesturesEnabled
            mMap!!.uiSettings.isIndoorLevelPickerEnabled = true
            mMap!!.uiSettings.isZoomGesturesEnabled
            mMap!!.uiSettings.isIndoorLevelPickerEnabled
            mMap!!.setMaxZoomPreference(50f)
            mMap!!.uiSettings.isZoomGesturesEnabled = true
            mMap!!.projection.visibleRegion


        } else {

            // enableGpsService();
        }

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
            //mMap.setMyLocationEnabled(true);

            val AM_PM: String
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val min = c.get(Calendar.MINUTE)
            val ds = c.get(Calendar.AM_PM)
            Log.e("date_string", "" + ds)
            if (ds == 0)
                AM_PM = "am"
            else
                AM_PM = "pm"


            val styleManager = MapStyleManager.attachToMap(this, mMap!!)
            styleManager.addStyle(0f, R.raw.map_silver_json)
            styleManager.addStyle(10f, R.raw.map_silver_json)
            styleManager.addStyle(12f, R.raw.map_silver_json)

        }

    }


    inner class RefreshReceiver : BroadcastReceiver() {
        @SuppressLint("NewApi")
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver") {
                println("triparrived----------------------")
                //Tv_headerTitle!!.text = resources.getString(R.string.action_driver_arrived)
                tv_driver_estTime!!.text = resources.getString(R.string.action_driver_arrived)

                rl_endTrip!!.visibility = View.GONE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    // rl_callDriver!!.background = resources.getDrawable(R.drawable.curve_background)
                }
                // track_your_ride_view1!!.visibility = View.GONE
            } else if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip") {
                println("tripbegin----------------------")
                //  Tv_headerTitle!!.text = resources.getString(R.string.action_enjy_your_ride)
                rl_endTrip!!.visibility = View.GONE
                tv_driver_estTime!!.text = resources.getString(R.string.action_enjy_your_ride)

                ll_source_destination!!.visibility = View.VISIBLE
                tv_trackride_source_loc!!.text = source_Location
                tv_trackride_desti_loc!!.text = desti_location
                ll_confirm_call_or_sms!!.visibility = View.GONE

                //rl_callDriver!!.background = resources.getDrawable(R.drawable.curve_background)
                // track_your_ride_view1!!.visibility = View.GONE
                panic_btn!!.visibility = View.VISIBLE
            } else if (intent.action == "com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver") {

                //System.out.println("check--------------REFRESH_UpdateDriver");

            }

            //System.out.println("check--------------");
            if (intent.extras != null && intent.extras!!.containsKey("drop_lat") && intent.extras!!.containsKey("drop_lng")) {
                val lat = intent.extras!!.get("drop_lat") as String
                val lng = intent.extras!!.get("drop_lng") as String
                val pickUp_lat = intent.extras!!.get("pickUp_lat") as String
                val pickUp_lng = intent.extras!!.get("pickUp_lng") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    val pick_lat_decimal = java.lang.Double.parseDouble(pickUp_lat)
                    val pick_lng_decimal = java.lang.Double.parseDouble(pickUp_lng)

                    Log.e("UPDATELATLNG_BEFORE",""+lat_decimal+"-----"+lng_decimal+"-----"+pick_lat_decimal+"-----"+pick_lng_decimal)

                    updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal)
                    println("updateGoogleMapTrackRide--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

            }
            println("out else--------------")
            if (intent.extras != null && intent.extras!!.containsKey(Iconstant.isContinousRide)) {
                val lat = intent.extras!!.get("latitude") as String
                val lng = intent.extras!!.get("longitude") as String

                val ride_id = intent.extras!!.get("ride_id") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)

                    println("updateDriverOnMap-----------updateing location---------")
                    updateDriverOnMap(lat_decimal, lng_decimal)
                } catch (e: Exception) {
                }

            }
        }
    }

    private fun updateDriverOnMap(lat_decimal: Double, lng_decimal: Double) {

        val latLng = LatLng(lat_decimal, lng_decimal)

        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }

        Log.d("tess", "inside run ")
        // animateMarkerNew(latLng, curentDriverMarker);

        if (curentDriverMarker != null) {

            Log.d("UPDATEDRIVERONMAP", "inside run IF")

            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)

            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
            // animateMarkerNew(latLng, curentDriverMarker)

            if (isFirstPosition) {
                startPositionn = curentDriverMarker!!.getPosition();


                curentDriverMarker!!.setAnchor(0.5f, 0.5f);

                mMap!!.moveCamera(CameraUpdateFactory
                        .newCameraPosition
                        (CameraPosition.Builder()
                                .target(startPositionn)
                                .zoom(15.5f)
                                .build()));

                isFirstPosition = false;

            } else {
                endPosition = LatLng(latLng.latitude, latLng.longitude);

               // Log.d("ahsdgashd", startPosition!!.latitude + "--" + endPosition!!.latitude + "--Check --" + startPosition!!.longitude + "--" + endPosition!!.longitude);

                if ((startPositionn!!.latitude != endPosition!!.latitude) || (startPositionn!!.longitude != endPosition!!.longitude)) {

                    Log.e("asdhashd", "NOT SAME");
                    startBikeAnimation(startPositionn!!, endPosition!!);

                } else {

                    Log.e("dfsdfsd", "SAMME");
                }
            }


        } else {

            Log.d("UPDATEDRIVERONMAP", "inside run ELSE")

            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon))
                    .anchor(0.5f, 0.5f)
                    .flat(true))

            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)

            // animateMarkerNew(latLng, curentDriverMarker)

        }


    }

    private fun updateGoogleMapTrackRide(lat_decimal: Double, lng_decimal: Double, pick_lat_decimal: Double, pick_lng_decimal: Double) {
        if (mMap != null) {
            mMap!!.clear()
        }

        Log.e("UPDATELATLNG_INSIDE",""+lat_decimal+"-----"+lng_decimal+"-----"+pick_lat_decimal+"-----"+pick_lng_decimal)
         dropLatLng = LatLng(lat_decimal, lng_decimal)
         pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)

        Log.e("UPDATELATLNG_INSIDELATLNG",""+dropLatLng+"-----"+pickUpLatLng)

        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng!!, pickUpLatLng!!)
        draw_route_asyncTask.execute()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.track_your_ride)
        trackyour_ride_class = this@TrackYourRide
        initialize()
        try {
            setLocationRequest()
            buildGoogleApiClient()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initializeMap()

        panic_btn!!.setOnClickListener {
            println("----------------panic onclick method-----------------")
            panic()
        }


        current_location_track!!.setOnClickListener {

            gps = GPSTracker(this)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return@setOnClickListener
            }
            mMap!!.setMyLocationEnabled(false)
            if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {

                MyCurrent_lat = gps!!.getLatitude()
                MyCurrent_long = gps!!.getLongitude()

                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                current_location_track!!.setVisibility(View.VISIBLE)

            } else {
                //enableGpsService()
                //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
            }

        }


        //Start XMPP Chat Service
    }


    private fun initialize() {
        cd = ConnectionDetector(this@TrackYourRide)
        isInternetPresent = cd!!.isConnectingToInternet
        gps = GPSTracker(this@TrackYourRide)
        session = SessionManager(this@TrackYourRide)
        itemlist_reason = ArrayList<CancelTripPojo>()
        markerOptions = MarkerOptions()
        tv_done = findViewById(R.id.track_your_ride_done_textview) as TextView
        tv_drivername = findViewById(R.id.track_your_ride_driver_name) as TextView
        tv_carModel = findViewById(R.id.track_your_ride_driver_carmodel) as TextView
        tv_driver_estTime = findViewById(R.id.tv_confride_driver_esti_time) as CustomTextView
        tv_carNo = findViewById(R.id.track_your_ride_driver_carNo) as TextView
        tv_rating = findViewById(R.id.track_your_ride_driver_rating) as TextView
        tv_track_cab_type = findViewById(R.id.tv_track_cab_type) as ImageView
        driver_image = findViewById(R.id.track_your_ride_driverimage) as RoundedImageView
        rl_callDriver = findViewById(R.id.track_your_ride_calldriver_layout) as ImageView
        track_your_ride_carimage = findViewById(R.id.track_your_ride_carimage) as ImageView
        iv_userchat = findViewById(R.id.iv_userchat) as ImageView
        rl_endTrip = findViewById(R.id.track_your_ride_endtrip_layout) as CustomTextView
        Tv_headerTitle = findViewById(R.id.track_your_ride_track_label) as TextView
        //track_your_ride_view1 = findViewById(R.id.track_your_ride_view1) as View
        panic_btn = findViewById(R.id.track_your_ride_panic_cardview_layout) as CardView
        current_location_track = findViewById(R.id.track_your_ride_current_loc_cardview_layout) as CardView
        ll_source_destination = findViewById(R.id.ll_source_destination) as LinearLayout
        tv_trackride_source_loc = findViewById(R.id.tv_trackride_source_loc) as CustomTextView
        tv_trackride_desti_loc = findViewById(R.id.tv_trackride_desti_loc) as CustomTextView
        ll_confirm_call_or_sms = findViewById(R.id.ll_confirm_call_or_sms) as LinearLayout

        val intent = intent
        if (intent != null) {


            driverID = intent.getStringExtra("driverID")
            driverName = intent.getStringExtra("driverName")
            driverImage = intent.getStringExtra("driverImage")
            driverRating = intent.getStringExtra("driverRating")
            driverLat = intent.getStringExtra("driverLat")
            driverLong = intent.getStringExtra("driverLong")
            driverTime = intent.getStringExtra("driverTime")
            rideID = intent.getStringExtra("rideID")
            driverMobile = intent.getStringExtra("driverMobile")
            driverCar_no = intent.getStringExtra("driverCar_no")
            driverCar_model = intent.getStringExtra("driverCar_model")
            source_Location = intent.getStringExtra("source_Location")
            driverEst_time = intent.getStringExtra("driverTime")
            userLat = intent.getStringExtra("userLat")
            userLong = intent.getStringExtra("userLong")
            cat_type = intent.getStringExtra("cat_type")
            desti_location = intent.getStringExtra("destination_Location")


            // cab_type_img = intent.getStringExtra("cab_type_image")
        }


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")
        registerReceiver(refreshReceiver, intentFilter)


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        tv_drivername!!.text = driverName
        tv_carNo!!.text = driverCar_no
        tv_carModel!!.text = driverCar_model
        tv_driver_estTime!!.text = "Driver Arrives in " + driverEst_time
        tv_rating!!.text = driverRating + "*"
        // tv_track_cab_type!!.text = "Ridehub"
        /* if(cab_type_img.equals("Ridehub")){
             tv_track_cab_type!!.setImageDrawable(resources.getDrawable(R.mipmap.cab_type_uber))
         }else{
             tv_track_cab_type!!.setImageDrawable(resources.getDrawable(R.mipmap.cab_type_uber))
         }*/

        Picasso.with(this)
                .load(driverImage)
                .into(driver_image)
        tv_done!!.setOnClickListener(this)
        rl_callDriver!!.setOnClickListener(this)
        rl_endTrip!!.setOnClickListener(this)


        //Uber Receipt Dialog

        uber_receipt_dialog = Dialog(this)
        uber_receipt_dialog.window
        uber_receipt_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        uber_receipt_dialog.setContentView(R.layout.uber_reciept_dialog)
        //  uber_dialog.setCanceledOnTouchOutside(false)
        // uber_dialog.show()

        ll_fare_breakup_total_amount = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_total_amount)
        ll_farebreakup_discount_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_farebreakup_discount_uber)
        ll_fare_breakup_duration_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_duration_uber)
        ll_fare_breakup_uberdistance = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_fare_breakup_uberdistance)
        ll_subtotal_uber = uber_receipt_dialog.findViewById<LinearLayout>(R.id.ll_subtotal_uber)
        fare_breakup_total_amount_textview = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_total_amount_textview)
        fare_breakup_discount_amount_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_discount_amount_uber)
        fare_breakup_duration_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_duration_uber)
        fare_breakup_uber_distance = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_uber_distance)
        fare_breakup_subtotal_uber = uber_receipt_dialog.findViewById<CustomTextView>(R.id.fare_breakup_subtotal_uber)
        btn_uberreceipt_ok_dialog = uber_receipt_dialog.findViewById<Button>(R.id.btn_uberreceipt_ok_dialog)
        btn_uberreceipt_ok_dialog.setOnClickListener {
            uber_receipt_dialog.dismiss()

        }


        iv_userchat!!.setOnClickListener {
            val i = Intent(this@TrackYourRide, MainActivity::class.java)
            i.putExtra("driverID", driverID)
            i.putExtra("driverName", driverName)
            i.putExtra("driverImage", driverImage)
            startActivity(i)

        }


    }

    private fun panic() {
        println("----------------panic method-----------------")
        val view = View.inflate(this@TrackYourRide, R.layout.panic_page, null)
        val dialog = MaterialDialog(this@TrackYourRide)
        dialog.setContentView(view).setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
        ) { dialog.dismiss() }.show()
        val call_police = view.findViewById(R.id.call_police_button) as Button
        val call_fire = view.findViewById(R.id.call_fireservice_button) as Button
        val call_ambulance = view.findViewById(R.id.call_ambulance_button) as Button
        call_police.setOnClickListener {
            dialog.dismiss()

            sSelectedPanicNumber = "+100"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "+100")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "+100")
                startActivity(callIntent)
            }
        }

        call_fire.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+101"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "101")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "101")
                startActivity(callIntent)
            }
        }
        call_ambulance.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+108"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "108")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "108")
                startActivity(callIntent)
            }
        }

    }

    private fun initializeMap() {

        if (mMap == null) {
            //mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.book_my_ride_mapview));
            val mapFragment = supportFragmentManager.findFragmentById(R.id.track_your_ride_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                //Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }

            if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
                val Dlatitude = gps!!.getLatitude()
                val Dlongitude = gps!!.getLongitude()
                MyCurrent_lat = Dlatitude
                MyCurrent_long = Dlongitude
                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(15f).build()
                //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                Alert(resources.getString(R.string.action_error), resources.getString(R.string.alert_gpsEnable))
            }
            //set marker for driver location.
            if (cat_type.equals("Uber")) {
                UberMap(session!!.getlyft_ride_id())
            } else {
                if (driverLat != null && driverLong != null) {
                    fromPosition = LatLng(java.lang.Double.parseDouble(userLat), java.lang.Double.parseDouble(userLong))
                    toPosition = LatLng(java.lang.Double.parseDouble(driverLat), java.lang.Double.parseDouble(driverLong))
                    if (fromPosition != null && toPosition != null) {
                        val draw_route_asyncTask = GetRouteTask()
                        draw_route_asyncTask.execute()
                    }
                }
            }


        }
    }


    override fun onClick(v: View) {

        if (v === tv_done) {
            /* Intent finish_timerPage = new Intent();
                finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
                sendBroadcast(finish_timerPage);*/

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

        } else if (v === rl_callDriver) {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile!!)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile!!)
                    startActivity(callIntent)
                }
            } else {
                Alert(this@TrackYourRide.resources.getString(R.string.alert_label_title), this@TrackYourRide.resources.getString(R.string.track_your_ride_alert_content1))
            }
        } else if (v === rl_endTrip) {
            val mDialog = PkDialog(this@TrackYourRide)
            mDialog.setDialogTitle(resources.getString(R.string.my_rides_detail_cancel_ride_alert_title))
            mDialog.setDialogMessage(resources.getString(R.string.my_rides_detail_cancel_ride_alert))
            mDialog.setPositiveButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_yes), View.OnClickListener {
                mDialog.dismiss()
                cd = ConnectionDetector(this@TrackYourRide)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    if (cat_type.equals("Uber")) {
                        DeleteUber_Ride()
                    } else {
                        postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url)
                    }


                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            })
            mDialog.setNegativeButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_no), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()

        }

    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@TrackYourRide)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        override fun onPreExecute() {

            Log.e("GETDROPROUTE","GetRouteTask")

        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(toPosition, fromPosition, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {

            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map


                        markerOptions!!.position(fromPosition!!)

                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(fromPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                        curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon)))

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(fromPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))


                    markerOptions!!.position(toPosition!!)
                    polyline = mMap!!.addPolyline(rectLine)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private fun postRequest_CancelRides_Reason(Url: String) {
        dialog = Dialog(this@TrackYourRide)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------MyRide Cancel Reason Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(this@TrackYourRide)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MyRide Cancel Reason Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val reason_array = response_object.getJSONArray("reason")
                            if (reason_array.length() > 0) {
                                itemlist_reason!!.clear()
                                for (i in 0 until reason_array.length()) {
                                    val reason_object = reason_array.getJSONObject(i)
                                    val pojo = CancelTripPojo()
                                    pojo.setReason(reason_object.getString("reason"))
                                    pojo.setReasonId(reason_object.getString("id"))
                                    itemlist_reason!!.add(pojo)
                                }
                                isReasonAvailable = true
                            } else {
                                isReasonAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                    if (Sstatus.equals("1", ignoreCase = true) && isReasonAvailable) {
                        val finish_timerPage = Intent()
                        finish_timerPage.action = "com.pushnotification.finish.TimerPage"
                        sendBroadcast(finish_timerPage)
                        val passIntent = Intent(this@TrackYourRide, TrackRideCancelTrip::class.java)
                        val bundleObject = Bundle()
                        bundleObject.putSerializable("Reason", itemlist_reason)
                        passIntent.putExtras(bundleObject)
                        passIntent.putExtra("RideID", rideID)
                        startActivity(passIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

            return true
        }
        return false
    }

    public override fun onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver)
        ChatService.disableChat()
        super.onDestroy()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {




        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        private var endLocation: LatLng? = null
        private var currentLocation: LatLng? = null

        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng) {

            Log.e("UPDATELATLNG_INSIDEDROP",""+currentLocation+"-----"+endLocation)
           this.currentLocation = currentLocation
            this.endLocation = endLocation


        }

        override fun onPreExecute() {

            Log.e("GETDROPROUTE","GetDropRouteTask")
            currentLocationn = currentLocation

        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values


            document = v2GetRouteDirection.getDocument(endLocation, currentLocation, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {


            if (polyline != null) {
                polyline!!.remove()
            }
           /* if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(
                            resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map
                    mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(endLocation!!)
                    markerOptions!!.position(currentLocation!!)
                    markerOptions!!.draggable(true)

                    //mMap.addMarker(markerOptions);
                    mMap!!.addMarker(MarkerOptions()
                            .position(endLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                    mMap!!.addMarker(MarkerOptions()
                            .position(currentLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))

                    *//* curentDriverMarker =  mMap.addMarker(new MarkerOptions()
                            .position(endLocation)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));*//*

                    println("inside---------marker--------------")

                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(endLocation!!)
                    builder.include(currentLocation!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))


                    curentDriverMarker = mMap!!.addMarker(MarkerOptions().position(endLocation!!).icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon)))


                    /// Location targetlocation = new Location("");
                    //targetlocation.setLatitude(endLocation.latitude);
                    // targetlocation.setLongitude(endLocation.longitude);
                    //post again
                    Log.d("tess", "inside run ")
                    // animateMarkerNew(targetlocation, curentDriverMarker);

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }*/

            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map


                    if(firsttimepolyline!!){
                        markerOptions!!.position(fromPosition!!)

                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(fromPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                        curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon)))

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(fromPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                    }
                    polyline = mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(toPosition!!)


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }


        }
    }


    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODE)
    }


    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODES)
    }


    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + driverMobile!!)
                startActivity(callIntent)
            }


            PERMISSION_REQUEST_CODES ->

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                    startActivity(callIntent)
                }
        }
    }


    private fun animateMarkerNew(destination: LatLng, marker: Marker?) {

        if (marker != null) {

            Log.e("ANIMATEMARKER", "ANIMATE")
            val startPosition = marker.position
            val endPosition = LatLng(destination.latitude, destination.longitude)

            val startRotation = marker.rotation
            val latLngInterpolator = LatLngInterpolatorNew.LinearFixed()

            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 3000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction

                    lng = v * endPosition.longitude + (1 - v) * startPosition.longitude
                    lat = v * endPosition.latitude + (1 - v) * startPosition.latitude
                    val newPos = LatLng(lat, lng)
                    marker.setPosition(newPos)
                    marker.setAnchor(0.5f, 0.5f)
                    marker.rotation = getBearing(startPosition, newPos)
                    mMap!!.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition.Builder()
                                    .target(newPos)
                                    .zoom(15.5f)
                                    .build()))


                    /*LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
        marker.setPosition(newPosition);
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(newPosition)
                .zoom(15.5f)
                .build()));

        marker.setRotation(getBearing(startPosition, new LatLng(destination.latitude, destination.longitude)));
        Log.d("BEARING Values",""+getBearing(startPosition, new LatLng(destination.latitude, destination.longitude)));*/
                    val line = mMap!!.addPolyline(PolylineOptions().add(startPosition, endPosition)
                            .width(10f).color(resources.getColor(R.color.trackride_polyline)))
                } catch (ex: Exception) {
                    //I don't care atm..
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            })
            valueAnimator.start()
        }
    }

    private interface LatLngInterpolatorNew {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng

        class LinearFixed : LatLngInterpolatorNew {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }


    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return 1f
    }

    companion object {
        lateinit var trackyour_ride_class: TrackYourRide
        private var curentDriverMarker: Marker? = null
        private val movingMarker: Marker? = null
        internal val REQUEST_LOCATION = 199

        internal var latLngInterpolator: LatLngInterpolator = LatLngInterpolator.Spherical()

        fun updateMap(latLng: LatLng) {
            MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator)
        }
    }


    private fun UberMap(request_id_stg: String) {


        var latitude_pickup_stg = ""
        var longitude_pickup_stg = ""
        var latitude_stg = ""
        var longitude_stg = ""
        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_ID_stg + request_id_stg, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("UberMapRESP", "---" + response)
                val v2GetRouteDirection = GMapV2GetRouteDirection()
                var document: Document
                try {
                    val main_ride_obj = JSONObject(response)
                    if (main_ride_obj.has("product_id")) {
                        val product_id_stg = main_ride_obj.getString("product_id")
                    }
                    val request_id_stg = main_ride_obj.getString("request_id")
                    val status_stg_UBERMAP = main_ride_obj.getString("status")

                    if (main_ride_obj.has("destination")) {
                        val destination_obj = main_ride_obj.getJSONObject("destination")
                        latitude_stg = destination_obj.getString("latitude")
                        longitude_stg = destination_obj.getString("longitude")
                    }
                    if (main_ride_obj.has("pickup")) {
                        val pickup_obj = main_ride_obj.getJSONObject("pickup")

                        latitude_pickup_stg = pickup_obj.getString("latitude")
                        longitude_pickup_stg = pickup_obj.getString("longitude")
                    }

                    if (status_stg_UBERMAP.equals("accepted")) {

                        Log.e("UBERCOMPLETED", "ACCEPTED")

                        Log.e("accepted", "---" + "accepted")
                        val driver_obj = main_ride_obj.getJSONObject("driver")
                        val vehicle_obj = main_ride_obj.getJSONObject("vehicle")
                        val driver_name = driver_obj.getString("name")
                        val phone_number = driver_obj.getString("phone_number")
                        val driver_rating = driver_obj.getString("rating")
                        val driver_picture_url = driver_obj.getString("picture_url")
                        val driver_sms_number = driver_obj.getString("sms_number")

                        val vehicle_make = vehicle_obj.getString("make")
                        val vehicle_picture_url = vehicle_obj.getString("picture_url")
                        val vehicle_model = vehicle_obj.getString("model")
                        val vehicle_license_plate = vehicle_obj.getString("license_plate")


                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")
                        //  UberMap(request_id_stg)
                        // startUberTimer()
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)

                    } else if (status_stg_UBERMAP.equals("arriving")) {
                        //arriving
                        Log.e("UBERCOMPLETED", "ARRIVING")

                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")
                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                    } else if (status_stg_UBERMAP.equals("in_progress")) {


                        rl_endTrip!!.visibility = View.GONE
                        Log.e("UBERCOMPLETED", "INPROGRESS")

                        val location_obj = main_ride_obj.getJSONObject("location")
                        val latitude_stg = location_obj.getString("latitude")
                        val longitude_stg = location_obj.getString("longitude")
                        val bearing_stg = location_obj.getString("bearing")


                        Ubermarker(latitude_stg, longitude_stg, bearing_stg)
                    } else if (status_stg_UBERMAP.equals("completed")) {
                        //completed

                        Log.e("UBERCOMPLETED", "COMPLETED")
                        stopUbertimertask()
                        UberRecipet(request_id_stg)
                    } else if (status_stg_UBERMAP.equals("rider_canceled")) {
                        //completed
                        Log.e("UBERCANCELLED", "CANCELLED")

                        stopUbertimertask()
                        UberRecipet(request_id_stg)
                    }

                    Log.e("AFTERCOMPLETED", status_stg_UBERMAP)

                    val data_pickup = LatLng(latitude_pickup_stg.toDouble(), longitude_pickup_stg.toDouble())
                    val data_drop = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())
                    document = v2GetRouteDirection.getDocument(data_drop, data_pickup, GMapV2GetRouteDirection.MODE_DRIVING)
                    try {
                        val directionPoint = v2GetRouteDirection.getDirection(document)
                        val rectLine = PolylineOptions().width(10f).color(
                                resources.getColor(R.color.black)).geodesic(true)
                        for (i in directionPoint.indices) {
                            rectLine.add(directionPoint.get(i))
                        }
                        // Adding route on the map
                        mMap!!.addPolyline(rectLine)
                        markerOptions!!.position(data_drop)
                        markerOptions!!.position(data_pickup)
                        markerOptions!!.draggable(true)
                        mMap!!.isMyLocationEnabled = false

                        //googleMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_drop)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_loc_dot)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(data_pickup)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_loc_dot)))


                        /*curentDriverMarker = googleMap.addMarker(new MarkerOptions()
                                .position(endLocation)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));*/

                        println("inside---------marker--------------")

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(data_drop)
                        builder.include(data_pickup)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300))
                        mMap!!.getMinZoomLevel();


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (timer_stop.equals("0")) {
                        timer_stop = "1"
                        startUberTimer()
                    }

                    //Toast.makeText(context, "Ride Request in Current Status : " + status_stg, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(this@TrackYourRide, error)
                Log.e("estimation_id_error", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }


    fun startUberTimer() {
        cd = ConnectionDetector(this)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent as Boolean) {
            uber_timer = Timer()
            //initialize the TimerTask's job
            initializeUberTimerTask()
            //schedule the timer, to wake up every 1 second
            uber_timer!!.schedule(timerTask_uber, 3000, 5000)
        }//
    }


    fun initializeUberTimerTask() {
        timerTask_uber = object : TimerTask() {
            override fun run() {
                try {
                    Log.e("TIMER", "arm")
                    cd = ConnectionDetector(this@TrackYourRide)
                    isInternetPresent = cd!!.isConnectingToInternet
                    if (isInternetPresent as Boolean) {
                        if (!session!!.getlyft_ride_id().equals("")) {
                            lyft_ride_id = session!!.getlyft_ride_id()
                            Log.e("lyft_ride_id_lll", lyft_ride_id)
                        }
                        if (cat_type.equals("Uber")) {
                            // UberCurrentReq()
                            Log.e("lyft_ride_id_lll", "UBER")
                            UberMap(session!!.getlyft_ride_id())
                        } else if (cat_type.equals("Lyft")) {
                            Log.e("lyft_ride_id_lll", "LYFT")
                            // postRequest_tripdetails_lyft(session!!.getlyft_ride_id())
                        } else {
                            //UberCurrentReq()
                            UberMap(session!!.getlyft_ride_id())
                            // postRequest_tripdetails_lyft(session!!.getlyft_ride_id())
                        }
                        Log.e("tier", "timer_arm_arm")
                    }
                } catch (e: Exception) {
                    Log.e("TIMER_EXCEPTION", e.toString())
                }

            }
        }
    }

    fun stopUbertimertask() {
        //stop the timer, if it's not already null
        if (uber_timer != null) {
            uber_timer!!.cancel()
            uber_timer = null
        }
    }


    fun Ubermarker(latitude_stg: String, longitude_stg: String, bearing_stg: String) {
        mMap!!.clear()

        if (old_lat_long.equals("old")) {
            old_lat_long = ""
        } else {
            old_lat_long = "old"
        }
        var Dlatitude = 0.00
        var Dlongitude = 0.00
        if (first_time_long.equals("old")) {
            Dlatitude = java.lang.Double.parseDouble(latitude_stg)
            Dlongitude = java.lang.Double.parseDouble(longitude_stg)
            first_time_long = ""
        } else {
            Dlatitude = java.lang.Double.parseDouble(second_lat)
            Dlongitude = java.lang.Double.parseDouble(second_long)
        }

        //val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
        val marker = mMap!!.addMarker(MarkerOptions()
                .position(LatLng(Dlatitude, Dlongitude))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon))
                .anchor(0.5f, 0.5f)
                .flat(true))

        if (marker != null) {

            Log.e("ANIMATEMARKER", "ANIMATE")
            val startPosition = marker.position
            val endPosition = LatLng(latitude_stg.toDouble(), longitude_stg.toDouble())


            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 5000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction

                    val lng = v * endPosition.longitude + (1 - v) * startPosition.longitude
                    val lat = v * endPosition.latitude + (1 - v) * startPosition.latitude
                    val newPos = LatLng(lat, lng)
                    marker.setPosition(newPos)
                    marker.setAnchor(0.5f, 0.5f)
                    marker.rotation = getBearing(startPosition, newPos)
                    mMap!!.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition.Builder()
                                    .target(newPos)
                                    .bearing(bearing_stg.toFloat())
                                    .zoom(15.5f)
                                    .build()))

                    val line = mMap!!.addPolyline(PolylineOptions().add(startPosition, newPos)
                            .width(10f).color(resources.getColor(R.color.spinner_arrow_color)))

                    if (old_lat_long.equals("old")) {
                        second_lat = latitude_stg
                        second_long = longitude_stg
                    } else {
                        second_lat = latitude_stg
                        second_long = longitude_stg
                    }

                } catch (ex: Exception) {
                    //I don't care atm..
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                }
            })
            valueAnimator.start()
        }

    }


    fun DeleteUber_Ride() {

        uber_Req_current = object : StringRequest(Request.Method.DELETE, Iconstant.API_REQ_CURRENT_stg, object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("UberRIDEREQUESTRESPONSE", "---" + response)

                try {


                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(this@TrackYourRide, error)
                Log.e("estimation_id_error", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }


    //Uber Reciept

    private fun UberRecipet(request_id_stg: String) {
        uber_Req_current = object : StringRequest(Request.Method.GET, Iconstant.API_REQ_ID_stg + request_id_stg + "/receipt", object : com.android.volley.Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.e("UBERRECEIPTRES", "---" + response)
                try {
                    val recipet_obj = JSONObject(response)
                    val subtotal_stg = recipet_obj.getString("subtotal")
                    val total_charged_stg = recipet_obj.getString("total_charged")
                    val total_owed_stg = recipet_obj.getString("total_owed")
                    val total_fare_stg = recipet_obj.getString("total_fare")
                    val currency_code_stg = recipet_obj.getString("currency_code")
                    val duration_stg = recipet_obj.getString("duration")
                    val distance_stg = recipet_obj.getString("distance")
                    val request_id = recipet_obj.getString("request_id")
                    val distance_label_stg = recipet_obj.getString("distance_label")


                    uber_receipt_dialog.show()

                    fare_breakup_total_amount_textview.setText(total_fare_stg)
                    //fare_breakup_discount_amount_uber.setText()
                    fare_breakup_duration_uber.setText(duration_stg)
                    fare_breakup_uber_distance.setText(distance_stg + " " + distance_label_stg)
                    fare_breakup_subtotal_uber.setText(currency_code_stg + " " + subtotal_stg)


                } catch (e: Exception) {

                }
            }
        }, object : com.android.volley.Response.ErrorListener {

            override fun onErrorResponse(error: VolleyError) {
                VolleyErrorResponse.volleyError(this@TrackYourRide, error)
                Log.e("estimation_id_error_rec", "---" + error.networkTimeMs + "---" + error.networkResponse)
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //headers.put("Authorization", "Bearer " + access_token_stg_final)
                headers.put("Authorization", Iconstant.API_UBER_AUTH_KEY_stg)
                // headers.put("Content-type", "application/json")
                return headers
            }

        }
        uber_Req_current!!.retryPolicy = DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        uber_Req_current!!.setShouldCache(false)
        AppController.getInstance().addToRequestQueue(uber_Req_current)
    }




    private fun startBikeAnimation(start: LatLng, end: LatLng) {
        val handler = Handler()
        Log.e("Startbikeanimation","Entered to me")

            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 3000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction

                    lng = v * end!!.longitude + (1 - v) * start!!.longitude
                    lat = v * end!!.latitude + (1 - v) * start!!.latitude

                    val newPos = LatLng(lat, lng)

                    curentDriverMarker!!.setPosition(newPos)
                    curentDriverMarker!!.setAnchor(0.5f, 0.5f)
                    curentDriverMarker!!.rotation = getBearing(start, end)
                    mMap!!.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition.Builder()
                                    .target(newPos)
                                    .zoom(15.5f)
                                    .build()))
                    startPositionn = curentDriverMarker!!.getPosition();

                    /*val line = mMap!!.addPolyline(PolylineOptions().add(start, end)
                            .width(10f).color(resources.getColor(R.color.trackride_polyline)))*/
                } catch (ex: Exception) {
                    //I don't care atm..
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                }
            })
            valueAnimator.start()


        firsttimepolyline = false



        Log.e("currentLocationn", currentLocationn.toString())
        //to add new polyline
        val runnable = Runnable {
            currentLocationn = start
            if (currentLocationn != null) {
                val getRoute = GetDropRouteTask()
                getRoute.setToAndFromLocation(dropLatLng!!, pickUpLatLng!!)
                getRoute.execute()
            }
        }
        handler.postDelayed(runnable, 3000)


    }






}