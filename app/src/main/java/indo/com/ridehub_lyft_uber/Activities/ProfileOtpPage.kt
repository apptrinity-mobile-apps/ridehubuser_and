
package indo.com.ridehub_lyft_uber.Activities
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import indo.com.mylibrary.dialog.PkDialog
import indo.com.mylibrary.volley.ServiceRequest
import indo.com.ridehub_lyft_uber.HockeyApp.ActivityHockeyApp
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.utils.ConnectionDetector
import indo.com.ridehub_lyft_uber.utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ProfileOtpPage : ActivityHockeyApp() {
    private var context: Context? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null

    private var back: RelativeLayout? = null
    private var Eotp: EditText? = null
    private var send: Button? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog

    private var Suserid = ""
    private var Sphone = ""
    private var ScountryCode = ""
    private var Sotp_Status = ""
    private var Sotp = ""

    //----------------------Code for TextWatcher-------------------------
    private val EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Eotp!!.text.length > 0) {
                Eotp!!.error = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_otp_page)
        context = getApplicationContext()
        initialize()

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        send!!.setOnClickListener {
            if (Eotp!!.text.toString().length == 0) {
                erroredit(Eotp!!, getResources().getString(R.string.otp_label_alert_otp))
            } else if (Sotp != Eotp!!.text.toString()) {
                erroredit(Eotp!!, getResources().getString(R.string.otp_label_alert_invalid))
            } else {
                if (isInternetPresent!!) {
                    PostRequest(Iconstant.profile_edit_mobileNo_url)
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            }
        }


        Eotp!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(Eotp!!.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
            false
        }
    }

    private fun initialize() {
        session = SessionManager(this@ProfileOtpPage)
        cd = ConnectionDetector(this@ProfileOtpPage)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.profile_otp_header_back_layout) as RelativeLayout
        Eotp = findViewById(R.id.profile_otp_password_editText) as EditText
        send = findViewById(R.id.profile_otp_submit_button) as Button

        Eotp!!.addTextChangedListener(EditorWatcher)

        val intent = getIntent()
        Suserid = intent.getStringExtra("UserID")
        Sphone = intent.getStringExtra("Phone")
        ScountryCode = intent.getStringExtra("CountryCode")
        Sotp_Status = intent.getStringExtra("Otp_Status")
        Sotp = intent.getStringExtra("Otp")

        if (Sotp_Status.equals("development", ignoreCase = true)) {
            Eotp!!.setText(Sotp)
        } else {
            Eotp!!.setText("")
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ProfileOtpPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@ProfileOtpPage, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    //-----------------Move Back on pressed phone back button------------------
   override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }


    // -----------------code for OTP Verification Post Request----------------
    private fun PostRequest(Url: String) {

        dialog = Dialog(this@ProfileOtpPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_otp))

        println("--------------Otp url-------------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = Suserid
        jsonParams["country_code"] = ScountryCode
        jsonParams["phone_number"] = Sphone
        jsonParams["otp"] = Sotp

        mRequest = ServiceRequest(this@ProfileOtpPage)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                println("--------------Otp reponse-------------------$response")
                var Sstatus = ""
                var Smessage = ""
                var Scountry_code = ""
                var Sphone_number = ""
                try {

                    val `object` = JSONObject(response)

                    Sstatus = `object`.getString("status")
                    Smessage = `object`.getString("response")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        Scountry_code = `object`.getString("country_code")
                        Sphone_number = `object`.getString("phone_number")
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (Sstatus.equals("1", ignoreCase = true)) {
                    ProfilePage.updateMobileDialog(Scountry_code, Sphone_number)
                    session!!.setMobileNumberUpdate(Scountry_code, Sphone_number)

                    val mDialog = PkDialog(this@ProfileOtpPage)
                    mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                    mDialog.setDialogMessage(getResources().getString(R.string.profile_lable_mobile_success))
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                        mDialog.dismiss()
                        finish()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    })
                    mDialog.show()

                } else {

                    val mDialog = PkDialog(this@ProfileOtpPage)
                    mDialog.setDialogTitle(getResources().getString(R.string.action_error))
                    mDialog.setDialogMessage(Smessage)
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
                    mDialog.show()
                }

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(Eotp!!.windowToken, 0)

                dialog.dismiss()
            }

           override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

}