package indo.com.ridehub_lyft_uber.HockeyApp

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.UpdateManager

open class FragmentActivityHockeyApp : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkForUpdates()
    }

    override fun onResume() {
        super.onResume()
        checkForCrashes()
    }

    override fun onPause() {
        super.onPause()
        unregisterManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterManagers()
    }

    private fun checkForCrashes() {
        CrashManager.register(this, APP_ID)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, APP_ID)
    }

    private fun unregisterManagers() {
        UpdateManager.unregister()
        // unregister other managers if necessary...
    }

    companion object {
        private val APP_ID = "9f8e1861d5cc413ba593e3367676bca3"
    }
}