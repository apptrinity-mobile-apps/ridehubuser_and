package indo.com.ridehub_lyft_uber.iconstant;

public interface Iconstant {

    // String base_url = "http://54.169.136.144/";

    //LOCAL
    /*String BaseUrl = "http://54.169.136.144/v4/";
    String XMPP_HOST_URL = "54.169.136.144";
    String XMPP_SERVICE_NAME = "54.169.136.144";*/

    //Live URL
    String BaseUrl = "https://ridehub.co/v4/";
    String XMPP_HOST_URL = "54.169.136.144";
    String XMPP_SERVICE_NAME = "54.169.136.144";
    String google_api_key = "AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw";

    String setUserLocation = BaseUrl + "api/v1/app/set-user-geo";
    String loginurl = BaseUrl + "api/v1/app/login";
    String register_url = BaseUrl + "api/v1/app/check-user";
    String facebook_register_url = BaseUrl + "api/v1/app/social-login";
    String social_check_url = BaseUrl + "api/v1/app/social-check";

    String register_otp_url = BaseUrl + "api/v1/app/register";
    String forgot_password_url = BaseUrl + "api/v1/app/user/reset-password";
    String reset_password_url = BaseUrl + "api/v1/app/user/update-reset-password";
    String BookMyRide_url = BaseUrl + "api/v1/app/get-map-view";
    String Available_drivers_url = BaseUrl + "api/v1/app/get-drivers-map-view";
    String couponCode_apply_url = BaseUrl + "api/v1/app/apply-coupon";
    String confirm_ride_url = BaseUrl + "api/v1/app/book-ride";
    String delete_ride_url = BaseUrl + "api/v1/app/delete-ride";
    String estimate_price_url = BaseUrl + "api/v1/app/get-eta";
    String get_coupons_url = BaseUrl + "api/v1/app/get-coupons";
    String send_driver_enquery = BaseUrl + "api/v1/app/driver_enquery";

    String profile_getFavouriteaddress_url = BaseUrl + "api/v1/app/getFavoriteLocations";
    String profile_AddFavouriteaddress_url = BaseUrl + "api/v1/app/addFavoriteLocations";


    //AIzaSyAKDx43QL5xXBitDdviXavpqLPsGZ3uY6o

    //Latest
    //AIzaSyDmUit0g15zG716ZszuF_vdKabFYcK68zo
    //eweels places apikey
    //OLD
    // String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=AIzaSyAKPB2tr0VhexNxUIfgRELCI3b9imqJ_Zs&input=";
    //Latest
    String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw&input=";

    //AIzaSyDmUit0g15zG716ZszuF_vdKabFYcK68zo

    //AIzaSyAKDx43QL5xXBitDdviXavpqLPsGZ3uY6o

    //Latest
    String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw&placeid=";
    //OLD
    //String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAKPB2tr0VhexNxUIfgRELCI3b9imqJ_Zs&placeid=";

    String changePassword_url = BaseUrl + "api/v1/app/user/change-password";
    String profile_edit_userName_url = BaseUrl + "api/v1/app/user/change-name";
    String profile_edit_mobileNo_url = BaseUrl + "api/v1/app/user/change-mobile";
    String logout_url = BaseUrl + "api/v1/app/logout";

    String emergencycontact_add_url = BaseUrl + "api/v1/app/user/set-emergency-contact";
    String emergencycontact_view_url = BaseUrl + "api/v1/app/user/view-emergency-contact";
    String emergencycontact_delete_url = BaseUrl + "api/v1/app/user/delete-emergency-contact";
    String emergencycontact_send_message_url = BaseUrl + "api/v1/app/user/alert-emergency-contact";

    String invite_earn_friends_url = BaseUrl + "api/v1/app/get-invites";
    String refer_points_url = BaseUrl + "api/v1/app/points-list";
    String app_latest_news_url = BaseUrl + "api/v1/app/latest-news";

    String ratecard_select_city_url = BaseUrl + "api/v1/app/get-location";
    String ratecard_select_cartype_url = BaseUrl + "api/v1/app/get-category";
    String ratecard_display_url = BaseUrl + "api/v1/app/get-ratecard";

    String cabily_money_url = BaseUrl + "api/v1/app/get-money-page";
    String cabily_add_money_url = BaseUrl + "api/v1/mobile/wallet-recharge/stripe-process?";
    String cabily_money_webview_url = BaseUrl + "api/v1/mobile/wallet-recharge/payform?user_id=";
    String eweels_add_payment_wallet = BaseUrl + "api/v1/mobile/wallet-recharge/add_payment_wallet";
    String cabily_money_transaction_url = BaseUrl + "api/v1/app/get-trans-list";

    String myrides_url = BaseUrl + "api/v1/app/my-rides";
    String goods_types_url = BaseUrl + "api/v1/app/user/goods-types";
    String mycommisions_url = BaseUrl + "api/v1/app/user/agent-commissions";
    String myride_details_inVoiceEmail_url = BaseUrl + "api/v1/app/mail-invoice";
    String myride_details_url = BaseUrl + "api/v1/app/view-ride";
    String myride_details_track_your_ride_url = BaseUrl + "api/v3/track-driver";
    String cancel_myride_reason_url = BaseUrl + "api/v1/app/cancellation-reason";
    String cancel_myride_url = BaseUrl + "api/v1/app/cancel-ride";
    String paymentList_url = BaseUrl + "api/v1/app/payment-list";
    String getfareBreakUpURL = BaseUrl + "api/v1/app/get-fare-breakup";

    String makepayment_cash_url = BaseUrl + "api/v1/app/payment/by-cash";
    String makepayment_swipe_url = BaseUrl + "api/v1/app/payment/by-swipe";
    String makepayment_wallet_url = BaseUrl + "api/v1/app/payment/by-wallet";
    String makepayment_autoDetect_url = BaseUrl + "api/v1/app/payment/by-auto-detect";
    String makepayment_Get_webview_mobileId_url = BaseUrl + "api/v1/app/payment/by-gateway";
    String makepayment_webview_url = BaseUrl + "api/v1/mobile/proceed-payment?mobileId=";
    public static String receivedonline_amount_url = BaseUrl + "api/v1/app/online-payment";


    String myride_rating_url = BaseUrl + "api/v1/app/review/options-list";
    String myride_rating_submit_url = BaseUrl + "api/v1/app/review/submit";

    String favoritelist_display_url = BaseUrl + "api/v1/app/favourite/display";
    String favoritelist_add_url = BaseUrl + "api/v1/app/favourite/add";
    String favoritelist_edit_url = BaseUrl + "api/v1/app/favourite/edit";
    String favoritelist_delete_url = BaseUrl + "api/v1/app/favourite/remove";
    String home_recent_reides_url = BaseUrl + "api/v1/app/recent-rides";

    String tip_add_url = BaseUrl + "api/v1/app/apply-tips";
    String tip_remove_url = BaseUrl + "api/v1/app/remove-tips";

    String pay_by_referpoints = BaseUrl + "api/v1/app/payment/by-points";

    String share_trip_url = BaseUrl + "api/v3/track-driver/share-my-ride";

    String app_info_url = BaseUrl + "api/v3/get-app-info";

    String app_facebook_post_url = BaseUrl + "api/v3/get-app-info";


    String makepayment_stripe_cardList = BaseUrl + "android/user/get_saved_stripe_cards";


    //----------------------UserAgent---------------------
    String cabily_userAgent = "cabily2k15android";
    String cabily_IsApplication = "1";
    String cabily_AppLanguage = "en";
    String cabily_AppType = "android";
    String cabily_AppToken = "";


    //-----------------PushNotification Key--------------------

    String PushNotification_AcceptRide_Key = "ride_confirmed";
    String PushNotification_CabArrived_Key = "cab_arrived";
    String PushNotification_RideCancelled_Key = "ride_cancelled";
    String PushNotification_RideCompleted_Key = "ride_completed";
    String PushNotification_RequestPayment_Key = "requesting_payment";
    String PushNotification_RequestPayment_makepayment_Stripe_Key = "make_payment";
    String PushNotification_PaymentPaid_Key = "payment_paid";
    String pushNotificationBeginTrip = "trip_begin";
    String pushNotificationDriverLoc = "driver_loc";


    /*Ride Accept Key*/
    String DriverID = "key1";
    String DriverName = "key2";
    String DriverEmail = "key3";
    String DriverImage = "key4";
    String DriverRating = "key5";
    String DriverLat = "key6";
    String DriverLong = "key7";
    String DriverTime = "key8";
    String RideID = "key9";
    String DriverMobile = "key10";
    String DriverCar_No = "key11";
    String DriverCar_Model = "key12";
    String SourceLocation = "key13";
    String UserLat = "key14";
    String UserLong = "key15";
    String CabType = "key16";
    String DestinationLocation = "key17";
    String Push_Message = "message";
    String Push_Action = "action";
    String latitude = "latitude";
    String background = "background";
    String longitude = "longitude";
    String ride_id = "ride_id";
    String isContinousRide = "isContinousRide";
    /*Ride Arrived Key*/
    String UserID_Arrived = "key1";
    String RideID_Arrived = "key2";
    String Push_Message_Arrived = "message";
    String Push_Action_Arrived = "action";

    /*Ride Cancelled Key*/
    String RideID_Cancelled = "key1";
    String Push_Message_Cancelled = "message";
    String Push_Action_Cancelled = "action";

    /*Ride Completed Key*/
    String Push_Message_Completed = "message";
    String Push_Action_Completed = "action";

    /*Request Payment Key*/
    String Push_Message_Request_Payment = "message";
    String Push_Action_Request_Payment = "action";
    String CurrencyCode_Request_Payment = "key1";
    String TotalAmount_Request_Payment = "key2";
    String TravelDistance_Request_Payment = "key3";
    String Duration_Request_Payment = "key4";
    String WaitingTime_Request_Payment = "key5";
    String RideID_Request_Payment = "key6";
    String UserID_Request_Payment = "key7";
    String TipStatus_Request_Payment = "key8";
    String TipAmount_Request_Payment = "key9";
    String DriverName_Request_Payment = "key10";
    String DriverImage_Request_Payment = "key11";
    String DriverRating_Request_Payment = "key12";
    String Driver_Latitude_Request_Payment = "key13";
    String Driver_Longitude_Request_Payment = "key14";
    String UserName_Request_Payment = "key15";
    String User_Latitude_Request_Payment = "key16";
    String User_Longitude_Request_Payment = "key17";
    String subTotal_Request_Payment = "key18";
    String coupon_Request_Payment = "key19";
    String serviceTax_Request_Payment = "key20";
    String Total_Request_Payment = "key21";
    String serviceTax_Percentage = "key22";
    String refer_Points = "key23";
    String number_of_Points = "key24";
    String amount_per_Point = "key25";

    String Make_Payment = "key1";
    String drop_lat = "key3";
    String drop_lan = "key4";


    /*Payment Paid Key*/
    String Push_Message_Payment_paid = "message";
    String Push_Action_Payment_paid = "action";
    String RideID_Payment_paid = "key1";
    String UserID_Payment_paid = "key2";


    /*Ads Key*/
    String pushNotification_Ads = "ads";
    String Ads_title = "key1";
    String Ads_Message = "key2";
    String Ads_image = "key3";


    /*PAYTM URLS*/

    String paytm_generate_checksum = "http://eweels.com/Paytm_Android/generateChecksum_android.php";
    String paytm_callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";


    //uber Urls

   /* String API_UBER_SNADBOX_stg = "https://sandbox-api.uber.com/v1.2/";
    String API_ESTI_stg = API_UBER_SNADBOX_stg+"requests/estimate";
    //chaitanya
    //String API_UBER_AUTH_KEY_stg = "Bearer JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAG8AAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAAkAAAABwAAAAEAAAAEAAAAM_5YK25F-xSsgg-l1hyohxsAAAAnnUsch9ziu0H269hTiJU3HK3kb1umdXv44oiz-R9FQ4B5EyO5qtPJArzp6cqwom8ltbYKzm7DhDUtA1uwRyNAn2KpALu27sCrgRt_mvxygE2iMSE_z36RT9RpngsnhBeMOcVuommUWJMeV1eDAAAANoOjZOqMwRvE0GlOCQAAABiMGQ4NTgwMy0zOGEwLTQyYjMtODA2ZS03YTRjZjhlMTk2ZWU";
    //ar
    String API_UBER_AUTH_KEY_stg = "Bearer JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAG8AAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAAkAAAABwAAAAEAAAAEAAAAG8lRDvLSIExNEmURFG2ys1sAAAAT6yQ_FrImv3uZFY8IL1M40iVvhKLJdzdc4kgZl4MYX70kWMlhKS4AejPQvQ5_vxaY9oUUbDKry6ypnIJbUykxRaY_sBSuPYVduTeZXROhguzE53DIYcS5AIFLaqVpeJlO5231b84f5No2qhMDAAAANgcIp5b756onvSthCQAAABiMGQ4NTgwMy0zOGEwLTQyYjMtODA2ZS03YTRjZjhlMTk2ZWU";

    String API_REQ_stg = API_UBER_SNADBOX_stg+"requests";
    String API_REQ_CURRENT_stg = API_UBER_SNADBOX_stg+"requests/current";
    String API_REQ_ID_stg = API_UBER_SNADBOX_stg+"requests/";
    String API_PROD_stg = API_UBER_SNADBOX_stg+"products";

    String API_PROD_TIME_stg = API_UBER_SNADBOX_stg+"estimates/time";*/


    //UBER Urls

    String API_UBER_SNADBOX_stg = "https://sandbox-api.uber.com/v1.2/";
    //String API_UBER_SNADBOX_stg = "https://api.uber.com/v1.2/";
    String API_ESTI_stg = API_UBER_SNADBOX_stg + "requests/estimate";

    String API_UBER_AUTH_KEY_stg = "Bearer JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAG8AAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAAkAAAABwAAAAEAAAAEAAAAFZ8FmIV4l7-zXShEamhGn1sAAAA9aGQUdMGarsTjNf5pK2DzDyTY_EtYmQqEtLZinDpIz76LaW2XkvbaINUN7dSBrFdaHUzfJ7KJ7_lniReof_-4CKKBYZkrefu5U8bGNG1CLzwm-rebgo8ET2DnFUENSeiYHPJ2aLyN8b8R_shDAAAABPkmCuV3WKvzwLdCCQAAABiMGQ4NTgwMy0zOGEwLTQyYjMtODA2ZS03YTRjZjhlMTk2ZWU";

    String API_REQ_stg = API_UBER_SNADBOX_stg + "requests";
    String API_REQ_CURRENT_stg = API_UBER_SNADBOX_stg + "requests/current";
    String API_REQ_ID_stg = API_UBER_SNADBOX_stg + "requests/";
    String API_REQ_HISTORY_stg = API_UBER_SNADBOX_stg + "history";
    String API_PROD_stg = API_UBER_SNADBOX_stg + "products";
    String API_ESTI_PRICE_stg = API_UBER_SNADBOX_stg + "estimates/price";

    String API_PROD_TIME_stg = API_UBER_SNADBOX_stg + "estimates/time";

    String API_LOGIN_TOKEN_stg = "https://login.uber.com/oauth/v2/token";

    //LYFT
    String LyftBaseUrl = "https://api.lyft.com/";
    String LyftAuthUrl = LyftBaseUrl + "oauth/token";
    String Lyft_client_id = "9RIzrmiCR5BM";
    String Lyft_client_secret_key = "38sjtWUiac7ugw_k_KAHf-hbXKl6GriZ";


}
