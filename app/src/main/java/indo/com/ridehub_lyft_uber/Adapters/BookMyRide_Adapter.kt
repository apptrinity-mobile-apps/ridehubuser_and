package indo.com.ridehub_lyft_uber.Adapters

import android.app.Activity
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.ridehub_lyft_uber.PojoResponse.HomePojo
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.ImageLoader
import java.util.*

class BookMyRide_Adapter(private val context: Activity, private val data: ArrayList<HomePojo>) : BaseAdapter() {
    private val imageLoader: ImageLoader
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
        imageLoader = ImageLoader(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var image: ImageView? = null
        internal var iv_carcapacity: ImageView? = null
        internal var name: CustomTextView? = null
        internal var car_price: CustomTextView? = null
        internal var car_capacity_min: CustomTextView? = null
        internal val car_capacity_max: CustomTextView? = null
        internal var time: TextView? = null
        internal var Ll_car: LinearLayout? = null

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.bookmyride_single, parent, false)
            holder = ViewHolder()
            holder?.name = view.findViewById<View>(R.id.bookmyride_single_carname) as CustomTextView
            holder?.car_price = view.findViewById<View>(R.id.bookmyride_single_carprice) as CustomTextView
            //holder?.car_capacity_min = view.findViewById<View>(R.id.bookmyride_single_carcapacity_min) as CustomTextView
            //holder?.iv_carcapacity = view.findViewById<View>(R.id.iv_carcapacity) as ImageView

            val typeface = Typeface.createFromAsset(context.assets, "fonts/Poppins-Regular.ttf")
            holder?.name!!.setTypeface(typeface, Typeface.BOLD)
            holder?.time = view.findViewById<View>(R.id.bookmyride_single_time) as TextView
            holder?.image = view.findViewById<View>(R.id.bookmyride_single_car_image) as ImageView
            holder.Ll_car = view.findViewById<View>(R.id.bookmyride_single_car_layout) as LinearLayout
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }
        holder.name!!.text = data[position].getCat_name()
        holder.car_price!!.text = data[position].getCat_time()


       // holder.car_price!!.text = "$ " + data[position].getMin_amount()!!


        val cabtype = data[position].getMain_type().toString()
        Log.e("MAINTYPE",cabtype)

        if(cabtype.equals("Ridehub")){
           // imageLoader.DisplayImage(context.resources.getDrawable(R.drawable.cab_type_ridehub).toString(),holder.image!!)
            holder?.image!!.setImageDrawable(context.resources.getDrawable(R.drawable.cab_type_ridehub))

        }else if(cabtype.equals("Uber")){
           // imageLoader.DisplayImage(context.resources.getDrawable(R.mipmap.cab_type_uber).toString(),holder.image!!)
            holder?.image!!.setImageDrawable(context.resources.getDrawable(R.drawable.uber))
        }else if(cabtype.equals("Lyft")){
            // imageLoader.DisplayImage(context.resources.getDrawable(R.mipmap.cab_type_uber).toString(),holder.image!!)
            holder?.image!!.setImageDrawable(context.resources.getDrawable(R.drawable.lyft_icon_latest))
        }

        /*if (data[position].selected_Cat!!.equals(data[position].cat_id!!, ignoreCase = true)) {
            imageLoader.DisplayImage(data[position].icon_active.toString(), holder.image!!)
            holder?.name!!.setTextColor(context.resources.getColor(R.color.white))
            holder?.car_price!!.setTextColor(context.resources.getColor(R.color.app_color))
            holder?.car_capacity_min!!.setTextColor(context.resources.getColor(R.color.white))
            holder.iv_carcapacity!!.setImageDrawable(context.resources.getDrawable(R.drawable.car_capacity_white))
            Log.e("ICON", data[position].icon_active.toString())
        } else {
            imageLoader.DisplayImage(data[position].icon_normal.toString(), holder.image!!)
            Log.e("ICON_selected", data[position].icon_normal.toString())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.Ll_car!!.background = context.resources.getDrawable(R.drawable.border_horizontal_list_inactive)
            }
            holder?.name!!.setTextColor(context.resources.getColor(R.color.blue_text_bg))
            holder?.car_price!!.setTextColor(context.resources.getColor(R.color.blue_ridehub))
            holder?.car_capacity_min!!.setTextColor(context.resources.getColor(R.color.app_color))
            holder.iv_carcapacity!!.setImageDrawable(context.resources.getDrawable(R.drawable.car_capacity_blue))


        }*/





        return view
    }
}