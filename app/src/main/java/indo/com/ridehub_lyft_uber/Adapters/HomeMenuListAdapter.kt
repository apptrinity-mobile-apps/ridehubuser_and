package indo.com.ridehub_lyft_uber.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import indo.com.mylibrary.widgets.RoundedImageView
import indo.com.ridehub_lyft_uber.R
import indo.com.ridehub_lyft_uber.utils.SessionManager

class HomeMenuListAdapter(internal var context: Context, internal var mTitle: Array<String>, internal var mIcon: IntArray) : BaseAdapter() {
    lateinit var  inflater: LayoutInflater
    lateinit var itemView: View
    lateinit var session: SessionManager

    init {
        Log.e("icons", "" + mIcon.size)
    }

    override fun getCount(): Int {
        return mTitle.size
    }

    override fun getItem(position: Int): Any {
        return mTitle[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // Declare Variables
        val txtTitle: TextView
        val profile_name: TextView
        val profile_mobile: TextView
        val walletMoney: TextView
        val profile_icon: RoundedImageView
        val imgIcon: ImageView
        val general_layout: RelativeLayout
        val profile_layout: RelativeLayout
        val drawer_view: View

        session = SessionManager(context)

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        itemView = inflater.inflate(R.layout.drawer_list_item, parent, false)

        txtTitle = itemView.findViewById(R.id.title) as TextView
        walletMoney = itemView.findViewById(R.id.drawer_item_list_wallet_money) as TextView
        profile_mobile = itemView.findViewById(R.id.profile_mobile_number) as TextView
        imgIcon = itemView.findViewById(R.id.icon) as ImageView
        profile_name = itemView.findViewById(R.id.profile_name) as TextView
        profile_icon = itemView.findViewById(R.id.profile_icon) as RoundedImageView
        general_layout = itemView.findViewById(R.id.drawer_list_item_normal_layout) as RelativeLayout
        profile_layout = itemView.findViewById(R.id.drawer_list_item_profile_layout) as RelativeLayout
        drawer_view = itemView.findViewById(R.id.drawer_list_view) as View


        if (position == 0) {
            val user = session.getUserDetails()
            val UserprofileImage = user[SessionManager.KEY_USERIMAGE]
            val User_phone = user[SessionManager.KEY_PHONENO]
            val User_phoneCode = user[SessionManager.KEY_COUNTRYCODE]
            val User_fullname = user[SessionManager.KEY_USERNAME]

            profile_layout.visibility = View.VISIBLE
            general_layout.visibility = View.GONE
            drawer_view.visibility = View.GONE

            Log.e("USERPROFILEIMAGE", UserprofileImage)

            // profile_icon.setImageResource(R.drawable.no_profile_image_avatar_icon);
            Picasso.with(context).load(UserprofileImage.toString()).placeholder(R.drawable.no_profile_image_avatar_icon).into(profile_icon)
            profile_name.text = User_fullname
            profile_mobile.text = "$User_phoneCode $User_phone"
        } else {


            profile_layout.visibility = View.GONE
            general_layout.visibility = View.VISIBLE

           // val amount = session.getWalletAmount()
           // val wallet_money = amount.get(SessionManager.KEY_WALLET_AMOUNT)

            imgIcon.setImageResource(mIcon[position])
            txtTitle.text = mTitle[position]
            //walletMoney.setText(wallet_money)
        }

        return itemView
    }
}