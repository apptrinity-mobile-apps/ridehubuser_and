package indo.com.ridehub_lyft_uber.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.ridehub_lyft_uber.Activities.MyRidesDetail
import indo.com.ridehub_lyft_uber.PojoResponse.MyRidesPojo
import indo.com.ridehub_lyft_uber.R
import java.util.*


class MyRidesAdapter(private val context: Context, private val data: ArrayList<MyRidesPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {

        return 1
    }

    inner class ViewHolder {
        val car_type_image: ImageView? = null
        var myride_pickuploc: CustomTextView? = null
        var myride_destiloc: CustomTextView? = null
        var tv_myride_datetime: CustomTextView? = null
        var btn_ridestatus: Button? = null
        var cv_listitemheader: CardView? = null
        internal val status: TextView? = null
        var iv_user_image: ImageView? = null
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_myrides, parent, false)
            holder = ViewHolder()

            holder.myride_pickuploc = view.findViewById<View>(R.id.tv_myride_pickuploc) as CustomTextView
            holder.myride_destiloc = view.findViewById<View>(R.id.tv_myride_destinationloc) as CustomTextView
            holder.tv_myride_datetime = view.findViewById<View>(R.id.tv_myride_datetime) as CustomTextView
            holder.cv_listitemheader = view.findViewById<View>(R.id.cv_listitemheader) as CardView
            holder.btn_ridestatus = view.findViewById<View>(R.id.btn_ridestatus) as Button
            holder.iv_user_image = view.findViewById<View>(R.id.iv_user_image) as ImageView


            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.myride_pickuploc!!.text = data[position].getPickUp()
        holder.myride_destiloc!!.text = data[position].getPickUp()
        holder.tv_myride_datetime!!.text = data[position].getRide_date()

        if (data[position].getType().equals("LYFT")) {
            holder.iv_user_image!!.setImageResource(R.drawable.lyft_icon)
        } else if (data[position].getType().equals("RIDEHUB")) {
            holder.iv_user_image!!.setImageResource(R.drawable.cab_type_ridehub)
        } else if (data[position].getType().equals("UBER")) {
            holder.iv_user_image!!.setImageResource(R.drawable.uber)
        }

        if (data[position].getRide_status().equals("Booked")) {

            holder.btn_ridestatus!!.text = "Upcoming"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_upcoming)
            holder.btn_ridestatus!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ride_upcoming, 0)


        } else if (data[position].getRide_status().equals("Completed") || data[position].getRide_status().equals("completed")) {

            holder.btn_ridestatus!!.text = "Completed"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_completed)
            holder.btn_ridestatus!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ride_completed, 0)

        } else if (data[position].getRide_status().equals("Cancelled") || data[position].getRide_status().equals("canceled")) {

            holder.btn_ridestatus!!.text = "Canceled"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_cancelled)
            holder.btn_ridestatus!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ride_cancelled, 0)

        } else if (data[position].getRide_status().equals("Onride")) {

            holder.btn_ridestatus!!.text = "Onride"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_upcoming)

        } else if (data[position].getRide_status().equals("Finished")) {

            holder.btn_ridestatus!!.text = "Finished"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_completed)

        } else if (data[position].getRide_status().equals("Confirmed")) {

            holder.btn_ridestatus!!.text = "Confirmed"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_completed)

        } else if (data[position].getRide_status().equals("Arrived")) {

            holder.btn_ridestatus!!.text = "Arrived"
            holder.btn_ridestatus!!.setBackgroundResource(R.drawable.button_curve_background_myrides_completed)

        }

        holder.cv_listitemheader!!.setOnClickListener {
            Log.e("RIDEID", data[position].getRide_id())
            val intent = Intent(context, MyRidesDetail::class.java)
            intent.putExtra("RideID", data[position].getRide_id())
            intent.putExtra("RideType", data[position].getType())
            intent.putExtra("PickUp", data[position].getPickUp())

            if (data[position].getDistance() != null || data[position].getDistance().equals("")){
                intent.putExtra("Distance", data[position].getDistance())
            }else{
                intent.putExtra("Distance", "")
            }

            if (data[position].getDuration() != null || data[position].getDuration().equals("")){
                intent.putExtra("Duration", data[position].getDuration())
            }else{
                intent.putExtra("Duration", "")
            }

            if (data[position].getDatetime() != null || data[position].getDatetime().equals("")){
                intent.putExtra("Date", data[position].getDatetime())
            }else{
                intent.putExtra("Date", "")
            }

            if (data[position].getSource_lat() != null || data[position].getSource_lat().equals("")){
                intent.putExtra("Latitude", data[position].getSource_lat())
            }else{
                intent.putExtra("Latitude", "")
            }

            if (data[position].getSource_lng() != null || data[position].getSource_lng().equals("")){
                intent.putExtra("Longitude", data[position].getSource_lng())
            }else{
                intent.putExtra("Longitude", "")
            }

            context.startActivity(intent)
        }

        return view
    }
}