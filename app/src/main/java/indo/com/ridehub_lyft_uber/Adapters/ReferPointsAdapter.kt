package indo.com.ridehub_lyft_uber.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import indo.com.mylibrary.widgets.CustomTextView
import indo.com.ridehub_lyft_uber.PojoResponse.ReferPointsPojo
import indo.com.ridehub_lyft_uber.R
import java.util.*


class ReferPointsAdapter(private val context: Context, private val data: ArrayList<ReferPointsPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {

        return 1
    }

    inner class ViewHolder {
        var tv_refer_name: CustomTextView? = null
        var tv_refer_points: CustomTextView? = null
       // var tv_refer_type: CustomTextView? = null
        var tv_refer_rideid: CustomTextView? = null
        var tv_datereferal: CustomTextView? = null
        var tv_refer_level: CustomTextView? = null
        var tv_refer_transaction_type: CustomTextView? = null

    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_refer_points, parent, false)
            holder = ViewHolder()

            holder.tv_refer_name = view.findViewById<View>(R.id.tv_refer_name) as CustomTextView
            holder.tv_refer_points = view.findViewById<View>(R.id.tv_refer_points) as CustomTextView
            holder.tv_datereferal = view.findViewById<View>(R.id.tv_datereferal) as CustomTextView
           // holder.tv_refer_type = view.findViewById<View>(R.id.tv_refer_type) as CustomTextView
            holder.tv_refer_rideid = view.findViewById<View>(R.id.tv_refer_rideid) as CustomTextView
            holder.tv_refer_level = view.findViewById<View>(R.id.tv_refer_level) as CustomTextView
            //holder.tv_refer_transaction_type = view.findViewById<View>(R.id.tv_refer_transaction_type) as CustomTextView

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }


        Log.e("REFERDATE",data[position].getDate())

        holder.tv_refer_name!!.text = data[position].getReferrer_name()
        holder.tv_refer_points!!.text = data[position].getTrans_type()+ " - " +data[position].getPoints()
       // holder.tv_refer_type!!.text = data[position].getType()
        holder.tv_refer_rideid!!.text = "Refer ID : " +data[position].getRide_id()
        holder.tv_refer_level!!.text = "Refer Level : " +data[position].getLevel()
        holder.tv_datereferal!!.text = "Date : "+data[position].getDate()


       /* if(data[position].getTrans_type()!!.equals("CR")){
            holder.tv_refer_transaction_type!!.text = data[position].getTrans_type()
            holder.tv_refer_transaction_type!!.setTextColor(context.resources.getColor(R.color.green_alert_dialog_text))
        }else {
            holder.tv_refer_transaction_type!!.text = data[position].getTrans_type()
            holder.tv_refer_transaction_type!!.setTextColor(context.resources.getColor(R.color.alert_red))
        }*/



        return view
    }
}