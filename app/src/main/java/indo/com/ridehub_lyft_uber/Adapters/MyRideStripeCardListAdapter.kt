package indo.com.ridehub_lyft_uber.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import indo.com.ridehub_lyft_uber.PojoResponse.StripeCardListPojo
import indo.com.ridehub_lyft_uber.R
import java.util.*

class MyRideStripeCardListAdapter(private val context: Context, private val data: ArrayList<StripeCardListPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var reason: TextView? = null
        internal var card_type: TextView? = null
        internal var card_expiry: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_stripecard_list, parent, false)
            holder = ViewHolder()
            holder.reason = view.findViewById(R.id.myride_payment_list_textview) as TextView
            holder.card_type = view.findViewById(R.id.myride_stripe_card_type_textview) as TextView
            holder.card_expiry = view.findViewById(R.id.myride_stripe_card_expiry_textview) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.reason!!.text = "Card ending with " + data[position].getLast4_digits()
        holder.card_type!!.setText(data[position].getCredit_card_type())
        holder.card_expiry!!.setText(data[position].getExp_month() + " / " + data[position].getExp_year())
        return view
    }
}