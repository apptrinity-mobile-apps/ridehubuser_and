package indo.com.ridehub_lyft_uber.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import indo.com.ridehub_lyft_uber.PojoResponse.PaymentListPojo
import indo.com.ridehub_lyft_uber.R
import java.util.*

class MyRidePaymentListAdapter(private val context: Context, private val data: ArrayList<PaymentListPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        var reason: TextView? = null
        var image_pay_type: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_payment_list_single, parent, false)
            holder = ViewHolder()
            holder?.reason = view.findViewById(R.id.myride_payment_list_textview) as TextView
            holder?.image_pay_type = view.findViewById(R.id.iv_payment_type) as ImageView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder?.reason!!.setText(data[position].getPaymentName())
        return view
    }
}