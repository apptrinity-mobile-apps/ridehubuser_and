package indo.com.ridehub_lyft_uber.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import indo.com.ridehub_lyft_uber.PojoResponse.CancelTripPojo
import indo.com.ridehub_lyft_uber.R
import java.util.*

/**
 * Created by Prem Kumar and Anitha on 11/2/2015.
 */
class MyRideCancelTripAdapter(private val context: Context, private val data: ArrayList<CancelTripPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var reason: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_cancel_trip_single, parent, false)
            holder = ViewHolder()
            holder?.reason = view.findViewById<View>(R.id.myride_cancel_trip_reason_textview) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.reason!!.setText(data[position].getReason())
        return view
    }
}

