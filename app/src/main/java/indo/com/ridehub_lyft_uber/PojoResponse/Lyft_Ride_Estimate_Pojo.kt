package indo.com.ridehub_lyft_uber.PojoResponse

class Lyft_Ride_Estimate_Pojo {

     var ride_type: String = ""
     var display_name: String = ""
     var primetime_percentage: String = ""
     var primetime_confirmation_token: String = ""
     var cost_token: String = ""
     var price_quote_id: String = ""
     var price_group_id: String = ""
     var is_valid_estimate: String = ""
     var estimated_duration_seconds: String = ""
     var estimated_distance_miles: String = ""
     var estimated_cost_cents_min: String = ""
     var estimated_cost_cents_max: String = ""
     var can_request_ride: String = ""

    constructor() {}

    constructor(ride_type: String, display_name: String, primetime_percentage: String, primetime_confirmation_token: String, cost_token: String, price_quote_id: String, price_group_id: String, is_valid_estimate: String, estimated_duration_seconds: String, estimated_distance_miles: String, estimated_cost_cents_min: String, estimated_cost_cents_max: String, can_request_ride: String) {
        this.ride_type = ride_type
        this.display_name = display_name
        this.primetime_percentage = primetime_percentage
        this.primetime_confirmation_token = primetime_confirmation_token
        this.cost_token = cost_token
        this.price_quote_id = price_quote_id
        this.price_group_id = price_group_id
        this.is_valid_estimate = is_valid_estimate
        this.estimated_duration_seconds = estimated_duration_seconds
        this.estimated_distance_miles = estimated_distance_miles
        this.estimated_cost_cents_min = estimated_cost_cents_min
        this.estimated_cost_cents_max = estimated_cost_cents_max
        this.can_request_ride = can_request_ride
    }
}