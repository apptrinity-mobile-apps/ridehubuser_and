package indo.com.ridehub_lyft_uber.PojoResponse

import com.google.android.gms.maps.model.LatLng

class Lyft_Nearby_Drivers_Pojo {
      var ride_type: String = ""
      var drivers: ArrayList<LatLng> = ArrayList<LatLng>()

    constructor(){}
    
    constructor(ride_type: String, drivers: ArrayList<LatLng>) {
        this.ride_type = ride_type
        this.drivers = drivers
    }
}