package indo.com.ridehub_lyft_uber.PojoResponse

class HomePojo {

   private var driver_lat: String? = null
    private var driver_long:String? = null
    private var cattype_name: String? = null
    private var cat_name:String? = null
    private var cat_time:String? = null
    private var cat_image:String? = null
    private var cat_id:String? = null
    private var min_amount:String? = null
    private var max_amount:String? = null
    private var car_capacity_min:String? = null
    private var car_capacity_max:String? = null
    private var Selected_Cat: String? = null
    private var Selected_SubCat:String? = null
    private var icon_normal: String? = null
    private var icon_active:String? = null
    private var rate_cartype: String? = null
    private var rate_note:String? = null
    private var minfare_amt:String? = null
    private var minfare_km:String? = null
    private var afterfare_amt:String? = null
    private var afterfare_km:String? = null
    private var otherfare_amt:String? = null
    private var otherfare_km:String? = null
    private var currencyCode: String? = null
    private var mainType: String? = null


    fun getMain_type(): String? {
        return mainType
    }

    fun setMain_type(mainType: String) {
        this.mainType = mainType
    }

    fun getCat_id(): String? {
        return cat_id
    }

    fun setCat_id(cat_id: String) {
        this.cat_id = cat_id
    }

    fun getMin_amount(): String? {
        return min_amount
    }

    fun setMin_amount(min_amount: String) {
        this.min_amount = min_amount
    }


    fun getDriver_lat(): String? {
        return driver_lat
    }

    fun setDriver_lat(driver_lat: String) {
        this.driver_lat = driver_lat
    }

    fun getDriver_long(): String? {
        return driver_long
    }

    fun setDriver_long(driver_long: String) {
        this.driver_long = driver_long
    }

    fun getCatType_name(): String? {
        return cattype_name
    }

    fun setCatType_name(cattype_name: String) {
        this.cattype_name = cattype_name
    }


    fun getCat_name(): String? {
        return cat_name
    }

    fun setCat_name(cat_name: String) {
        this.cat_name = cat_name
    }

    fun getMax_amount(): String? {
        return max_amount
    }

    fun setMax_amount(max_amount: String) {
        this.max_amount = max_amount
    }

    fun getCat_time(): String? {
        return cat_time
    }

    fun setCat_time(cat_time: String) {
        this.cat_time = cat_time
    }

    fun getCat_image(): String? {
        return cat_image
    }

    fun setCat_image(cat_image: String) {
        this.cat_image = cat_image
    }

    fun getSelected_Cat(): String? {
        return Selected_Cat
    }

    fun getSelected_SubCat(): String? {
        return Selected_SubCat
    }

    fun setSelected_Cat(selected_Cat: String) {
        Selected_Cat = selected_Cat
    }

    fun setSelected_SubCat(selected_SubCat: String) {
        Selected_SubCat = selected_SubCat
    }

    fun getIcon_normal(): String? {
        return icon_normal
    }

    fun setIcon_normal(icon_normal: String) {
        this.icon_normal = icon_normal
    }

    fun getIcon_active(): String? {
        return icon_active
    }

    fun setIcon_active(icon_active: String) {
        this.icon_active = icon_active
    }

    fun getRate_cartype(): String? {
        return rate_cartype
    }

    fun setRate_cartype(rate_cartype: String) {
        this.rate_cartype = rate_cartype
    }

    fun getRate_note(): String? {
        return rate_note
    }

    fun setRate_note(rate_note: String) {
        this.rate_note = rate_note
    }

    fun getMinfare_amt(): String? {
        return minfare_amt
    }

    fun setMinfare_amt(minfare_amt: String) {
        this.minfare_amt = minfare_amt
    }

    fun getMinfare_km(): String? {
        return minfare_km
    }

    fun setMinfare_km(minfare_km: String) {
        this.minfare_km = minfare_km
    }

    fun getAfterfare_amt(): String? {
        return afterfare_amt
    }

    fun setAfterfare_amt(afterfare_amt: String) {
        this.afterfare_amt = afterfare_amt
    }

    fun getAfterfare_km(): String? {
        return afterfare_km
    }

    fun setAfterfare_km(afterfare_km: String) {
        this.afterfare_km = afterfare_km
    }

    fun getOtherfare_amt(): String? {
        return otherfare_amt
    }

    fun setOtherfare_amt(otherfare_amt: String) {
        this.otherfare_amt = otherfare_amt
    }

    fun getOtherfare_km(): String? {
        return otherfare_km
    }

    fun setOtherfare_km(otherfare_km: String) {
        this.otherfare_km = otherfare_km
    }

    fun getCurrencyCode(): String? {
        return currencyCode
    }

    fun setCurrencyCode(currencyCode: String) {
        this.currencyCode = currencyCode
    }


    fun getCar_capacity_min(): String? {
        return car_capacity_min
    }

    fun setCar_capacity_min(car_capacity_min: String) {
        this.car_capacity_min = car_capacity_min
    }


    fun getCar_capacity_max(): String? {
        return car_capacity_max
    }

    fun setCar_capacity_max(car_capacity_max: String) {
        this.car_capacity_max = car_capacity_max
    }




    // comparing dates and arranging in HIGH TO LOW order
    var priceComparator: Comparator<HomePojo> = object : Comparator<HomePojo> {
        override fun compare(s1: HomePojo, s2: HomePojo): Int {
            var Stime1 = ""
            var Stime2 = ""
            if (s1.getCat_time()!!.toString().contains("/")) {
                Stime1 = s1.getCat_time()!!.toString().replace("/", "-")
            } else if (s1.getCat_time()!!.toString().contains("")) {
                // for canceled rides in lyft
                Stime2 = ""
            } else {
                Stime1 = s1.getCat_time()!!.toString()
            }

            if (s2.getCat_time()!!.toString().contains("/")) {
                Stime2 = s2.getCat_time()!!.toString().replace("/", "-")
            } else if (s2.getCat_time()!!.toString().contains("")) {
                // for canceled rides in lyft
                Stime2 = ""
            } else {
                Stime2 = s2.getCat_time()!!.toString()
            }
            return Stime2!!.compareTo(Stime1!!)


        }
    }

    override fun toString(): String {
        return cat_time!!
    }


    /*operator fun compareTo(o: Any): Int {
        return if (this.getCat_time()!! < (o as HomePojo).getCat_time().toString()) -1 else if (this.getCat_time() === (o as HomePojo).getCat_time()) 0 else 1
    }*/



}