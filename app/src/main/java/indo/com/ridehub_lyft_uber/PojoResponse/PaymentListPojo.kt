package indo.com.ridehub_lyft_uber.PojoResponse

class PaymentListPojo {

    private var paymentName: String? = null
    private var paymentCode:String? = null

    fun getPaymentName(): String? {
        return paymentName
    }

    fun setPaymentName(paymentName: String) {
        this.paymentName = paymentName
    }

    fun getPaymentCode(): String? {
        return paymentCode
    }

    fun setPaymentCode(paymentCode: String) {
        this.paymentCode = paymentCode
    }


}