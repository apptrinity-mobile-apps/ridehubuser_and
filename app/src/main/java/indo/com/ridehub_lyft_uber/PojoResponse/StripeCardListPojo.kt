package indo.com.ridehub_lyft_uber.PojoResponse

class StripeCardListPojo {

    private var card_number: String? = null
    private var exp_month:String? = null
    private var exp_year:String? = null
    private var credit_card_type:String? = null
    private var customer_id:String? = null
    private var card_id:String? = null
    private var last4_digits:String? = null

    fun getCard_number(): String? {
        return card_number
    }

    fun setCard_number(card_number: String) {
        this.card_number = card_number
    }

    fun getExp_month(): String? {
        return exp_month
    }

    fun setExp_month(exp_month: String) {
        this.exp_month = exp_month
    }

    fun getExp_year(): String? {
        return exp_year
    }

    fun setExp_year(exp_year: String) {
        this.exp_year = exp_year
    }

    fun getCredit_card_type(): String? {
        return credit_card_type
    }

    fun setCredit_card_type(credit_card_type: String) {
        this.credit_card_type = credit_card_type
    }

    fun getCustomer_id(): String? {
        return customer_id
    }

    fun setCustomer_id(customer_id: String) {
        this.customer_id = customer_id
    }

    fun getLast4_digits(): String? {
        return last4_digits
    }

    fun setLast4_digits(last4_digits: String) {
        this.last4_digits = last4_digits
    }


    fun getCard_id(): String? {
        return card_id
    }

    fun setCard_id(card_id: String) {
        this.card_id = card_id
    }
}