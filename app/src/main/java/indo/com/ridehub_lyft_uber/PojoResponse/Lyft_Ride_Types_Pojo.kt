package indo.com.ridehub_lyft_uber.PojoResponse

class Lyft_Ride_Types_Pojo {
      var display_name: String = ""
      var category_key: String = ""
      var image_url: String = ""
      var service_fee_description: String = ""
      var base_charge: String = ""
      var cost_per_mile: String = ""
      var cancel_penalty_amount: String = ""
      var cost_per_minute: String = ""
      var cost_minimum: String = ""
      var trust_and_service: String = ""
      var seats: String = ""
      var ride_type: String = ""

    constructor() {}

    constructor(display_name: String, category_key: String, image_url: String, service_fee_description: String, base_charge: String, cost_per_mile: String, cancel_penalty_amount: String, cost_per_minute: String, cost_minimum: String, trust_and_service: String, seats: String, ride_type: String) {
        this.display_name = display_name
        this.category_key = category_key
        this.image_url = image_url
        this.service_fee_description = service_fee_description
        this.base_charge = base_charge
        this.cost_per_mile = cost_per_mile
        this.cancel_penalty_amount = cancel_penalty_amount
        this.cost_per_minute = cost_per_minute
        this.cost_minimum = cost_minimum
        this.trust_and_service = trust_and_service
        this.seats = seats
        this.ride_type = ride_type
    }
}