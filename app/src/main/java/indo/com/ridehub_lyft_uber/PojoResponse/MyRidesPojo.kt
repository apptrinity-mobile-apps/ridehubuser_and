package indo.com.ridehub_lyft_uber.PojoResponse

import java.text.SimpleDateFormat
import java.util.*

class MyRidesPojo {

   private var ride_id: String? = null
    private var ride_time: String? = null
    private  var ride_date: String? = null
    private var pickup: String? = null
    internal var ride_status: String? = null
    private var group: String? = null
    private var datetime: String? = null
    private var type: String? = null
    private var distance: String? = null
    private var duration: String? = null
    private var source_lat: String? = null
    private var source_lng: String? = null


    fun getRide_id(): String? {
        return ride_id
    }

    fun setRide_id(ride_id: String) {
        this.ride_id = ride_id
    }

    fun getRide_time(): String? {
        return ride_time
    }

    fun setRide_time(ride_time: String) {
        this.ride_time = ride_time
    }

    fun getRide_date(): String? {
        return ride_date
    }

    fun setRide_date(ride_date: String) {
        this.ride_date = ride_date
    }

    fun getPickUp(): String? {
        return pickup
    }

    fun setPickup(pickup: String) {
        this.pickup = pickup
    }


    fun getRide_status(): String? {
        return ride_status
    }

    fun setRide_status(ride_status: String) {
        this.ride_status = ride_status
    }

    fun getGroup(): String? {
        return group
    }

    fun setGroup(group: String) {
        this.group = group
    }

    fun getDatetime(): String? {
        return datetime
    }

    fun setDatetime(datetime: String) {
        this.datetime = datetime
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

    fun getDistance(): String? {
        return distance
    }

    fun setDistance(distance: String) {
        this.distance = distance
    }

    fun getDuration(): String? {
        return duration
    }

    fun setDuration(duration: String) {
        this.duration = duration
    }

    fun getSource_lat(): String? {
        return source_lat
    }

    fun setSource_lat(source_lat: String) {
        this.source_lat = source_lat
    }

    fun getSource_lng(): String? {
        return source_lng
    }

    fun setSource_lng(source_lng: String) {
        this.source_lng = source_lng
    }


    // comparing dates and arranging in HIGH TO LOW order
    var dateWiseComparator: Comparator<MyRidesPojo> = object : Comparator<MyRidesPojo> {
        override fun compare(s1: MyRidesPojo, s2: MyRidesPojo): Int {
            var Sdate1 = ""
            var Sdate2 = ""
            val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
            if (s1.getRide_date()!!.toString().contains("/")) {
                Sdate1 = s1.getRide_date()!!.toString().replace("/", "-")
            } else if (s1.getRide_date()!!.toString().contains("")) {
                // for canceled rides in lyft
                Sdate1 = "00-00-0000"
            } else {
                Sdate1 = s1.getRide_date()!!.toString()
            }

            if (s2.getRide_date()!!.toString().contains("/")) {
                Sdate2 = s2.getRide_date()!!.toString().replace("/", "-")
            } else if (s2.getRide_date()!!.toString().contains("")) {
                // for canceled rides in lyft
                Sdate2 = "00-00-0000"
            } else {
                Sdate2 = s2.getRide_date()!!.toString()
            }
            val date1 = sdf.parse(Sdate1)
            val date2 = sdf.parse(Sdate2)
            return date2!!.compareTo(date1!!)

            /* for LOW TO HIGH order */
            // return date1!!.compareTo(date2!!)

        }
    }

    override fun toString(): String {
        return ride_date!!
    }


}